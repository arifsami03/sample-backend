// import { Connection } from '../../base';
import { Sequelize } from 'sequelize-typescript';
import * as _ from 'lodash';
import { UserModel, GROUP, USER, AppFeatureModel, UserGroupModel, RoleAssignmentModel, FeaturePermissionModel, RoleWFStateFlowModel } from '../';
import { CONFIGURATIONS } from '../../base';
import { WFStateFlowModel } from '../../setting';

export class CheckPermissionModel {

  /**
   * Check permissions of user
   * @param userId 
   * @param originalUrl 
   * @returns true | false
   */
  public checkPermission(userId, originalUrl) {


    // For now don't need to open connection manually because UserModel will open connection
    //let  connection = new Connection().createConnection();
    /**
     * Check if user is superUser then bypass RBAC and return true  
     */
    // new Connection().createConnection()
    return new UserModel().findByCondition(['id', 'isSuperUser'], { id: userId, isActive: true }).then(_user => {

      if (_user.isSuperUser === true) {

        CONFIGURATIONS.identity['isSuperUser'] = true;

        return true;

      } else {

        CONFIGURATIONS.identity['isSuperUser'] = false;

        /**
          * Check if requested url/route is part of RBAC i.e corresponding permission exists in AppFeature table then proceed to our 
          * RBAC logic otehrwise return true
        **/
        let featureId = this.convertUrlToFeatureId(originalUrl);

        return new AppFeatureModel().findAllByConditions(['id'], { id: featureId }).then(_appFeature => {

          if (_appFeature.length == 0) {

            return true;

          } else {

            // Find roles assigned to user and user's group/groups
            let groupIds = [];

            return new UserGroupModel().findAllByConditions(['id', 'groupId'], { userId: userId }).then(userGroups => {

              _.each((userGroups), (item) => { groupIds.push(item.groupId); });

              // find user roles 
              let roleAssignmentModel = new RoleAssignmentModel();
              return roleAssignmentModel.findAllByConditions(['id', 'roleId'], { parentId: userId, parent: USER }).then(UserRoleAssignmentResult => {

                return roleAssignmentModel.findAllByConditions(['id', 'roleId'], { parentId: { [Sequelize.Op.in]: groupIds }, parent: GROUP }).then(GroupRoleAssignmentResult => {

                  if (UserRoleAssignmentResult.length > 0 || GroupRoleAssignmentResult.length > 0) {

                    let roleIds = [];

                    _.each(UserRoleAssignmentResult, (item) => roleIds.push(item.roleId));
                    _.each(GroupRoleAssignmentResult, (item) => roleIds.push(item.roleId));

                    //TODO:low We can find only those permissions which has the true status and that will avoid the after loop to set status on the bases of permission.status
                    // check if this permission is assigned to any assigned role
                    return new FeaturePermissionModel().findAllByConditions(['id', 'status'], { featureId: featureId, parent: 'role', parentId: roleIds }).then(permissions => {

                      if (permissions.length > 0) {

                        let status: boolean = false;

                        permissions.forEach(permission => {
                          if (permission.status == true) {
                            status = permission.status;
                          }
                        });

                        //TODO:low: Implement proper bizzrules
                        //Find associated workflow states with these roles.
                        return new RoleWFStateFlowModel().findAllByConditions(['WFStateFlowId'], { roleId: roleIds }).then(RoleWFSRes => {

                          let WFStateFlowIds = [];

                          _.each(RoleWFSRes, (item) => { WFStateFlowIds.push(item['WFStateFlowId']); })

                          return new WFStateFlowModel().findAllByConditions(['fromId', 'toId'], { id: WFStateFlowIds }).then(WFSFRes => {

                            let WFStateIds = [];

                            _.each(WFSFRes, (item) => {
                              WFStateIds.push(item['fromId']);
                            });

                            CONFIGURATIONS.GDataObject['WFStateIds'] = [];

                            CONFIGURATIONS.GDataObject['WFStateIds'] = _.sortedUniq(WFStateIds);;

                          });

                        }).then(() => {

                          return status;

                        })



                      } else {

                        // If no permission is assigned to any role of user then return false
                        return false;

                      }
                    });
                  } else {
                    return false;
                  }

                });
              });
            })
          };
        })
      }
    })
  }

  /**
   * Convert requested url/route to corresponding AppFeatureId
   * @param url 
   */
  private convertUrlToFeatureId(url) {
    let beforeQryString = url.split('?')[0];
    let featureId = beforeQryString.split('/');

    // Controller name must be on index 2 and method name must be on index 3 
    return featureId[2].concat('.', featureId[3]);
  }
}
