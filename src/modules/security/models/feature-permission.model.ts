import { Promise } from 'bluebird';
import { BaseModel, CONFIGURATIONS } from '../../base';
import { FeaturePermission, AppFeature } from '../';
import { AppFeatureModel } from './app-feature.model';

export class FeaturePermissionModel extends BaseModel {
  constructor() {
    super(FeaturePermission);
  }

  private appFeatureModel = new AppFeatureModel();

  /**
   * Update (assign/revoke) permission of feature on the bases of provided status (true/false)
   *
   * We are reciving here all features list (which are visible true) in app features regardless if its status is true or false.
   * Then we will create/update feature permission and set true/false recursively (regarding their children in appFeatures)
   *
   * @param fpParent string
   * @param fpParentId number
   * @param appFeatures string
   */
  updatePermissions(fpParent, fpParentId, appFeatures) {
    //TODO:low: If we are not going to use base model here in case of avoiding multiple connection opening then we have to open connection here.
    // More importantly fix the issue of sequelize open connection on each time a new model instance is created.
    // this.openConnection();

    // Iterate all app feature items (which are considered as parents)
    return Promise.each(appFeatures, afParentItem => {

      return Promise.each(afParentItem['children'], afChildItem => {

        // If user didnot click on checkbox for the first time then status is not being added in afChildItem i.e. af is App Feature
        let status: boolean = afChildItem['status'] ? afChildItem['status'] : false;

        // Check if that feature is already asigned or not. 
        // If it is not then we will create new permission. If it exists then we will do as follow.
        return super.findByCondition(['id'], { parent: fpParent, parentId: fpParentId, featureId: afChildItem['id'] }).then(fpResult => {

          // If feature is already assigned then just update the record else insert new record
          if (fpResult) {
            let itemToUpdate = { status: status, updatedBy: CONFIGURATIONS.identity.userId };
            return this.update(fpResult['id'], itemToUpdate);
          }
          else {
            // Create new feature permission
            let createItem = { featureId: afChildItem['id'], parent: fpParent, parentId: fpParentId, status: status };
            createItem = BaseModel.extendItem(createItem, true);
            return this.create(createItem);

          }
        }).then(() => {
          // Now the magical part :) For which I, Mohsin, have to come office on 23 March, on holly day
          // Arif please make sure you understand the logical part and next time avoid the hell-code

          // Assign permission on features recursively
          return this.assignPermRecursively(fpParent, fpParentId, afChildItem['id'], status);

        });
      });
    });
  }


  /**
   * Assign Permission Recursively
   *
   * @param fpParent string (role, group, user)
   * @param fpParentId number
   * @param afParentId number (AppFeature Parent Id)
   * @param status boolean
   */
  private assignPermRecursively(fpParent, fpParentId, afParentId, status) {

    // Find all children of provided app feature (afParentId)
    return this.appFeatureModel.findAllByConditions(['id', 'parentId'], { parentId: afParentId }).then(afResult => {

      return Promise.each(afResult, afItem => {

        // Check if feature already exist in FeaturePermission
        return this.findByCondition(['id', 'featureId'], { parent: fpParent, parentId: fpParentId, featureId: afItem['id'] }).then(fpResult => {

          // If found then just update the record else insert new record
          if (fpResult) {
            // If status is false then just check if its parent is also false. To achive that
            // 1. Get its parents
            // 2. Check if any of those parent is true in FeaturePermission
            // 3. Then we are not setting it false :)
            // In other words, if any permission is set to false then verify if it's parent is also false.
            if (status == false) {
              return this.appFeatureModel.findAllByConditions(['id', 'parentId'], { id: fpResult['featureId'] }).then(afResult2 => {

                let parents = [];

                for (let c = 0; c < afResult2.length; c++) {
                  parents.push(afResult2[c]['parentId']);
                }

                return this.findAllByConditions(['id'], { status: true, featureId: parents, parent: fpParent, parentId: fpParentId }).then(tfr => {
                  if (tfr.length == 0) {
                    let itemToUpdate = { status: status, updatedBy: CONFIGURATIONS.identity.userId };
                    return this.update(fpResult['id'], itemToUpdate);
                  } else {
                    // Need to return an empty promise to make parent promisable object
                    return new Promise((resolve, reject) => {
                      resolve(true);
                    });
                  }
                });
              });
            } else {
              // In case if permission is set to true then just set it true
              let itemToUpdate = { status: status, updatedBy: CONFIGURATIONS.identity.userId };
              return this.update(fpResult['id'], itemToUpdate);
            }
          } else {
            // And if permission does not exist then just create it with whatever status provided.
            let createItem = {
              featureId: afItem['id'],
              parent: fpParent,
              parentId: fpParentId,
              status: status
            };
            createItem = BaseModel.extendItem(createItem, true);

            return this.create(createItem);
          }
        })
          .then(() => {
            return this.assignPermRecursively(fpParent, fpParentId, afItem['id'], status);
          });
      });
    });
  }
}
