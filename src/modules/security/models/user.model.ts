import { Sequelize } from 'sequelize-typescript';
import * as _ from 'lodash';
import * as jwt from 'jsonwebtoken';

import { BaseModel, CONFIGURATIONS } from '../../base';
import { IUserModel } from './interfaces/IUserModel';
import { IPermissionModel } from './interfaces/IPermissionModel';
import { ErrorHandler } from '../../base/conf/error-handler';
import { Helper } from '../../base/helpers/helper';

import { User, FeaturePermission, AppFeature, UserGroup, Group, RoleAssignment, Role, USER, GROUP } from '../';
import { RoleAssignmentModel } from './role-assignment.model';
import { UserGroupModel } from './user-group.model';
import { RoleModel } from './role.model';
import { GroupModel } from './group.model';
import { FeaturePermissionModel } from './feature-permission.model';
import { AppFeatureModel } from './app-feature.model';

export class UserModel extends BaseModel {

  constructor() {
    super(User);
  }
  private roleModel = new RoleModel();
  private roleAssignmentModel = new RoleAssignmentModel();
  private groupModel = new GroupModel();
  private userGroupModel = new UserGroupModel();

  /**
   * Validate user to login and return jwt token and permissions
   * 
   * @param item 
   */
  public login(item: IUserModel) {

    return super.findByCondition(['id', 'username', 'password', 'portal', 'isActive', 'isSuperUser'], { username: item.username }).then(res => {

      if (res) {

        let userRes = <IUserModel>res;

        return Helper.verifyPassword(item.password, userRes.password).then(match => {

          if (res && match && userRes.isActive) {

            let token = jwt.sign({ id: res['id'], username: res['username'], iat: Math.floor(Date.now() / 1000) - 30 }, CONFIGURATIONS.SECRET);

            let result = {
              id: userRes.id, username: userRes.username, isSuperUser: userRes.isSuperUser,
              token: token, permissions: [], portal: userRes.portal
            }
            // if user is superuser return with empty permissions
            if (userRes.isSuperUser) {

              return result;

            } else { // else check for user permissions

              return this.findPermissions(userRes.id).then(_permissions => {

                if (_permissions.length > 0) {
                  result.permissions = _permissions;
                  return result;
                }
                else {
                  return ErrorHandler.noRoleAssigned;
                }

              });
            }

          } else {
            // Return invalid credentials message
            return ErrorHandler.invalidLogin;
          }
        });

      } else {

        return ErrorHandler.invalidLogin;

      }
    });
  }

  /**
   * Find permissions of user i.e permissions assigned to user -> role(s) and user -> group(s) -> role(s)
   * 
   * @param userId 
   */
  private findPermissions(userId: number) {
    let permissions: IPermissionModel[] = [];

    return this.findUserPermissions(userId, permissions).then(() => {

      return this.findGroupPermissions(userId, permissions).then(() => {

        return permissions;

      });
    });

  }

  /**
   * Find user permissions i.e permissions assinged to user -> role(s)
   * 
   * @param userId 
   */
  private findUserPermissions(userId: number, permissions: IPermissionModel[]) {

    let includeObj = [{
      model: Role, as: 'role', attributes: ['id', 'name'], where: BaseModel.cb(), required: false, include: [
        { model: FeaturePermission, as: 'permissions', attributes: ['id', 'featureId', 'status'], where: BaseModel.cb(), required: false }
      ]
    }];

    return this.roleAssignmentModel.findAllByConditions(['id', 'roleId'], { parentId: userId, parent: USER }, includeObj).then(UserRoleAssignmentResult => {

      _.each(UserRoleAssignmentResult, roleAssignment => {

        _.each(roleAssignment.role.permissions, permission => {

          let pitem = { name: permission.featureId.toLowerCase(), status: permission.status };

          let existedItem = permissions.find(o => o['name'] == pitem.name);

          if (!existedItem) { permissions.push(pitem) } else {

            if (existedItem.status == false) { existedItem.status = pitem.status }
          }

        });
      });

      return permissions;

    });

  }

  /**
   * Find user permissions i.e permissions assinged to user -> group(s) ->  role(s)
   * @param userId 
   */
  private findGroupPermissions(userId: number, permissions: IPermissionModel[]) {

    let groupIds = [];
    return this.userGroupModel.findAllByConditions(['id', 'groupId'], { userId: userId }).then(userGroups => {

      _.each((userGroups), (item) => { groupIds.push(item.groupId) });

      let condition = { parentId: { [Sequelize.Op.in]: groupIds }, parent: GROUP };
      let includeObj = [{
        model: Role, as: 'role', attributes: ['id', 'name'], where: BaseModel.cb(), required: false,
        include: [{ model: FeaturePermission, as: 'permissions', attributes: ['id', 'featureId', 'status'], where: BaseModel.cb(), required: false }]
      }];

      return this.roleAssignmentModel.findAllByConditions(['id', 'roleId'], condition, includeObj
      ).then(GroupRoleAssignmentResult => {

        _.each(GroupRoleAssignmentResult, roleAssignment => {

          _.each(roleAssignment.role.permissions, permission => {

            let pitem = { name: permission.featureId.toLowerCase(), status: permission.status };

            let existedItem = permissions.find(o => o['name'] == pitem.name);

            if (!existedItem) { permissions.push(pitem) } else {

              if (existedItem.status == false) { existedItem.status = pitem.status }

            }

          });

        });

        return permissions;

      });
    });

  }

  /**
   * Insert user record into database after validate unique username i.e email
   * 
   * @param item 
   */
  public createUser(item) {

    return this.validateUniqueEmail(item.username).then(result => {

      if (result) {

        return ErrorHandler.duplicateEmail;

      } else {

        return Helper.encrypt(item.password).then(hashedPassword => {

          item.password = hashedPassword;
          return super.create(item);

        });
      }
    });
  }

  /**
   * Update user after validate unique username i.e email
   * 
   * @param id 
   * @param item 
   */
  public updateUser(id, item) {

    return this.validateUniqueEmail(item.username, id).then((result) => {

      if (result) {

        return ErrorHandler.duplicateEmail;

      } else {

        return super.update(id, item);

      }
    });
  }

  /**
   * Retrieve list of groups that are not yet assigned to user
   * 
   * @param id 
   * @param attributes 
   */
  findAllGroups(id, attributes) {

    return this.userGroupModel.findAllByConditions(['id', 'groupId'], { userId: id }).then(UserGroupsResult => {

      let groupIds = [];
      _.each(UserGroupsResult, item => { groupIds.push(item.groupId) });

      return this.groupModel.findAllByConditions(attributes, { id: { [Sequelize.Op.notIn]: groupIds } });

    });
  }

  /**
   * Retrieve list of roles that are not yet assigned to user
   * 
   * @param id 
   * @param attributes 
   */
  findAllRoles(id, attributes) {

    return this.roleAssignmentModel.findAllByConditions(['id', 'roleId'], { parentId: id, parent: USER }).then(RoleAssignmentsResult => {

      let roleIds = [];
      _.each(RoleAssignmentsResult, item => { roleIds.push(item.roleId) });

      return this.roleModel.findAllByConditions(attributes, { id: { [Sequelize.Op.notIn]: roleIds } });

    });
  }

  /**
   * Retrieve list of all permissions
   * 
   * @param id 
   * @param attributes 
   */
  findAllPermissions(id, attributes) {

    return new FeaturePermissionModel().findAllByConditions(attributes, { parentId: id, parent: USER }).then(_featurePermissions => {

      let includeObj = [{ model: AppFeature, as: 'children', attributes: ['id', 'parentId', 'title'], where: BaseModel.cb(), required: false }];
      return new AppFeatureModel().findAllByConditions(['id', 'parentId', 'title'], { parentId: null, isVisible: true }, includeObj).then(_appFeatures => {

        let result = _appFeatures;

        _appFeatures.forEach((_appFeature, i) => {

          if (_appFeature.children.length > 0) {

            _appFeature.children.forEach((_children, index) => {

              result[i].children[index]['dataValues']['status'] = false;

              _featurePermissions.forEach(_featurePermission => {

                if (_featurePermission.status == true && _featurePermission.featureId === result[i].children[index].id) {

                  result[i].children[index]['dataValues']['status'] = true;

                }
              });
            });
          }
        });
        return result;
      });
    });
  }

  /**
   * TODO:low findUserOnly should be find method but Developer has overrided the original find method.
   * @param id number
   * @param attributes string array
   */
  findUserOnly(id, attributes?) {

    return super.find(id, attributes);

  }

  /**
   * Find User and its roles and groups
   * @param id
   * @param attributes
   */
  findWithRelations(id, attributes?) {

    return super.find(id, attributes).then(_user => {

      // if user is not superuser return its roles and groups
      if (!_user.isSuperUser) {

        let includeObj = [
          {
            model: RoleAssignment, as: 'roleAssignments', attributes: ['id'], where: BaseModel.cb(), required: false,
            include: [{ model: Role, as: 'role', attributes: ['id', 'name'], where: BaseModel.cb(), required: false }]
          },
          {
            model: UserGroup, as: 'userGroups', attributes: ['id'], where: BaseModel.cb(), required: false,
            include: [{ model: Group, as: 'group', attributes: ['id', 'name'], where: BaseModel.cb(), required: false }]
          }
        ];

        return this.findByCondition(attributes, { id: id }, includeObj);

      } else {

        return _user;

      }
    });
  }

  /**
   * Delete a User, its relation to Group and Role and its permissions
   * @param id
   */
  deleteWithRelations(id) {

    //TODO:high: remove this manual connection opening
    this.openConnection();

    return this.connection.transaction(t => {

      return UserGroup.destroy({ where: { userId: id }, transaction: t }).then(() => {

        return RoleAssignment.destroy({ where: { parent: USER, parentId: id }, transaction: t }).then(() => {

          return User.destroy({ where: { id: id }, transaction: t });
        });
      });
    });

  }

  /**
   * Validate username i.e email to create and update user
   * 
   * @param email 
   * @param id 
   */
  public validateUniqueEmail(email: string, id?: number, ) {

    // if id exists (i.e update scenario validate username except current user)
    if (id) {

      return super.findByCondition(['id'], { id: { [Sequelize.Op.not]: id }, username: email });

    } else {

      return super.findByCondition(['id'], { username: email });

    }
  }

  findAllWithAssignedRoles() {
    let includeObj = [
      {
        model: RoleAssignment, as: 'roleAssignments', attributes: ['id', 'roleId', 'parent', 'parentId'], where: BaseModel.cb(), required: false,
        include: [{ model: Role, as: 'role', attributes: ['id', 'name'], where: BaseModel.cb(), required: false }]
      },
      {
        model: UserGroup, as: 'userGroups', attributes: ['id', 'userId', 'groupId'], where: BaseModel.cb(), required: false,
        include: [{ model: Group, as: 'group', attributes: ['id', 'name'], where: BaseModel.cb(), required: false }]
      }
    ];

    return super.findAllByConditions(['id', 'username', 'name', 'isActive'], { portal: CONFIGURATIONS.PORTAL.INSTITUTE  }, includeObj);
  }

}
