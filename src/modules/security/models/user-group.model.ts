/**
 * Importing related Models
 */
import { BaseModel } from '../../base';
import { UserGroup } from '..';

export class UserGroupModel extends BaseModel {
  constructor() {
    super(UserGroup);
  }
}
