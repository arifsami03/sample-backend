import { Table, Column, Model, BelongsTo, HasMany, PrimaryKey } from 'sequelize-typescript';

import { FeaturePermission } from '../..';

@Table({ timestamps: true }) // for adding createdAt and updatedAt Columns
export class AppFeature extends Model<AppFeature> {

  /**
   * Adding Columns and their properties of AppFeature Table
   */
  @Column parentId: string;

  @Column title: string;

  @Column isVisible: boolean;

  @Column weight: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * BelongsTo Relationships
   */
  @BelongsTo(() => AppFeature, {
    foreignKey: 'parentId',
    constraints: false
  })
  parent: AppFeature;

  /**
   * HasMany Relatonship
   */
  @HasMany(() => AppFeature, {
    foreignKey: 'parentId',
    constraints: false
  })
  children: AppFeature[];

  @HasMany(() => FeaturePermission, {
    foreignKey: 'featureId',
    constraints: false
  })
  permissions: FeaturePermission[];
}
