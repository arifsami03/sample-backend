import {
  Table,
  Column,
  Model,
  ForeignKey,
  HasMany,
  DataType
} from 'sequelize-typescript';
/**
 * Importing related Models
 */
import { RoleAssignment, FeaturePermission } from '../..';
@Table({ timestamps: true }) // for adding createdAt and updatedAt Columns
export class Role extends Model<Role> {
  /**
   * Adding Columns and their properties of Role Table
   */

  @Column name: string;
  @Column(DataType.TEXT)
  description: string;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * HasMany Relatonship
   */
  @HasMany(() => RoleAssignment, {
    foreignKey: 'parentId',
    constraints: false,
    scope: {
      parent: 'group'
    }
  })
  groups: RoleAssignment[];

  @HasMany(() => RoleAssignment, {
    foreignKey: 'roleId'
  })
  roleAssignments: RoleAssignment[];

  @HasMany(() => RoleAssignment, {
    foreignKey: 'roleId',
    scope: {
      parent: 'group'
    }
  })
  assignedGroups: RoleAssignment[];

  @HasMany(() => RoleAssignment, {
    foreignKey: 'roleId',
    scope: {
      parent: 'user'
    }
  })
  assignedUsers: RoleAssignment[];

  /**
   * Get assigned permissions which have status true
   */
  @HasMany(() => FeaturePermission, {
    foreignKey: 'parentId',
    constraints: false,
    scope: {
      parent: 'role',
    }
  })
  permissions: FeaturePermission[];

  /**
   * Get all assigned permissions with status true or false
   */
  @HasMany(() => FeaturePermission, {
    foreignKey: 'parentId',
    constraints: false,
    scope: {
      parent: 'role'
    }
  })
  assignedPermissions: FeaturePermission[];
}
