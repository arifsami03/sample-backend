import {
  Table,
  Column,
  Model,
  ForeignKey,
  BelongsTo
} from 'sequelize-typescript';
/**
 * Importing related Models
 */
import { Group, User, Role } from '../..';
@Table({ timestamps: true }) // for adding createdAt and updatedAt Columns
export class RoleAssignment extends Model<RoleAssignment> {
  /**
   * Adding Columns and their properties of RoleAssignment Table
   */
  @ForeignKey(() => Role)
  @Column
  roleId: number;

  @Column parent: string;

  @Column parentId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * BelongsTo Relationships
   */
  @BelongsTo(() => User, {
    foreignKey: 'parentId',
    constraints: false
  })
  user: User;

  @BelongsTo(() => Group, {
    foreignKey: 'parentId',
    constraints: false
  })
  group: Group;

  @BelongsTo(() => Role, {
    foreignKey: 'roleId'
  })
  role: Role;
}
