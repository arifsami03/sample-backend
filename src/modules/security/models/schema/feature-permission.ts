import { Table, Column, Model, BelongsTo } from 'sequelize-typescript';

import { AppFeature, User, Group, Role } from '../..';

@Table({ timestamps: true }) // for adding createdAt and updatedAt Columns
export class FeaturePermission extends Model<FeaturePermission> {
  /**
   * Adding Columns and their properties of FeaturePermission Table
   */
  @Column featureId: string;

  @Column parent: string;

  @Column parentId: number;

  @Column status: boolean;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * BelongsTo Relationships
   */
  @BelongsTo(() => AppFeature, {
    foreignKey: 'featureId',
    constraints: false
  })
  appFeature: AppFeature;

  @BelongsTo(() => User, {
    foreignKey: 'parentId',
    constraints: false
  })
  user: User;

  @BelongsTo(() => Group, {
    foreignKey: 'parentId',
    constraints: false
  })
  group: Group;

  @BelongsTo(() => Role, {
    foreignKey: 'parentId',
    constraints: false
  })
  role: Role;
}
