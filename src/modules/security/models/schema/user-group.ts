import {
  Table,
  Column,
  Model,
  ForeignKey,
  BelongsTo
} from 'sequelize-typescript';
/**
 * Importing related Models
 */
import { User, Group } from '../..';

@Table({ timestamps: true }) // for adding createdAt and updatedAt Columns
export class UserGroup extends Model<UserGroup> {
  /**
   * Adding Columns and their properties of UserGroup Table
   */
  @ForeignKey(() => User)
  @Column
  userId: number;

  @ForeignKey(() => Group)
  @Column
  groupId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * BelongsTo Relationships
   */
  @BelongsTo(() => User, {
    foreignKey: 'userId'
  })
  user: User;

  @BelongsTo(() => Group, {
    foreignKey: 'groupId'
  })
  group: Group;
}
