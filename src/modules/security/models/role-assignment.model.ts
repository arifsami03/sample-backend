/**
 * Importing related Models
 */
import { BaseModel } from '../../base';
import { RoleAssignment } from '..';

export class RoleAssignmentModel extends BaseModel {
  constructor() {
    super(RoleAssignment);
  }
}
