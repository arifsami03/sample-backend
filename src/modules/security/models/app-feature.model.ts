/**
 * Importing related Models
 */
import { BaseModel } from '../../base';
import { AppFeature } from '..';

export class AppFeatureModel extends BaseModel {
  constructor() {
    super(AppFeature);
  }
}