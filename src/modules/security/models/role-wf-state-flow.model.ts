/**
 * Importing related Models
 */
import { BaseModel } from '../../base';
import { RoleWFStateFlow } from '..';
import { Promise } from 'bluebird';
import { WFStateFlowModel, WFStateModel, WorkFlowModel } from '../../setting';

export class RoleWFStateFlowModel extends BaseModel {
  constructor() {
    super(RoleWFStateFlow);
  }

  list(id: number) {
    let wfStateModel = new WFStateModel();
    let wfStateFlowModel = new WFStateFlowModel();
    let workFlowModel = new WorkFlowModel();

    let myflows = [];

    // Find all role wf state flows of current role.
    return this.findAllByConditions(['id', 'roleId', 'WFStateFlowId'], { roleId: id }).then(roleFlows => {
      let wfs = [];

      return Promise.each(roleFlows, flowItem => {
        //TODO:low: move this query to include with upper query
        // find regarding state flows
        return wfStateFlowModel.findByCondition(['fromId', 'toId', 'WFId'], { id: flowItem['WFStateFlowId'] }).then(stateFlow => {
          let flows = [];

          // Find States
          return wfStateModel.findByCondition(['state', 'title'], { id: stateFlow['fromId'] }).then(fromState => {
            // TODO:high: this query can be avoided by adding an or condition in upper query.
            return wfStateModel.findByCondition(['state', 'title'], { id: stateFlow['toId'] }).then(toState => {
              // Check if WFId is already added or not then push {WFId: stateFlow['WFId']}
              // Push this { from: fromState['state'], to: toState['state'] } where wfs[index wehre WfId exist] in wfs['flows']

              // This condition should not be checked here as it will not add flows more than one in work flow suggested by Arif
              // let foundItem = wfs.find(o => o['WFId'] == stateFlow['WFId']);
              // if (!foundItem || foundItem == undefined || foundItem == 'undefined') {

              // finding the title of WorkFlow
              return workFlowModel.sequelizeModel
                .find({
                  attributes: ['title'],
                  order: [['title', 'DESC']],
                  where: { WFId: stateFlow['WFId'] }
                })
                .then(_workFlow => {

                  // pushing the workflow title as well
                  // Condition to check if work flow is already added in returning array then do not add it again coded by Arif
                  let foundItem = wfs.find(o => o['WFId'] == stateFlow['WFId']);
                  if (!foundItem || foundItem == undefined || foundItem == 'undefined') {
                    wfs.push({ WFId: stateFlow['WFId'], title: _workFlow['title'], flows: [] });
                  }

                  let wfsIndex = wfs.findIndex(o => o['WFId'] == stateFlow['WFId']);
                  if (wfsIndex > -1) {
                    wfs[wfsIndex]['flows'].push({ from: fromState['title'], to: toState['title'] });
                  }
                });

              // flows.push({
              //   from: fromState['state'],
              //   to: toState['state'],
              // })
            });
          });
        });
      }).then(() => {
        return wfs;
        // return myflows;
      });
    });
  }

  create(item) {
    return Promise.each(item.data, row => {
      return this.findByCondition(['id'], { roleId: row['roleId'], WFStateFlowId: row['WFStateFlowId'] }).then(res => {
        if (res != undefined) {
        } else {
          return super.create(row);
        }
      });
    }).then(() => {
      return this.deleteWorkFlows(item);
    });
  }

  /**
   * Function to Delete Role WF State Flow
   * @param item Posted Data from Front End
   */

  private deleteWorkFlows(item) {
    return Promise.each(item['deletedIds'], row => {
      if (item['deletedIds'].length != 0) {
        return this.deleteByConditions({ roleId: row['roleId'], WFStateFlowId: row['WFStateFlowId'] });
      }
    });
  }

  getWFStateFlowByRole(id: number) {
    return this.findAllByConditions(['WFStateFlowId'], { roleId: id });
  }
}
