export interface IUserModel {
  id?: number;
  username?: string;
  password?: string;
  permissions?: string[];
  isActive?: boolean;
  isSuperUser?: boolean;
  portal?: string;
}
