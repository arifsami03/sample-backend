import { Promise } from 'bluebird';
import { Sequelize } from 'sequelize-typescript';
import * as _ from 'lodash';

import { BaseModel, CONFIGURATIONS } from '../../base';

import { Role, RoleAssignment, FeaturePermission, Group, User, GROUP, USER, AppFeatureModel, FeaturePermissionModel, ROLE } from '../';
import { RoleAssignmentModel } from './role-assignment.model';
import { UserModel } from './user.model';
import { GroupModel } from './group.model';

export class RoleModel extends BaseModel {


  private appFeatureModel = new AppFeatureModel();

  /**
   * To find all featureIds in FeaturePermission
   */
  private featureIdsAry: string[] = [];

  constructor() {
    super(Role);
  }

  /**
   * Retrieve Role with its user(s), group(s)
   * 
   * @param id 
   * @param attributes 
   */
  findWithRelations(id, attributes?) {

    let includeObj = [
      {
        model: RoleAssignment, as: 'assignedGroups', attributes: ['id', 'roleId', 'parent', 'parentId'], where: BaseModel.cb(), required: false,
        include: [{ model: Group, as: 'group', attributes: ['name'], where: BaseModel.cb(), required: false }]
      },
      {
        model: RoleAssignment, as: 'assignedUsers', attributes: ['id', 'roleId', 'parent', 'parentId'], where: BaseModel.cb(), required: false,
        include: [{ model: User, as: 'user', attributes: ['id', 'username'], where: BaseModel.cb(), required: false }]
      }
    ];

    return this.findByCondition(attributes, { id: id }, includeObj);
  }

  /**
   * Retrieve list of users that are not yet assigned to role
   * 
   * @param id 
   * @param attributes 
   */
  findAllUsers(id, attributes) {

    return new RoleAssignmentModel().findAllByConditions(['id', 'parentId'], { roleId: id, parent: USER }).then(RoleAssignmentsResult => {

      let userIds = [];
      _.each(RoleAssignmentsResult, item => { userIds.push(item.parentId) });

      let condition = { id: { [Sequelize.Op.notIn]: userIds }, isSuperUser: { [Sequelize.Op.or]: [false, null] }, portal: CONFIGURATIONS.PORTAL.INSTITUTE };
      return new UserModel().findAllByConditions(attributes, condition);
    });
  }

  /**
   * Retrieve list of groups that are not yet assigned to role
   * 
   * @param id 
   * @param attributes 
   */
  findAllGroups(id, attributes) {

    return new RoleAssignmentModel().findAllByConditions(['id', 'parentId'], { roleId: id, parent: GROUP }).then(RoleAssignmentsResult => {

      let groupIds = [];
      _.each(RoleAssignmentsResult, item => { groupIds.push(item.parentId) });

      return new GroupModel().findAllByConditions(attributes, { id: { [Sequelize.Op.notIn]: groupIds } });
    });
  }

  /**
   * Find All Features and set statuses accordingly assigned permissions.
   * 
   * Find all features and put all level of children on same array "children" if its set to visible (true)
   *    i.e. [{id: 'users.*', title: 'Users', parentId: '', children : [{id: 'users.create', title: 'Create'}, {id: 'users.view', title: 'View'}]}]
   * @param roleId number
   */
  public findAllFeatures(roleId) {

    let appFeatures = [];

    //TODO:high: parentId: null will not work if in db its '' empty instead of null
    return this.appFeatureModel.findAllByConditions(['id', 'parentId', 'title', 'isVisible'], { parentId: null, isVisible: true }, null, [['title', 'ASC']]).then(featuresAry => {

      return Promise.each(featuresAry, (item) => {

        let childrenArray = [];

        return this.findChildFeatures(item['id'], childrenArray).then(() => {

          let obj = { id: item['id'], title: item['title'], children: childrenArray }

          appFeatures.push(obj);
        })

      })
    }).then(() => {

      // Now we also need a list of Feature permissions which are assigend to role but only those which we fetched in above block ;)
      return new FeaturePermissionModel().findAllByConditions(['featureId', 'status'], { parent: 'role', parentId: roleId, featureId: this.featureIdsAry }).then(fpResult => {

        // Now set ststus true or false.
        return Promise.each(appFeatures, (afItems) => {
          return Promise.each(afItems['children'], (afItem) => {

            let fpObj = fpResult.find((element) => {
              return element['featureId'] == afItem['id'];
            });

            if (fpObj) {
              afItem['status'] = fpObj['status'];
            }
          })
        })

      })

    }).then(() => {
      return appFeatures;
    })
  }


  /**
   * Find child features recursivly
   * 
   * @param parentId string
   * @param childrenAry array
   */
  private findChildFeatures(parentId, childrenAry) {

    return this.appFeatureModel.findAllByConditions(['id', 'parentId', 'title', 'isVisible'], { parentId: parentId, isVisible: true }, null, [['title', 'ASC']]).then(featuresAry => {

      if (featuresAry && featuresAry.length) {

        return Promise.each(featuresAry, (item) => {

          let obj = { id: item['id'], title: item['title'], }

          this.featureIdsAry.push(item['id']);

          childrenAry.push(obj);

          return this.findChildFeatures(item['id'], childrenAry);
        })

      }
    })
  }

  /**
   * Delete a Role, its relation with User, Group and its Permissions
   * @param id
   */
  delete(id) {

    //TODO:high: remove this manual connection opening
    this.openConnection();

    return this.connection.transaction(t => {

      return RoleAssignment.destroy({ where: { roleId: id }, transaction: t }).then(() => {

        return FeaturePermission.destroy({ where: { parent: ROLE, parentId: id }, transaction: t }).then(() => {

          return Role.destroy({ where: { id: id }, transaction: t });

        });
      });
    });
  }
}
