import { Sequelize } from 'sequelize-typescript';
import * as _ from 'lodash';

import { GROUP } from '../conf/constants';
import { BaseModel, CONFIGURATIONS } from '../../base';

import { Group, UserGroup, FeaturePermission, RoleAssignment, AppFeature, Role, User } from '../';
import { UserGroupModel } from './user-group.model';
import { UserModel } from './user.model';
import { RoleAssignmentModel } from './role-assignment.model';
import { RoleModel } from './role.model';
import { FeaturePermissionModel } from './feature-permission.model';
import { AppFeatureModel } from './app-feature.model';

export class GroupModel extends BaseModel {

  constructor() {
    super(Group);
  }

  /**
   * * Retrieve Group and its user(s) and role(s)
   * 
   * @param id 
   * @param attributes 
   */
  findWithRelations(id, attributes) {

    let includeObj = [
      {
        model: UserGroup, as: 'userGroups', attributes: ['id', 'userId', 'groupId'], where: BaseModel.cb(), required: false,
        include: [{ model: User, as: 'user', attributes: ['id', 'username'], where: BaseModel.cb(), required: false }]
      },
      {
        model: RoleAssignment, as: 'roleAssignments', attributes: ['id', 'roleId', 'parent', 'parentId'],
        include: [{ model: Role, as: 'role', attributes: ['id', 'name'] }]
      }
    ];

    return this.findByCondition(attributes, { id: id }, includeObj);
  }

  /**
   * Retrieve list of users that are not yet assigned to group
   * 
   * @param id 
   * @param attributes 
   */
  findAllUsers(id, attributes) {

    return new UserGroupModel().findAllByConditions(['id', 'userId'], { groupId: id }).then(UserGroupsResult => {

      let userIds = [];
      _.each(UserGroupsResult, item => { userIds.push(item.userId) });

      let condition = { id: { [Sequelize.Op.notIn]: userIds }, isSuperUser: { [Sequelize.Op.or]: [false, null] }, portal: CONFIGURATIONS.PORTAL.INSTITUTE };

      return new UserModel().findAllByConditions(attributes, condition);
    });
  }

  /**
   * Retrieve list of roles that are not yet assigned to group
   * 
   * @param id 
   * @param attributes 
   */
  findAllRoles(id, attributes) {

    return new RoleAssignmentModel().findAllByConditions(['id', 'roleId'], { parentId: id, parent: GROUP }).then(RoleAssignmentsResult => {

      let roleIds = [];
      _.each(RoleAssignmentsResult, item => { roleIds.push(item.roleId) });

      return new RoleModel().findAllByConditions(attributes, { id: { [Sequelize.Op.notIn]: roleIds } });
    });
  }


  findAllPermissions(id, attributes) {

    return new FeaturePermissionModel().findAllByConditions(attributes, { parentId: id, parent: GROUP }).then(_featurePermissions => {

      return new AppFeatureModel().findAllByConditions(['id', 'parentId', 'title'], { parentId: null, isVisible: true },
        [{ model: AppFeature, as: 'children', attributes: ['id', 'parentId', 'title'] }]).then(_appFeatures => {

          let result = _appFeatures;

          _appFeatures.forEach((_appFeature, i) => {

            if (_appFeature.children.length > 0) {

              _appFeature.children.forEach((_children, index) => {

                result[i].children[index]['dataValues']['status'] = false;

                _featurePermissions.forEach(_featurePermission => {

                  if (_featurePermission.status == true && _featurePermission.featureId === result[i].children[index].id) {

                    result[i].children[index]['dataValues']['status'] = true;

                  }
                });
              });
            }
          });

          return result;

        });
    });
  }



  /**
   * Delete a Group, its relation to Group and Role and its permissions
   * @param id
   */
  deleteWithRelations(id) {

    //TODO:high: remove this manual connection opening
    this.openConnection();

    return this.connection.transaction(t => {

      return UserGroup.destroy({ where: { groupId: id }, transaction: t }).then(() => {

        return RoleAssignment.destroy({ where: { parent: GROUP, parentId: id }, transaction: t }).then(() => {

          return Group.destroy({ where: { id: id }, transaction: t });
        });
      });
    });
  }
}
