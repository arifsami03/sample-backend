import * as express from 'express';
import { ErrorHandler } from '../../base/conf/error-handler';

import { FeaturePermissionModel } from '..';

export class FeaturePermissionsController {
  constructor() { }

  /**
   * Update Role Permissions
   * 
   * @param req 
   * @param res 
   * @param next 
   */
  updateRolePermissions(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;
    let item = req.body;

    new FeaturePermissionModel().updatePermissions('role', id, item).then((result) => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Update Group Permissions
   * 
   * @param req 
   * @param res 
   * @param next 
   */
  updateGroupPermissions(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;
    let item = req.body;

    new FeaturePermissionModel().updatePermissions('group', id, item).then((result) => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Update User Permissions
   * 
   * @param req 
   * @param res 
   * @param next 
   */
  updateUserPermissions(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;
    let item = req.body;

    new FeaturePermissionModel().updatePermissions('user', id, item).then((result) => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

}
