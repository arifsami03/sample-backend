import * as express from 'express';
import { UserGroupModel } from '..';
import { ErrorHandler } from '../../base/conf/error-handler';

export class UserGroupsController {
  constructor() { }

  /**
   * assign a group to user
   *
   * @param req {userId, groupId, createdBy}
   * @param res {true | false }
   */
  assign(req: express.Request, res: express.Response, next: express.NextFunction) {

    let item = req.body;

    new UserGroupModel().create(item).then((result) => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * delete a record
   *
   * @param req route
   * @param res {success | error}
   */
  revoke(req: express.Request, res: express.Response, next: express.NextFunction) {

    new UserGroupModel().delete(req.params.id).then((result) => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

}
