import * as express from 'express';

import { ErrorHandler } from '../../base/conf/error-handler';
/**
 * Importing related Models
 */
import { RoleWFStateFlowModel } from '..';

export class RoleWFStateFlowsController {
  constructor() { }



  /**
   * retrieve single Role with its association
   *
   * @param req id
   * @param res {Group}
   */
  list(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let id = req.params.id;

    new RoleWFStateFlowModel()
      .list(id)
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  getWFStateFlowByRole(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let id = req.params.id;

    new RoleWFStateFlowModel()
      .getWFStateFlowByRole(id)
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * update a role
   * @param req params: id, {Role}
   * @param res { success | error}
   */
  update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let id = req.params.id;
    let item = req.body;
    new RoleWFStateFlowModel()
      .update(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * inserting single Role into database
   *
   * @param req {Role}
   * @param res {true | false }
   */
  create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new RoleWFStateFlowModel()
      .create(req.body)
      .then(result => {

        res.json(result);

      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * deletes single Role, its relation with Users and Groups and its Permissions
   *
   * @param req route
   * @param res {success | error}
   */
  delete(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new RoleWFStateFlowModel()
      .delete(req.params.id)
      .then(result => {
        res.send({ success: 'Role has been deleted successfully' });
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }




}
