import * as express from 'express';
import * as _ from 'lodash';

import { ErrorHandler } from '../../base/conf/error-handler';
import { UserModel } from '..';
import { CONFIGURATIONS } from '../../base';

export class UsersController {
  constructor() { }

  /**
   * Login
   *
   * @param req {User}
   */
  login(req: express.Request, res: express.Response, next: express.NextFunction) {

    new UserModel().login(req.body).then(result => {

      if (result && !result['error']) {

        res.json(result);

      } else {

        ErrorHandler.send(result, res, next);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }


  /**
   * Retrieve list of all Users with portal uinversity
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {

    new UserModel().findAllWithAssignedRoles().then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Retrieve User with its association
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;
    let attributes = ['id', 'username', 'name', 'phoneNumber', 'address', 'gender', 'isSuperUser', 'isActive'];

    new UserModel().findWithRelations(id, attributes).then(result => {

      if (result) {

        res.json(result);

      } else {

        res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Retreive portal of a user
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  getPortal(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.body.id;

    new UserModel().find(id, ['portal']).then(result => {

      if (result) {

        res.json(result);

      } else {

        res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);

      }
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }
  /**
   * Retreive list of all users with portal = uinversity with minimum attributes
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesList(req: express.Request, res: express.Response, next: express.NextFunction) {

    new UserModel().findAllByConditions(['id', 'username'], { portal: CONFIGURATIONS.PORTAL.INSTITUTE }).then(result => {

      if (result) {

        res.json(result);

      } else {

        res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Update
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;

    // prepare record to update i.e restrict to update password here
    let item = {
      username: req.body.username, isActive: req.body.isActive,
      name: req.body.name, address: req.body.address, phoneNumber: req.body.phoneNumber,
      gender: req.body.gender, updatedBy: req.body.updatedBy
    };

    new UserModel().updateUser(id, item).then(result => {

      if (result && !result['error']) {

        res.json(result);

      } else {

        ErrorHandler.send(result, res, next);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Create user
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {

    let item = req.body;

    // set default portal and superUser status for new user
    item.portal = CONFIGURATIONS.PORTAL.INSTITUTE;
    item.isSuperUser = false;    //TODO: implement proper way to set superuser false

    new UserModel().createUser(item).then(result => {

      if (result && !result['error']) {

        res.json(result);

      } else {

        ErrorHandler.send(result, res, next);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Delete a User, its relation with Group and Role and its permissions
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {

    new UserModel().deleteWithRelations(req.params.id).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Retrieve list of groups that are not yet assigned to user
   *
   * @param req route
   * @param res [{User}... | error]
   */
  findAllGroups(req: express.Request, res: express.Response, next: express.NextFunction) {

    let userId = req.params.userId;

    new UserModel().findAllGroups(userId, ['id', 'name']).then(result => {

      if (result) {

        res.json(_.sortBy(result, ['name']));

      } else {

        res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Retrieve list of roles that are not yet assigned to user
   *
   * @param req route
   * @param res [{User}... | error]
   */
  findAllRoles(req: express.Request, res: express.Response, next: express.NextFunction) {

    let userId = req.params.userId;

    new UserModel().findAllRoles(userId, ['id', 'name']).then(result => {

      if (result) {

        res.json(_.sortBy(result, ['name']));

      } else {

        res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Retrieve list of all permissions
   *
   * @param req route
   * @param res [{Role}... | error]
   */
  findAllPermissions(req: express.Request, res: express.Response, next: express.NextFunction) {

    let userId = req.params.userId;
    let attributes = ['id', 'featureId', 'parent', 'parentId', 'status'];

    new UserModel().findAllPermissions(userId, attributes).then(result => {

      if (result) {

        res.json(result);

      } else {

        res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }
}
