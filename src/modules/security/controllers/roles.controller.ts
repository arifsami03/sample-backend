import * as express from 'express';
import * as _ from 'lodash';

import { ErrorHandler } from '../../base/conf/error-handler';
import { RoleModel } from '..';

export class RolesController {
  constructor() { }

  /**
   * Retrieve list of all Roles
   *
   * @param req route
   * @param res [{Role}... | error]
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    new RoleModel().findAll(['id', 'name']).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Retrieve Role with its user(s), group(s)
   *
   * @param req id
   * @param res {Group}
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;

    new RoleModel().findWithRelations(id, ['id', 'name', 'description']).then(result => {

      if (result) {

        res.json(result);

      } else {

        res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Update a role
   * 
   * @param req params: id, {Role}
   * @param res { success | error}
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;
    let item = req.body;

    new RoleModel().update(id, item).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Insert Role into database
   *
   * @param req {Role}
   * @param res {true | false }
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {

    new RoleModel().create(req.body).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Deletes Role, its relation with Users and Groups and its Permissions
   *
   * @param req route
   * @param res {success | error}
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {

    new RoleModel().delete(req.params.id).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Retrieve list of users that are not yet assigned to role
   *
   * @param req route
   * @param res [{Role}... | error]
   */
  findAllUsers(req: express.Request, res: express.Response, next: express.NextFunction) {

    let roleId = req.params.roleId;

    new RoleModel().findAllUsers(roleId, ['id', 'username']).then(result => {

      if (result) {

        res.json(_.sortBy(result, ['username']));

      } else {

        res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Retrieve list of groups that are not yet assigned to role
   *
   * @param req route
   * @param res [{Role}... | error]
   */
  findAllGroups(req: express.Request, res: express.Response, next: express.NextFunction) {

    let roleId = req.params.roleId;

    new RoleModel().findAllGroups(roleId, ['id', 'name']).then(result => {

      if (result) {

        res.json(_.sortBy(result, ['name']));

      } else {

        res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Retrieve list of all permissions
   *
   * @param req route
   * @param res [{Role}... | error]
   */
  findAllFeatures(req: express.Request, res: express.Response, next: express.NextFunction) {

    let roleId = req.params.roleId;

    new RoleModel().findAllFeatures(roleId).then(result => {

      if (result) {

        res.json(result);

      } else {

        res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

}
