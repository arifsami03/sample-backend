import * as express from 'express';
import { ErrorHandler } from '../../base/conf/error-handler';

import { AppFeatureModel } from '..';

export class AppFeaturesController {
  constructor() { }

  /**
   * Retrieve list of all parent AppFeatures 
   *
   * @param req route
   * @param res [{AppFeatures}... | error]
   */
  findAllParents(req: express.Request, res: express.Response, next: express.NextFunction) {

    new AppFeatureModel().findAllByConditions(['id', 'title'], { parentId: null }).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Update a AppFeature
   * 
   * @param req params: id, {Role}
   * @param res { success | error}
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;
    let item = req.body;

    new AppFeatureModel().update(id, item).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Insert AppFeature into database
   *
   * @param req {AppFeature}
   * @param res {AppFeature | false }
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {

    let item = req.body;

    new AppFeatureModel().create(item).then((result) => {
      res.json(result);
    })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }


  /**
   * Deleting AppFeature
   *
   * @param req route
   * @param res {success | error}
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {

    new AppFeatureModel().delete(req.params.id).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }
}
