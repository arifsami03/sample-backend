import * as express from 'express';
import * as _ from 'lodash';
import { ErrorHandler } from '../../base/conf/error-handler';
import { GroupModel } from '..';

export class GroupsController {
  constructor() { }

  /**
   * Retrieve list of all Groups
   *
   * @param req route
   * @param res [{Group}... | error]
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {

    new GroupModel().findAll(['id', 'name']).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Retrieve Group and its user(s) and role(s)
   *
   * @param req id
   * @param res {Group}
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;
    let attributes = ['id', 'name'];

    new GroupModel().findWithRelations(id, attributes).then(result => {

      if (result) {

        res.json(result);

      } else {

        res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Update group 
   * 
   * @param req params: id, {Group}
   * @param res { success | error}
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;
    let item = req.body;

    new GroupModel().update(id, item).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Insert Group into database
   *
   * @param req {Group}
   * @param res {true | false }
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {

    let item = req.body;

    new GroupModel().create(item).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Delete Group and its relation with Users and Roles and its permissions
   *
   * @param req route
   * @param res {success | error}
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {

    new GroupModel().deleteWithRelations(req.params.id).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Retrieve list of users that are not yet assigned to group
   *
   * @param req route
   * @param res [{Group}... | error]
   */
  findAllUsers(req: express.Request, res: express.Response, next: express.NextFunction) {

    let groupId = req.params.groupId;
    let attributes = ['id', 'username'];

    new GroupModel().findAllUsers(groupId, attributes).then(result => {

      if (result) {

        res.json(_.sortBy(result, ['username']));

      } else {

        res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }


  /**
   * Retrieve list of roles that are not yet assigned to group
   *
   * @param req route
   * @param res [{Group}... | error]
   */
  findAllRoles(req: express.Request, res: express.Response, next: express.NextFunction) {

    let groupId = req.params.groupId;
    let attributes = ['id', 'name'];

    new GroupModel().findAllRoles(groupId, attributes).then(result => {

      if (result) {

        // TODO:medium we have to sort result in query instead on result array
        res.json(_.sortBy(result, ['name']));

      } else {

        res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Retrieve list of all permissions
   *
   * @param req route
   * @param res [{Role}... | error]
   */
  findAllPermissions(req: express.Request, res: express.Response, next: express.NextFunction) {

    let groupId = req.params.groupId;
    let attributes = ['id', 'featureId', 'parent', 'parentId', 'status'];

    new GroupModel().findAllPermissions(groupId, attributes).then(result => {

      if (result) {

        res.json(result);

      } else {

        res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

}
