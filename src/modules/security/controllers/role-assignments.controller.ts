import * as express from 'express';

import { RoleAssignmentModel, USER, GROUP } from '..';
import { ErrorHandler } from '../../base/conf/error-handler';

export class RoleAssignmentsController {
  constructor() { }

  /**
   * assign role to user
   *
   * @param req {userId, roleId, createdBy}
   * @param res {success | error }
   */
  assignToUser(req: express.Request, res: express.Response, next: express.NextFunction) {

    let item = req.body;
    item.parent = USER;
    item.parentId = item.userId;

    new RoleAssignmentModel().create(item).then((result) => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * assign role to group
   *
   * @param req {groupId, roleId, createdBy}
   * @param res {success | error }
   */
  assignToGroup(req: express.Request, res: express.Response, next: express.NextFunction) {

    let item = req.body;
    item.parent = GROUP;
    item.parentId = item.groupId;

    new RoleAssignmentModel().create(item).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * delete a record
   *
   * @param req route
   * @param res {success | error}
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {

    new RoleAssignmentModel().delete(req.params.id).then((result) => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

}
