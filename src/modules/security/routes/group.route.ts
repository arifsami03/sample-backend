import { Router } from 'express';
import { GroupsController } from '..';

/**
 * / route
 *
 * @class Group
 */
export class GroupRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class GroupRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class GroupRoute
   * @method create
   *
   */
  public create() {
    let controller = new GroupsController();

    this.router.route('/security/groups/index').get(controller.index);

    this.router.route('/security/groups/find/:id').get(controller.find);

    this.router.route('/security/groups/findAllUsers/:groupId').get(controller.findAllUsers);

    this.router.route('/security/groups/findAllRoles/:groupId').get(controller.findAllRoles);

    this.router.route('/security/groups/findAllPermissions/:groupId').get(controller.findAllPermissions);

    this.router.route('/security/groups/update/:id').put(controller.update);

    this.router.route('/security/groups/create').post(controller.create);

    this.router.route('/security/groups/delete/:id').delete(controller.delete);
  }
}
