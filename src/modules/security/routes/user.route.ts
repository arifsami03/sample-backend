import { Router } from 'express';

import { UsersController } from '..';

/**
 * / route
 *
 * @class User
 */
export class UserRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class UserRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class UserRoute
   * @method create
   *
   */
  public create() {
    let controller = new UsersController();

    this.router.route('/security/users/index').get(controller.index);

    this.router.route('/security/users/find/:id').get(controller.find);

    this.router.route('/security/users/findAllGroups/:userId').get(controller.findAllGroups);

    this.router.route('/security/users/findAllRoles/:userId').get(controller.findAllRoles);

    this.router.route('/security/users/findAllPermissions/:userId').get(controller.findAllPermissions);

    this.router.route('/security/users/findAttributesList').get(controller.findAttributesList);

    this.router.route('/security/users/update/:id').put(controller.update);

    this.router.route('/security/users/login').post(controller.login);

    this.router.route('/security/users/create').post(controller.create);

    this.router.route('/security/users/delete/:id').delete(controller.delete);

    this.router.route('/security/users/getPortal').post(controller.getPortal);
  }
}
