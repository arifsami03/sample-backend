import { NextFunction, Request, Response, Router } from 'express';
import {
  UserRoute,
  GroupRoute,
  UserGroupRoute,
  RoleRoute,
  RoleAssignmentRoute,
  AppFeatureRoute,
  FeaturePermissionRoute,
  RoleWFStateFlowsRoute,
} from '../..';

/**
 * / route
 *
 * @class BaseRoute
 */
export class SecurityBaseRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class BaseRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.initAll();
  }

  /**
   * init all routes
   */
  public initAll() {
    new UserRoute(this.router);
    new GroupRoute(this.router);
    new UserGroupRoute(this.router);
    new RoleRoute(this.router);
    new RoleAssignmentRoute(this.router);
    new AppFeatureRoute(this.router);
    new FeaturePermissionRoute(this.router);
    new RoleWFStateFlowsRoute(this.router);
  }
}
