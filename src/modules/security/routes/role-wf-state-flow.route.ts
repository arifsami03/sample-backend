import { Router } from 'express';

import { RoleWFStateFlowsController } from '..';

/**
 * / route
 *
 * @class User
 */
export class RoleWFStateFlowsRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class UserRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class UserRoute
   * @method create
   *
   */
  public create() {
    let controller = new RoleWFStateFlowsController();

    this.router.route('/security/roleWFStateFlow/list/:id').get(controller.list);

    this.router.route('/security/roleWFStateFlow/create').post(controller.create);

    this.router.route('/security/roleWFStateFlow/getWFStateFlowByRole/:id').get(controller.getWFStateFlowByRole);

  }
}
