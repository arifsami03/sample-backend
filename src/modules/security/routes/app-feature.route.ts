import { Router } from 'express';
import { AppFeaturesController } from '..';

/**
 * / route
 *
 * @class AppFeature
 */
export class AppFeatureRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class AppFeatureRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class AppFeatureRoute
   * @method create
   *
   */
  public create() {
    let controller = new AppFeaturesController();

    this.router.route('/security/appFeatures/findAllParents').get(controller.findAllParents);

    this.router.route('/security/appFeatures/update/:id').put(controller.update);

    this.router.route('/security/appFeatures/create').post(controller.create);

    this.router.route('/security/appFeatures/delete/:id').delete(controller.delete);
  }
}
