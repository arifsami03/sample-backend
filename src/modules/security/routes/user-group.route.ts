import { Router } from 'express';
import { UserGroupsController } from '..';

/**
 * / route
 *
 * @class UserGroupRoute
 */
export class UserGroupRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class UserGroupRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class UserGroupRoute
   * @method create
   *
   */
  public create() {
    let controller = new UserGroupsController();

    this.router.route('/security/userGroups/assign').post(controller.assign);

    this.router.route('/security/userGroups/revoke/:id').delete(controller.revoke);
  }
}
