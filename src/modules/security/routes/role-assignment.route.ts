import { Router } from 'express';
import { RoleAssignmentsController } from '..';

/**
 * / route
 *
 * @class RoleAssignmentRoute
 */
export class RoleAssignmentRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class RoleAssignmentRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class RoleAssignmentRoute
   * @method create
   *
   */
  public create() {
    let controller = new RoleAssignmentsController();

    this.router.route('/security/roleAssignments/assignToUser').post(controller.assignToUser);

    this.router.route('/security/roleAssignments/assignToGroup').post(controller.assignToGroup);

    this.router.route('/security/roleAssignments/delete/:id').delete(controller.delete);

  }
}
