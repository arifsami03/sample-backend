import { Router } from 'express';
import { FeaturePermissionsController } from '..';

/**
 * / route
 *
 * @class FeaturePermissionRoute
 */
export class FeaturePermissionRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class FeaturePermissionRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class FeaturePermissionRoute
   * @method create
   *
   */
  public create() {
    let controller = new FeaturePermissionsController();

    this.router.route('/security/featurePermissions/updateRolePermissions/:id').post(controller.updateRolePermissions);

    this.router.route('/security/featurePermissions/updateGroupPermissions/:id').post(controller.updateGroupPermissions);

    this.router.route('/security/featurePermissions/updateUserPermissions/:id').post(controller.updateUserPermissions);
  }
}
