import { Router } from 'express';
import { RolesController } from '..';

/**
 * / route
 *
 * @class RoleRoute
 */
export class RoleRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class RoleRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class RoleRoute
   * @method create
   *
   */
  public create() {
    let controller = new RolesController();

    this.router.route('/security/roles/index').get(controller.index);

    this.router.route('/security/roles/find/:id').get(controller.find);

    this.router.route('/security/roles/findAllGroups/:roleId').get(controller.findAllGroups);

    this.router.route('/security/roles/findAllUsers/:roleId').get(controller.findAllUsers);

    this.router.route('/security/roles/findAllFeatures/:roleId').get(controller.findAllFeatures);

    this.router.route('/security/roles/update/:id').put(controller.update);

    this.router.route('/security/roles/create').post(controller.create);

    this.router.route('/security/roles/delete/:id').delete(controller.delete);
  }
}
