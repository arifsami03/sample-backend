import { Router } from 'express';

import { SettingBaseRoute } from '../../setting';
import { FeeManagementBaseRoute } from '../../fee-management';
import { InstitutesBaseRoute } from '../../institute';
import { SecurityBaseRoute } from '../../security';
import { CampusBaseRoute } from '../../campus';
import { DownloadPdfRoute } from '..';
import { ProfileRoute } from '../../profile';
import { TimeTableBaseRoute } from '../../time-table';
import { AcademicBaseRoute } from '../../academic';
import { AdmissionBaseRoute } from '../../admission';

/**
 * / route
 *
 * @class BaseRoute
 */
export class BaseRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class BaseRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.initAll();
  }

  /**
   * init all routes
   */
  public initAll() {
    new SettingBaseRoute(this.router);
    new InstitutesBaseRoute(this.router);
    new SecurityBaseRoute(this.router);
    new CampusBaseRoute(this.router);
    new DownloadPdfRoute(this.router);
    new ProfileRoute(this.router);
    new TimeTableBaseRoute(this.router);
    new AcademicBaseRoute(this.router);
    new AdmissionBaseRoute(this.router);
    new FeeManagementBaseRoute(this.router);
  }
}
