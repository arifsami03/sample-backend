export const CONNECTION_STRING = {
  database: 'eis_db',
  dialect: 'mssql',
  username: 'SA',
  password: 'Word2P@ss',
  storage: ':memory:'
  // pool: {
  //   max: 5,
  //   min: 0,
  //   acquire: 30000,
  //   idle: 10000
  // }
};
export const DEV_ENV = {
  frontendURL : 'http://localhost:4200',
  backendURL : 'http://localhost:3000',
}
export const PROD_ENV = {
  frontendURL : 'http://eis-dev.mekdizlabs.com',
  backendURL : 'http://eis-dev-api.mekdizlabs.com',
}
