import { Sequelize } from 'sequelize-typescript';

import { CONNECTION_STRING } from './local-settings';

/**
 * Import from Security Module
 * 
 */
import { AppFeature, FeaturePermission, Group, RoleAssignment, Role, UserGroup, User, RoleWFStateFlow } from '../../security';

/**
 * Import from Campus Module
 * 
 */
import {
  CRApplication, CRApplicant, CRAvailableBuilding, CRReference, CRTargettedLocations, CRAffiliation, CRAddress, CRRemittance,
  CRProgram, CRProgramDetail, CRESSection, CRESQuestion, CREvaluationSheet, CRMOU, CRMOUVerification,
  Campus, CampusProgram, CampusProgramDetail, CPDAccount, CampusBuilding, CBFloor, CBFRoom,CRAAdditionalInfo
} from '../../campus';

/**
 * Import from Setting Module
 * 
 */
import {
  Configuration, ConfigurationOption, Bank, OutgoingMailServer, EmailTemplate, WorkFlow, WFState, WFStateFlow, EvaluationSheet,
  ESSection, ESQuestion, ESQuestionOption, EmailTemplateAssignment, WorkFlowLog, ESCategory, Attachment, AttachedFile, PassingYear,
  Board, BoardNOCNotRequired,
  Vendor, CampusDefaultUser
} from '../../setting';
import {
  FeeHeads, FeeStructure, FeeStructureClasses, FeeStructureFeeHeads, Concession, ConcessionFeeHead, Scholarship, ScholarshipFeeHead,
  FSCampus, FSCFeeHead, FSInstallement, FSILineItem, FS_InstallmentPlan, LicenseFee
} from '../../fee-management'

/**
 * Import from Admission Module
 * 
 */
import {
  EligibilityCriteria, MeritList, ECPassingYear, ECSubject, ECPreReqProgram, Document, CampusAdmission, CAProgramDetail
} from '../../admission';

/**
 * Import from Time Table Module
 * 
 * 
 */
import {
  Slot, Section, TimeTable, TTDetail, TTDSection, TTDDay, TimeTableValidity, TimeTableCampus, CampusTimeTable, CTTDetail,
  CTTDDay, CTTDSection,
} from '../../time-table'

/**
 * Import from Institute Module
 * 
 */
import {
  Institute, EducationLevel, InstituteType,
  CampusAcademicCalendar,

} from '../../institute';

/**
 * Academic
 * 
 */
import {
  Program, ProgramDetail, Classes, Course, AcademicCalendar, AcademicCalendarProgramDetail, RoadMap, RoadMapPreRequisiteCourse, RoadMapCourse,
  CACTerm, Activity,ACActivity
} from '../../academic';


export class Connection {
  sequelize: Sequelize;
  constructor() { }
  public createConnection(): Sequelize {
    /** Instantiating Sequelize instance for creating connection */
    this.sequelize = new Sequelize(CONNECTION_STRING);

    this.sequelize
      .authenticate()
      .then(() => {
        // console.log('Connection has been established successfully.');
      })
      .catch(err => {
        // console.error('Unable to connect to the database:', err);
      });
    this.sequelize.addModels([
      AppFeature,
      FeaturePermission,
      Group,
      RoleAssignment,
      Role,
      UserGroup,
      User,
      Configuration,
      ConfigurationOption,
      CRApplication,
      CRApplicant,
      CRAvailableBuilding,
      CRReference,
      CRTargettedLocations,
      Institute,
      Program,
      CRAffiliation,
      CRAddress,
      CRRemittance,
      Bank,
      EducationLevel,
      ProgramDetail,
      InstituteType,
      OutgoingMailServer,
      EmailTemplate,
      LicenseFee,
      Course,
      Classes,
      AcademicCalendar,
      AcademicCalendarProgramDetail,
      WorkFlow,
      WFState,
      WFStateFlow,
      EvaluationSheet,
      ESSection,
      RoadMap,
      RoadMapPreRequisiteCourse,
      ESQuestion,
      ESQuestionOption,
      RoadMapCourse,
      RoleWFStateFlow,
      CRProgram,
      CRProgramDetail,
      EmailTemplateAssignment,
      WorkFlowLog,
      Attachment,
      AttachedFile,
      ESCategory,
      CRESSection,
      CRESQuestion,
      CREvaluationSheet,
      CRMOU,
      CRMOUVerification,
      Campus,
      CampusProgram,
      CampusProgramDetail,
      CPDAccount,
      PassingYear,
      FeeHeads,
      FeeStructure,
      FeeStructureClasses,
      FeeStructureFeeHeads,
      Concession,
      ConcessionFeeHead,
      Scholarship,
      ScholarshipFeeHead,
      EligibilityCriteria,
      ECPassingYear,
      ECSubject,
      ECPreReqProgram,
      FSCampus,
      FSCFeeHead,
      MeritList,
      CampusAcademicCalendar,
      Board,
      BoardNOCNotRequired,
      Vendor,
      Slot,
      Section,
      TimeTable,
      TTDetail,
      TTDSection,
      TTDDay,
      TimeTableValidity,
      TimeTableCampus,
      CampusBuilding,
      CBFloor,
      CBFRoom,
      FSInstallement,
      FSILineItem,
      CACTerm,
      CampusTimeTable,
      CTTDetail,
      CTTDDay,
      CTTDSection,
      FS_InstallmentPlan,
      CampusAdmission,
      CAProgramDetail,
      Document, 
      CampusDefaultUser,
      CRAAdditionalInfo,
      Activity,
      ACActivity
    ]);
    return this.sequelize;
  }
}
