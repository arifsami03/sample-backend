import { Router } from 'express';

import { ProfileController } from '..';

/**
 * / route
 *
 * @class Profile
 */
export class ProfileRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class ProfileRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class ProfileRoute
   * @method create
   *
   */
  public create() {
    let controller = new ProfileController();

    this.router.route('/profile/profile/find/:id').get(controller.find);

    this.router.route('/profile/profile/update/:id').put(controller.update);

    this.router.route('/profile/profile/forgotPassword').post(controller.forgotPassword);

    this.router.route('/profile/profile/changePassword/:id').post(controller.changePassword);

    this.router.route('/profile/profile/resetPassword').post(controller.resetPassword);

    this.router.route('/profile/profile/validateVerificationCode').post(controller.validateVerificationCode);

  }
}
