import { BaseModel } from '../../base';
import { CampusTimeTable } from '..';
import * as _ from 'lodash';
import { Promise } from 'bluebird';
import { ProgramDetailModel, ClassesModel } from '../../academic'
import { TTDetailModel, TTDSectionModel, TTDDayModel } from '..'
import { CTTDetailModel } from './ctt-detail.model';

export class CampusTimeTableModel extends BaseModel {
  constructor() {
    super(CampusTimeTable);
  }

  private cttDetailModel = new CTTDetailModel();

  indexByRelatedData() {
    let progDetail = new ProgramDetailModel();
    let classModel = new ClassesModel();
    let finalResult = [];

    //Getting List of all the Time Table
    return this.findAll(['id', 'title', 'programDetailId', 'classId']).then(timeTablList => {

      //Getting title of Program Detail and Class for each Time Table
      return Promise.each(timeTablList, (timeTableItem) => {

        //Getting title of Progam Detail
        return progDetail.findByCondition(['id', 'name'], { id: timeTableItem['programDetailId'] }).then(progDetRes => {

          //Getting Title of Class
          return classModel.findByCondition(['id', 'name'], { id: timeTableItem['classId'] }).then(classRes => {

            //Preparing array to pass on front end to display list of Time Table.
            finalResult.push({
              id: timeTableItem['id'],
              programDetailId: timeTableItem['programDetailId'],
              classId: timeTableItem['classId'],
              title: timeTableItem['title'],
              programDetail: progDetRes['name'],
              classTitle: classRes['name'],
            });

          })


        })

      }).then(() => {

        return finalResult;

      })
    })

  }

  deleteByRelatedData(id) {
    let ttDetailModel = new TTDetailModel();

    //getting list of All the Time table Detail mapped on time Table.
    return ttDetailModel.findAllByConditions(['id'], { timeTableId: id }).then(ttDetailList => {

      let deletedIds = [];
      ttDetailList.forEach(item => {
        deletedIds.push(item['id']);
      });

      //Deleting All the Sections which are mapped on the Time table

      return Promise.each(deletedIds, deletedId => {
        return ttDetailModel.deleteByRelatedData(deletedId);
      }).then(() => {
        return super.delete(id);
      })

    })

  }

  deleteByConditionsWithAssociatedRecords(condition) {
    return this.findAllByConditions(['id'], condition).then(ctts => {
      let ttDetailModel = new TTDetailModel();

      return Promise.each(ctts, ctt => {
        //getting list of All the Time table Detail mapped on time Table.
        return ttDetailModel.findAllByConditions(['id'], { timeTableId: ctt['id'] }).then(ttDetailList => {

          let deletedIds = [];
          ttDetailList.forEach(item => {
            deletedIds.push(item['id']);
          });

          //Deleting All the Sections which are mapped on the Time table

          return Promise.each(deletedIds, deletedId => {
            return ttDetailModel.deleteByRelatedData(deletedId);
          }).then(() => {
            return super.delete(ctt['id']);
          })

        })

      })
    })

  }

  findByRelatedData(id) {

    let progDetail = new ProgramDetailModel();
    let classModel = new ClassesModel();
    let finalResult = {};

    //Getting Detail of Time Table based on Id
    return this.findByCondition(['id', 'title', 'programDetailId', 'classId'], { id: id }).then(timeTable => {

      //Getting Names of Program Detail which are mapped on Time Table
      return progDetail.findByCondition(['id', 'name'], { id: timeTable['programDetailId'] }).then(progDetRes => {

        //Getting Names of Classes which are mapped on Time Table
        return classModel.findByCondition(['id', 'name'], { id: timeTable['classId'] }).then(classRes => {

          //Preparing Object of Time Table.
          finalResult['id'] = timeTable['id']
          finalResult['programDetailId'] = timeTable['programDetailId']
          finalResult['classId'] = timeTable['classId']
          finalResult['title'] = timeTable['title']
          finalResult['programDetail'] = progDetRes['name']
          finalResult['classTitle'] = classRes['name']


        })


      }).then(() => {

        return finalResult;

      })
    })

  }



  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Campus Time Table';
    retResult['records'] = [];

    return this.findAllByConditions(['id', 'title'], condition).then((cttRes) => {

      return Promise.each(cttRes, item => {

        let tempItem = item['dataValues'];
        tempItem['children'] = [];
        return this.findChildrenForDel(item['dataValues']).then(res => {

          tempItem['children'] = res;

          retResult['records'].push(tempItem);

        });

      })
    }).then(() => {

      return retResult;

    });

  }

  private findChildrenForDel(item) {

    let itRes = [];

    return this.cttDetailModel.findRelatedDataWithCondition({ campusTimeTableId: item['id'] }).then(cttdRes => {

      itRes.push(cttdRes);

    }).then(() => {
      return itRes;
    });
  }

  
}
