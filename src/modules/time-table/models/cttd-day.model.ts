import { BaseModel } from '../../base';
import { CTTDDay } from '..';
import { Sequelize } from 'sequelize-typescript';

export class CTTDDayModel extends BaseModel {
  constructor() {
    super(CTTDDay);
  }

  deleteAllByConditions(condition: any) {
    return this.deleteByConditions(condition);
  }
  
}
