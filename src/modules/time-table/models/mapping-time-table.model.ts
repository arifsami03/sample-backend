import { BaseModel } from '../../base';
import { Sequelize } from 'sequelize-typescript';
import { TimeTableModel, CampusTimeTableModel, CTTDetailModel, CTTDDayModel, CTTDSectionModel, TTDetailModel, TTDDayModel, TTDSectionModel } from '..'
import { Promise } from 'bluebird';
import { CampusModel } from '../../campus';
import * as _ from 'lodash';

export class MappingTimeTableModel {
  constructor() {
  }

  indexWithRelatedData() {

    let timeTableModel = new TimeTableModel();
    let campusTTModel = new CampusTimeTableModel();
    let cttdetailModel = new CTTDetailModel();
    let cttdayModel = new CTTDDayModel();
    let cttdsecModel = new CTTDSectionModel();
    let campusModel = new CampusModel();
    let finalResult = [];

    return campusTTModel.findAll(['id', 'timeTableId', 'campusId', 'validFrom', 'validTo']).then(mappingList => {

      return Promise.each(mappingList, (item) => {

        //Find all the Time Table mapped on the Mapping of Time table 
        return timeTableModel.findByCondition(['title'], { id: item['timeTableId'] }).then(timeTableTitle => {

          return campusModel.findByCondition(['campusName'], { id: item['campusId'] }).then((campusName) => {
            //Preparing array of data to be passed on front end.
            finalResult.push({
              id: item['id'],
              timeTableId: item['timeTableId'],
              validFrom: item['validFrom'],
              validTo: item['validTo'],
              timeTableTitle: timeTableTitle['title'],
              campusTitle: campusName['campusName']
            })
          });


        })
      }).then(() => {

        return finalResult;

      })


    })

  }

  deleteWithRelatedData(id) {

    let campusTTModel = new CampusTimeTableModel();
    let cttdetailModel = new CTTDetailModel();
  
    return cttdetailModel.findAllByConditions(['id'], { campusTimeTableId: id }).then(cttDetails => {
      return Promise.each(cttDetails, cttDetail => {
        return cttdetailModel.deleteByRelatedData(cttDetail['id']);
      }).then(() => {
        return campusTTModel.delete(id)
      })

    })


  }

  createWithRelatedData(postData) {


    let campusTTModel = new CampusTimeTableModel();
    let cttdetailModel = new CTTDetailModel();
    let cttdayModel = new CTTDDayModel();
    let cttdsecModel = new CTTDSectionModel();

    let timeTableModel = new TimeTableModel();
    let ttdetailModel = new TTDetailModel();
    let ttdayModel = new TTDDayModel();
    let ttsecModel = new TTDSectionModel();

    let timeTableResData = {};

    //Get all Record of Time Table using Time table Id.
    return timeTableModel.findByCondition(['id', 'programDetailId', 'classId', 'title'], { id: postData['ttValidityData']['timeTableId'] }).then(timeTableData => {

      //Create an object of TimeTableResData on which Time table and all its associated data is added.
      timeTableResData['timeTable'] = {
        id: timeTableData['id'],
        programDetailId: timeTableData['programDetailId'],
        classId: timeTableData['classId'],
        title: timeTableData['title'],
      }

      //Get all the Details of Time table
      return ttdetailModel.findAllByConditions(['id', 'slotId', 'slotOrder', 'courseId'], { timeTableId: postData['ttValidityData']['timeTableId'] }).then(ttdetailData => {


        //As Time Table Details Can be multiple so in Time table Object we add an array of Time Table Detials.
        timeTableResData['timeTable']['ttDetailData'] = [];

        let sectionIds = [];
        let dayIds = [];

        //Against Each time table Detail id Sections and Days are mapped, Get that Record.
        return Promise.each(ttdetailData, ttdetailId => {

          sectionIds = [];
          dayIds = [];

          //Get Sections against each Time Table Detail
          return ttsecModel.findAllByConditions(['sectionId'], { TTDetailId: ttdetailId['id'] }).then(sectionData => {

            sectionData.forEach(element => {

              sectionIds.push(
                element['sectionId']
              )

            });



          }).then(() => {

            //Get Days Against each Time Talbles Detail.
            return ttdayModel.findAllByConditions(['dayId'], { TTDetailId: ttdetailId['id'] }).then(daysData => {

              daysData.forEach(element => {
                dayIds.push(
                  element['dayId']
                )
              });

              //Prepare Array of Time Table Detail with all the associated sections and days.
              timeTableResData['timeTable']['ttDetailData'].push({
                slotId: ttdetailId['slotId'],
                slotOrder: ttdetailId['slotOrder'],
                courseId: ttdetailId['courseId'],
                sections: sectionIds,
                days: dayIds
              })


            })

          })

        }
        ).then(() => {

          //Create Object of campusTimeTableData to add record in the CampusTimeTable
          var campusTimeTableData = {};
          campusTimeTableData['campusId'] = postData['campusIds'];
          campusTimeTableData['timeTableId'] = postData['ttValidityData']['timeTableId'];
          campusTimeTableData['programDetailId'] = timeTableResData['timeTable']['programDetailId'];
          campusTimeTableData['classId'] = timeTableResData['timeTable']['classId'];
          campusTimeTableData['title'] = timeTableResData['timeTable']['title'];
          campusTimeTableData['validFrom'] = postData['ttValidityData']['validFrom'];
          campusTimeTableData['validTo'] = postData['ttValidityData']['validTo'];

          //Add campus Time Table Record in Campus Time Table.
          return campusTTModel.create(campusTimeTableData).then(resCampusTT => {

            //Against each time table details
            return Promise.each(timeTableResData['timeTable']['ttDetailData'], detailItem => {

              //Prepare an object of Campus time table detail to be added.
              var detailData = {};
              detailData['campusTimeTableId'] = resCampusTT['id'];
              detailData['slotId'] = detailItem['slotId'];
              detailData['slotOrder'] = detailItem['slotOrder'];
              detailData['courseId'] = detailItem['courseId'];

              //save the campus Time Table Detail object
              return cttdetailModel.create(detailData).then(detailId => {

                //For Bulk Insertion of Section along with the Campus Time Table Detail Creare an array.
                var sectionData = [];
                detailItem['sections'].forEach(element => {
                  sectionData.push({
                    CTTDetailId: detailId['id'],
                    sectionId: element,
                  })
                });

                //For Bulk Insertion of Days along with the Campus Time Table Detail Creare an array.
                var dayData = [];
                detailItem['days'].forEach(element => {
                  dayData.push({
                    CTTDetailId: detailId['id'],
                    dayId: element,
                  })
                });

                //BUlk insert the Campus Time Table Sections
                return cttdsecModel.sequelizeModel.bulkCreate(sectionData).then(() => {

                  //BUlk insert the Campus Time Table Days
                  return cttdayModel.sequelizeModel.bulkCreate(dayData);

                });


              })
            })



          })

        })

      })




    })


  }

  findWithRelatedData(id) {


    let campusTTModel = new CampusTimeTableModel();
    let cttdetailModel = new CTTDetailModel();
    let cttdayModel = new CTTDDayModel();
    let cttdsecModel = new CTTDSectionModel();


    let finalResult = {};

    // First get detail of Time Table validity by Id
    return campusTTModel.findByCondition(['id', 'timeTableId', 'campusId', 'validFrom', 'validTo'], { id: id }).then(ttValidityItem => {

      return new TimeTableModel().findByCondition(['title'], { id: ttValidityItem['timeTableId'] }).then((timeTableTitle) => {


        // Prepare array to pass it to front end
        finalResult['id'] = ttValidityItem['id'];
        finalResult['timeTableId'] = ttValidityItem['timeTableId'];
        finalResult['validFrom'] = ttValidityItem['validFrom'];
        finalResult['validTo'] = ttValidityItem['validTo'];
        finalResult['campusId'] = ttValidityItem['campusId'];
        finalResult['timeTableTitle'] = timeTableTitle['title'];

      }).then(() => {

        return finalResult;

      })



    })

  }

  updateWithRelatedData(id, postData) {



    let campusTTModel = new CampusTimeTableModel();
    let cttdetailModel = new CTTDetailModel();
    let cttdayModel = new CTTDDayModel();
    let cttdsecModel = new CTTDSectionModel();

    let timeTableModel = new TimeTableModel();
    let ttdetailModel = new TTDetailModel();
    let ttdayModel = new TTDDayModel();
    let ttsecModel = new TTDSectionModel();

    //Update Time table details.
    return campusTTModel.update(id, postData.ttValidityData).then(ttValidity => {

      return cttdetailModel.findAllByConditions(['id'], { campusTimeTableId: id }).then(cttdetailIds => {

        return Promise.each(cttdetailIds, detailId => {

          return cttdsecModel.deleteByConditions({ CTTDetailId: detailId['id'] }).then(() => {

            return cttdayModel.deleteByConditions({ CTTDetailId: detailId['id'] })

          })

        }).then(() => {

          return cttdetailModel.deleteByConditions({ campusTimeTableId: id }).then(() => {

            let timeTableResData = {};

            //Get all Record of Time Table using Time table Id.
            return timeTableModel.findByCondition(['id', 'programDetailId', 'classId', 'title'], { id: postData['ttValidityData']['timeTableId'] }).then(timeTableData => {

              //Create an object of TimeTableResData on which Time table and all its associated data is added.
              timeTableResData['timeTable'] = {
                id: timeTableData['id'],
                programDetailId: timeTableData['programDetailId'],
                classId: timeTableData['classId'],
                title: timeTableData['title'],
              }

              //Get all the Details of Time table
              return ttdetailModel.findAllByConditions(['id', 'slotId', 'slotOrder', 'courseId'], { timeTableId: postData['ttValidityData']['timeTableId'] }).then(ttdetailData => {


                //As Time Table Details Can be multiple so in Time table Object we add an array of Time Table Detials.
                timeTableResData['timeTable']['ttDetailData'] = [];

                let sectionIds = [];
                let dayIds = [];

                //Against Each time table Detail id Sections and Days are mapped, Get that Record.
                return Promise.each(ttdetailData, ttdetailId => {

                  sectionIds = [];
                  dayIds = [];

                  //Get Sections against each Time Table Detail
                  return ttsecModel.findAllByConditions(['sectionId'], { TTDetailId: ttdetailId['id'] }).then(sectionData => {

                    sectionData.forEach(element => {

                      sectionIds.push(
                        element['sectionId']
                      )

                    });



                  }).then(() => {

                    //Get Days Against each Time Talbles Detail.
                    return ttdayModel.findAllByConditions(['dayId'], { TTDetailId: ttdetailId['id'] }).then(daysData => {

                      daysData.forEach(element => {
                        dayIds.push(
                          element['dayId']
                        )
                      });

                      //Prepare Array of Time Table Detail with all the associated sections and days.
                      timeTableResData['timeTable']['ttDetailData'].push({
                        slotId: ttdetailId['slotId'],
                        slotOrder: ttdetailId['slotOrder'],
                        courseId: ttdetailId['courseId'],
                        sections: sectionIds,
                        days: dayIds
                      })


                    })

                  })

                }
                ).then(() => {

                  //Create Object of campusTimeTableData to add record in the CampusTimeTable
                  var campusTimeTableData = {};
                  campusTimeTableData['campusId'] = postData['campusIds'];
                  campusTimeTableData['timeTableId'] = postData['ttValidityData']['timeTableId'];
                  campusTimeTableData['programDetailId'] = timeTableResData['timeTable']['programDetailId'];
                  campusTimeTableData['classId'] = timeTableResData['timeTable']['classId'];
                  campusTimeTableData['title'] = timeTableResData['timeTable']['title'];
                  campusTimeTableData['validFrom'] = postData['ttValidityData']['validFrom'];
                  campusTimeTableData['validTo'] = postData['ttValidityData']['validTo'];


                  //Against each time table details
                  return Promise.each(timeTableResData['timeTable']['ttDetailData'], detailItem => {

                    //Prepare an object of Campus time table detail to be added.
                    var detailData = {};
                    detailData['campusTimeTableId'] = id;
                    detailData['slotId'] = detailItem['slotId'];
                    detailData['slotOrder'] = detailItem['slotOrder'];
                    detailData['courseId'] = detailItem['courseId'];

                    //save the campus Time Table Detail object
                    return cttdetailModel.create(detailData).then(detailId => {

                      //For Bulk Insertion of Section along with the Campus Time Table Detail Creare an array.
                      var sectionData = [];
                      detailItem['sections'].forEach(element => {
                        sectionData.push({
                          CTTDetailId: detailId['id'],
                          sectionId: element,
                        })
                      });

                      //For Bulk Insertion of Days along with the Campus Time Table Detail Creare an array.
                      var dayData = [];
                      detailItem['days'].forEach(element => {
                        dayData.push({
                          CTTDetailId: detailId['id'],
                          dayId: element,
                        })
                      });

                      //BUlk insert the Campus Time Table Sections
                      return cttdsecModel.sequelizeModel.bulkCreate(sectionData).then(() => {

                        //BUlk insert the Campus Time Table Days
                        return cttdayModel.sequelizeModel.bulkCreate(dayData);

                      });


                    })
                  })
                })
              })
            })
          })
        })
      })
    })
  }

}
