import { BaseModel } from '../../base';
import { CTTDSection } from '..';
import { Sequelize } from 'sequelize-typescript';

export class CTTDSectionModel extends BaseModel {
  constructor() {
    super(CTTDSection);
  }

  deleteAllByConditions(condition: any) {
    return this.deleteByConditions(condition);
  }
  
}
