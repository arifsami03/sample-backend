import { BaseModel } from '../../base';
import { TTDSection } from '..';
import { Sequelize } from 'sequelize-typescript';

export class TTDSectionModel extends BaseModel {
  constructor() {
    super(TTDSection);
  }

  deleteAllByConditions(condition: any) {
    return this.deleteByConditions(condition);
  }


}
