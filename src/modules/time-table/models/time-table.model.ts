import { BaseModel } from '../../base';
import { TimeTable } from '..';
import * as _ from 'lodash';
import * as dateFormat from 'dateformat';
import { Promise } from 'bluebird';
import { ProgramDetailModel, ClassesModel, CourseModel, Program } from '../../academic'
import { TTDetailModel, TTDSectionModel, TTDDayModel, SlotModel, CampusTimeTableModel, CTTDetailModel, CTTDDayModel, CTTDSectionModel, SectionModel } from '..'
import { EducationLevel, InstituteType } from '../../institute';
import { CampusTimeTable } from './schema/campus-time-table';

export class TimeTableModel extends BaseModel {
  constructor() {
    super(TimeTable);
  }

  private ttDetailModel = new TTDetailModel();
  private cttModel = new CampusTimeTableModel();

  indexByRelatedData() {

    let progDetail = new ProgramDetailModel();
    let classModel = new ClassesModel();
    let finalResult = [];

    //Getting List of all the Time Table
    return this.findAll(['id', 'title', 'programDetailId', 'classId']).then(timeTablList => {

      //Getting title of Program Detail and Class for each Time Table
      return Promise.each(timeTablList, (timeTableItem) => {
        // {{ progDetail.program.educationLevel.instituteType.name }} - {{ progDetail.program.educationLevel.education }} - {{ progDetail.program.name
        // }} - {{ progDetail.name }}
        //Getting title of Progam Detail
        return progDetail.findByCondition(['id', 'name', 'programId'], { id: timeTableItem['programDetailId'] }).then(progDetRes => {

          //Getting Title of Class
          return classModel.findByCondition(['id', 'name'], { id: timeTableItem['classId'] }).then(classRes => {

            //Preparing array to pass on front end to display list of Time Table.
            finalResult.push({
              id: timeTableItem['id'],
              programDetailId: timeTableItem['programDetailId'],
              classId: timeTableItem['classId'],
              title: timeTableItem['title'],
              programDetail: progDetRes.name,
              classTitle: classRes['name'],
            });

          })


        })

      }).then(() => {

        return finalResult;

      })
    })

  }

  deleteByRelatedData(id) {

    let ttDetailModel = new TTDetailModel();

    //getting list of All the Time table Detail mapped on time Table.
    return ttDetailModel.findAllByConditions(['id'], { timeTableId: id }).then(ttDetailList => {

      return Promise.each(ttDetailList, ttDetail => {

        return ttDetailModel.deleteByRelatedData(ttDetail['id']);

      }).then(() => {

        return super.delete(id);

      })
    })

  }

  // deleteByConditionsWithAssociatedRecords(condition) {


  //   return this.findAllByConditions(['id'], condition).then(tts => {
  //     let ttDetailModel = new TTDetailModel();
  //     let campusTimeTableModel = new CampusTimeTableModel();

  //     return Promise.each(tts, tt => {

  //       //getting list of All the Time table Detail mapped on time Table.
  //       return ttDetailModel.findAllByConditions(['id'], { timeTableId: tt['id'] }).then(ttDetailList => {

  //         return Promise.each(ttDetailList, ttDetail => {

  //           return ttDetailModel.deleteByRelatedData(ttDetail['id']);

  //         }).then(() => {


  //           return campusTimeTableModel.findAllByConditions(['id'], { timeTableId: tt['id'] }).then(campusTimeTables => {

  //             return Promise.each(campusTimeTables, campusTimeTable => {

  //               return campusTimeTableModel.deleteByRelatedData(campusTimeTable['id']);

  //             }).then(() => {

  //               return super.delete(tt['id']);

  //             })
  //           })
  //         })

  //       })

  //     })
  //   })

  // }

  findByRelatedData(id) {

    let progDetail = new ProgramDetailModel();
    let classModel = new ClassesModel();
    let finalResult = {};

    //Getting Detail of Time Table based on Id
    return this.findByCondition(['id', 'title', 'programDetailId', 'classId'], { id: id }).then(timeTable => {

      //Getting Names of Program Detail which are mapped on Time Table
      return progDetail.findByCondition(['id', 'name', 'programId'], { id: timeTable['programDetailId'] }, [
        {
          model: Program,
          as: 'program',
          attributes: ['id', 'name', 'educationLevelId'],
          include: [
            {
              model: EducationLevel,
              as: 'educationLevel',
              attributes: ['id', 'education', 'instituteTypeId'],
              include: [
                {
                  model: InstituteType,
                  as: 'instituteType',
                  attributes: ['id', 'name']
                }
              ]
            }
          ]
        }
      ]).then(progDetRes => {

        //Getting Names of Classes which are mapped on Time Table
        return classModel.findByCondition(['id', 'name'], { id: timeTable['classId'] }).then(classRes => {

          //Preparing Object of Time Table.
          finalResult['id'] = timeTable['id']
          finalResult['programDetailId'] = timeTable['programDetailId']
          finalResult['classId'] = timeTable['classId']
          finalResult['title'] = timeTable['title']
          finalResult['programDetail'] = progDetRes.program.educationLevel.education + '-' + progDetRes.program.name + '-' + progDetRes.name
          finalResult['classTitle'] = classRes['name']


        })


      }).then(() => {

        return finalResult;

      })
    })

  }

  getListOfSlotsInTimeTable(id) {

    let ttDetailModel = new TTDetailModel();
    let ttSecModel = new TTDSectionModel();
    let ttDayModel = new TTDDayModel();
    let slotModel = new SlotModel();

    let finalResult = [];

    return ttDetailModel.findAllByConditions(['id', 'slotId', 'slotOrder', 'courseId'], { timeTableId: id }).then(ttdetails => {

      return Promise.each(ttdetails, item => {

        return slotModel.findByCondition(['fromTime', 'toTime'], { id: item['slotId'] }).then(slot => {

          return ttSecModel.findAllByConditions(['sectionId'], { TTDetailId: item['id'] }).then(sections => {

            return ttDayModel.findAllByConditions(['dayId'], { TTDetailId: item['id'] }).then(days => {

              var daysIds = [];
              days.forEach(element => {

                daysIds.push(element['dayId']);

              });
              var sectionsIds = [];
              sections.forEach(element => {

                sectionsIds.push(element['sectionId']);

              });


              finalResult.push({
                slotId: item['slotId'],
                slotOrder: item['slotOrder'],
                courseId: item['courseId'],
                slotTitle: dateFormat(slot['fromTime'], "UTC:hh:MM TT") + ' - ' + dateFormat(slot['toTime'], "UTC:hh:MM TT"),
                sections: sectionsIds,
                days: daysIds
              })

            })

          })

        })

      }).then(() => {
        return finalResult;
      })

    })


  }

  saveDuplicateTimeTable(id, postData) {

    let timeTableModel = new TimeTableModel();
    let ttdetailModel = new TTDetailModel();
    let ttdayModel = new TTDDayModel();
    let ttsecModel = new TTDSectionModel();

    let campusTTModel = new CampusTimeTableModel();
    let cttdetailModel = new CTTDetailModel();
    let cttdayModel = new CTTDDayModel();
    let cttdsecModel = new CTTDSectionModel();

    let timeTableResData = {};

    //Get all Record of Time Table using Time table Id.
    return timeTableModel.findByCondition(['id', 'programDetailId', 'classId', 'title'], { id: id }).then(timeTableData => {

      //Create an object of TimeTableResData on which Time table and all its associated data is added.
      timeTableResData['timeTable'] = {
        id: timeTableData['id'],
        programDetailId: timeTableData['programDetailId'],
        classId: timeTableData['classId'],
        title: timeTableData['title'],
      }

      //Get all the Details of Time table
      return ttdetailModel.findAllByConditions(['id', 'slotId', 'slotOrder', 'courseId'], { timeTableId: id }).then(ttdetailData => {


        //As Time Table Details Can be multiple so in Time table Object we add an array of Time Table Detials.
        timeTableResData['timeTable']['ttDetailData'] = [];

        let sectionIds = [];


        //Against Each time table Detail id Sections and Days are mapped, Get that Record.
        return Promise.each(ttdetailData, ttdetailId => {

          sectionIds = [];

          //Get Sections against each Time Table Detail
          return ttsecModel.findAllByConditions(['sectionId'], { TTDetailId: ttdetailId['id'] }).then(sectionData => {

            sectionData.forEach(element => {

              sectionIds.push(
                element['sectionId']
              )

            });



          })

        }
        ).then(() => {

          var duplicateTimeTable = {
            title: postData['timeTable']['timeTableTitle'],
            classId: timeTableResData['timeTable']['classId'],
            programDetailId: timeTableResData['timeTable']['programDetailId'],
          }

          return timeTableModel.create(duplicateTimeTable).then(duplicateTTId => {

            return Promise.each(postData['slotDetails'], item => {

              var duplicateTTdetail = {
                slotId: item['slotId'],
                slotOrder: item['slotOrder'],
                courseId: item['courseId'],
                timeTableId: duplicateTTId['id'],

              };

              return ttdetailModel.create(duplicateTTdetail).then(duplicateTTdetailId => {

                //For Bulk Insertion of Section along with the Campus Time Table Detail Creare an array.
                var sectionData = [];
                item['sections'].forEach(element => {
                  sectionData.push({
                    TTDetailId: duplicateTTdetailId['id'],
                    sectionId: element,
                  })
                });

                //For Bulk Insertion of Days along with the Campus Time Table Detail Creare an array.
                var days = [];
                var dayData = [];
                if (item['monday']) {
                  days.push('MON')
                }
                if (item['tuesday']) {
                  days.push('TUE')
                }
                if (item['wednesday']) {
                  days.push('WED')
                }
                if (item['thursday']) {
                  days.push('THR')
                }
                if (item['friday']) {
                  days.push('FRI')
                }
                if (item['saturday']) {
                  days.push('SAT')
                }
                if (item['sunday']) {
                  days.push('sun')
                }
                days.forEach(element => {
                  dayData.push({
                    TTDetailId: duplicateTTdetailId['id'],
                    dayId: element,
                  })
                });


                //BUlk insert the  Time Table Sections
                return ttsecModel.sequelizeModel.bulkCreate(sectionData).then(() => {

                  //BUlk insert the  Time Table Days
                  return ttdayModel.sequelizeModel.bulkCreate(dayData);

                });

              })


            }).then(() => {
              timeTableResData = {};

              //Get all Record of Time Table using Time table Id.
              return timeTableModel.findByCondition(['id', 'programDetailId', 'classId', 'title'], { id: duplicateTTId['id'] }).then(timeTableData => {

                //Create an object of TimeTableResData on which Time table and all its associated data is added.
                timeTableResData['timeTable'] = {
                  id: timeTableData['id'],
                  programDetailId: timeTableData['programDetailId'],
                  classId: timeTableData['classId'],
                  title: timeTableData['title'],
                }

                //Get all the Details of Time table
                return ttdetailModel.findAllByConditions(['id', 'slotId', 'slotOrder', 'courseId'], { timeTableId: duplicateTTId['id'] }).then(ttdetailData => {


                  //As Time Table Details Can be multiple so in Time table Object we add an array of Time Table Detials.
                  timeTableResData['timeTable']['ttDetailData'] = [];

                  let sectionIds = [];
                  let dayIds = [];

                  //Against Each time table Detail id Sections and Days are mapped, Get that Record.
                  return Promise.each(ttdetailData, ttdetailId => {

                    sectionIds = [];
                    dayIds = [];

                    //Get Sections against each Time Table Detail
                    return ttsecModel.findAllByConditions(['sectionId'], { TTDetailId: ttdetailId['id'] }).then(sectionData => {

                      sectionData.forEach(element => {

                        sectionIds.push(
                          element['sectionId']
                        )

                      });



                    }).then(() => {

                      //Get Days Against each Time Talbles Detail.
                      return ttdayModel.findAllByConditions(['dayId'], { TTDetailId: ttdetailId['id'] }).then(daysData => {

                        daysData.forEach(element => {
                          dayIds.push(
                            element['dayId']
                          )
                        });

                        //Prepare Array of Time Table Detail with all the associated sections and days.
                        timeTableResData['timeTable']['ttDetailData'].push({
                          slotId: ttdetailId['slotId'],
                          slotOrder: ttdetailId['slotOrder'],
                          courseId: ttdetailId['courseId'],
                          sections: sectionIds,
                          days: dayIds
                        })


                      })

                    })

                  }
                  ).then(() => {

                    return Promise.each(postData['campusIds'], campusId => {
                      //Create Object of campusTimeTableData to add record in the CampusTimeTable
                      var campusTimeTableData = {};
                      campusTimeTableData['campusId'] = campusId;
                      campusTimeTableData['timeTableId'] = duplicateTTId['id'];
                      campusTimeTableData['programDetailId'] = timeTableResData['timeTable']['programDetailId'];
                      campusTimeTableData['classId'] = timeTableResData['timeTable']['classId'];
                      campusTimeTableData['title'] = timeTableResData['timeTable']['title'];
                      campusTimeTableData['validFrom'] = postData['timeTable']['validFrom'];
                      campusTimeTableData['validTo'] = postData['timeTable']['validTo'];

                      //Add campus Time Table Record in Campus Time Table.
                      return campusTTModel.create(campusTimeTableData).then(resCampusTT => {

                        //Against each time table details
                        return Promise.each(timeTableResData['timeTable']['ttDetailData'], detailItem => {

                          //Prepare an object of Campus time table detail to be added.
                          var detailData = {};
                          detailData['campusTimeTableId'] = resCampusTT['id'];
                          detailData['slotId'] = detailItem['slotId'];
                          detailData['slotOrder'] = detailItem['slotOrder'];
                          detailData['courseId'] = detailItem['courseId'];

                          //save the campus Time Table Detail object
                          return cttdetailModel.create(detailData).then(detailId => {

                            //For Bulk Insertion of Section along with the Campus Time Table Detail Creare an array.
                            var sectionData = [];
                            detailItem['sections'].forEach(element => {
                              sectionData.push({
                                CTTDetailId: detailId['id'],
                                sectionId: element,
                              })
                            });

                            //For Bulk Insertion of Days along with the Campus Time Table Detail Creare an array.
                            var dayData = [];
                            detailItem['days'].forEach(element => {
                              dayData.push({
                                CTTDetailId: detailId['id'],
                                dayId: element,
                              })
                            });

                            //BUlk insert the Campus Time Table Sections
                            return cttdsecModel.sequelizeModel.bulkCreate(sectionData).then(() => {

                              //BUlk insert the Campus Time Table Days
                              return cttdayModel.sequelizeModel.bulkCreate(dayData);

                            });
                          })
                        })
                      })
                    }).then(() => {
                      return duplicateTTId['id'];
                    })


                  })
                })
              })
            })
          })
        })
      })
    })

  }


  viewTTDetails(id) {

    let progDetail = new ProgramDetailModel();
    let classModel = new ClassesModel();
    let courseModel = new CourseModel();
    let sectionModel = new SectionModel();

    let ttDetailModel = new TTDetailModel();
    let ttSecModel = new TTDSectionModel();
    let ttDayModel = new TTDDayModel();
    let slotModel = new SlotModel();


    let finalResult = {};

    //Getting Detail of Time Table based on Id
    return this.findByCondition(['id', 'title', 'programDetailId', 'classId'], { id: id }).then(timeTable => {

      //Getting Names of Program Detail which are mapped on Time Table
      return progDetail.findByCondition(['id', 'name'], { id: timeTable['programDetailId'] }).then(progDetRes => {

        //Getting Names of Classes which are mapped on Time Table
        return classModel.findByCondition(['id', 'name'], { id: timeTable['classId'] }).then(classRes => {


          return ttDetailModel.findAllByConditions(['id', 'slotId', 'slotOrder', 'courseId'], { timeTableId: id }).then(ttdetails => {

            finalResult['timeTableDetails'] = [];

            return Promise.each(ttdetails, item => {


              return slotModel.findByCondition(['fromTime', 'toTime'], { id: item['slotId'] }).then(slot => {

                return ttSecModel.findAllByConditions(['sectionId'], { TTDetailId: item['id'] }).then(sections => {

                  return ttDayModel.findAllByConditions(['dayId'], { TTDetailId: item['id'] }).then(days => {

                    return courseModel.findByCondition(['title'], { id: item['courseId'] }).then(courseTitle => {

                      var daysIds = [];

                      days.forEach(element => {

                        daysIds.push(element['dayId']);

                      });
                      var sectionsIds = [];
                      sections.forEach(element => {

                        sectionsIds.push(element['sectionId']);

                      });

                      var sectionTitles = [];
                      return Promise.each(sectionsIds, id => {

                        return sectionModel.findByCondition(['title'], { id: id }).then(sectionsTitle => {


                          sectionTitles.push(sectionsTitle['title']);

                        })

                      }).then(() => {

                        //Preparing Object of Time Table.
                        finalResult['id'] = timeTable['id']
                        finalResult['programDetailId'] = timeTable['programDetailId']
                        finalResult['classId'] = timeTable['classId']
                        finalResult['title'] = timeTable['title']
                        finalResult['programDetail'] = progDetRes['name']
                        finalResult['classTitle'] = classRes['name']
                        finalResult['timeTableDetails'].push({
                          slotId: item['slotId'],
                          courseTitle: courseTitle['title'],
                          slotOrder: item['slotOrder'],
                          courseId: item['courseId'],
                          sectionTitles: sectionTitles,
                          slotTitle: dateFormat(slot['fromTime'], "UTC:hh:MM TT") + ' - ' + dateFormat(slot['toTime'], "UTC:hh:MM TT"),
                          fromTime: slot['fromTime'],
                          toTime: slot['toTime'],
                          sections: sectionsIds,
                          days: daysIds
                        })

                      })

                    })

                  })

                })

              })

            }).then(() => {
              //Sort Function to order array by the From Time of Slot.
              finalResult['timeTableDetails'].sort(function (a, b) {
                return a['fromTime'] - b['fromTime'];
              });
              return finalResult;
            })

          })




        })


      })
    })

  }

  findRelatedData(id) {

    let retResult = {};

    return this.find(id, ['id', 'title']).then((itRes) => {

      retResult = itRes['dataValues'];
      retResult['label'] = 'Time Table';
      retResult['children'] = [];

      return this.findChildrenForDel(itRes['dataValues']).then(res => {

        retResult['children'] = res;

      });
      // return new CampusTimeTableModel().findRelatedDataWithCondition({ timeTableId: id }).then(cttRes => {
      //   retResult['children'].push(cttRes);

      // });

    }).then(() => {
      return retResult;
    })
  }


  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Time Table';
    retResult['records'] = [];

    return this.findAllByConditions(['id', 'title'], condition).then((ttRes) => {

      return Promise.each(ttRes, item => {

        let tempItem = item['dataValues'];
        tempItem['children'] = [];
        return this.findChildrenForDel(item['dataValues']).then(res => {

          tempItem['children'] = res;

          retResult['records'].push(tempItem);

        });

      })
    }).then(() => {

      return retResult;

    });
  }

  private findChildrenForDel(item) {

    let itRes = [];

    return this.ttDetailModel.findRelatedDataWithCondition({ timeTableId: item['id'] }).then(ttdRes => {

      itRes.push(ttdRes);

      return this.cttModel.findRelatedDataWithCondition({ timeTableId: item['id'] }).then(cttRes => {

        itRes.push(cttRes);

      });

    }).then(() => {
      return itRes;
    });
  }

  /**
   * Check for associated records and then delete or returns associated records
   * 
   * @param id 
   */
  deleteOne(id) {

    return this.checkForChildren(id).then(res => {

      if (res['children'].length > 0) {

        return res;

      } else {

        return this.deleteByRelatedData(id);
      }
    })

  }

  /**
   * Check if children exists
   * 
   * @param id 
   */
  checkForChildren(id) {
    let retResult = {};

    let includeObj = [{ model: CampusTimeTable, as: 'campusTimeTables', attributes: ['id'], where: BaseModel.cb(), required: false }];

    return this.find(id, ['id', 'title'], includeObj).then(res => {

      retResult['id'] = res['dataValues']['id'];
      retResult['label'] = 'Time Table';
      retResult['title'] = res['dataValues']['title'];
      retResult['children'] = [];

      if (res['campusTimeTables'].length > 0) {
        retResult['children'].push({ title: 'Campus Time Table' });
      }

      return retResult;
    })
  }

}
