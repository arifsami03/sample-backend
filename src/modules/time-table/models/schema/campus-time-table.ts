import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class CampusTimeTable extends Model<CampusTimeTable> {

  @Column title: string;
  @Column programDetailId: number;
  @Column classId: number;
  @Column campusId: number;
  @Column timeTableId: number;
  @Column validFrom: Date;
  @Column validTo: Date;

  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

}
