import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class TimeTableValidity extends Model<TimeTableValidity> {

  @Column timeTableId: number;
  @Column validFrom: Date;
  @Column validTo: Date;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

}
