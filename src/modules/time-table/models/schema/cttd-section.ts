import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class CTTDSection extends Model<CTTDSection> {

  @Column CTTDetailId: number;
  @Column sectionId: number;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

}
