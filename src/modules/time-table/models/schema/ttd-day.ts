import { Table, Column, Model, BelongsTo, ForeignKey } from 'sequelize-typescript';
import { TTDetail } from '../../'
@Table({ timestamps: true })
export class TTDDay extends Model<TTDDay> {


  @ForeignKey(() => TTDetail)
  @Column TTDetailId: number;

  @Column dayId: string;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

  @BelongsTo(() => TTDetail, {
    foreignKey: 'TTDetailId',
  })
  timeTableDetail: TTDetail
}
