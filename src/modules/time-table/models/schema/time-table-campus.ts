import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class TimeTableCampus extends Model<TimeTableCampus> {

  @Column timeTableValidityId: number;
  @Column campusId: number;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

}
