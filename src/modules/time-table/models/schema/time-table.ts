import { Table, Column, Model, HasMany } from 'sequelize-typescript';
import { TTDetail } from '../../'
import { CampusTimeTable } from './campus-time-table';
@Table({ timestamps: true })
export class TimeTable extends Model<TimeTable> {

  @Column title: string;
  @Column programDetailId: number;
  @Column classId: number;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

  @HasMany(() => TTDetail, { foreignKey: 'timeTableId' })
  ttDetails: TTDetail[];

  @HasMany(() => CampusTimeTable, { constraints: false, foreignKey: 'timeTableId' })
  campusTimeTables: CampusTimeTable[];

}
