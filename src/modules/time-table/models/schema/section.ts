import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class Section extends Model<Section> {
  @Column title: string;
  @Column abbreviation: string;
  @Column sectionId: string;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

}
