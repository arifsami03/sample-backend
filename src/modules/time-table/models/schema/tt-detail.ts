import { Table, Column, Model, BelongsTo, ForeignKey, HasMany } from 'sequelize-typescript';
import { TimeTable, TTDDay, TTDSection, Slot } from '../..';
import { Course } from '../../../academic';

@Table({ timestamps: true })
export class TTDetail extends Model<TTDetail> {

  @ForeignKey(() => TimeTable)
  @Column timeTableId: number;

  @Column slotId: number;

  @Column slotOrder: number;

  @ForeignKey(() => Course)
  @Column courseId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => TimeTable, {
    foreignKey: 'timeTableId',
  })
  timeTable: TimeTable

  @BelongsTo(() => Course, {
    foreignKey: 'courseId',
  })
  course: Course

  @BelongsTo(() => Slot, {
    foreignKey: 'slotId',
  })
  slot: Slot

  @HasMany(() => TTDDay, {
    foreignKey: 'TTDetailId'
  })
  ttdDays: TTDDay[];

  @HasMany(() => TTDSection, {
    foreignKey: 'TTDetailId'
  })
  ttdSections: TTDSection[];

}
