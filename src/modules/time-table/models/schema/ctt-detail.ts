import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Course } from '../../../academic';

@Table({ timestamps: true })
export class CTTDetail extends Model<CTTDetail> {

  @Column campusTimeTableId: number;
  @Column slotId: number;
  @Column slotOrder: number;
  @ForeignKey(() => Course)
  @Column courseId: number;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

  @BelongsTo(() => Course, {
    foreignKey: 'courseId',
  })
  course: Course

}
