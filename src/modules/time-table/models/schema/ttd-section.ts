import { Table, Column, Model, BelongsTo, ForeignKey } from 'sequelize-typescript';
import { TTDetail, Section } from '../../'

@Table({ timestamps: true })
export class TTDSection extends Model<TTDSection> {
  @ForeignKey(() => TTDetail)
  @Column TTDetailId: number;

  @Column sectionId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => TTDetail, {
    foreignKey: 'TTDetailId',
  })
  timeTableDetail: TTDetail

  @BelongsTo(() => Section, {
    foreignKey: 'sectionId',
  })
  section: Section
}
