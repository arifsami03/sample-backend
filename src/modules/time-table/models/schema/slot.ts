import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class Slot extends Model<Slot> {
  @Column fromTime: string;
  @Column toTime: string;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

}
