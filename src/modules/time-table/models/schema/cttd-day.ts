import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class CTTDDay extends Model<CTTDDay> {
  @Column CTTDetailId: number;
  @Column dayId: string;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

}
