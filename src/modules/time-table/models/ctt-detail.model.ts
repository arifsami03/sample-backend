import { BaseModel } from '../../base';
import { Sequelize } from 'sequelize-typescript';
import { TTDSectionModel, TTDDayModel, SlotModel, CTTDetail } from '..'
import { CourseModel, Course } from '../../academic'
import { Promise } from 'bluebird'
import * as _ from 'lodash';

export class CTTDetailModel extends BaseModel {
  constructor() {
    super(CTTDetail);
  }

  createByRelatedData(item) {

    let ttdSectionModel = new TTDSectionModel();
    let ttdDayModel = new TTDDayModel();


    //Create Time table Detail
    return super.create(item.ttDetail).then(ttDetail => {

      let sectionData = [];

      //Prepare Data to be inserted in Time table section table
      item.sectionIds.forEach(item => {

        sectionData.push({
          sectionId: item,
          TTDetailId: ttDetail['id']
        })

      });

      //Bulk insert the Time table sections
      return ttdSectionModel.sequelizeModel.bulkCreate(sectionData).then(() => {

        let daysData = [];
        //Prepare data of time table days to be inserted
        item.dayIds.forEach(item => {

          daysData.push({
            dayId: item,
            TTDetailId: ttDetail['id']
          })

        });

        //Bulk insert time table detail days.
        return ttdDayModel.sequelizeModel.bulkCreate(daysData);

      })


    })

  }

  updateByRelatedData(id, item) {

    let ttdSectionModel = new TTDSectionModel();
    let ttdDayModel = new TTDDayModel();

    //Update Time table details.
    return super.update(id, item.ttDetail).then(ttDetail => {

      let sectionData = [];
      let deletedSectionIds = [];
      let daysData = [];
      let deleteddayIds = [];

      //Getting list of all the sections which are mapped on the time table details.
      return ttdSectionModel.findAllByConditions(['sectionId'], { TTDetailId: id }).then(ttdSections => {

        let sec = <any>[];
        ttdSections.forEach(element => {
          sec.push(element['sectionId']);

        });

        //Checking if there is new record to be inserted in time table detail section 
        //If the List getting from above query does not include in the incoming list of section then that id is to be deleted.
        //If incoming list of sections contain id which is not in the already present list that mean new record has to inserted.

        item.sectionIds.forEach(item => {

          if (sec.includes(item)) {

          }
          else {
            sectionData.push({
              sectionId: item,
              TTDetailId: id
            })
          }

        });
        ttdSections.forEach(element => {

          if (item.sectionIds.includes(element['sectionId'])) {

          }
          else {
            deletedSectionIds.push(element['sectionId'])
          }

        });


        //Bulk insert the Time Table section
        return ttdSectionModel.sequelizeModel.bulkCreate(sectionData).then(() => {

          //If there are some sections ids to be deleted then deleted in from  the table.
          return ttdSectionModel.deleteAllByConditions({ TTDetailId: id, sectionId: { [Sequelize.Op.in]: deletedSectionIds } }).then(() => {

            //Checking if there is new record to be inserted in time table detail day 
            //If the List getting from above query does not include in the incoming list of days then that id is to be deleted.
            //If incoming list of days contain id which is not in the already present list that mean new record has to inserted.

            return ttdDayModel.findAllByConditions(['dayId'], { TTDetailId: id }).then(ttdays => {

              let day = <any>[];
              ttdays.forEach(element => {
                day.push(element['dayId']);

              });

              item.dayIds.forEach(item => {

                if (day.includes(item)) {

                }
                else {
                  daysData.push({
                    dayId: item,
                    TTDetailId: id
                  })
                }

              });
              ttdays.forEach(element => {

                if (item.dayIds.includes(element['dayId'])) {

                }
                else {
                  deleteddayIds.push(element['dayId'])
                }

              });

              //Bulk insert the data of time table detail days.
              return ttdDayModel.sequelizeModel.bulkCreate(daysData).then(() => {

                ////Delete days ids
                return ttdDayModel.deleteAllByConditions({ TTDetailId: id, dayId: { [Sequelize.Op.in]: deleteddayIds } })
              })

            })


          })

        })

      })


    })

  }

  findAllTTDetails(id) {

    let ttdSectionModel = new TTDSectionModel();
    let ttdDayModel = new TTDDayModel();
    let courseModel = new CourseModel();
    let slotModel = new SlotModel();
    let finalResult = [];

    //Getting list of All the Time Table detail based on the Time Table.
    return super.findAllByConditions(['id', 'slotOrder', 'slotId', 'courseId'], { timeTableId: id }).then(ttDetails => {

      //Against each time table detail find the sections and days
      return Promise.each(ttDetails, (item) => {

        //Find all the sections mapped on the Time table detail
        return ttdSectionModel.findAllByConditions(['sectionId'], { TTDetailId: item['id'] }).then(ttdSections => {


          let sec = [];

          //Preparing array to pass it to front end
          ttdSections.forEach(element => {

            sec.push(element['sectionId'])

          });

          //Find all the days mapped on the Time Table Detail/
          return ttdDayModel.findAllByConditions(['dayId'], { TTDetailId: item['id'] }).then(ttDays => {

            let day = [];
            //Preparing array to pass it to front End
            ttDays.forEach(element => {

              day.push(element['dayId'])

            });

            //Foreach time table detail get the Slot from time and to time for displaying process
            return slotModel.findByCondition(['fromTime', 'toTime'], { id: item['slotId'] }).then((slots) => {

              //Foreach time table detail get the Course title for displaying process
              return courseModel.findByCondition(['title'], { id: item['courseId'] }).then(courses => {

                //Preparing array of data to be passed on front end.
                finalResult.push({
                  id: item['id'],
                  slotOrder: item['slotOrder'],
                  slotId: item['slotId'],
                  courseId: item['courseId'],
                  sections: sec,
                  days: day,
                  slot: slots,
                  course: courses['title']
                });

              })

            })



          })


        })

      }).then(() => {

        return finalResult;

      })

    })


  }

  findByRelatedData(id) {

    let ttdSectionModel = new TTDSectionModel();
    let ttdDayModel = new TTDDayModel();
    let finalResult = {};

    //Find the Time Table Detail
    return super.findByCondition(['id', 'timeTableId', 'slotOrder', 'slotId', 'courseId'], { id: id }).then(ttDetails => {

      //Find Time table detail section mapped by time table detail id
      return ttdSectionModel.findAllByConditions(['sectionId'], { TTDetailId: id }).then(ttdSections => {

        //Find time table Days mapped by time table detail Id
        return ttdDayModel.findAllByConditions(['dayId'], { TTDetailId: id }).then(ttDays => {


          var sections = [];
          //preparing array of sections getting from above query of sections
          ttdSections.forEach(item => {

            sections.push(item['sectionId'])

          });

          var days = [];

          //preparing array of days getting from above query of Days
          ttDays.forEach(item => {

            days.push(item['dayId'])

          });

          //Preparing object to pass to the front end 
          finalResult['timeTableId'] = ttDetails['timeTableId']
          finalResult['slotOrder'] = ttDetails['slotOrder']
          finalResult['slotId'] = ttDetails['slotId']
          finalResult['courseId'] = ttDetails['courseId']
          finalResult['sections'] = sections
          finalResult['days'] = days

        })


      })


        .then(() => {
          return finalResult;

        })

    })



  }

  deleteByRelatedData(id) {

    let ttdSectionModel = new TTDSectionModel();
    let ttdDayModel = new TTDDayModel();

    //Deleting All the time table section which belongs to Time table detail
    return ttdSectionModel.deleteAllByConditions({ TTDetailId: id }).then(() => {

      //Deleting All the time table Days which belongs to Time table detail
      return ttdDayModel.deleteAllByConditions({ TTDetailId: id }).then(() => {

        //Deleting Time table detail
        return super.delete(id);


      })

    })

  }

  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Campus Time Table Detail of Course';
    retResult['records'] = [];

    return this.findAllByConditions(['id'], condition, [{ model: Course, as: 'course', attributes: ['id', 'title'] }]).then((ttdCRes) => {

      _.each(ttdCRes, item => { retResult['records'].push(item['course']) });

      return retResult;

    });
  }


}
