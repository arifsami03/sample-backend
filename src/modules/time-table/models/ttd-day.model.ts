import { BaseModel } from '../../base';
import { TTDDay } from '..';
import { Sequelize } from 'sequelize-typescript';

export class TTDDayModel extends BaseModel {
  constructor() {
    super(TTDDay);
  }
  deleteAllByConditions(condition: any) {
    return this.deleteByConditions(condition);
  }

  
}
