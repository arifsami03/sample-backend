import * as express from 'express';


import { TTDetailModel } from '..';
import { ErrorHandler } from '../../base/conf/error-handler';

export class TTDetailsController {
  constructor() { }

  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesList(req: express.Request, res: express.Response, next: express.NextFunction) {
    new TTDetailModel()
      .findAll(['id', 'slotOrder', 'slotId', 'timeTableId'])
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Find one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new TTDetailModel()
      .findByRelatedData(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Find one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAllTTDetails(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new TTDetailModel()
      .findAllTTDetails(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Create new record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {

    new TTDetailModel()
      .createByRelatedData(req.body).then(response => {

        if (response['status'] == true) {
          res.json(response['result']);
        }
        else if (response['status'] == false) {
          res.status(403).send(response['result']);
        }

      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Delete record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new TTDetailModel()
      .deleteByRelatedData(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;
    new TTDetailModel()
      .updateByRelatedData(id, item)
      .then(response => {

        if (response['status'] == true) {
          res.json(response['result']);
        }
        else if (response['status'] == false) {
          res.status(403).send(response['result']);
        }

      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
}
