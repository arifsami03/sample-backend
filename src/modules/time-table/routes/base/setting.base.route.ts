import { Router } from 'express';

import {
  
  SlotRoute,
  SectionRoute,
  TimeTableRoute,
  TTDetailRoute,
  MappingTimeTableRoute,
} from '../..';

/**
 *
 *
 * @class TimeTableBaseRoute
 */
export class TimeTableBaseRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class BaseRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.initAll();
  }

  /**
   * init all routes
   */
  public initAll() {
    new SlotRoute(this.router);
    new SectionRoute(this.router);
    new TimeTableRoute(this.router);
    new TTDetailRoute(this.router);
    new MappingTimeTableRoute(this.router);
  }
}
