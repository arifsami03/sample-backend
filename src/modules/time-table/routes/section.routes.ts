import { Router } from 'express';
import { SectionsController } from '..';

export class SectionRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new SectionsController();

    this.router.route('/timeTable/sections/index').get(controller.index);
    this.router.route('/timeTable/sections/find/:id').get(controller.find);
    this.router.route('/timeTable/sections/create').post(controller.create);
    this.router.route('/timeTable/sections/update/:id').put(controller.update);
    this.router.route('/timeTable/sections/delete/:id').delete(controller.delete);
    this.router.route('/timeTable/sections/findAttributesList').get(controller.findAttributesList);
  }
}
