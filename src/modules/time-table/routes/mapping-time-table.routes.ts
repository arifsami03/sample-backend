import { Router } from 'express';
import { MappingTimeTablesController } from '..';

export class MappingTimeTableRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new MappingTimeTablesController();

    this.router.route('/timeTable/mappingTimeTables/index').get(controller.index);
    this.router.route('/timeTable/mappingTimeTables/find/:id').get(controller.find);
    this.router.route('/timeTable/mappingTimeTables/create').post(controller.create);
    this.router.route('/timeTable/mappingTimeTables/update/:id').put(controller.update);
    this.router.route('/timeTable/mappingTimeTables/delete/:id').delete(controller.delete);
    // this.router.route('/timeTable/mappingTimeTables/findAttributesList').get(controller.findAttributesList);
  }
}
