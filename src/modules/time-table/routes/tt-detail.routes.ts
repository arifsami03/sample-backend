import { Router } from 'express';
import { TTDetailsController } from '..';

export class TTDetailRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new TTDetailsController();

    this.router.route('/timeTable/ttDetails/find/:id').get(controller.find);
    this.router.route('/timeTable/ttDetails/findAllTTDetails/:id').get(controller.findAllTTDetails);
    this.router.route('/timeTable/ttDetails/create').post(controller.create);
    this.router.route('/timeTable/ttDetails/update/:id').put(controller.update);
    this.router.route('/timeTable/ttDetails/delete/:id').delete(controller.delete);
    this.router.route('/timeTable/ttDetails/findAttributesList').get(controller.findAttributesList);
  }
}
