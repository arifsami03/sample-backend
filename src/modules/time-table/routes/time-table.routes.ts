import { Router } from 'express';
import { TimeTablesController } from '..';

export class TimeTableRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new TimeTablesController();

    this.router.route('/timeTable/timeTables/index').get(controller.index);
    this.router.route('/timeTable/timeTables/find/:id').get(controller.find);
    this.router.route('/timeTable/timeTables/viewTTDetails/:id').get(controller.viewTTDetails);
    this.router.route('/timeTable/timeTables/create').post(controller.create);
    this.router.route('/timeTable/timeTables/update/:id').put(controller.update);
    this.router.route('/timeTable/timeTables/saveDuplicateTimeTable/:id').put(controller.saveDuplicateTimeTable);
    this.router.route('/timeTable/timeTables/delete/:id').delete(controller.delete);
    this.router.route('/timeTable/timeTables/findAttributesList').get(controller.findAttributesList);
    this.router.route('/timeTable/timeTables/getListOfSlotsInTimeTable/:id').get(controller.getListOfSlotsInTimeTable);
  }
}
