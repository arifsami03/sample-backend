import { Router } from 'express';
import { SlotsController } from '..';

export class SlotRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new SlotsController();

    this.router.route('/timeTable/slots/index').get(controller.index);
    this.router.route('/timeTable/slots/find/:id').get(controller.find);
    this.router.route('/timeTable/slots/create').post(controller.create);
    this.router.route('/timeTable/slots/update/:id').put(controller.update);
    this.router.route('/timeTable/slots/delete/:id').delete(controller.delete);
    this.router.route('/timeTable/slots/findAttributesList').get(controller.findAttributesList);
  }
}
