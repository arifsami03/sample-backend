export * from './routes/base/setting.base.route';

// Slot
export * from './models/schema/slot';
export * from './models/slot.model';
export * from './controllers/slots.controller';
export * from './routes/slot.routes';

// Section
export * from './models/schema/section';
export * from './models/section.model';
export * from './controllers/sections.controller';
export * from './routes/section.routes';

//Time Table
export * from './models/schema/time-table';
export * from './models/schema/tt-detail';
export * from './models/schema/ttd-section';
export * from './models/schema/ttd-day';
export * from './models/time-table.model';
export * from './models/tt-detail.model';
export * from './models/ttd-section.model';
export * from './models/ttd-day.model';
export * from './controllers/time-tables.controller';
export * from './routes/time-table.routes';
export * from './routes/tt-detail.routes';
export * from './controllers/tt-details.controller';

//Time Table Mapping
export * from './models/schema/time-table-validity';
export * from './models/schema/time-table-campus';
export * from './models/mapping-time-table.model';
export * from './routes/mapping-time-table.routes';
export * from './controllers/mapping-time-tables.controllers';

export * from './models/campus-time-table.model';
export * from './models/ctt-detail.model';
export * from './models/cttd-day.model';
export * from './models/cttd-section.model';
export * from './models/schema/campus-time-table';
export * from './models/schema/ctt-detail';
export * from './models/schema/cttd-day';
export * from './models/schema/cttd-section';
