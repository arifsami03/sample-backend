import { Request, Response, Router } from 'express';

import { CRApplicationsController } from '..';

/**
 * / route
 *
 * @class CRApplicationRoute
 */
export class CRApplicationRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class CRApplicationRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class CRApplicationRoute
   * @method create
   *
   */
  public create() {
    let controller = new CRApplicationsController();
    // this.router.route('/api').post(controller.fileUpload);
  }
}
