import { Router } from 'express';

import { CampusController } from '..';


/**
 * / route
 *
 * @class CampusRoute
 */
export class CampusRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class CampusRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class CampusRoute
   * @method create
   *
   */
  public create() {
    let controller = new CampusController();

    this.router.route('/campus/campus/index').get(controller.index);
    this.router.route('/campus/campus/create').post(controller.create);
    this.router.route('/campus/campus/find/:CRApplicationId').get(controller.find);
    this.router.route('/campus/campus/findCampus/:id').get(controller.findCampus);
    this.router.route('/campus/campus/update/:id').put(controller.update);
    this.router.route('/campus/campus/findAttributesList').get(controller.findAttributesList);
    this.router.route('/campus/campus/findAllCampuses').get(controller.findAllCampuses);
    this.router.route('/campus/campus/getCampusByProgramDetailId/:programDetailId').get(controller.getCampusByProgramDetailId);
    this.router.route('/campus/campus/findAttributesData/:id').get(controller.findAttributesData);
    this.router.route('/campus/campus/approveCampus/:CRApplicationId').get(controller.approveCampus);
    this.router.route('/campus/campus/rejectCampus/:CRApplicationId').get(controller.rejectCampus);

  }
}
