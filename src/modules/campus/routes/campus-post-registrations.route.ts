import { Request, Response, Router } from 'express';

import { CampusPostRegistrationsController } from '..';

/**
 * / route
 *
 * @class CRApplicationRoute
 */
export class CampusPostRegistrationsRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class CRApplicationRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class CRApplicationRoute
   * @method create
   *
   */
  public create() {
    let controller = new CampusPostRegistrationsController();

    this.router.route('/campus/postRegistrations/save/:id').get(controller.save);
    this.router.route('/campus/postRegistrations/index').get(controller.index);
    this.router.route('/campus/postRegistrations/findStatus/:id').get(controller.findStatus);
    this.router.route('/campus/postRegistrations/findCRAAdditionalInfo/:id').get(controller.findCRAAdditionalInfo);
    this.router.route('/campus/postRegistrations/getState/:id').get(controller.getState);
    this.router.route('/campus/postRegistrations/status/:id').put(controller.changeStatus);
    this.router.route('/campus/postRegistrations/updateCRApplication/:id').put(controller.updateCRApplication);
    this.router.route('/campus/postRegistrations/forwardForAnalysis').post(controller.forwardForAnalysis);
    this.router.route('/campus/postRegistrations/findUserId/:id').get(controller.findUserId);
    this.router.route('/campus/postRegistrations/find/:id').get(controller.find);
    this.router.route('/campus/postRegistrations/getSubmitAppData/:id').get(controller.getSubmitAppData);
    this.router.route('/campus/postRegistrations/findEvaluationSheet/:id').get(controller.findEvaluationSheet);
    
    this.router.route('/campus/postRegistrations/updateEvaluationSheet/:id').put(controller.updateEvaluationSheet);
    this.router.route('/campus/postRegistrations/updateCommentsOfEvaluationSheet/:id').put(controller.updateCommentsOfEvaluationSheet);
    this.router.route('/campus/postRegistrations/updateESStatus/:id').put(controller.updateESStatus);
    this.router.route('/campus/postRegistrations/findAllEvaluationSheets/:id').get(controller.findAllEvaluationSheets);
    this.router.route('/campus/postRegistrations/findAllESSections/:id').get(controller.findAllESSections);
    this.router.route('/campus/postRegistrations/findAllAssignedWFStates').get(controller.findAllAssignedWFStates);
    this.router.route('/campus/postRegistrations/findRemittanceStatus/:crApplicantId').get(controller.findRemittanceStatus);

  }
}
