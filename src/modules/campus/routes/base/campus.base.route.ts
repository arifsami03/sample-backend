import { NextFunction, Request, Response, Router } from 'express';
import {
  CRApplicationRoute, CRApplicantRoute, CampusInfoRoute, RemittancesRoute, CPDAccountRoute, CampusPreRegistrationsRoute,
  CampusRoute, CampusPostRegistrationsRoute, CRCampusMOURoute, CRMOUVerificationRoute, ApplicationRoute, CampusBuildingsRoute, CBFloorsRoute, CBFRoomsRoute
} from '../..';

/**
 *
 *
 * @class CampusBaseRoute
 */
export class CampusBaseRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class BaseRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.initAll();
  }

  /**
   * init all routes
   */
  public initAll() {
    new ApplicationRoute(this.router);
    new CRApplicationRoute(this.router);
    new CRApplicantRoute(this.router);
    new CampusInfoRoute(this.router);
    new RemittancesRoute(this.router);
    new CampusPreRegistrationsRoute(this.router);
    new CampusPostRegistrationsRoute(this.router);
    new CRCampusMOURoute(this.router);
    new CRMOUVerificationRoute(this.router);
    new CampusRoute(this.router);
    new CPDAccountRoute(this.router);
    new CampusBuildingsRoute(this.router);
    new CBFloorsRoute(this.router);
    new CBFRoomsRoute(this.router);

  }
}
