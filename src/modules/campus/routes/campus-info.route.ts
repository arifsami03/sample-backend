import { Request, Response, Router } from 'express';
import { CampusInfoController } from '..';

/**
 * / route
 *
 * @class CampusInfoRoute
 */
export class CampusInfoRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class CampusInfoRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class CampusInfoRoute
   * @method create
   *
   */
  public create() {
    let controller = new CampusInfoController();

    this.router.route('/campus/campusInfo/findCampusInfo/:userId').get(controller.findCampusInfo);

    this.router.route('/campus/campusInfo/findCampusEducationLevel/:id').get(controller.findCampusEducationLevel);

    this.router.route('/campus/campusInfo/updateCampusInfo/:id').put(controller.updateCampusInfo);
    this.router.route('/campus/campusInfo/createCampusInfo').post(controller.createCampusInfo);

  }
}
