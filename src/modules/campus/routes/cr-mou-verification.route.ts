import { Request, Response, Router } from 'express';

import { CRMOUVerificationsController } from '..';


/**
 * / route
 *
 * @class CRCampusMOURoute
 */
export class CRMOUVerificationRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class CRCampusMOURoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class CRApplicationRoute
   * @method create
   *
   */
  public create() {
    let controller = new CRMOUVerificationsController();

    this.router.route('/campus/campusMOUVerifications/create').post(controller.create);
    this.router.route('/campus/campusMOUVerifications/find/:CRApplicationId').get(controller.find);
    this.router.route('/campus/campusMOUVerifications/compareMOUAmounts/:applicationId/:amount').get(controller.compareMOUAmounts);
    this.router.route('/campus/campusMOUVerifications/update/:id').put(controller.update);

  }
}
