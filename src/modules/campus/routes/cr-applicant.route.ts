import { Request, Response, Router } from 'express';

import { CRApplicantsController } from '..';

/**
 * / route
 *
 * @class CRApplicationRoute
 */
export class CRApplicantRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class CRApplicantRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class CRApplicantRoute
   * @method create
   *
   */
  public create() {
    let controller = new CRApplicantsController();

    this.router.route('/applicant/registration/findPersonalInfo/:userId').get(controller.findPersonalInfo);
    this.router.route('/applicant/registration/getApplicantId/:userId').get(controller.getApplicantId);
    this.router.route('/applicant/registration/getApplicantIdByApplicationId/:applicationId').get(controller.getApplicantIdByApplicationId);
    this.router.route('/applicant/registration/update/:userId').put(controller.update);
    this.router.route('/applicant/registration/create').post(controller.create);


  }
}
