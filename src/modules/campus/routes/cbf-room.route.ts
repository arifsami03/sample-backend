import { Request, Response, Router } from 'express';
import { CBFRoomsController } from '..';

/**
 * / route
 *
 * @class CampusInfoRoute
 */
export class CBFRoomsRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class CampusBuildingsRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class CampusBuildingsRoute
   * @method create
   *
   */
  public create() {
    let controller = new CBFRoomsController();

    this.router.route('/campus/cbfRooms/index').get(controller.index);
    this.router.route('/campus/cbfRooms/create').post(controller.create);
    this.router.route('/campus/cbfRooms/find/:id').get(controller.find);
    this.router.route('/campus/cbfRooms/update/:id').put(controller.update);
    this.router.route('/campus/cbfRooms/delete/:id').delete(controller.delete);

  }
}
