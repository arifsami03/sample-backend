import { Request, Response, Router } from 'express';

import { CRMOUController } from '..';


/**
 * / route
 *
 * @class CRCampusMOURoute
 */
export class CRCampusMOURoute {
  router: Router;

  /**
   * Constructor
   *
   * @class CRCampusMOURoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class CRApplicationRoute
   * @method create
   *
   */
  public create() {

    let controller = new CRMOUController();

    this.router.route('/campus/campusMOU/create').post(controller.create);
    this.router.route('/campus/campusMOU/find/:CRApplicationId').get(controller.find);
    this.router.route('/campus/campusMOU/update/:id').put(controller.update);

  }
}
