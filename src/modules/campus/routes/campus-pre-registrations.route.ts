import { Request, Response, Router } from 'express';

import { CampusPreRegistrationsController } from '..';

/**
 * / route
 *
 * @class CRApplicationRoute
 */
export class CampusPreRegistrationsRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class CRApplicationRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class CRApplicationRoute
   * @method create
   *
   */
  public create() {
    let controller = new CampusPreRegistrationsController();

    this.router.route('/campus/preRegistrations/create').post(controller.create);
    this.router.route('/campus/preRegistrations/index').get(controller.index);
    this.router.route('/campus/preRegistrations/find/:id').get(controller.find);
    this.router.route('/campus/preRegistrations/status/:id').put(controller.changeStatus);
    this.router.route('/campus/preRegistrations/findUserId/:id').get(controller.findUserId);
    this.router.route('/campus/preRegistrations/activeUser/:id').put(controller.activeUser);
    this.router.route('/campus/preRegistrations/validateUniqueEmail').post(controller.validateUniqueEmail);
    this.router.route('/campus/preRegistrations/findAllAssignedWFStates').get(controller.findAllAssignedWFStates);
  }
}
