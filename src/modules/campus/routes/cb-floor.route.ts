import { Request, Response, Router } from 'express';
import { CBFloorsController } from '..';

/**
 * / route
 *
 * @class CampusInfoRoute
 */
export class CBFloorsRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class CampusBuildingsRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class CampusBuildingsRoute
   * @method create
   *
   */
  public create() {
    let controller = new CBFloorsController();

    this.router.route('/campus/cbFloors/index').get(controller.index);
    this.router.route('/campus/cbFloors/create').post(controller.create);
    this.router.route('/campus/cbFloors/find/:id').get(controller.find);
    this.router.route('/campus/cbFloors/update/:id').put(controller.update);
    this.router.route('/campus/cbFloors/delete/:id').delete(controller.delete);
    this.router.route('/campus/cbFloors/findAttributesListWithCampusAndBuilding').get(controller.findAttributesListWithCampusAndBuilding);

  }
}
