import { Request, Response, Router } from 'express';
import { ApplicationsController } from '..';


/**
 * / route
 *
 * @class ApplicationRoute
 */
export class ApplicationRoute {

  router: Router;

  /**
   * Constructor
   *
   * @class ApplicationRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class ApplicationRoute
   * @method create
   *
   */
  public create() {

    let controller = new ApplicationsController();

    this.router.route('/campus/applications/index').get(controller.index);

    // this.router.route('/campus/applications/find/:id').put(controller.updateCampusInfo);

  }
}
