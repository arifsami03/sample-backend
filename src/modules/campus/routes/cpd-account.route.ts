import { Request, Response, Router } from 'express';

import { CPDAccountsController } from '..';


/**
 * / route
 *
 * @class CPDAccountRoute
 */
export class CPDAccountRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class CPDAccountRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class CPDAccountRoute
   * @method create
   *
   */
  public create() {
    let controller = new CPDAccountsController();

    this.router.route('/campus/cpdAccounts/create').post(controller.create);
    this.router.route('/campus/cpdAccounts/find/:id').get(controller.find);
    this.router.route('/campus/cpdAccounts/getProgramDetails/:id').get(controller.getProgramDetails);
    this.router.route('/campus/cpdAccounts/getProgramDetailsAccountInfo/:id').get(controller.getProgramDetailsAccountInfo);
    this.router.route('/campus/cpdAccounts/update/:id').put(controller.update);
  }
}
