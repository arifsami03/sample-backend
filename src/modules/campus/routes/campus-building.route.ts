import { Request, Response, Router } from 'express';
import { CampusBuildingsController } from '..';

/**
 * / route
 *
 * @class CampusInfoRoute
 */
export class CampusBuildingsRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class CampusBuildingsRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class CampusBuildingsRoute
   * @method create
   *
   */
  public create() {
    let controller = new CampusBuildingsController();

    this.router.route('/campus/campusBuildings/index').get(controller.index);
    this.router.route('/campus/campusBuildings/create').post(controller.create);
    this.router.route('/campus/campusBuildings/find/:id').get(controller.find);
    this.router.route('/campus/campusBuildings/update/:id').put(controller.update);
    this.router.route('/campus/campusBuildings/delete/:id').delete(controller.delete);
    this.router.route('/campus/campusBuildings/findAttributesListWithCampusName').get(controller.findAttributesListWithCampusName);

  }
}
