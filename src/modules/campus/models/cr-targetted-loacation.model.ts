import { BaseModel } from '../../base';
import { CRTargettedLocations } from '..';
import * as _ from 'lodash';


export class CRTargettedLocationsModel extends BaseModel {
  constructor() {
    super(CRTargettedLocations);
  }

  /********************************************************************************************************************************
  * Portion to display related data before deletion
  ********************************************************************************************************************************/

  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Targetted Location';
    retResult['records'] = [];

    return this.findAllByConditions(['id', ['address', 'title']], condition).then((crTLRes) => {
      _.each(crTLRes, item => {
        retResult['records'].push(item['dataValues']);
      });

      return retResult;


    });
  }

  /********************************************************************************************************************************
   * End of portion to display related data before deletion
   ********************************************************************************************************************************/

}
