import { BaseModel } from '../../base';
import { CRAvailableBuilding } from '..';
import * as _ from 'lodash';

export class CRAvailableBuildingModel extends BaseModel {
  constructor() {
    super(CRAvailableBuilding);
  }

  /********************************************************************************************************************************
  * Portion to display related data before deletion
  ********************************************************************************************************************************/

  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Available Building';
    retResult['records'] = [];

    return this.findAllByConditions(['id', ['buildingType', 'title']], condition).then((crABRes) => {
      _.each(crABRes, item => {
        retResult['records'].push(item['dataValues']);
      });

      return retResult;


    });
  }

  /********************************************************************************************************************************
   * End of portion to display related data before deletion
   ********************************************************************************************************************************/

}
