import { BaseModel } from '../../base';
import { CRMOU } from '..';
import * as _ from 'lodash';


export class CRMOUModel extends BaseModel {
  constructor() {
    super(CRMOU);
  }

  /********************************************************************************************************************************
* Portion to display related data before deletion
********************************************************************************************************************************/

  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'MOU';
    retResult['records'] = [];

    return this.findAllByConditions(['id', ['campusTitle', 'title']], condition).then((crMOURes) => {

      _.each(crMOURes, item => { retResult['records'].push(item['dataValues']) });

      return retResult;

    });
  }

  /********************************************************************************************************************************
   * End of portion to display related data before deletion
   ********************************************************************************************************************************/

}
