import { BaseModel } from '../../base';
import { CBFloor, CampusBuilding, Campus, CBFRoomModel } from '..';
import { ErrorHandler } from '../../base/conf/error-handler';
import { Sequelize } from 'sequelize-typescript';

export class CBFloorModel extends BaseModel {

  constructor() {
    super(CBFloor);
  }

  findAllWithBuildingAndCampus(attributes) {
    let includeObj = [
      {
        model: CampusBuilding, as: 'campusBuilding', attributes: ['id', 'name'], where: BaseModel.cb(), required: false,
        include: [{ model: Campus, as: 'campus', attributes: ['id', 'campusName'], where: BaseModel.cb(), required: false }]
      }
    ];

    return this.findAll(attributes, null, includeObj);
  }

  /**
   * First check if code for floor in selected building is unique then create floor or return error
   * @param item 
   */
  create(item) {
    return super.findByCondition(['id'], { CBId: item.CBId, code: item.code }).then(result => {
      if (result) {
        return ErrorHandler.duplicateEntry;
      } else {
        return super.create(item);
      }
    });
  }

  /**
   * First check if code for building in selected campus is unique then create building or return error
   * @param item 
   */
  update(id, item) {
    return super.findByCondition(['id'], { id: { [Sequelize.Op.not]: id }, CBId: item.CBId, code: item.code }).then(result => {
      if (result) {
        return ErrorHandler.duplicateEntry;
      } else {
        return super.update(id, item);
      }
    });
  }

  /**
   * First deletes Floor childs i.e rooms then delete Floor itself
   * @param id 
   */
  deleteWithChilds(id) {
    return new CBFRoomModel().deleteByConditions({ CBFloorId: id }).then(() => {
      return super.delete(id);
    })
  }

}
