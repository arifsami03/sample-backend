import { BaseModel } from '../../base';
import { CPDAccount } from '..';
import { CampusProgramDetailModel, CampusProgramModel } from '..';
import { Promise } from 'bluebird';
import { Sequelize } from 'sequelize-typescript';
import { ProgramDetailModel, Program } from '../../academic';
import { EducationLevel } from '../../institute';


export class CPDAccountModel extends BaseModel {
  constructor() {
    super(CPDAccount);
  }

  getProgramDetails(id) {
    let returnResult = {};
    return this.findAllByConditions(['programDetailId'], { campusId: id }).then(programDetails => {

      let programsDetailsNotIN = [];
      programDetails.forEach(element => {
        programsDetailsNotIN.push(element.programDetailId)
      });

      let campProg = new CampusProgramModel();

      let cpProDetResult = [];

      return campProg.findAllByConditions(['id', 'programId'], { campusId: id }).then(camProgRes => {

        return Promise.each(camProgRes, (camProgItem) => {

          return new CampusProgramDetailModel().findAllByConditions(['id', 'programDetailId'], { campusProgramId: camProgItem['id'], programDetailId: { [Sequelize.Op.notIn]: programsDetailsNotIN } }).then(camProgDetRes => {

            cpProDetResult.push({
              programId: camProgItem['programId'],
              programDetails: camProgDetRes
            });
          })

        })

      }).then(() => {
        returnResult['cammpusProgramDetails'] = cpProDetResult;
        return returnResult;
      })

    })
  }
  getProgramDetailsAccountInfo(id) {
    let progDetail = new ProgramDetailModel();
    let returnResult = {};
    let cpProDetResult = [];
    return this.findAllByConditions(['id', 'branch', 'account', 'programDetailId'], { campusId: id }).then(programDetails => {

      let programsDetailsIN = [];
      programDetails.forEach(element => {
        programsDetailsIN.push(element.programDetailId)
      });

      return Promise.each(programDetails, (programDetailsItem) => {

        return progDetail.findByCondition(['name'], { id: programDetailsItem['programDetailId'] }, [{ model: Program, as: 'program', attributes: ['id', 'name'], include: [{ model: EducationLevel, as: 'educationLevel', attributes: ['id', 'education'] }] }]).then(programName => {
          cpProDetResult.push({
            id: programDetailsItem['id'],
            branch: programDetailsItem['branch'],
            programDetail: programName['program']['educationLevel']['education'] + ' - ' + programName['program']['name'] + ' - ' + programName['name'],
            account: programDetailsItem['account'],
          })
        })

      });
    }
    ).then(() => {
      returnResult['programDetailsWithAccountInfo'] = cpProDetResult;
      return returnResult;
    })


  }

  create(postData) {
    var accountInfoList = [];
    postData.programDetailIds.forEach(element => {
      accountInfoList.push({
        campusId: postData.campusId,
        bankId: postData.accountInfo.bankId,
        account: postData.accountInfo.account,
        challanType: postData.accountInfo.challanType,
        branch: postData.accountInfo.branch,
        programDetailId: element
      })
    });
    return this.sequelizeModel.bulkCreate(accountInfoList);
  }


}
