import { Promise } from 'bluebird';
import { BaseModel } from '../../base';
import { CRRemittance, CRApplication } from '..';
import * as _ from 'lodash';
import { CRApplicationModel } from './cr-application.model';

export class CRRemittanceModel extends BaseModel {
  constructor() {
    super(CRRemittance);
  }

  /**
   * Create
   * @param _data
   */
  save(_data: any) {
    return new Promise((resolve, reject) => {
      const applicantId = _data.applicantId;
      let remittance = _data.remittance;
      new CRApplicationModel().findByCondition(['id'], { CRApplicantId: applicantId }).then(_campus => {
        super.findByCondition(['id'], { id: _campus.id }).then(
          res => {
            if (res) {
              return resolve(super.update(res.id, remittance));
            } else {
              remittance['id'] = _campus.id;
              return resolve(this.sequelizeModel.create(remittance));
            }
          },
          error => {
            return reject(error);
          }
        );
      });
    });
  }

  view(applicantId: number) {
    return new Promise((resolve, reject) => {
      // const userId = _userId;
      this.openConnection();
      new CRApplicationModel().findByCondition(['id'], { CRApplicantId: applicantId }).then(_campus => {
        super.findByCondition(null, { id: _campus.id }).then(
          res => {
            return resolve(res);
          },
          error => {
            return reject(error);
          }
        );
      });
    });
  }

  /********************************************************************************************************************************
  * Portion to display related data before deletion
  ********************************************************************************************************************************/


  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Remittance';
    retResult['records'] = [];

    return this.findAllByConditions(['id', ['purpose', 'title']], condition).then((crRemRes) => {
      _.each(crRemRes, item => {
        retResult['records'].push(item['dataValues']);
      });

      return retResult;


    });
  }

  /********************************************************************************************************************************
   * End of portion to display related data before deletion
   ********************************************************************************************************************************/
}
