import { BaseModel, CONFIGURATIONS } from '../../base';
import { CampusProgramModel, CampusProgramDetailModel, Campus } from '..';
import { CampusAcademicCalendarModel } from '../../institute';
import { AcademicCalendarProgramDetailModel, ProgramDetail, AcademicCalendar } from '../../academic';
import { Promise } from 'bluebird';
import * as dateFormat from 'dateformat';
import { Sequelize } from 'sequelize-typescript';
import { CampusTimeTableModel, TimeTableModel } from '../../time-table';
import { CampusBuildingModel } from './campus-building.model';
import { CPDAccountModel } from './cpd-account.model';
import { FSCampusModel } from '../../fee-management';
import { UserModel, RoleAssignmentModel } from '../../security';


export class CampusModel extends BaseModel {

  constructor() {
    super(Campus);
  }

  private cttModel = new CampusTimeTableModel();
  private ttModel = new TimeTableModel();

  findWithRelatedData(CRApplicationId) {

    let returnResult = {};

    // return super.find(id, ['id', 'campusId', 'codeNumber', 'campusName', 'email', 'mobileNumber', 'address', 'landLineNumber', 'academicYearId']).then(result => {
    let attributes = ['id', 'campusId', 'codeNumber', 'campusName', 'email', 'mobileNumber', 'address', 'landLineNumber', 'academicYear', 'academicMonth'];

    return super.findByCondition(attributes, { CRApplicationId: CRApplicationId }).then(result => {

      returnResult['campus'] = result;

      let campProg = new CampusProgramModel();

      let cpProDetResult = [];


      if (result) {

        return campProg.findAllByConditions(['id', 'programId'], { campusId: result['id'] }).then(camProgRes => {
          return new UserModel().findAllByConditions(['id', 'username', 'CDUId'], { campusId: result['id'] }).then(userRes => {
            returnResult['user'] = userRes;
            return Promise.each(camProgRes, (camProgItem) => {

              return new CampusProgramDetailModel().findAllByConditions(['id', 'programDetailId'], { campusProgramId: camProgItem['id'] }).then(camProgDetRes => {

                cpProDetResult.push({
                  programId: camProgItem['programId'],
                  programDetails: camProgDetRes
                });

              })

            })
          })


        }).then(() => {
          returnResult['cammpusProgramDetails'] = cpProDetResult;
          return returnResult;
        })

      } else {

        returnResult['cammpusProgramDetails'] = cpProDetResult;
        return returnResult;

      }

    })
  }

  findCampusWithRelatedData(id) {

    let returnResult = {};

    // return super.find(id, ['id', 'campusId', 'codeNumber', 'campusName', 'email', 'mobileNumber', 'address', 'landLineNumber', 'academicYearId']).then(result => {
    let attributes = ['id', 'CRApplicationId', 'campusId', 'codeNumber', 'campusName', 'email', 'mobileNumber', 'address', 'landLineNumber', 'academicYear', 'academicMonth'];

    return super.findByCondition(attributes, { id: id }).then(result => {

      returnResult['campus'] = result;

      let campProg = new CampusProgramModel();

      let cpProDetResult = [];

      if (result) {

        return campProg.findAllByConditions(['id', 'programId'], { campusId: result['id'] }).then(camProgRes => {

          return Promise.each(camProgRes, (camProgItem) => {

            return new CampusProgramDetailModel().findAllByConditions(['id', 'programDetailId'], { campusProgramId: camProgItem['id'] }).then(camProgDetRes => {

              cpProDetResult.push({
                programId: camProgItem['programId'],
                programDetails: camProgDetRes
              });
            })

          })

        }).then(() => {
          returnResult['cammpusProgramDetails'] = cpProDetResult;
          return returnResult;
        })

      } else {

        returnResult['cammpusProgramDetails'] = cpProDetResult;
        return returnResult;

      }

    })
  }

  createWithRelatedData(item) {
    // Add status 'draft' to newly adding campus
    item.campus['status'] = CONFIGURATIONS.CMAPUS_STATUS.DRAFT;
    let finalResult = {
      campusId: false,
      codeNumber: false
    };

    return this.findByCondition(['id'], { campusId: item.campus['campusId'] }).then(campusIdRed => {

      if (campusIdRed) {

        finalResult['campusId'] = true;
        return this.findByCondition(['id'], { codeNumber: item.campus['codeNumber'] }).then(codeNumberRed => {

          if (codeNumberRed) {
            finalResult['codeNumber'] = true;
          }
          else {
            finalResult['codeNumber'] = false;
          }

        })

      }
      else {
        finalResult['campusId'] = false;
        return this.findByCondition(['id'], { codeNumber: item.campus['codeNumber'] }).then(codeNumberRed => {

          if (codeNumberRed) {
            finalResult['codeNumber'] = true;
          }
          else {
            return super.create(item.campus).then(res => {

              item['campusId'] = res.id;



              return this.saveCRProgram(res.id, item.campusProgram);

            }).then(() => {
              return Promise.each(item['users'], user => {
                user['campusId'] = item['campusId'];
                return this.addUsers(user)
              })
            })
          }

        });

      }

    }).then(() => {
      return finalResult;
    })


  }

  addUsers(user) {

    let userModel = new UserModel();
    let roleAssignmentModel = new RoleAssignmentModel();


    return userModel.create(user).then(userRes => {

      let postData = {
        parent: 'user',
        parentId: userRes['id'],
        roleId: user['roleId']
      }
      return roleAssignmentModel.create(postData);

    })

  }
  updateUser(user) {

    let userModel = new UserModel();
    return userModel.update(user['id'], user)

  }
  deleteUser(campusId, deletedIds) {

    let userModel = new UserModel();
    let roleAssignmentModel = new RoleAssignmentModel();
    return Promise.each(deletedIds, CDUId => {

      return userModel.findByCondition(['id'], { CDUId: CDUId, campusId: campusId }).then(userIdRes => {

        return roleAssignmentModel.deleteByConditions({ parentId: userIdRes['id'] }).then(() => {
          return userModel.delete(userIdRes['id']);
        })


      })

    })

  }


  updateWithRelatedData(id, item) {

    item.campus['status'] = CONFIGURATIONS.CMAPUS_STATUS.DRAFT;
    let finalResult = {
      campusId: false,
      codeNumber: false
    };

    return this.findByCondition(['id'], { campusId: item.campus['campusId'], id: { [Sequelize.Op.ne]: id } }).then(campusIdRed => {

      if (campusIdRed) {

        finalResult['campusId'] = true;
        return this.findByCondition(['id'], { codeNumber: item.campus['codeNumber'], id: { [Sequelize.Op.ne]: id } }).then(codeNumberRed => {

          if (codeNumberRed) {
            finalResult['codeNumber'] = true;
          }
          else {
            finalResult['codeNumber'] = false;
          }

        })

      }
      else {
        finalResult['campusId'] = false;
        return this.findByCondition(['id'], { codeNumber: item.campus['codeNumber'], id: { [Sequelize.Op.ne]: id } }).then(codeNumberRed => {

          if (codeNumberRed) {
            finalResult['codeNumber'] = true;
          }
          else {

            return super.update(id, item.campus).then(() => {
              item['campusId'] = id;
              return this.deleteCRProgram(item.campus.id).then(() => {

                return this.saveCRProgram(item.campus.id, item.campusProgram);

              })

            }).then(() => {
              return Promise.each(item['users'], user => {
                user['campusId'] = item['campusId'];
                if (user['roleId'] != '') {
                  return this.addUsers(user)
                }
                else {
                  return this.updateUser(user)
                }

              })
            }).then(() => {
              return this.deleteUser(item['campusId'], item['deletedUserIds']);
            });
          }

        });

      }

    }).then(() => {
      return finalResult;
    })

  }

  deleteCRProgram(id) {

    let cPro = new CampusProgramModel();
    let cProDet = new CampusProgramDetailModel();

    return cPro.findAllByConditions(['id'], { campusId: id }).then(res => {

      var deleted = [];
      if (res.length > 0) {

        for (var i = 0; i < res.length; i++) {

          deleted.push(res[i].id)
        }

      }

      return cProDet.deleteByConditions({ campusProgramId: deleted }).then(() => {
        return cPro.deleteByConditions({ campusId: id });
      })
    })
  }

  public saveCRProgram(id, item) {

    // return new Promise((resolve, reject) => {
    // ['dataValues']

    let cPro = new CampusProgramModel();
    let cProProDet = new CampusProgramDetailModel();

    if (item.length != 0) {
      //var campusPrograms = {};

      return Promise.each(item, (campusProgramItem) => {

        return cPro.create({
          campusId: id,
          programId: campusProgramItem['programId']
        }).then(res => {
          return Promise.each(campusProgramItem['programDetails'], programDetails => {
            return cProProDet.create({
              campusProgramId: res['id'],
              programDetailId: programDetails
            })

          })
        });

      })
    }
    else {
      return true;
    }
  }


  getCampusByProgramDetailId(progarmDetailId) {

    let campusModel = new CampusModel();
    let campusProgModel = new CampusProgramModel();
    let campusProgDetailModel = new CampusProgramDetailModel();
    let campusAcademicCalendarModel = new CampusAcademicCalendarModel();
    let academicCalendarProgramDetailModel = new AcademicCalendarProgramDetailModel();
    let finalResult = [];

    return campusProgDetailModel.findAllByConditions(['campusProgramId'], { programDetailId: progarmDetailId }).then(programId => {
      if (programId.length != 0) {

        let programIdRes = [];
        programId.forEach(element => {
          programIdRes.push(element.campusProgramId);
        });

        return campusProgModel.findAllByConditions(['campusId'], { id: { [Sequelize.Op.in]: programIdRes } }).then(campusPrograms => {
          let campusProgramIds = [];
          campusPrograms.forEach(element => {
            campusProgramIds.push(element.campusId);
          });
          return campusModel.findAllByConditions(['id', 'campusName'], { id: { [Sequelize.Op.in]: campusProgramIds }, status: CONFIGURATIONS.CMAPUS_STATUS.DONE }).then(campuses => {
            return Promise.each(campuses, item => {

              return campusAcademicCalendarModel.findByCondition(['id', 'academicCalendarProgramDetailId'], { campusId: item['id'] }).then(academicCalendar => {

                if (academicCalendar) {
                  return academicCalendarProgramDetailModel.findByCondition(['academicCalendarId', 'programDetailId', 'startDate', 'endDate'], { id: academicCalendar['academicCalendarProgramDetailId'] }, [
                    {
                      model: ProgramDetail,
                      as: 'programDetail',
                      attributes: ['id', 'name']
                    },
                    {
                      model: AcademicCalendar,
                      as: 'academicCalendar',
                      attributes: ['id', 'title']
                    }
                  ]).then(academicCalendar => {

                    finalResult.push({
                      id: item['id'],
                      title: item['campusName'],
                      academicCalendar: academicCalendar['academicCalendar']['title'] + ' : ' + dateFormat(academicCalendar['startDate'], "mmmm dS, yyyy") + ' - ' + dateFormat(academicCalendar['endDate'], "mmmm dS, yyyy"),
                      startDate: academicCalendar['startDate'],
                      endDate: academicCalendar['endDate'],
                    })

                  })


                }
              }


              )

            }).then(() => {
              return finalResult;
            })

          })


        })
      } else {
        return finalResult;
      }
    })

  }

  approveCampus(id: number) {
    return super.findByCondition(['id'], { CRApplicationId: id }).then(campus => {
      let item = { status: CONFIGURATIONS.CMAPUS_STATUS.DONE };
      return super.update(campus.id, item);
    });
  }

  rejectCampus(id: number) {
    return super.findByCondition(['id'], { CRApplicationId: id }).then(campus => {
      let item = { status: CONFIGURATIONS.CMAPUS_STATUS.CLOSE };
      return super.update(campus.id, item);
    });
  }

  deleteWithAssociatedRecords(id) {
    return new CampusTimeTableModel().deleteByConditionsWithAssociatedRecords({ campusId: id }).then(() => {

      return new CampusProgramModel().deleteByConditionsWithAssociatedRecords({ campusId: id }).then(() => {

        return new CampusBuildingModel().deleteByConditions({ campusId: id }).then(() => {

          return new CPDAccountModel().deleteByConditions({ campusId: id }).then(() => {

            return new FSCampusModel().deleteByConditions({ campusId: id }).then(() => {

              return new CampusAcademicCalendarModel().deleteByConditions({ campusId: id }).then(() => {

                return this.delete(id);

              });

            });

          });

        });

      })

    })

  }
  deleteByConditionsWithAssociatedRecords(condition) {

    return this.findAllByConditions(['id'], condition).then(campuses => {

      let cttModel = new CampusTimeTableModel();
      let cpModel = new CampusProgramModel();
      let cbModel = new CampusBuildingModel();
      let cpdAccountModel = new CPDAccountModel();
      let fscModel = new FSCampusModel();
      let cacModel = new CampusAcademicCalendarModel();

      return Promise.each(campuses, campus => {

        return cttModel.deleteByConditionsWithAssociatedRecords({ campusId: campus['id'] }).then(() => {

          return cpModel.deleteByConditionsWithAssociatedRecords({ campusId: campus['id'] }).then(() => {

            return cbModel.deleteByConditions({ campusId: campus['id'] }).then(() => {

              return cpdAccountModel.deleteByConditions({ campusId: campus['id'] }).then(() => {

                return fscModel.deleteByConditions({ campusId: campus['id'] }).then(() => {

                  return cacModel.deleteByConditions({ campusId: campus['id'] }).then(() => {

                    return this.delete(campus['id']);

                  });

                });

              });

            });

          })

        })

      })

    })

  }

  /********************************************************************************************************************************
  * Portion to display related data before deletion
  ********************************************************************************************************************************/


  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Campus';
    retResult['records'] = [];

    return this.findAllByConditions(['id', ['campusName', 'title']], condition).then((campRes) => {

      return Promise.each(campRes, item => {

        let tempItem = item['dataValues'];
        tempItem['children'] = [];
        return this.findChildrenForDel(item['dataValues']).then(res => {

          tempItem['children'] = res;

          retResult['records'].push(tempItem);

        });

      })
    }).then(() => {

      return retResult;

    });

    //   _.each(campRes, item => {
    //     retResult['records'].push(item['dataValues']);
    //   });

    //   return retResult;

    // });
  }

  private findChildrenForDel(item) {

    let itRes = [];

    return this.cttModel.findRelatedDataWithCondition({ campusId: item['id'] }).then(cttRes => {

      itRes.push(cttRes);

    }).then(() => {

      return itRes;

    });
  }

  /********************************************************************************************************************************
   * End of portion to display related data before deletion
   ********************************************************************************************************************************/
}
