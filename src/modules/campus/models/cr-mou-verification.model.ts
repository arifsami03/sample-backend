import { BaseModel } from '../../base';
import { CRMOUVerification, CRMOUModel } from '..';
import { ManagementFeeModel } from '../../fee-management';
import * as _ from 'lodash';

export class CRMOUVerificationModel extends BaseModel {
  constructor() {
    super(CRMOUVerification);
  }

  compareMOUAmounts(applicationId, amount) {

    return new CRMOUModel().findByCondition(['licenseFeeId'], { CRApplicationId: applicationId }).then(licenseAmountId => {

      // if (licenseAmountId){
      return new ManagementFeeModel().findByCondition(['amount'], { id: licenseAmountId['licenseFeeId'] }).then(licenseAmount => {

        if (licenseAmount['amount'] == amount) {

          return true;

        }
        else {

          return false;

        }

      })
      // }else {
      //   return true;
      // }



    })

  }

  /********************************************************************************************************************************
  * Portion to display related data before deletion
  ********************************************************************************************************************************/


  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'MOU Verification';
    retResult['records'] = [];

    return this.findAllByConditions(['id', ['purpose', 'title']], condition).then((crMVRes) => {

      _.each(crMVRes, item => {
        retResult['records'].push(item['dataValues']);
      });

      return retResult;

    });
  }

  /********************************************************************************************************************************
   * End of portion to display related data before deletion
   ********************************************************************************************************************************/





}
