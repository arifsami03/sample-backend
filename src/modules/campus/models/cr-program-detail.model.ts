import { BaseModel } from '../../base';
import { CRProgramDetail } from '..';
import * as _ from 'lodash';

export class CRProgramDetailModel extends BaseModel {
  constructor() {
    super(CRProgramDetail);
  }

  /********************************************************************************************************************************
    * Portion to display related data before deletion
    ********************************************************************************************************************************/

  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Program Detail';
    retResult['records'] = [];

    return this.findAllByConditions(['id'], condition).then((crpdRes) => {
      _.each(crpdRes, item => {
        retResult['records'].push(item['dataValues']);
      });

      return retResult;


    });
  }


  /********************************************************************************************************************************
   * End of portion to display related data before deletion
   ********************************************************************************************************************************/


}
