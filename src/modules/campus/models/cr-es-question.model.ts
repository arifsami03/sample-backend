import { BaseModel } from '../../base';
import { CRESQuestion } from '..';
import * as _ from 'lodash';

export class CRESQuestionModel extends BaseModel {
  constructor() {
    super(CRESQuestion);
  }

  /********************************************************************************************************************************
* Portion to display related data before deletion
********************************************************************************************************************************/

  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Question';
    retResult['records'] = [];

    return this.findAllByConditions(['id', ['question', 'title']], condition).then((crAddrRes) => {

      _.each(crAddrRes, item => { retResult['records'].push(item['dataValues']) });

      return retResult;

    });
  }

  /********************************************************************************************************************************
   * End of portion to display related data before deletion
   ********************************************************************************************************************************/
}
