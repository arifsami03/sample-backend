import { BaseModel } from '../../base';
import { CRAddress } from '..';
import * as _ from 'lodash';


export class CRAddressModel extends BaseModel {
  constructor() {
    super(CRAddress);
  }

  /********************************************************************************************************************************
  * Portion to display related data before deletion
  ********************************************************************************************************************************/

  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Address';
    retResult['records'] = [];

    return this.findAllByConditions(['id', ['address', 'title']], condition).then((crAddrRes) => {

      _.each(crAddrRes, item => { retResult['records'].push(item['dataValues']) });

      return retResult;

    });
  }

  /********************************************************************************************************************************
   * End of portion to display related data before deletion
   ********************************************************************************************************************************/

}
