import { BaseModel } from '../../base';
import { CRAffiliation } from '..';
import * as _ from 'lodash';


export class CRAffiliationModel extends BaseModel {
  constructor() {
    super(CRAffiliation);
  }

  /********************************************************************************************************************************
  * Portion to display related data before deletion
  ********************************************************************************************************************************/

  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Affiliation';
    retResult['records'] = [];

    return this.findAllByConditions(['id', ['affiliation', 'title']], condition).then((crAffRes) => {

      _.each(crAffRes, item => { retResult['records'].push(item['dataValues']) });

      return retResult;

    });
  }

  /********************************************************************************************************************************
   * End of portion to display related data before deletion
   ********************************************************************************************************************************/

}
