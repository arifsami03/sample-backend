import { Sequelize } from 'sequelize-typescript';
import { BaseModel } from '../../base';
import { CRApplicant, CRApplicationModel } from '../';
import { UserModel, RoleAssignmentModel, RoleModel } from '../../security/index';
import { WFStateModel, ConfigurationModel } from '../../setting';
import { CRReferenceModel } from '../models/cr-reference.model';
import { Promise } from 'bluebird';
import { ErrorHandler } from '../../base/conf/error-handler';
import { Helper } from '../../base/helpers/helper';
import { CONFIGURATIONS } from '../../base/conf/configurations';
import { InstituteTypeModel, EducationLevelModel } from '../../institute';


export class CRApplicantModel extends BaseModel {
  constructor() {
    super(CRApplicant);
  }

  //TODO:high: system is crashing the provided id is actually user id but when admin open the application information 
  // it is sending application id it is very critical we need to fix it asap
  getApplicantPersonalInfo(CRApplicantId) {
    let cRef = new CRReferenceModel();
    // return new UserModel().findByCondition(['CRApplicantId'], { id: id }).then(userResult => {
    return super.find(CRApplicantId).then((data) => {

      let result = data;
      result['dataValues']['references'] = [];


      return cRef.findAllByConditions(
        ['id', 'fullName', 'mobileNumber', 'address'],
        { [Sequelize.Op.or]: [{ CRApplicantId: CRApplicantId }] }
      ).then((metaData) => {
        metaData.forEach(meta => {
          result['dataValues']['references'].push({ id: meta.id, refName: meta.fullName, refMobileNumber: meta.mobileNumber, refAddress: meta.address });


        });
        return result;
      });
    })
    // })
  }

  getApplicantId(userId) {

    return new UserModel().findByCondition(['CRApplicantId'], { id: userId });

  }
  getApplicantIdByApplicationId(applicationId) {

    return new CRApplicationModel().findByCondition(['CRApplicantId'], { id: applicationId });

  }

  updateCRApplicantPersonalInfo(id, item) {

    return this.update(id, item['personalInfo']).then((result) => {

      return this.createReferences(id, item['personalInfo']).then(result => {

        return this.updateReferences(item['personalInfo']).then(res => {

          return this.deleteReferences(item)
          // .then(res => {

          //   return this.updateUserEmail(item);

          // })


        })

      })

    })
  }

  createCRApplicantPersonalInfo(item) {

    let userModel = new UserModel();

    return userModel.validateUniqueEmail(item.email).then(result => {

      if (result) {

        return ErrorHandler.duplicateEmail;

      } else {

        return Helper.encrypt('pass2word').then(hashedPassword => {

          // item.password = hashedPassword;

          return this.create(item).then((CRApplicantRes) => {

            return this.createReferences(CRApplicantRes['id'], item).then(() => {

              return this.createCRApplication(CRApplicantRes['id'], item).then((CRApplicationRes) => {
                let userPostData = {
                  username: item.email,
                  password: hashedPassword,
                  isActive: true,
                  portal: CONFIGURATIONS.PORTAL.REGISTRATION,
                  CRApplicantId: CRApplicantRes['id']
                }

                return this.createUserUsingApplicant(userPostData).then(() => {

                  let finalRes = {
                    CRApplicationId: CRApplicationRes['id']
                  }


                  return finalRes;
                });
              })

            })

          })

        });
      }

    })
  }

  createCRApplication(CRApplicantId, item) {

    return new WFStateModel().findByCondition(['id'], { WFId: 'post-registration', state: 'draft' }).then(wfRes => {

      return new InstituteTypeModel().findAll(['id']).then(insRes => {

        return new EducationLevelModel().findAllByConditions(['id'], { instituteTypeId: insRes[0]['id'] }).then(edRes => {
          return new ConfigurationModel().findByCondition(['value'], { key: 'showRemittance' }).then(confRes => {

            let cityId;
            if (item['cityId'] == null) {

              return new ConfigurationModel().findAllByConditions(['id'], { key: 'city' }).then(citiesRes => {

                cityId = citiesRes[0]['id'];

                let postData = {
                  CRApplicantId: CRApplicantId,
                  campusName: '',
                  applicationType: 'create',
                  stateId: wfRes['id'],
                  instituteTypeId: insRes[0]['id'],
                  educationLevelId: edRes[0]['id'],
                  remittanceStatus: confRes['value'],
                  cityId: cityId
                }
                return new CRApplicationModel().create(postData);
              });

            }
            else {
              let postData = {
                CRApplicantId: CRApplicantId,
                campusName: '',
                applicationType: 'create',
                stateId: wfRes['id'],
                instituteTypeId: insRes[0]['id'],
                educationLevelId: edRes[0]['id'],
                remittanceStatus: confRes['value'],
                cityId: item['cityId']
              }
              return new CRApplicationModel().create(postData);
            }


          });

        })


      })


    })


  }

  createUserUsingApplicant(item) {

    let roleAssignmentModel = new RoleAssignmentModel();

    return new UserModel().create(item).then(userRes => {

      return new RoleModel().findByCondition(['id'], { name: 'Applicant' }).then(roleId => {


        let postData = {

          roleId: roleId['id'],
          parent: 'user',
          parentId: userRes['id']

        }

        return roleAssignmentModel.create(postData);

      }
      )

    })

  }

  private createReferences(id, item) {

    let cRef = new CRReferenceModel();
    var references = [];

    for (var i = 0; i < item['references'].length; i++) {

      if (item['references'][i].refName != '' && item['references'][i].refAddress != '' && item['references'][i].refMobileNumber != '' && item['references'][i].id == '') {
        references.push({
          CRApplicantId: id,
          fullName: item['references'][i].refName,
          mobileNumber: item['references'][i].refMobileNumber,
          address: item['references'][i].refAddress
        })
      }
    }
    return cRef.sequelizeModel.bulkCreate(references);
  }

  private updateReferences(item) {
    let cRef = new CRReferenceModel();

    return Promise.each(item['references'], (references) => {

      if (references['id'] != '') {
        return cRef.update(references['id'], { fullName: references['refName'], mobileNumber: references['refMobileNumber'], address: references['refAddress'] }).then(res => {
        });
      }

    })

  }
  private updateUserEmail(item) {
    // let user = new UserModel();
    // return user.update()

    // return Promise.each(item['references'], (references) => {

    //   if (references['id'] != '') {
    //     return cRef.update(references['id'], { fullName: references['refName'], mobileNumber: references['refMobileNumber'], address: references['refAddress'] }).then(res => {
    //     });
    //   }

    // })
  }

  private deleteReferences(item) {

    return new Promise((resolve, reject) => {

      let cRef = new CRReferenceModel();

      if (item['deleteRefernceIds'].length != 0) {

        return cRef.deleteByConditions({ id: item['deleteRefernceIds'] }).then(res => {
          resolve(res);
        });

      }
      else {
        resolve(true);
      }
    })
  }


}
