import * as nodemailer from 'nodemailer';
import * as _ from 'lodash';
import { Configuration, WFStateModel, DocStateManagerModel, WFState } from '../../setting';
import { Sequelize } from 'sequelize-typescript';
import { BaseModel, CONFIGURATIONS } from '../../base';
import { CRApplication, CRApplicant, CRApplicantModel } from '..';
import { UserModel, Role, RoleAssignment } from '../../security';

import { User } from '../../security';
import { Promise } from 'bluebird';
import { CRRemittanceModel } from '..';
import { CampusInfoModel } from './campus-info.model';
import { Helper } from '../../base/helpers/helper';
import { CRAddress } from './schema/cr-address';

export class CRApplicationModel extends BaseModel {
  constructor() {
    super(CRApplication);
  }

  /**
   * Find data of applicatoin with campus owner information
   * @param id 
   */
  findWithCRApplicant(attributes, condition) {
    return CRApplication.findOne({
      attributes: attributes,
      where: condition,
      include: [
        { model: CRApplicant, as: 'applicant', attributes: ['fullName', 'email', 'mobileNumber', 'address'] },
        { model: WFState, as: 'wfState', attributes: ['id', 'state', 'title'] },
        { model: CRAddress, as: 'crAddresses', attributes: ['id', 'address'] },
      ]
    });

  }

  /**
   * create a new record for pre regigitration
   * @param item
   */
  public preRegistrationSave(item) {
    let _campus = item.campus;
    let _applicant = item.applicant;
    let _user = item.user;

    let campusRes;
    this.openConnection();

    return this.sequelizeModel.sequelize.transaction(t => {
      _applicant = BaseModel.extendItem(_applicant, true);
      return CRApplicant.create(_applicant, {
        transaction: t
      }).then(_cmpOwn => {
        _campus['CRApplicantId'] = _cmpOwn['id'];
        _applicant = BaseModel.extendItem(_campus, true);

        return CRApplication.create(_campus, {
          validate: true,
          transaction: t
        }).then(campusResult => {

          campusRes = campusResult;

          _user['CRApplicantId'] = _cmpOwn['id'];

          return Helper.encrypt(_user.password).then(hashedPassword => {
            _user.password = hashedPassword;
            _user = BaseModel.extendItem(_user, true);

            return User.create(_user, { validate: true, transaction: t });
          });

        });
      });
    }).then(() => {

      // Send email because state is changed and we need to chekc if any email is attached.
      return this.logCRApplicationWFStateFlowAndSendEmail(campusRes['id']);

    });
  }

  /**
   * When campus pre-registration form is submitted and saved, 
   * log its state flow information and send email if any is attached with new to submit state flow
   * 
   * @param CRApplicationId number
   */
  private logCRApplicationWFStateFlowAndSendEmail(CRApplicationId: number) {

    let docStateManagerModel = new DocStateManagerModel();

    return docStateManagerModel.getStateIds('pre-registration', 'new', 'submit').then(stateFlowRes => {

      let logData = {
        parent: CONFIGURATIONS.ENUMS.LogParents.campus,
        parentId: CRApplicationId,
        fromStateId: stateFlowRes['fromStateId'],
        toStateId: stateFlowRes['toStateId']
      }

      return docStateManagerModel.createLog(logData).then(() => {

        let item = {
          fromStateId: stateFlowRes['fromStateId'],
          toStateId: stateFlowRes['toStateId'],
          CRApplicationId: CRApplicationId
        }
        return docStateManagerModel.sendEmail(item);

      })
    })
  }

  /**
   * Find all applications according to specified workflow and query
   * @param workflow 
   * @param query 
   */
  public getRegistrationList(workflow, query) {

    // If query exists i.e request to find record with specific state
    if (query.hasOwnProperty('state')) {

      if (query.state == CONFIGURATIONS.CMAPUS_STATUS.ACTION_PERFORMED) {

        return this.getRegistrationListWithoutSearchQuery(workflow, true);

      } else {

        return this.getRegistrationListWithSearchQuery(workflow, query);

      }

    } else { // Find all applications

      return this.getRegistrationListWithoutSearchQuery(workflow, false);

    }

  }

  private getRegistrationListWithSearchQuery(workflow, query) {

    query.WFId = workflow;

    return new WFStateModel().findByCondition(['id'], query).then(wfState => {

      let foundInGData;

      //Bypass for superuser
      if (CONFIGURATIONS.identity['isSuperUser']) {
        foundInGData = true;
      }
      else {
        foundInGData = _.find(CONFIGURATIONS.GDataObject['WFStateIds'], (o) => { return o == wfState['id'] }) ? true : false;
      }


      if (wfState && foundInGData) {

        return super.findAllByConditions(['id'], { stateId: wfState['id'] }, [
          { model: Configuration, as: 'city', attributes: ['id', 'value'] },
          { model: CRApplicant, as: 'applicant', attributes: ['id', 'fullName', 'email', 'applicationType'] },
          { model: WFState, as: 'wfState', attributes: ['id', 'state', 'title'] }
        ]);

      } else {
        return [];
      }
    });
  }

  private getRegistrationListWithoutSearchQuery(workflow, onlyPreviousRecords) {

    let wfStateIds = [];

    // Find workflow states of requested workflow
    return new WFStateModel().findAllByConditions(['id'], { WFId: workflow }).then(_wfStates => {

      _.each((_wfStates), (item) => { wfStateIds.push(item.id); });

      let intersectedIds = CONFIGURATIONS.identity['isSuperUser'] ? wfStateIds : _.intersection(wfStateIds, CONFIGURATIONS.GDataObject['WFStateIds']);

      let performedStatesLikeCondition = [];

      _.each((intersectedIds), (item) => {
        performedStatesLikeCondition.push({ performedStates: { [Sequelize.Op.like]: '%{' + item + '}%' } });
      });

      let stateIdCondition = { stateId: { [Sequelize.Op.in]: intersectedIds } };

      let condition = onlyPreviousRecords ? { [Sequelize.Op.or]: performedStatesLikeCondition } : { [Sequelize.Op.or]: performedStatesLikeCondition.concat(stateIdCondition) };

      return super.findAllByConditions(['id'], condition, [
        // return super.findAllByConditions(['id'], { [Sequelize.Op.or]: performedStatesLikeCondition.concat(stateIdCondition) }, [
        { model: Configuration, as: 'city', attributes: ['id', 'value'] },
        { model: CRApplicant, as: 'applicant', attributes: ['id', 'fullName', 'email', 'applicationType'] },
        { model: WFState, as: 'wfState', attributes: ['id', 'state', 'title'] }
      ]);
    });

  }

  private getActionPerformedRegistrationList(workflow) {

  }


  /**
   * Find all workflow states assigned to user
   * @param workflow 
   */
  findAllAssignedWFStates(workflow: string) {
    let wfStateIds = CONFIGURATIONS.GDataObject['WFStateIds'] ? CONFIGURATIONS.GDataObject['WFStateIds'] : [];

    return WFState.findAll({
      attributes: [['title', 'label'], ['state', 'value']],
      where: { id: { [Sequelize.Op.in]: wfStateIds }, WFId: workflow }
    });

  }


  /**
   * preRegistrationDetail finding the one record of pre regitsrtaion form on basis of id
   * @param _id
   */
  public findPreRegistration(_id: number) {
    this.openConnection();
    return this.sequelizeModel.findOne({
      attributes: [
        'id',
        'CRApplicantId',
        'campusName',
        'instituteTypeId',
        'educationLevelId',
        'tentativeSessionStart',
        'applicationType',
        'campusLocation',
        'website',
        'officialEmail',
        'establishedSince',
        'noOfCampusTransfer',
        'stateId',
        'buildingAvailable',
        'cityId',
        'createdAt'
      ],

      where: { id: _id },
      include: [
        {
          model: Configuration,
          as: 'city',
          attributes: ['id', 'value']
        },
        {
          model: CRApplicant,
          as: 'applicant'
        },
        {
          model: WFState,
          as: 'wfState',
          attributes: ['id', 'title']
        }
      ]
    });
  }

  // public updateStateId(id,stateId){

  //   let item = {
  //     stateId:stateId
  //   }
  //   return this.update(id,item);

  // }

  /**
   * changeStatus will change the status of pre registration form
   * @param id
   * @param item
   */
  public changeStatus(id: number, item) {
    this.openConnection();
    return super.update(id, item).then(_campus => {
      //TODO:high: remove hardcoded statuses
      //Finding applicant from
      return this.sequelizeModel
        .findOne({
          where: { id: id },
          include: [
            {
              model: CRApplicant,
              as: 'applicant'
            }
          ]
        })
        .then(_cmp => {
          // Change the status of user isActive for login access
          if (item.status == 'approve') {
            return User.update({ isActive: true }, { where: { CRApplicantId: _cmp.CRApplicantId } }).then(_user => {
              return this.sendEmail(
                'developer.muhammadumair@gmail.com',
                'Programming4Fun',
                _cmp.applicant.email,
                'Approval for Campus Pre-Registration',
                '',
                `<h1>Congratulations!</h1>
                  <p>Your campus pre-registration application is approved.Here are your credential for login.</p>
                  <p>Username : ${_cmp.applicant.email}</p>
                  <p>Password : pass2word</p>
                  <a href='http://localhost:4200'>
                  Click Here to Login 
                  </a>`
              );
            });
          } else if (item.status == 'reject') {
            return User.update({ isActive: false }, { where: { CRApplicantId: _cmp.CRApplicantId } }).then(_user => {
              return this.sendEmail(
                'developer.muhammadumair@gmail.com',
                'Programming4Fun',
                _cmp.applicant.email,
                'Approval for Campus Pre-Registration',
                '',
                `<h1>News!</h1>
                  <p> Your campus pre-registration application is rejected.</p>
                  `
              );
            });
          }
        });
    });
  }

  /**
   *
   * @param _from sender email address
   * @param _fromPassword sender password
   * @param _to receiver email adresses
   * @param _subject subject of email
   * @param _text text of email
   * @param _html html in email
   */
  private sendEmail(_from: string, _fromPassword: string, _to: string, _subject: string, _text: string, _html: string) {
    return new Promise((resolve, reject) => {
      nodemailer.createTestAccount((err, account) => {
        let transporter = nodemailer.createTransport({
          from: 'noreply@gmail.com',
          host: 'smtp.gmail.com', // hostname
          secureConnection: false, // use SSL
          // port: 465, // port for secure SMTP
          transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts
          auth: {
            user: 'developer.muhammadumair@gmail.com',
            pass: 'Programming4Fun'
          }
        });
        let mailOptions = {
          from: _from, // sender address
          to: _to, // list of receivers
          subject: _subject, // Subject line
          text: _text, // plain text body
          html: _html
        };

        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            reject({ error: 'Something went wrong.', detail: error });
          } else {
            resolve({ success: 'Sent', detail: info });
          }
        });
      });
    });
  }

  /**
   * validateUniqueEmail
   * @param item
   */
  validateUniqueEmail(item: any) {
    //TODO:low can check also in the user but I am running transaction on adding pre reg form so i think there is no posibility to add duplicate entry
    return CRApplicant.findOne({ where: { email: item.email } });
  }

  /**
   * Save campus state when applicant submit post registration form
   * 
   * 1. Update state to post-registration submit
   * 2. Creat Log
   * 3. Check for attached emails and send email.
   * 
   * @param userId number user id
   */
  save(userId: number) {

    //TODO:low: manually open connection should be controlled.
    this.openConnection();


    let docStateManagerModel = new DocStateManagerModel();

    // As per we reciving user id and we need to find campus id.
    return User.findOne({
      where: { id: userId },
      include: [
        {
          model: CRApplicant,
          as: 'applicant',
          include: [{ model: CRApplication, as: 'application', attributes: { exclude: ['CRApplicationId'] } }]
        }
      ]
    }).then(_res => {

      let CRApplicationId = _res.applicant.application.id;

      return docStateManagerModel.getStateIds('post-registration', 'draft', 'submit').then(stateFlowRes => {

        let item = {
          parent: 'campus',
          parentId: CRApplicationId,
          previousStateId: stateFlowRes['fromStateId'],
          stateId: stateFlowRes['toStateId'],
        }

        return docStateManagerModel.updateStateId(item);

      })

    });
  }

  /**
   *
   * @param id
   */
  getRegistrationStatus(id: number) {
    return new UserModel().findByCondition(['CRApplicantId'], { id: id }).then(applicant => {
      return this.findByCondition(['stateId'], { CRApplicantId: applicant['CRApplicantId'] }).then(stateId => {
        return new WFStateModel().findByCondition(['state', 'WFId'], { id: stateId['stateId'] });
      });
    });
  }
  /**
   * Function to get User iD based on campus Id
   * @param _id
   */
  findUserId(id: number) {
    return this.findByCondition(['CRApplicantId'], { id: id }).then(res => {
      return new UserModel().findByCondition(['id'], { CRApplicantId: res['CRApplicantId'] });
    });
  }

  activeUser(_id: number, item) {
    return new UserModel().update(_id, item).then(() => {
      return User.find({ where: { id: _id } }).then(_userResult => {
        return Role.find({ where: { name: 'Applicant' } }).then(_role => {
          let roleAssignmentItem = {
            roleId: _role['id'],
            parent: 'user',
            parentId: _userResult['id']
          };
          roleAssignmentItem = BaseModel.extendItem(roleAssignmentItem, item);
          return RoleAssignment.create(roleAssignmentItem);
        });
      });
    });
  }

  getSubmitAppData(CRApplicationId: number) {

    let result = [];
    // Campus Owner Id
    // Campus Owner -> UserID
    return new CRApplicationModel().find(CRApplicationId, ['CRApplicantId']).then(campusRes => {
      return new UserModel().findByCondition(['id'], { CRApplicantId: campusRes['CRApplicantId'] }).then(userRes => {

        return new CRApplicantModel().getApplicantPersonalInfo(userRes['id']).then(ownerResult => {
          return new CRRemittanceModel().view(userRes['id']).then(remittanceResult => {

            return new CampusInfoModel().getCampusInfo(userRes['id']).then(campus => {
              result.push({
                ownerInfo: ownerResult,
                campusInfo: campus,
                remittance: remittanceResult

              })
            }).then(() => {
              return result;
            })
          });
        })

      })
    })

  }

  updateCRApplication(id, postData) {

    return super.update(id, postData);

  }
}
