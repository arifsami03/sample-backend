import { BaseModel } from '../../base';
import { CBFRoom, CBFloor, CampusBuilding, Campus } from '..';
import { ErrorHandler } from '../../base/conf/error-handler';
import { Sequelize } from 'sequelize-typescript';

export class CBFRoomModel extends BaseModel {

  constructor() {
    super(CBFRoom);
  }

  findAllWithFloorBuildingAndCampus(attributes) {
    let includeObj = [
      {
        model: CBFloor, as: 'cbFloor', attributes: ['id', 'name'], where: BaseModel.cb(), required: false,
        include: [
          {
            model: CampusBuilding, as: 'campusBuilding', attributes: ['id', 'name'], where: BaseModel.cb(), required: false,
            include: [{ model: Campus, as: 'campus', attributes: ['id', 'campusName'], where: BaseModel.cb(), required: false }]
          }
        ]
      }
    ];

    return this.findAll(attributes, null, includeObj);
  }

  findWithClassroomTypes(id, attributes) {
    return super.find(id, attributes).then(result => {
      let classroomTypes = [];
      // return classroomtype as array
      if (result.classroomType != '') {
        classroomTypes = result.classroomType.split(",");
      }
      result['dataValues']['classroomTypes'] = classroomTypes;
      return result;
    })
  }

  /**
   * First check if code for room in selected floor is unique then create room or return error
   * @param item 
   */
  create(item) {
    return super.findByCondition(['id'], { CBFloorId: item.CBFloorId, code: item.code }).then(result => {
      if (result) {
        return ErrorHandler.duplicateEntry;
      } else {
        // set classroomType as CSV format
        let classroomType = '';
        item.classroomTypes.forEach((element, index) => {
          if (element.checked) {
            if (index != 0) {
              classroomType += ',';
            }
            classroomType += element.value;
          }
        });
        item['classroomType'] = classroomType;
        return super.create(item);
      }
    });
  }

  /**
   * First check if code for building in selected campus is unique then create building or return error
   * @param item 
   */
  update(id, item) {
    return super.findByCondition(['id'], { id: { [Sequelize.Op.not]: id }, CBFloorId: item.CBFloorId, code: item.code }).then(result => {
      if (result) {
        return ErrorHandler.duplicateEntry;
      } else {
        // set classroomType as CSV format
        let classroomType = '';
        item.classroomTypes.forEach((element, index) => {
          if (element.checked) {
            if (index != 0) {
              classroomType += ',';
            }
            classroomType += element.value;
          }
        });
        item['classroomType'] = classroomType;
        return super.update(id, item);
      }
    });
  }

}
