import * as nodemailer from 'nodemailer';
import * as _ from 'lodash';
import { Configuration, WFStateModel, DocStateManagerModel, WFState, ConfigurationModel } from '../../setting';
import { Sequelize } from 'sequelize-typescript';
import { BaseModel, CONFIGURATIONS } from '../../base';
import { CRApplication, CRApplicant, CRApplicantModel } from '..';
import { UserModel, Role, RoleAssignment } from '../../security';

import { User } from '../../security';
import { Promise } from 'bluebird';
import { CRRemittanceModel } from '..';
import { CampusInfoModel } from './campus-info.model';
import { Helper } from '../../base/helpers/helper';
import { CRAddress } from './schema/cr-address';
import { CampusProgramModel } from './campus-program.model';
import { CRMOUModel } from './cr-mou.model';
import { CREvaluationSheetModel } from './cr-evaluation-sheet.model';
import { CRAddressModel } from './cr-address.model';
import { CRTargettedLocations } from './schema/cr-targetted-loacation';
import { CRTargettedLocationsModel } from './cr-targetted-loacation.model';
import { CRAffiliationModel } from './cr-affiliation.model';
import { CRAvailableBuildingModel } from './cr-available-building.model';
import { CRMOUVerificationModel } from './cr-mou-verification.model';
import { CRProgramModel } from './cr-program.model';
import { Campus } from './schema/campus';
import { CampusModel } from './campus.model';
import { CRAAdditionalInfoModel } from './cra-additional-info.model';


export class CRApplicationModel extends BaseModel {
  constructor() {
    super(CRApplication);
  }

  /**
   * Find data of applicatoin with campus owner information
   * @param id 
   */
  findWithCRApplicant(attributes, condition) {
    let includeObj = [
      { model: CRApplicant, as: 'applicant', attributes: ['fullName', 'email', 'mobileNumber', 'address'], where: BaseModel.cb(), required: false },
      { model: WFState, as: 'wfState', attributes: ['id', 'state', 'title'], where: BaseModel.cb(), required: false },
      { model: CRAddress, as: 'crAddresses', attributes: ['id', 'address'], where: BaseModel.cb(), required: false },
    ];

    return this.findByCondition(attributes, condition, includeObj);

  }

  /**
   * create a new record for pre regigitration
   * @param item
   */
  public preRegistrationSave(item) {
    let _campus = item.campus;
    let _applicant = item.applicant;
    let _user = item.user;

    let campusRes;
    this.openConnection();

    return this.sequelizeModel.sequelize.transaction(t => {

      _applicant = BaseModel.extendItem(_applicant, true);

      return CRApplicant.create(_applicant, {

        transaction: t

      }).then(_cmpOwn => {

        return new ConfigurationModel().findWithOptions(['id', 'key', 'value', 'parentKey', 'parentKeyId'], { key: 'showRemittance', isSingle: true })
          .then(result => {
            Boolean(+result.value);
            _campus['CRApplicantId'] = _cmpOwn['id'];
            _campus['remittanceStatus'] =  Boolean(+result.value);

            _applicant = BaseModel.extendItem(_campus, true);

            return CRApplication.create(_campus, {

              validate: true,
              transaction: t
            })



          }).then(campusResult => {

            campusRes = campusResult;

            _user['CRApplicantId'] = _cmpOwn['id'];

            return Helper.encrypt(_user.password).then(hashedPassword => {

              _user.password = hashedPassword;
              _user = BaseModel.extendItem(_user, true);

              return User.create(_user, { validate: true, transaction: t });

            });

          });
      });
    }).then(() => {

      // Send email because state is changed and we need to chekc if any email is attached.
      return this.logCRApplicationWFStateFlowAndSendEmail(campusRes['id']);

    }).then(() => {
      return campusRes;
    });
  }

  /**
   * When campus pre-registration form is submitted and saved, 
   * log its state flow information and send email if any is attached with new to submit state flow
   * 
   * @param CRApplicationId number
   */
  private logCRApplicationWFStateFlowAndSendEmail(CRApplicationId: number) {

    let docStateManagerModel = new DocStateManagerModel();

    return docStateManagerModel.getStateIds('pre-registration', 'new', 'submit').then(stateFlowRes => {

      let logData = {
        parent: CONFIGURATIONS.ENUMS.LogParents.campus,
        parentId: CRApplicationId,
        fromStateId: stateFlowRes['fromStateId'],
        toStateId: stateFlowRes['toStateId']
      }

      return docStateManagerModel.createLog(logData).then(() => {

        let item = {
          fromStateId: stateFlowRes['fromStateId'],
          toStateId: stateFlowRes['toStateId'],
          CRApplicationId: CRApplicationId
        }
        return docStateManagerModel.sendEmail(item);

      })
    })
  }

  /**
   * Find all applications according to specified workflow and query
   * @param workflow 
   * @param query 
   */
  public getRegistrationList(workflow, query) {

    // If query exists i.e request to find record with specific state
    if (query.hasOwnProperty('state')) {

      if (query.state == CONFIGURATIONS.CMAPUS_STATUS.ACTION_PERFORMED) {

        return this.getRegistrationListWithoutSearchQuery(workflow, true);

      } else {

        return this.getRegistrationListWithSearchQuery(workflow, query);

      }

    } else { // Find all applications

      return this.getRegistrationListWithoutSearchQuery(workflow, false);

    }

  }

  private getRegistrationListWithSearchQuery(workflow, query) {

    query.WFId = workflow;

    return new WFStateModel().findByCondition(['id'], query).then(wfState => {

      let foundInGData;

      //Bypass for superuser
      if (CONFIGURATIONS.identity['isSuperUser']) {
        foundInGData = true;
      }
      else {
        foundInGData = _.find(CONFIGURATIONS.GDataObject['WFStateIds'], (o) => { return o == wfState['id'] }) ? true : false;
      }


      if (wfState && foundInGData) {

        return super.findAllByConditions(['id'], { stateId: wfState['id'] }, [
          { model: Configuration, as: 'city', attributes: ['id', 'value'] },
          { model: CRApplicant, as: 'applicant', attributes: ['id', 'fullName', 'email', 'applicationType'] },
          { model: WFState, as: 'wfState', attributes: ['id', 'state', 'title'] }
        ],[['updatedAt','DESC']]);

      } else {
        return [];
      }
    });
  }

  private getRegistrationListWithoutSearchQuery(workflow, onlyPreviousRecords) {

    let wfStateIds = [];

    // Find workflow states of requested workflow
    return new WFStateModel().findAllByConditions(['id'], { WFId: workflow }).then(_wfStates => {

      _.each((_wfStates), (item) => { wfStateIds.push(item.id); });

      let intersectedIds = CONFIGURATIONS.identity['isSuperUser'] ? wfStateIds : _.intersection(wfStateIds, CONFIGURATIONS.GDataObject['WFStateIds']);

      let performedStatesLikeCondition = [];

      _.each((intersectedIds), (item) => {
        performedStatesLikeCondition.push({ performedStates: { [Sequelize.Op.like]: '%{' + item + '}%' } });
      });

      let stateIdCondition = { stateId: { [Sequelize.Op.in]: intersectedIds } };

      let condition = onlyPreviousRecords ? { [Sequelize.Op.or]: performedStatesLikeCondition } : { [Sequelize.Op.or]: performedStatesLikeCondition.concat(stateIdCondition) };

      return super.findAllByConditions(['id'], { stateId: { [Sequelize.Op.in]: intersectedIds } ,  }, [
        // return super.findAllByConditions(['id'], condition, [
        // return super.findAllByConditions(['id'], { [Sequelize.Op.or]: performedStatesLikeCondition.concat(stateIdCondition) }, [
        { model: Configuration, as: 'city', attributes: ['id', 'value'] },
        { model: CRApplicant, as: 'applicant', attributes: ['id', 'fullName', 'email', 'applicationType'] },
        { model: WFState, as: 'wfState', attributes: ['id', 'state', 'title'] }
      ],[['updatedAt','DESC']]);
    });

  }


  /**
   * Find all workflow states assigned to user
   * @param workflow 
   */
  findAllAssignedWFStates(workflow: string) {
    let wfStateIds = CONFIGURATIONS.GDataObject['WFStateIds'] ? CONFIGURATIONS.GDataObject['WFStateIds'] : [];

    return new WFStateModel().findAll([['title', 'label'], ['state', 'value']], { id: { [Sequelize.Op.in]: wfStateIds }, WFId: workflow });

  }


  /**
   * preRegistrationDetail finding the one record of pre regitsrtaion form on basis of id
   * @param _id
   */
  public findPreRegistration(_id: number) {
    let attributes = ['id', 'CRApplicantId', 'campusName', 'instituteTypeId', 'educationLevelId', 'tentativeSessionStart', 'applicationType',
      'campusLocation', 'website', 'officialEmail', 'establishedSince', 'noOfCampusTransfer', 'stateId', 'buildingAvailable', 'cityId', 'createdAt'];

    let includeObj = [
      { model: Configuration, as: 'city', attributes: ['id', 'value'], where: BaseModel.cb(), required: false, },
      { model: CRApplicant, as: 'applicant', where: BaseModel.cb(), required: false, },
      { model: WFState, as: 'wfState', attributes: ['id', 'title'], where: BaseModel.cb(), required: false, }
    ];

    return this.findByCondition(attributes, { id: _id }, includeObj);

  }

  // public updateStateId(id,stateId){

  //   let item = {
  //     stateId:stateId
  //   }
  //   return this.update(id,item);

  // }

  /**
   * changeStatus will change the status of pre registration form
   * @param id
   * @param item
   */
  public changeStatus(id: number, item) {
    this.openConnection();
    return super.update(id, item).then(_campus => {
      //TODO:high: remove hardcoded statuses
      //Finding applicant from
      let includeObj = [{ model: CRApplicant, as: 'applicant', where: BaseModel.cb(), required: false }];

      return this.findByCondition(['id', 'CRApplicantId'], { id: id }, includeObj)

        .then(_cmp => {
          // Change the status of user isActive for login access
          if (item.status == 'approve') {
            return User.update({ isActive: true }, { where: { CRApplicantId: _cmp.CRApplicantId } }).then(_user => {
              return this.sendEmail(
                'developer.muhammadumair@gmail.com',
                'Programming4Fun',
                _cmp.applicant.email,
                'Approval for Campus Pre-Registration',
                '',
                `<h1>Congratulations!</h1>
                  <p>Your campus pre-registration application is approved.Here are your credential for login.</p>
                  <p>Username : ${_cmp.applicant.email}</p>
                  <p>Password : pass2word</p>
                  <a href='http://localhost:4200'>
                  Click Here to Login 
                  </a>`
              );
            });
          } else if (item.status == 'reject') {
            return User.update({ isActive: false }, { where: { CRApplicantId: _cmp.CRApplicantId } }).then(_user => {
              return this.sendEmail(
                'developer.muhammadumair@gmail.com',
                'Programming4Fun',
                _cmp.applicant.email,
                'Approval for Campus Pre-Registration',
                '',
                `<h1>News!</h1>
                  <p> Your campus pre-registration application is rejected.</p>
                  `
              );
            });
          }
        });
    });
  }

  /**
   *
   * @param _from sender email address
   * @param _fromPassword sender password
   * @param _to receiver email adresses
   * @param _subject subject of email
   * @param _text text of email
   * @param _html html in email
   */
  private sendEmail(_from: string, _fromPassword: string, _to: string, _subject: string, _text: string, _html: string) {
    return new Promise((resolve, reject) => {
      nodemailer.createTestAccount((err, account) => {
        let transporter = nodemailer.createTransport({
          from: 'noreply@gmail.com',
          host: 'smtp.gmail.com', // hostname
          secureConnection: false, // use SSL
          // port: 465, // port for secure SMTP
          transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts
          auth: {
            user: 'developer.muhammadumair@gmail.com',
            pass: 'Programming4Fun'
          }
        });
        let mailOptions = {
          from: _from, // sender address
          to: _to, // list of receivers
          subject: _subject, // Subject line
          text: _text, // plain text body
          html: _html
        };

        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            reject({ error: 'Something went wrong.', detail: error });
          } else {
            resolve({ success: 'Sent', detail: info });
          }
        });
      });
    });
  }

  /**
   * validateUniqueEmail
   * @param item
   */
  validateUniqueEmail(item: any) {
    //TODO:low can check also in the user but I am running transaction on adding pre reg form so i think there is no posibility to add duplicate entry
    let condition = { email: item.email };

    return new CRApplicantModel().findByCondition(['id'], condition);
  }

  /**
   * Save campus state when applicant submit post registration form
   * 
   * 1. Update state to post-registration submit
   * 2. Creat Log
   * 3. Check for attached emails and send email.
   * 
   * @param userId number user id
   */
  save(applicantId: number) {

    //TODO:low: manually open connection should be controlled.
    this.openConnection();


    let docStateManagerModel = new DocStateManagerModel();

    // As per we reciving user id and we need to find campus id.
    return new CRApplicationModel().findByCondition(['id'], { CRApplicantId: applicantId }).then(applicationId => {

      return docStateManagerModel.getStateIds('post-registration', 'draft', 'submit').then(stateFlowRes => {

        let item = {
          parent: 'campus',
          parentId: applicationId['id'],
          WFId: 'post-registration',
          state: 'draft',
          previousStateId: stateFlowRes['fromStateId'],
          stateId: stateFlowRes['toStateId'],
          id: stateFlowRes['toStateId'],
        }
        return docStateManagerModel.updateStateId(item);

      })
    })



    // });
  }

  /**
   *
   * @param id
   */
  getRegistrationStatus(id: number) {
    // return new UserModel().findByCondition(['CRApplicantId'], { id: id }).then(applicant => {
    return this.findByCondition(['stateId'], { CRApplicantId: id }).then(stateId => {
      return new WFStateModel().findByCondition(['state', 'WFId'], { id: stateId['stateId'] });
    });
    // });
  }
  /**
   *
   * @param id
   */
  findCRAAdditionalInfo(crApplicantId: number) {
    // return new UserModel().findByCondition(['CRApplicantId'], { id: id }).then(applicant => {
    return this.findByCondition(['id'], { CRApplicantId: crApplicantId }).then(crApplicationId => {

      if(crApplicationId){
        return new CRAAdditionalInfoModel().findAllByConditions(['id', 'question','optionType','optionsCSV','answer'], { CRApplicationId: crApplicationId['id'] });
      }
      else{
        return null;
      }

      

    });
    // });
  }
  /**
   * Function to get User iD based on campus Id
   * @param _id
   */
  findUserId(id: number) {
    return this.findByCondition(['CRApplicantId'], { id: id }).then(res => {
      return new UserModel().findByCondition(['id'], { CRApplicantId: res['CRApplicantId'] });
    });
  }

  activeUser(_id: number, item) {
    return new UserModel().update(_id, item).then(() => {
      return User.find({ where: { id: _id } }).then(_userResult => {
        return Role.find({ where: { name: 'Applicant' } }).then(_role => {
          let roleAssignmentItem = {
            roleId: _role['id'],
            parent: 'user',
            parentId: _userResult['id']
          };
          roleAssignmentItem = BaseModel.extendItem(roleAssignmentItem, item);
          return RoleAssignment.create(roleAssignmentItem);
        });
      });
    });
  }

  getSubmitAppData(CRApplicationId: number) {

    let result = [];
    // Campus Owner Id
    // Campus Owner -> UserID
    return new CRApplicationModel().find(CRApplicationId, ['CRApplicantId']).then(campusRes => {
      // return new UserModel().findByCondition(['id'], { CRApplicantId: campusRes['CRApplicantId'] }).then(userRes => {

      return new CRApplicantModel().getApplicantPersonalInfo(campusRes['CRApplicantId']).then(ownerResult => {
        return new CRRemittanceModel().view(campusRes['CRApplicantId']).then(remittanceResult => {

          return new CampusInfoModel().getCampusInfo(campusRes['CRApplicantId']).then(campus => {
            result.push({
              ownerInfo: ownerResult,
              campusInfo: campus,
              remittance: remittanceResult

            })
          }).then(() => {
            return result;
          })
        });
      })

      // })
    })

  }

  updateCRApplication(id, postData) {

    return super.update(id, postData);

  }

  deleteWithAssociatedRecords(id) {
    return new CampusProgramModel().deleteByConditionsWithAssociatedRecords({ CRApplicationId: id }).then(() => {
      return new CRMOUModel().deleteByConditions({ CRApplicationId: id }).then(() => {
        return new CREvaluationSheetModel().deleteByConditions({ CRApplicationId: id }).then(() => {
          return new CRAddressModel().deleteByConditions({ CRApplicationId: id }).then(() => {
            return new CRTargettedLocationsModel().deleteByConditions({ CRApplicationId: id }).then(() => {
              return new CRAffiliationModel().deleteByConditions({ CRApplicationId: id }).then(() => {
                return new CRAvailableBuildingModel().deleteByConditions({ CRApplicationId: id }).then(() => {
                  return new CRMOUVerificationModel().deleteByConditions({ CRApplicationId: id }).then(() => {
                    return new CRRemittanceModel().delete(id).then(() => {
                      return new CampusModel().deleteByConditionsWithAssociatedRecords({ CRApplicationId: id }).then(() => {

                        return this.delete(id);

                      })
                    });
                  })
                })
              })
            })
          })
        })
      })
    })
  }
  deleteByConditionsWithAssociatedRecords(condition) {


    return this.findAllByConditions(['id'], condition).then(CRApplications => {

      let crProgramModel = new CRProgramModel();
      let crMouModel = new CRMOUModel();
      let crESModel = new CREvaluationSheetModel();
      let crAModel = new CRAddressModel();
      let crTLModel = new CRTargettedLocationsModel();
      let crAffModel = new CRAffiliationModel();
      let crABModel = new CRAvailableBuildingModel();
      let crMVModel = new CRMOUVerificationModel();
      let crRemModel = new CRRemittanceModel();
      let campusModel = new CampusModel();
      return Promise.each(CRApplications, CRApplication => {

        return crProgramModel.deleteByConditionsWithAssociatedRecords({ CRApplicationId: CRApplication['id'] }).then(() => {
          return crMouModel.deleteByConditions({ CRApplicationId: CRApplication['id'] }).then(() => {
            return crESModel.deleteByConditionsWithAssociatedRecords({ CRApplicationId: CRApplication['id'] }).then(() => {
              return crAModel.deleteByConditions({ CRApplicationId: CRApplication['id'] }).then(() => {
                return crTLModel.deleteByConditions({ CRApplicationId: CRApplication['id'] }).then(() => {
                  return crAffModel.deleteByConditions({ CRApplicationId: CRApplication['id'] }).then(() => {
                    return crABModel.deleteByConditions({ CRApplicationId: CRApplication['id'] }).then(() => {
                      return crMVModel.deleteByConditions({ CRApplicationId: CRApplication['id'] }).then(() => {
                        return crRemModel.delete(CRApplication['id']).then(() => {
                          return campusModel.deleteByConditionsWithAssociatedRecords({ CRApplicationId: CRApplication['id'] }).then(() => {
                            return this.delete(CRApplication['id']);
                          });
                        });
                      })
                    })
                  })
                })
              })
            })
          })
        })

      })

    })

  }

  /********************************************************************************************************************************
  * Portion to display related data before deletion
  ********************************************************************************************************************************/

  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Campus Register Application';
    retResult['records'] = [];

    return this.findAllByConditions(['id', ['campusName', 'title']], condition).then((elRes) => {

      return Promise.each(elRes, item => {

        let tempItem = item['dataValues'];
        tempItem['title'] = (tempItem['title'] == '') ? 'Non Approved Campus Pre-Registration' : tempItem['title'];
        tempItem['children'] = [];
        return this.findData(item['dataValues']).then(res => {

          tempItem['children'] = res;

          retResult['records'].push(tempItem);

        });

      })
    }).then(() => {

      return retResult;


    });
  }
  findRemittanceStatus(crApplicantId) {
    return new CRApplicationModel().findByCondition(['id', 'remittanceStatus'], { CRApplicantId: crApplicantId })
  }
  private findData(item) {

    // let itRes = item;
    // itRes['children'] = [];
    let itRes = [];

    let campusModel = new CampusModel();

    return campusModel.findRelatedDataWithCondition({ CRApplicationId: item['id'] }).then((campusRes) => {

      itRes.push(campusRes);

    }).then(() => {
      return itRes;
    });

    // let itRes = item;
    // itRes['children'] = [];

    // let crProgramModel = new CRProgramModel();
    // let crMouModel = new CRMOUModel();
    // let crESModel = new CREvaluationSheetModel();
    // let crAddrModel = new CRAddressModel();
    // let crTLModel = new CRTargettedLocationsModel();
    // let crAffModel = new CRAffiliationModel();
    // let crABModel = new CRAvailableBuildingModel();
    // let crMVModel = new CRMOUVerificationModel();
    // let crRemModel = new CRRemittanceModel();
    // let campusModel = new CampusModel();

    // // return crProgramModel.findRelatedDataWithCondition({ CRApplicationId: CRApplication['id'] }).then((crProgramRes) => {
    // // itRes['children'].push(crProgramRes);
    // return crMouModel.findRelatedDataWithCondition({ CRApplicationId: CRApplication['id'] }).then((crMouRes) => {
    //   itRes['children'].push(crMouRes);
    //   return crESModel.findRelatedDataWithCondition({ CRApplicationId: CRApplication['id'] }).then((crESRes) => {
    //     itRes['children'].push(crESRes);
    //     return crAddrModel.findRelatedDataWithCondition({ CRApplicationId: CRApplication['id'] }).then((crAddrRes) => {
    //       itRes['children'].push(crAddrRes);
    //       return crTLModel.findRelatedDataWithCondition({ CRApplicationId: CRApplication['id'] }).then((crTLRes) => {
    //         itRes['children'].push(crTLRes);
    //         return crAffModel.findRelatedDataWithCondition({ CRApplicationId: CRApplication['id'] }).then((crAffRes) => {
    //           itRes['children'].push(crAffRes);
    //           return crABModel.findRelatedDataWithCondition({ CRApplicationId: CRApplication['id'] }).then((crABRes) => {
    //             itRes['children'].push(crABRes);
    //             return crMVModel.findRelatedDataWithCondition({ CRApplicationId: CRApplication['id'] }).then((crMVRes) => {
    //               itRes['children'].push(crMVRes);
    //               return crRemModel.findRelatedDataWithCondition({ id: CRApplication['id'] }).then((crRemRes) => {
    //                 itRes['children'].push(crRemRes);
    //                 // return campusModel.findRelatedDataWithCondition({ CRApplicationId: CRApplication['id'] }).then((campusRes) => {
    //                 //   itRes['children'].push(campusRes);
    //                 // });
    //               });
    //             });
    //           });
    //         });
    //       });
    //     });
    //   });
    //   // });
    // }).then(() => {
    //   return itRes;
    // })
  }

  /********************************************************************************************************************************
   * End of portion to display related data before deletion
   ********************************************************************************************************************************/


}
