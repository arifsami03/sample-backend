import { BaseModel } from '../../base';
import { CampusBuilding, Campus, CBFloorModel } from '..';
import { Promise } from 'bluebird';
import { ErrorHandler } from '../../base/conf/error-handler';
import { Sequelize } from 'sequelize-typescript';

export class CampusBuildingModel extends BaseModel {

  constructor() {
    super(CampusBuilding);
  }

  findAllWithCampus(attributes) {
    let includeObj = [{ model: Campus, as: 'campus', attributes: ['id', 'campusName'], where: BaseModel.cb(), required: false }];

    return this.findAll(attributes, null, includeObj);
  }

  /**
   * First check if code for building in selected campus is unique then create building or return error
   * @param item 
   */
  create(item) {
    return super.findByCondition(['id'], { campusId: item.campusId, code: item.code }).then(result => {
      if (result) {
        return ErrorHandler.duplicateEntry;
      } else {
        return super.create(item);
      }
    });
  }

  /**
   * First check if code for building in selected campus is unique then create building or return error
   * @param item 
   */
  update(id, item) {
    return super.findByCondition(['id'], { id: { [Sequelize.Op.not]: id }, campusId: item.campusId, code: item.code }).then(result => {
      if (result) {
        return ErrorHandler.duplicateEntry;
      } else {
        return super.update(id, item);
      }
    });
  }

  /**
  * First deletes Building childs i.e floors then delete Building itself
  * @param id 
  */
  deleteWithChilds(id) {
    // Find all floors to be deleted
    let cbFloorModel = new CBFloorModel();
    return cbFloorModel.findAllByConditions(['id'], { CBId: id }).then(floors => {
      return Promise.each(floors, floor => {
        //delete floor with their childs i.e rooms
        return cbFloorModel.deleteWithChilds(floor['id']);
      }).then(() => {
        return super.delete(id);
      });
    });
  }

}
