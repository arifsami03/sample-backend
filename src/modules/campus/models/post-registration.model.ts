import { Connection, BaseModel } from '../../base';
import { CREvaluationSheet, CRESSection, CRESQuestion } from '..'
import { ESSection, ESQuestion, ESQuestionOption } from '../../setting';
import { Promise } from 'bluebird';

export class PostRegistrationModel {
  protected connection;
  constructor() {
  }


  forwardForAnalysis(item) {

    //TODO:high: remove this manual connection opening
    //Established Connection as it is not inhereted from Base Model.
    this.connection = new Connection().createConnection();

    //Transaction to process because in case of any crash all record should truncate which is saved in previous table.
    return this.connection.transaction(t => {

      item = BaseModel.extendItem(item, true);
      //Save Campus Evaluation Sheet
      return CREvaluationSheet.create(item, { transaction: t }).then(cES => {

        //Find All Sections of the Evaluation sheet.
        return ESSection.findAll({ where: { ESId: cES['ESId'] }, attributes: ['id', 'title'], transaction: t }).then(esSections => {

          //Each Evaluation sheet can have multiple Sections so they should save in Promise 
          return Promise.each(esSections, (section) => {

            //Post data of Campus Evaluation sheet Section to be saved.
            var sectionData = {
              CRESId: cES['id'],
              ESSectionId: section['id'],
              title: section['title']
            };

            sectionData = BaseModel.extendItem(sectionData, true);

            return CRESSection.create(sectionData, { transaction: t }).then(sectionResult => {

              //Find Questions of all the section belongs to the Evaluation Sheet
              return ESQuestion.findAll({ where: { ESSectionId: sectionResult.ESSectionId }, attributes: ['id', 'question', 'countField', 'remarksField', 'optionType'], transaction: t }).then(questionResult => {
                return Promise.each(questionResult, (question) => {

                  if (question['optionType']) {
                    //Find Evaluation sheet question options
                    return ESQuestionOption.findAll({ where: { ESQuestionId: question['id'] }, attributes: ['id', 'optionString'], transaction: t }).then(questionOption => {
                      var answers = '';
                      questionOption.forEach(option => {
                        if (answers == '') {
                          answers = option['optionString'];
                        }
                        else {
                          answers = answers + ',' + option['optionString'];

                        }
                      });

                      //Each section have multiple Questions belongs to the Evaluation Sheet.
                      var questionData = {
                        CRESSectionId: sectionResult['id'],
                        ESQuestionId: question['id'],
                        question: question['question'],
                        countField: question['countField'],
                        remarksField: question['remarksField'],
                        optionType: question['optionType'],
                        optionsCSV: answers
                      };

                      //Save Functionality of Campus Evaluation sheet Questions with Options
                      questionData = BaseModel.extendItem(questionData, true);
                      return CRESQuestion.create(questionData, { transaction: t })
                    })
                  }




                });
              })

            })

          })

        });

      })
    });



  }

}

