import { Table, Column, Model, ForeignKey, BelongsTo, HasOne, PrimaryKey } from 'sequelize-typescript';

import { CRApplication } from '../..';

@Table({ timestamps: true })
export class CRRemittance extends Model<CRRemittance> {

  @ForeignKey(() => CRApplication)
  @PrimaryKey
  @Column
  id: number;

  @Column instrumentCategory: string;

  @Column currentDate: Date;

  @Column bankId: number;

  @Column branchName: string;
  @Column bankName: string;

  @Column instrumentDate: Date;

  @Column instrumentNumber: string;

  @Column amount: number;

  @Column purpose: string;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * BelongsTo relationships
   */
  @BelongsTo(() => CRApplication, {
    foreignKey: 'id'
  })
  application: CRApplication;
}
