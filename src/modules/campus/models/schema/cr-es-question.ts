import { Table, Column, Model, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { CRESSection } from '../..';

@Table({ timestamps: true })
export class CRESQuestion extends Model<CRESQuestion> {

  @ForeignKey(() => CRESSection)
  @Column CRESSectionId: number;

  @Column ESQuestionId: number;
  @Column question: string;
  @Column ESTitle: string;
  @Column status: string;
  @Column remarks: string;
  @Column optionType: string;
  @Column optionsCSV: string;
  @Column answer: string;
  @Column countField: boolean;
  @Column remarksField: boolean;
  @Column countValue: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;


}
