import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { CRApplicant } from './../../';

@Table({ timestamps: true })
export class CRReference extends Model<CRReference> {

  @ForeignKey(() => CRApplicant)
  @Column
  CRApplicantId: number;

  @Column fullName: string;

  @Column mobileNumber: string;

  @Column address: string;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * BelongsTo relationships
   */
  @BelongsTo(() => CRApplicant, {
    foreignKey: 'CRApplicantId'
  })
  applicant: CRApplicant;
}
