import { Table, Column, Model, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { ProgramDetail } from '../../../academic';
import { CampusProgram } from '../..';

@Table({ timestamps: true })
export class CampusProgramDetail extends Model<CampusProgramDetail> {

  @ForeignKey(() => CampusProgram)
  @Column campusProgramId: number;

  @ForeignKey(() => ProgramDetail)
  @Column
  programDetailId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @HasMany(() => ProgramDetail, {
    foreignKey: 'programDetailId'
  })
  programDetail: ProgramDetail;
}
