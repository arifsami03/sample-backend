import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { CampusBuilding } from './campus-building';

@Table({ timestamps: true })
export class CBFloor extends Model<CBFloor> {

  @ForeignKey(() => CampusBuilding)
  @Column CBId: string;

  @Column name: string;

  @Column abbreviation: string;

  @Column code: string;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => CampusBuilding, {
    foreignKey: 'CBId',
  })
  campusBuilding: CampusBuilding

}
