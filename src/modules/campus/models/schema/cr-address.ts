import { Table, Column, Model } from 'sequelize-typescript';


@Table({ timestamps: true })
export class CRAddress extends Model<CRAddress> {

  @Column CRApplicationId: number;
  @Column address: string;
  @Column latitude: number;
  @Column longitude: number;
  @Column buildingAvailableType: boolean;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

}
