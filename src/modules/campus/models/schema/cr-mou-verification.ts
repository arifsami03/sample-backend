import { Table, Column, Model} from 'sequelize-typescript';

@Table({ timestamps: true })
export class CRMOUVerification extends Model<CRMOUVerification> {

  @Column CRApplicationId: number;
  @Column instrumentTypeId: number;
  @Column instrumentNumber: string;
  @Column bankId: number;
  @Column amount: number;
  @Column currentDate: Date;
  @Column instrumentDate: Date;
  @Column bankName: string;
  @Column purpose: string;
  @Column status: string;
  @Column remarks: string;
  @Column account: string;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;


}
