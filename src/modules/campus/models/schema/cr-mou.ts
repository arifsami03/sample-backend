import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class CRMOU extends Model<CRMOU> {

  @Column CRApplicationId: number;
  @Column MOUDate: Date;
  @Column campusTitle: string;
  @Column purpose: string;
  @Column educationLevelId: number;
  @Column academicYear: number;
  @Column semSession: string;
  @Column licenseFeeId: number;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;


}
