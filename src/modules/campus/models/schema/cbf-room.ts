import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { CBFloor } from './cb-floor';

@Table({ timestamps: true })
export class CBFRoom extends Model<CBFRoom> {

  @ForeignKey(() => CBFloor)
  @Column CBFloorId: string;

  @Column name: string;

  @Column abbreviation: string;

  @Column code: string;

  @Column seatingCapability: number;

  @Column classroomType: string;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => CBFloor, {
    foreignKey: 'CBFloorId',
  })
  cbFloor: CBFloor

}
