import { Table, Column, Model, ForeignKey, BelongsTo, HasMany, PrimaryKey } from 'sequelize-typescript';

@Table({ timestamps: true })
export class CPDAccount extends Model<CPDAccount> {

  @Column bankId: number;
  @Column campusId: number;
  @Column programDetailId: number;
  @Column branch: string;
  @Column account: string;
  @Column challanType: string;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;


}
