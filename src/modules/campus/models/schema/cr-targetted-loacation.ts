import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import { CRApplication } from '../..';

@Table({ timestamps: true })
export class CRTargettedLocations extends Model<CRTargettedLocations> {

  @ForeignKey(() => CRApplication)
  @Column
  CRApplicationId: number;

  @Column address: string;

  @Column latitude: number;

  @Column longitude: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * BelongsTo relationships
   */
  @BelongsTo(() => CRApplication, {
    foreignKey: 'CRApplicationId'
  })
  application: CRApplication;
}
