import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Campus } from './campus';

@Table({ timestamps: true })
export class CampusBuilding extends Model<CampusBuilding> {

  @ForeignKey(() => Campus)
  @Column campusId: string;

  @Column name: string;

  @Column abbreviation: string;

  @Column code: string;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => Campus, {
    foreignKey: 'campusId',
  })
  campus: Campus

}
