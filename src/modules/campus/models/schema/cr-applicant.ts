import { Table, Column, Model, HasMany, ForeignKey, BelongsTo, HasOne } from 'sequelize-typescript';

import { CRReference, CRApplication } from '../..';
import { Configuration } from '../../../setting';
import { User } from '../../../security';

@Table({ timestamps: true })
export class CRApplicant extends Model<CRApplicant> {

  @Column fullName: string;

  @Column cnic: string;

  @Column mobileNumber: string;

  @Column email: string;

  @Column applicationType: string;

  @Column ntn: string;

  @Column countryId: number;

  @Column provinceId: number;

  @ForeignKey(() => Configuration)
  @Column
  cityId: number;

  @Column tehsilId: number;

  @Column natureOfWorkId: number;

  @Column approxMonthlyIncome: number;

  @Column nearestBankId: number;
  @Column bankName: string;

  @Column address: string;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * HasMany Relationships
   */

  // Considering CRApplicant Has Only One Campus
  @HasOne(() => CRApplication, { foreignKey: 'CRApplicantId' })
  application: CRApplication;

  @HasMany(() => CRApplication, { foreignKey: 'CRApplicantId' })
  applications: CRApplication[];

  @BelongsTo(() => User, { foreignKey: 'id' })
  user: User;
  /**
   * BelongsTo relationships
   */
  @BelongsTo(() => Configuration, {
    foreignKey: 'cityId'
  })
  city: Configuration;
}
