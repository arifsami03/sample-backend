import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class CRAffiliation extends Model<CRAffiliation> {

  @Column CRApplicationId: number;
  @Column affiliation: string;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

}
