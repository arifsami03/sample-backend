import { Table, Column, Model, ForeignKey, HasMany, BelongsTo } from 'sequelize-typescript';
import { Configuration, WFState } from '../../../setting';

import { CRAvailableBuilding, CRTargettedLocations, CRApplicant, CRAddress } from '../..';
import { InstituteType, EducationLevel } from '../../../institute';

@Table({ timestamps: true })
export class CRApplication extends Model<CRApplication> {

  @ForeignKey(() => CRApplicant)
  @Column
  CRApplicantId: number;

  @Column campusName: string;

  @Column instituteTypeId: number;

  @Column educationLevelId: number;

  @Column tentativeSessionStart: Date;

  @Column applicationType: string;

  @Column campusLocation: string;

  @Column website: string;

  @Column officialEmail: string;

  @Column establishedSince: string;

  @Column MOUApproveRemarks: string;

  @Column rejectRemarks: string;

  @Column noOfCampusTransfer: number;

  @Column stateId: number;

  @Column performedStates: string;

  @Column buildingAvailable: boolean;

  @Column remittanceStatus: boolean;

  @ForeignKey(() => Configuration)
  @Column
  cityId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * HasMany Relationships
   */
  @HasMany(() => CRAvailableBuilding, { foreignKey: 'CRApplicantId' })
  availableBuildings: CRAvailableBuilding[];
  @HasMany(() => CRAddress, { foreignKey: 'CRApplicationId' })
  crAddresses: CRAddress[];

  @HasMany(() => CRTargettedLocations, { foreignKey: 'CRApplicantId' })
  targettedLocations: CRTargettedLocations[];

  /**
   * BelongsTo relationships
   */
  @BelongsTo(() => CRApplicant, {
    foreignKey: 'CRApplicantId'
  })
  applicant: CRApplicant;

  @BelongsTo(() => Configuration, {
    foreignKey: 'cityId'
  })
  city: Configuration;

  @BelongsTo(() => InstituteType, {
    foreignKey: 'instituteTypeId',
    constraints: false
  })
  instituteType: InstituteType;

  @BelongsTo(() => EducationLevel, {
    foreignKey: 'educationLevelId',
    constraints: false
  })
  educationLevel: EducationLevel;

  @BelongsTo(() => WFState, {
    foreignKey: 'stateId',
    constraints: false
  })
  wfState: WFState;
}
