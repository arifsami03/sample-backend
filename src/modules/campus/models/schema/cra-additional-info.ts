import { Table, Column, Model, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';

@Table({ timestamps: true })
export class CRAAdditionalInfo extends Model<CRAAdditionalInfo> {

  @Column CRApplicationId: number;

  @Column question: string;
  @Column answer: string;
  @Column optionType: string;
  @Column optionsCSV: string;
  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;


}
