import { Table, Column, Model, ForeignKey, HasOne } from 'sequelize-typescript';
import { Program } from '../../../academic';
import { CRApplication } from '../..';

@Table({ timestamps: true })
export class CRProgram extends Model<CRProgram> {

  @ForeignKey(() => CRApplication)
  @Column CRApplicationId: number;

  @ForeignKey(() => Program)
  @Column programId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @HasOne(() => CRApplication, {
    foreignKey: 'CRApplicationId'
  })
  application: CRApplication;

  @HasOne(() => Program, {
    foreignKey: 'programId'
  })
  program: Program;
}
