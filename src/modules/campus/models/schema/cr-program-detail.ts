import { Table, Column, Model, ForeignKey, HasMany } from 'sequelize-typescript';
import { ProgramDetail } from '../../../academic';
import { CRProgram } from '../..';

@Table({ timestamps: true })
export class CRProgramDetail extends Model<CRProgramDetail> {

  @ForeignKey(() => CRProgram)
  @Column CRProgramId: number;

  @ForeignKey(() => ProgramDetail)
  @Column
  programDetailId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @HasMany(() => ProgramDetail, {
    foreignKey: 'programDetailId'
  })
  programDetail: ProgramDetail;
}
