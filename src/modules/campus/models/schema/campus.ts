import { Table, Column, Model, ForeignKey, BelongsTo, HasMany, PrimaryKey } from 'sequelize-typescript';
import { CampusAcademicCalendar } from '../../../institute';

@Table({ timestamps: true })
export class Campus extends Model<Campus> {

  @Column CRApplicationId: number;
  @Column campusId: string;
  @Column codeNumber: string;
  @Column campusName: string;
  @Column email: string;
  @Column mobileNumber: string;
  @Column address: string;
  @Column landLineNumber: string;
  @Column status: string;
  @Column academicYear: number;
  @Column academicMonth: number;
  @Column deleted: boolean;

  @Column createdBy: number;
  @Column updatedBy: number;

  /**
   * Has Many Relationships
   */
  @HasMany(() => CampusAcademicCalendar, { foreignKey: 'campusId' })
  campusAcademicCalendar: CampusAcademicCalendar[];


}
