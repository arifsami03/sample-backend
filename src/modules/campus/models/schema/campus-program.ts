import { Table, Column, Model, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { Program } from '../../../academic';
import { Campus, CampusProgramDetail } from '../..';

@Table({ timestamps: true })
export class CampusProgram extends Model<CampusProgram> {

  @ForeignKey(() => Campus)
  @Column campusId: number;

  @ForeignKey(() => Program)
  @Column programId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => Campus, {
    foreignKey: 'campusId'
  })
  campus: Campus;

  @HasMany(() => Program, {
    foreignKey: 'programId'
  })
  program: Program;


  @HasMany(() => CampusProgramDetail, {
    foreignKey: 'campusProgramId'
  })
  campusProgramDetails: CampusProgramDetail[];
}
