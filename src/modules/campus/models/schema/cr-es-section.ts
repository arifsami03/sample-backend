import { Table, Column, Model, ForeignKey } from 'sequelize-typescript';
import { CREvaluationSheet } from '../../index';

@Table({ timestamps: true })
export class CRESSection extends Model<CRESSection> {
  @ForeignKey(() => CREvaluationSheet)
  @Column CRESId: number;

  @Column ESSectionId: number;
  @Column title: string;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;


}
