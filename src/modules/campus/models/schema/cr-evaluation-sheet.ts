import { Table, Column, Model, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { CRApplication } from '../..';

@Table({ timestamps: true })
export class CREvaluationSheet extends Model<CREvaluationSheet> {

  @ForeignKey(() => CRApplication)
  @Column CRApplicationId: number;

  @Column ESCategoryId: number;

  @Column ESId: number;

  @Column ESTitle: string;

  @Column status: string;

  @Column comments: string;

  @Column isRecommendedForApproval: boolean;

  @Column remarks: string;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;


}
