import { BaseModel } from '../../base';
import { UserModel } from '../../security';
import { Promise } from 'bluebird';
import { CRApplication, CRProgramModel, CRAffiliationModel, CRAddressModel, CRAvailableBuildingModel, CRProgramDetailModel, CRAAdditionalInfoModel } from '..';

export class CampusInfoModel extends BaseModel {
  constructor() {
    super(CRApplication);
  }

  getCampusInfo(id) {
    let returnResult = {};

    // return new UserModel().find(id).then(user => {
    // let id = user['CRApplicantId'];
    return this.findByCondition(
      [
        'id',
        'campusName',
        'instituteTypeId',
        'educationLevelId',
        'tentativeSessionStart',
        'campusLocation',
        'buildingAvailable',
        'website',
        'officialEmail',
        'establishedSince',
        'noOfCampusTransfer',
        'applicationType'
      ],
      { CRApplicantId: id }
    )
      .then(campusResult => {
        returnResult = campusResult['dataValues'];

        let campAff = new CRAffiliationModel();

        return campAff.findAllByConditions(['id', 'affiliation'], { CRApplicationId: campusResult.id }).then(affResult => {
          returnResult['affiliations'] = affResult;
        });
      })
      .then(() => {
        let campAddress = new CRAddressModel();
        return campAddress.findAllByConditions(['id', 'address', 'latitude', 'longitude'], { CRApplicationId: returnResult['id'] }).then(camAddressResult => {
          returnResult['addresses'] = camAddressResult;
        });
      })
      .then(() => {
        let buildingAvialable = new CRAvailableBuildingModel();

        return buildingAvialable
          .findByCondition(
          [
            'id',
            'buildingType',
            'rentAgreementUpTo',
            'coverdArea',
            'openArea',
            'totalArea',
            'roomsQuantity',
            'washroomsQuantity',
            'teachingStaffQuantity',
            'labsQuantity',
            'nonTeachingStaffQty',
            'studentsQuantity',
            'playGround',
            'swimmingPool',
            'healthClinic',
            'mosque',
            'cafeteria',
            'transport',
            'library',
            'bankBranch'
          ],
          { CRApplicationId: returnResult['id'] }
          )
          .then(availableBuilding => {
            if (availableBuilding) {
              // availableBuilding = availableBuilding['dataValues'];
              returnResult['buildingType'] = availableBuilding['buildingType'];
              returnResult['rentAgreementUpTo'] = availableBuilding['rentAgreementUpTo'];
              returnResult['coverdArea'] = availableBuilding['coverdArea'];
              returnResult['openArea'] = availableBuilding['openArea'];
              returnResult['totalArea'] = availableBuilding['totalArea'];
              returnResult['roomsQuantity'] = availableBuilding['roomsQuantity'];
              returnResult['washroomsQuantity'] = availableBuilding['washroomsQuantity'];
              returnResult['teachingStaffQuantity'] = availableBuilding['teachingStaffQuantity'];
              returnResult['labsQuantity'] = availableBuilding['labsQuantity'];
              returnResult['nonTeachingStaffQty'] = availableBuilding['nonTeachingStaffQty'];
              returnResult['studentsQuantity'] = availableBuilding['studentsQuantity'];
              returnResult['playGround'] = availableBuilding['playGround'];
              returnResult['swimmingPool'] = availableBuilding['swimmingPool'];
              returnResult['healthClinic'] = availableBuilding['healthClinic'];
              returnResult['mosque'] = availableBuilding['mosque'];
              returnResult['cafeteria'] = availableBuilding['cafeteria'];
              returnResult['transport'] = availableBuilding['transport'];
              returnResult['library'] = availableBuilding['library'];
              returnResult['bankBranch'] = availableBuilding['bankBranch'];
            }
          });
      })
      .then(() => {
        let campProg = new CRProgramModel();

        let cpProDetResult = [];

        return campProg
          .findAllByConditions(['id', 'programId'], { CRApplicationId: returnResult['id'] })
          .then(camProgRes => {
            return Promise.each(camProgRes, camProgItem => {
              return new CRProgramDetailModel().findAllByConditions(['id', 'programDetailId'], { CRProgramId: camProgItem['id'] }).then(camProgDetRes => {
                cpProDetResult.push({
                  programId: camProgItem['programId'],
                  programDetails: camProgDetRes
                });
              });
            });
          })
          .then(() => {
            returnResult['cammpusProgramDetails'] = cpProDetResult;
            return returnResult;
          });
      });

    // });
  }

  updateCampusInfo(id, item) {
    return this.update(id, item).then(result => {
      return this.createAddresses(id, item).then(res => {
        return this.updateAddresses(item)
          .then(res => {
            return this.createBuildingAvailableDetails(id, item);
          })
          .then(res => {
            
            return this.saveCRAAdditionalInfo(id, item['craAdditionalInfo']).then(() => {
              return this.createAffiliations(id, item).then(res => {
                return this.updateAffiliations(item).then(res => {
                  return this.deleteAffiliations(item).then(res => {
                    return this.deleteCRProgram(id).then(res => {
                      return this.saveCRProgram(id, item)
                    });

                  });
                });
              });
            })

          });
      });
    });
  }


  createCampusInfo(item) {
    return this.create(item).then(result => {
      return this.createAddresses(result['id'], item).then(res => {
        return this.updateAddresses(item)
          .then(res => {
            return this.createBuildingAvailableDetails(result['id'], item);
          })
          .then(res => {
            
            return this.saveCRAAdditionalInfo(result['id'], item['craAdditionalInfo']).then(() => {
              return this.createAffiliations(result['id'], item).then(res => {
                return this.updateAffiliations(item).then(res => {
                  return this.deleteAffiliations(item).then(res => {
                    return this.deleteCRProgram(result['id']).then(res => {
                      return this.saveCRProgram(result['id'], item)
                    });

                  });
                });
              });
            })

          });
      });
    });
  }

  private saveCRAAdditionalInfo(id, item) {

    let craAdditionalInfoModel = new CRAAdditionalInfoModel();

    // First Find Additional Informations is present against the CRApplicationId
    return craAdditionalInfoModel.findAllByConditions(['id'], { CRApplicationId: id }).then(craAdditionalRes => {

      // If Additional Information is present then update that.
      if (craAdditionalRes.length != 0) {

        //Update Additional Information of campus
        return this.updateCRAAdditionalInfo(id, item);
      }
      //If Addtional Information is not present
      else {

        // Itterate Array of item
        item.forEach(element => {

          element.forEach(postData => {

            //Create data to be inserted
            let insertItem = {
              CRApplicationId: id,
              optionType: postData['optionType'],
              question: postData['question'],
              optionsCSV: postData['optionsCSV'],
              answer: postData['answer'],
            }

            // Save Record in table CRAAdditionalInfo.
            return craAdditionalInfoModel.create(insertItem);

          });

        });

      }

    })
  }

  private updateCRAAdditionalInfo(id, item) {

    let craAdditionalInfoModel = new CRAAdditionalInfoModel();

    // Iterate each record in item.
    item.forEach(element => {

      element.forEach(postData => {

        // Create an item to be updated in CRAAdditionalInfo
        let insertItem = {
          CRApplicationId: id,
          optionType: postData['optionType'],
          question: postData['question'],
          optionsCSV: postData['optionsCSV'],
          answer: postData['answer'],
        }

        // Update Record.
        return craAdditionalInfoModel.update(postData['id'], insertItem);

      });

    });

  }

  private createAddresses(id, item) {
    let cRef = new CRAddressModel();
    var addresses = [];

    for (var i = 0; i < item['addresses'].length; i++) {
      if ((item['addresses'][i].id == '' || item['addresses'][i].id == null) && item['addresses'][i].address != '' && item['addresses'][i].latitude != '' && item['addresses'][i].longitude != '') {
        addresses.push({
          CRApplicationId: id,
          address: item['addresses'][i].address,
          latitude: item['addresses'][i].latitude,
          longitude: item['addresses'][i].longitude,
          buildingAvailableType: item['buildingAvailable']
        });
      }
    }
    return cRef.sequelizeModel.bulkCreate(addresses);
  }

  private updateAddresses(item) {
    let mcModel = new CRAddressModel();

    return Promise.each(item['addresses'], addressItem => {
      if (addressItem['id'] != '') {
        return mcModel.update(addressItem['id'], { address: addressItem['address'], latitude: addressItem['latitude'], longitude: addressItem['longitude'] }).then(res => { });
      }
    });
  }
  private createBuildingAvailableDetails(id, item) {
    item['CRApplicationId'] = id;
    delete item.CRApplicantId;

    let cAvailableModel = new CRAvailableBuildingModel();
    return cAvailableModel.findAllByConditions(['id'], { CRApplicationId: id }).then(res => {
      if (res && res.length != 0) {
        return this.UpdateBuildingAvailable(res[0]['id'], item);
      } else {
        return cAvailableModel.create(item);
      }
    });
  }
  private UpdateBuildingAvailable(id, item) {
    let cAvailableModel = new CRAvailableBuildingModel();
    return cAvailableModel.update(id, item);
  }
  private createAffiliations(id, item) {
    let cAff = new CRAffiliationModel();
    var affiliations = [];

    for (var i = 0; i < item['affiliations'].length; i++) {
      if (item['affiliations'][i].id == '' && item['affiliations'][i].affiliation != '') {
        affiliations.push({
          CRApplicationId: id,
          affiliation: item['affiliations'][i].affiliation
        });
      }
    }
    return cAff.sequelizeModel.bulkCreate(affiliations);
  }
  private updateAffiliations(item) {
    let cAff = new CRAffiliationModel();

    return Promise.each(item['affiliations'], affiliationItem => {
      if (affiliationItem['id'] != '') {
        return cAff.update(affiliationItem['id'], { affiliation: affiliationItem['affiliation'] }).then(res => { });
      }
    });
  }
  private deleteAffiliations(item) {
    return new Promise((resolve, reject) => {
      let cAff = new CRAffiliationModel();

      if (item['deletedAffiliationIds'].length != 0) {
        var deleteItems = [];
        for (var i = 0; i < item['deletedAffiliationIds'].length; i++) {
          deleteItems.push(item['deletedAffiliationIds'][i].id);
        }

        return cAff.deleteByConditions({ id: deleteItems }).then(res => {
          resolve(res);
        });
      } else {
        resolve(true);
      }
    });
  }

  deleteCRProgram(id) {
    let cPro = new CRProgramModel();
    let cProProDet = new CRProgramDetailModel();
    return cPro.findAllByConditions(['id'], { CRApplicationId: id }).then(res => {
      var deleted = [];
      if (res.length > 0) {
        for (var i = 0; i < res.length; i++) {
          deleted.push(res[i].id);
        }
      }

      return cProProDet.deleteByConditions({ CRProgramId: deleted }).then(() => {
        return cPro.deleteByConditions({ CRApplicationId: id });
      });
    });
  }

  private saveCRProgram(id, item) {
    // return new Promise((resolve, reject) => {

    let cPro = new CRProgramModel();
    let cProProDet = new CRProgramDetailModel();

    if (item['campusPrograms'].length != 0) {
      var campusPrograms = {};

      return Promise.each(item['campusPrograms'], campusProgramItem => {
        return cPro
          .create({
            CRApplicationId: id,
            programId: campusProgramItem['programId']
          })
          .then(res => {
            return Promise.each(campusProgramItem['programDetails'], programDetails => {
              return cProProDet.create({
                CRProgramId: res['dataValues']['id'],
                programDetailId: programDetails
              });
            });
          });
      });

    } else {
    }
  }
}
