import { Promise } from 'bluebird';
import { BaseModel } from '../../base';
import { CREvaluationSheet, CRESSectionModel, CRESQuestionModel } from '..';
import { CONFIGURATIONS } from '../../base/conf/configurations';


export class CREvaluationSheetModel extends BaseModel {
  constructor() {
    super(CREvaluationSheet);
  }

  /**
  * Find evaluation sheet attached for campus with status draft, As per our discussion at time there is only one sheet be 
  * present with status draft 
  */
  findEvaluationSheet(id: number) {
    let result;

    // find evaluation sheet for campus with status 'draft'
    return this.findByCondition(['id', 'ESTitle', 'status', 'isRecommendedForApproval', 'remarks'],
      { CRApplicationId: id, status: CONFIGURATIONS.CAMPUS_EVALUATION_SHEET.STATUS.DRAFT }).then(evaluationSheet => {
        if (evaluationSheet) {

          result = evaluationSheet['dataValues'];

          // find all sections for evaluation sheet
          return new CRESSectionModel().findAllByConditions(['id', 'title'], { CRESId: evaluationSheet.id }).then(esSections => {
            let esQuestionModel = new CRESQuestionModel();
            let sections = [];

            // find all qeustions for each founded section
            return Promise.each(esSections, esSection => {
              return esQuestionModel.findAllByConditions(['id', 'question', 'countField', 'countValue', 'remarksField', 'remarks',
                'optionType', 'optionsCSV', 'answer'], { CRESSectionId: esSection['id'] }).then(esQuestions => {
                  let section = { id: esSection['id'], title: esSection['title'], questions: esQuestions };
                  sections.push(section);

                })

            }).then(() => {
              result.sections = sections;
              return result;
            })
          })
        } else {
          return null;
        }
      });
  }



  /**
  * Find ESSections and their questions against an evaluation sheet 
  */
  findAllESSections(id: number) {
    let result = [];

    return new CRESSectionModel().findAllByConditions(['id', 'title'], { CRESId: id }).then(esSections => {

      let esQuestionModel = new CRESQuestionModel();

      // find all qeustions for each founded section
      return Promise.each(esSections, esSection => {
        return esQuestionModel.findAllByConditions(['id', 'question', 'countField', 'countValue', 'remarksField', 'remarks',
          'optionType', 'optionsCSV', 'answer'], { CRESSectionId: esSection['id'] }).then(esQuestions => {
            let section = { id: esSection['id'], title: esSection['title'], questions: esQuestions };
            result.push(section);

          })

      }).then(() => {
        return result;
      })
    })
  }

  updateEvaluationSheet(id, item) {
    // prepare data to update
    let data = { isRecommendedForApproval: item.isRecommendedForApproval, remarks: item.remarks };

    return super.update(id, data).then(() => {
      let esQuestionModel = new CRESQuestionModel();
      return Promise.each(item.sections, section => {
        return Promise.each(section['questions'], question => {
          return esQuestionModel.update(question['id'], question);
        });
      });
    })
  }

  updateCommentsOfEvaluationSheet(id, item) {

    return super.update(id, item);

  }

  deleteWithAssociatedRecords(id) {
    return new CRESSectionModel().deleteByConditionsWithAssociatedRecords({ CRESId: id }).then(() => {
      return this.delete(id);
    })
  }
  deleteByConditionsWithAssociatedRecords(condition) {

    return this.findAllByConditions(['id'], condition).then(CRESs => {
      let crESSecModel = new CRESSectionModel();
      return Promise.each(CRESs, CRES => {

        return crESSecModel.deleteByConditionsWithAssociatedRecords({ CRESId: CRES['id'] }).then(() => {
          return this.delete(CRES['id']);
        })

      })

    })

  }

  /********************************************************************************************************************************
 * Portion to display related data before deletion
 ********************************************************************************************************************************/


  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Evaluation Sheet';
    retResult['records'] = [];

    return this.findAllByConditions(['id', ['ESTitle', 'title']], condition).then((crESRes) => {

      return Promise.each(crESRes, item => {

        return this.findData(item['dataValues']).then(res => {

          retResult['records'].push(res);

        });

      })
    }).then(() => {

      return retResult;

    });
  }

  private findData(item) {

    let itRes = item;
    itRes['children'] = [];
    let crESSecModel = new CRESSectionModel();

    return crESSecModel.findRelatedDataWithCondition({ CRESId: item['id'] }).then((crESSecRes) => {
      itRes['children'].push(crESSecRes);
    }).then(() => {
      return itRes;
    });
  }

  /********************************************************************************************************************************
   * End of portion to display related data before deletion
   ********************************************************************************************************************************/

}
