import { BaseModel } from '../../base';
import { CRProgram } from '..';
import { CRProgramDetailModel } from './cr-program-detail.model';
import { Promise } from 'bluebird';
import { Program } from '../../academic';


export class CRProgramModel extends BaseModel {
  constructor() {
    super(CRProgram);
  }

  deleteWithAssociatedRecords(id) {
    return new CRProgramDetailModel().deleteByConditions({ CRProgramId: id }).then(() => {
      return this.delete(id);
    })
  }

  deleteByConditionsWithAssociatedRecords(condition) {

    return this.findAllByConditions(['id'], condition).then(CRPrograms => {
      let crpdModel = new CRProgramDetailModel();

      return Promise.each(CRPrograms, CRProgram => {

        return crpdModel.deleteByConditions({ CRProgramId: CRProgram['id'] }).then(() => {

          return this.delete(CRProgram['id']);

        })

      })

    })

  }

  /********************************************************************************************************************************
  * Portion to display related data before deletion
  ********************************************************************************************************************************/


  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Program';
    retResult['records'] = [];

    return this.findAllByConditions(['id'], condition, [{ model: Program, as: 'program', attributes: ['id', 'name'] }]).then((elRes) => {

      return Promise.each(elRes, item => {

        return this.findData(item['dataValues']).then(res => {

          retResult['records'].push(res);

        });

      })
    }).then(() => {

      return retResult;

    });
  }

  private findData(item) {

    let itRes = item;
    itRes['title'] = item['program']['name'];
    itRes['children'] = [];

    let crpdModel = new CRProgramDetailModel();

    return crpdModel.findRelatedDataWithCondition({ CRProgramId: item['id'] }).then((crPdRes) => {
      itRes['children'].push(crPdRes);
    }).then(() => {
      return itRes;
    })
  }

  /********************************************************************************************************************************
   * End of portion to display related data before deletion
   ********************************************************************************************************************************/

}
