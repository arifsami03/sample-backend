import { BaseModel } from '../../base';
import { CRESSection } from '..';
import { CRESQuestionModel } from './cr-es-question.model';
import { Promise } from 'bluebird';


export class CRESSectionModel extends BaseModel {
  constructor() {
    super(CRESSection);
  }
  deleteWithAssociatedRecords(id) {
    return new CRESQuestionModel().deleteByConditions({ CRESSectionId: id }).then(() => {
      return this.delete(id);
    })
  }
  deleteByConditionsWithAssociatedRecords(condition) {

    return this.findAllByConditions(['id'], condition).then(ESSections => {

      let crESQueModel = new CRESQuestionModel();

      return Promise.each(ESSections, ESSection => {

        return crESQueModel.deleteByConditions({ CRESSectionId: ESSection['id'] }).then(() => {
          return this.delete(ESSection['id']);

        })

      })

    })

  }

  /********************************************************************************************************************************
* Portion to display related data before deletion
********************************************************************************************************************************/


  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Section';
    retResult['records'] = [];

    return this.findAllByConditions(['id', ['title', 'title']], condition).then((crESSRes) => {

      return Promise.each(crESSRes, item => {

        return this.findData(item['dataValues']).then(res => {

          retResult['records'].push(res);

        });

      })
    }).then(() => {

      return retResult;

    });
  }

  private findData(item) {

    let itRes = item;
    itRes['children'] = [];
    let crESQueModel = new CRESQuestionModel();

    return crESQueModel.findRelatedDataWithCondition({ CRESSectionId: item['id'] }).then((crESQueRes) => {
      itRes['children'].push(crESQueRes);
    }).then(() => {
      return itRes;
    });
  }

  /********************************************************************************************************************************
   * End of portion to display related data before deletion
   ********************************************************************************************************************************/
}
