import { BaseModel } from '../../base';
import { CRAAdditionalInfo } from '..';
import * as _ from 'lodash';

export class CRAAdditionalInfoModel extends BaseModel {
  constructor() {
    super(CRAAdditionalInfo);
  }

}
