import { Sequelize } from 'sequelize-typescript';
import { BaseModel } from '../../base';
import { CRReference } from '..';
import { UserModel } from '../../security';


export class CRReferenceModel extends BaseModel {
  constructor() {
    super(CRReference);
  }
}
