import { BaseModel } from '../../base';
import { CampusProgram } from '..';
import { CampusProgramDetailModel } from './campus-program-detail.model';
import { Promise } from 'bluebird';


export class CampusProgramModel extends BaseModel {
  constructor() {
    super(CampusProgram);
  }


  deleteWithAssociatedRecords(id) {
    return new CampusProgramDetailModel().deleteByConditions({ campusProgramId: id }).then(() => {
      return this.delete(id);
    })
  }
  deleteByConditionsWithAssociatedRecords(condition) {

    return this.findAllByConditions(['id'], condition).then(campusPrograms => {
let cpdModel = new CampusProgramDetailModel();
      return Promise.each(campusPrograms, campusProgram => {

        return cpdModel.deleteByConditions({ campusProgramId: campusProgram['id'] }).then(() => {

          return this.delete(campusProgram['id']);

        })

      })

    })

  }

}
