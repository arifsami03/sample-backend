import * as express from "express";
import { ErrorHandler } from "../../base/conf/error-handler";
import { ApplicationModel } from '..';
import { Helper } from '../../base/helpers/helper'
export class ApplicationsController {

  constructor() { }

  index(req: express.Request, res: express.Response, next: express.NextFunction) {

    let attributesSelect = ['id', 'CRApplicantId', 'campusName', 'tentativeSessionStart', 'campusLocation', 'buildingAvailable', 'cityId', 'stateId'];

    let query = Helper.query2Condition(req.query);
    
    new ApplicationModel().findAll(attributesSelect, query).then(result => {

      res.json(result);

    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

}
