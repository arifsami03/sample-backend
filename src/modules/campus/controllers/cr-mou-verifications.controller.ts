import * as express from 'express';
 

import { CRMOUVerificationModel } from '..';

export class CRMOUVerificationsController {
  constructor() { }

  /**
   * Create
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CRMOUVerificationModel()
      .create(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }

  /**
 * Update
 * 
 * @param req express.Request
 * @param res express.Response
 * @param next express.NextFunction
 */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;
    new CRMOUVerificationModel()
      .update(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }


  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {

    // new CRMOUVerificationModel().find(req.params.id).then(result => {
    let cond = { CRApplicationId: req.params.CRApplicationId }
    let attributes = ['CRApplicationId', 'instrumentTypeId', 'instrumentNumber', 'bankId', 'amount', 'currentDate', 'instrumentDate', 'bankName', 'purpose', 'status', 'remarks', 'account'];
    
    new CRMOUVerificationModel().findByCondition(attributes, cond).then(result => {
      res.json(result);
    })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  compareMOUAmounts(req: express.Request, res: express.Response, next: express.NextFunction) {

    let applicationId=req.params.applicationId;
    let amount=req.params.amount;

    
    new CRMOUVerificationModel().compareMOUAmounts(applicationId,amount).then(result => {
      res.json(result);
    })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }



}
