import * as express from 'express';

import { ErrorHandler } from '../../base/conf/error-handler';
/**
 * Importing related Models
 */
import { CBFloorModel } from '..';

export class CBFloorsController {
    constructor() { }

    /**
    * retrieve list of all Campus Buildings
    * @param req 
    * @param res 
    * @param next 
    */
    index(req: express.Request, res: express.Response, next: express.NextFunction) {
        new CBFloorModel().findAllWithBuildingAndCampus(['id', 'name', 'abbreviation', 'code']).then(result => {
            res.send(result);
        }).catch(err => {
            ErrorHandler.sendServerError(err, res, next);
        });
    }

    /**
   * Find one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
    find(req: express.Request, res: express.Response, next: express.NextFunction) {
        let id = req.params.id;
        new CBFloorModel()
            .find(id, ['id', 'CBId', 'name', 'abbreviation', 'code'])
            .then(result => {
                res.json(result);
            })
            .catch(err => {
                ErrorHandler.sendServerError(err, res, next);
            });
    }

    /**
  * Create new record
  *
  * @param req express.Request
  * @param res express.Response
  * @param next express.NextFunction
  */
    create(req: express.Request, res: express.Response, next: express.NextFunction) {
        new CBFloorModel().create(req.body).then(result => {
            if (result && !result['error']) {
                res.json(result);
            } else {
                ErrorHandler.send(result, res, next);
            }
        }).catch(err => {
            ErrorHandler.sendServerError(err, res, next);

        });
    }

    /**
   * Update record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
    update(req: express.Request, res: express.Response, next: express.NextFunction) {
        let item = req.body;
        let id = req.params.id;
        new CBFloorModel().update(id, item).then(result => {
            if (result && !result['error']) {
                res.json(result);
            } else {
                ErrorHandler.send(result, res, next);
            }
        }).catch(err => {
            ErrorHandler.sendServerError(err, res, next);

        });
    }

    /**
     * Delete record
     *
     * @param req express.Request
     * @param res express.Response
     * @param next express.NextFunction
     */
    delete(req: express.Request, res: express.Response, next: express.NextFunction) {
        let id = req.params.id;
        new CBFloorModel()
            .deleteWithChilds(id)
            .then(result => {
                res.json(result);
            })
            .catch(err => {
                ErrorHandler.sendServerError(err, res, next);
            });
    }

    /**
    * retrieve list of all Floors with their building and campus name. It is same like findAttributesList with extra relations
    * @param req 
    * @param res 
    * @param next 
    */
    findAttributesListWithCampusAndBuilding(req: express.Request, res: express.Response, next: express.NextFunction) {
        new CBFloorModel().findAllWithBuildingAndCampus(['id', 'name']).then(result => {
            res.send(result);
        }).catch(err => {
            ErrorHandler.sendServerError(err, res, next);
        });
    }




}
