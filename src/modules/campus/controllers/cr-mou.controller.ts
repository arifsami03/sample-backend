import * as express from 'express';


import { ErrorHandler } from "../../base/conf/error-handler";
import { CRMOUModel } from '..';

export class CRMOUController {
  constructor() { }

  /**
   * Create
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {

    new CRMOUModel().create(req.body).then(result => {

      res.json(result);

    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
 * Update
 * 
 * @param req express.Request
 * @param res express.Response
 * @param next express.NextFunction
 */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {

    let item = req.body;
    let id = req.params.id;

    new CRMOUModel().update(id, item).then(result => {

      res.json(result);

    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }


  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {

    // new CRMOUModel().find(req.params.CRApplicationId)

    new CRMOUModel().findByCondition(['id', 'CRApplicationId', 'MOUDate', 'campusTitle', 'purpose', 'educationLevelId', 'academicYear','semSession', 'licenseFeeId'], { CRApplicationId: req.params.CRApplicationId }).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }



}
