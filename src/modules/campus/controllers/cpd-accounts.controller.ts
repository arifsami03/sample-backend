import * as express from 'express';
 

import { CPDAccountModel } from '..';
import { UserModel } from '../../security';

export class CPDAccountsController {
  constructor() { }

  /**
   * Create
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CPDAccountModel()
      .create(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }

  /**
 * Update
 * 
 * @param req express.Request
 * @param res express.Response
 * @param next express.NextFunction
 */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;
    new CPDAccountModel()
      .update(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }


  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CPDAccountModel()
      .find(req.params.id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }
  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  getProgramDetails(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CPDAccountModel()
      .getProgramDetails(req.params.id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }
  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  getProgramDetailsAccountInfo(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CPDAccountModel()
      .getProgramDetailsAccountInfo(req.params.id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }



}
