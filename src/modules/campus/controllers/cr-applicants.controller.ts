import * as express from "express";

import { ErrorHandler } from "../../base/conf/error-handler";

import { CRApplicantModel } from '..';

export class CRApplicantsController {
  constructor() { }



  findPersonalInfo(req: express.Request,
    res: express.Response,
    next: express.NextFunction) {

    new CRApplicantModel().getApplicantPersonalInfo(req.params.userId)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  getApplicantId(req: express.Request,
    res: express.Response,
    next: express.NextFunction) {

    new CRApplicantModel().getApplicantId(req.params.userId)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  getApplicantIdByApplicationId(req: express.Request,
    res: express.Response,
    next: express.NextFunction) {

    new CRApplicantModel().getApplicantIdByApplicationId(req.params.applicationId)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.userId;
    let item = req.body;

    new CRApplicantModel().updateCRApplicantPersonalInfo(id, item).then(result => {

      res.json(result);

    })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Create
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {

    let item = req.body;

    new CRApplicantModel().createCRApplicantPersonalInfo(item).then(result => {

      console.log('----------------------------------')
      console.log(result)

      if (result && !result['error']) {

        res.json(result);

      } else {

        ErrorHandler.send(result, res, next);

      }

    })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

}
