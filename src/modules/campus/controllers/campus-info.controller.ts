import * as express from "express";

import { ErrorHandler } from "../../base/conf/error-handler";
import { CampusInfoModel } from '..';

export class CampusInfoController {
  constructor() { }



  findCampusInfo(req: express.Request,
    res: express.Response,
    next: express.NextFunction) {

    new CampusInfoModel().getCampusInfo(req.params.userId)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  findCampusEducationLevel(req: express.Request, res: express.Response, next: express.NextFunction) {

    new CampusInfoModel().findByCondition(['id', 'educationLevelId'], { CRApplicantId: req.params.id }).then(result => {
      res.json(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Update
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  updateCampusInfo(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;
    let item = req.body;

    new CampusInfoModel().updateCampusInfo(id, item).then(result => {

      res.json(result);

    })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Create
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  createCampusInfo(req: express.Request, res: express.Response, next: express.NextFunction) {

    let item = req.body;

    new CampusInfoModel().createCampusInfo(item).then(result => {

      res.json(result);

    })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

}
