import * as express from 'express';


import { CRApplicationModel, PostRegistrationModel, CREvaluationSheetModel } from '..';
import { CONFIGURATIONS } from '../../base';
import { ErrorHandler } from '../../base/conf/error-handler';
import { WFState } from '../../setting';

export class CampusPostRegistrationsController {
  constructor() { }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    let workflow = CONFIGURATIONS.APPLICATION_WORKFLOW.POST_REGISTRATION;
    let query = req.query;
    new CRApplicationModel().getRegistrationList(workflow, query).then(result => {
      res.json(result);
    }).catch(_error => {
      ErrorHandler.sendServerError(_error, res, next);
    });
  }

  /**
   * Find all workflow states assigned to user
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAllAssignedWFStates(req: express.Request, res: express.Response, next: express.NextFunction) {
    let workflow = CONFIGURATIONS.APPLICATION_WORKFLOW.POST_REGISTRATION;
    new CRApplicationModel().findAllAssignedWFStates(workflow).then(result => {
      res.json(result);
    }).catch(_error => {
      ErrorHandler.sendServerError(_error, res, next);
    });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  save(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CRApplicationModel()
      .save(req.params.id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        ErrorHandler.sendServerError(_error, res, next);
      });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CRApplicationModel()
      .findWithCRApplicant(['id', 'campusName', 'website', 'officialEmail', 'campusLocation'], { id: req.params.id })
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        ErrorHandler.sendServerError(_error, res, next);
      });
  }
  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findRemittanceStatus(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CRApplicationModel()
      .findRemittanceStatus(req.params.crApplicantId )
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        ErrorHandler.sendServerError(_error, res, next);
      });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findStatus(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CRApplicationModel()
      .getRegistrationStatus(req.params.id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        ErrorHandler.sendServerError(_error, res, next);
      });
  }
  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findCRAAdditionalInfo(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CRApplicationModel()
      .findCRAAdditionalInfo(req.params.id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        ErrorHandler.sendServerError(_error, res, next);
      });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  getState(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new CRApplicationModel().findByCondition(['id'], { id: id }, [{ model: WFState, as: 'wfState', attributes: ['id', 'state'] }])
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        ErrorHandler.sendServerError(_error, res, next);
      });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  changeStatus(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CRApplicationModel()
      .changeStatus(req.params.id, { status: req.body.status })
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        ErrorHandler.sendServerError(_error, res, next);
      });
  }
  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  updateCRApplication(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CRApplicationModel()
      .updateCRApplication(req.params.id, req.body)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        ErrorHandler.sendServerError(_error, res, next);
      });
  }
  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findUserId(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CRApplicationModel()
      .findUserId(req.params.id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        ErrorHandler.sendServerError(_error, res, next);
      });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  getSubmitAppData(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CRApplicationModel()
      .getSubmitAppData(req.params.id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        ErrorHandler.sendServerError(_error, res, next);
      });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  forwardForAnalysis(req: express.Request, res: express.Response, next: express.NextFunction) {
    new PostRegistrationModel()
      .forwardForAnalysis(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        ErrorHandler.sendServerError(_error, res, next);
      });
  }

  /**
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findEvaluationSheet(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CREvaluationSheetModel()
      .findEvaluationSheet(req.params.id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        ErrorHandler.sendServerError(_error, res, next);
      });
  }


  /**
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  updateEvaluationSheet(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CREvaluationSheetModel()
      .updateEvaluationSheet(req.params.id, req.body)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        ErrorHandler.sendServerError(_error, res, next);
      });
  }
  /**
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  updateCommentsOfEvaluationSheet(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CREvaluationSheetModel()
      .updateCommentsOfEvaluationSheet(req.params.id, req.body)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        ErrorHandler.sendServerError(_error, res, next);
      });
  }

  /**
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  updateESStatus(req: express.Request, res: express.Response, next: express.NextFunction) {
    let data = { status: CONFIGURATIONS.CAMPUS_EVALUATION_SHEET.STATUS.DONE };

    new CREvaluationSheetModel()
      .update(req.params.id, data)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        ErrorHandler.sendServerError(_error, res, next);
      });
  }

  /**
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAllEvaluationSheets(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CREvaluationSheetModel()
      .findAllByConditions(
        ['id', 'ESTitle', 'status', 'isRecommendedForApproval', 'remarks', 'comments', 'updatedAt'],
        { CRApplicationId: req.params.id, status: CONFIGURATIONS.CAMPUS_EVALUATION_SHEET.STATUS.DONE },
        [], [['updatedAt', 'DESC']]
      )
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        ErrorHandler.sendServerError(_error, res, next);
      });
  }

  /**
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAllESSections(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CREvaluationSheetModel()
      .findAllESSections(req.params.id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        ErrorHandler.sendServerError(_error, res, next);
      });
  }
}
