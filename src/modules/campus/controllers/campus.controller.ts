import * as express from 'express';


import { CampusModel } from '..';
import { ErrorHandler } from '../../base/conf/error-handler';
import { CONFIGURATIONS } from '../../base';

export class CampusController {
  constructor() { }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CampusModel().findAllByConditions(['id', 'campusId', 'codeNumber', 'campusName', 'email'], { status: CONFIGURATIONS.CMAPUS_STATUS.DONE }).then(result => {
      res.json(result);
    }).catch(_error => {
      next(_error);
    });
  }


  /**
   * Create
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CampusModel()
      .createWithRelatedData(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }

  /**
 * Update
 * 
 * @param req express.Request
 * @param res express.Response
 * @param next express.NextFunction
 */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;
    new CampusModel()
      .updateWithRelatedData(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }


  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {

    new CampusModel().findWithRelatedData(req.params.CRApplicationId)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findCampus(req: express.Request, res: express.Response, next: express.NextFunction) {

    new CampusModel().findCampusWithRelatedData(req.params.id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesData(req: express.Request, res: express.Response, next: express.NextFunction) {

    new CampusModel().findByCondition(['id', 'campusName'], { id: req.params.id, status: CONFIGURATIONS.CMAPUS_STATUS.DONE })
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }

  /**
   * Update campus status to be done
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  approveCampus(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.CRApplicationId
    new CampusModel().approveCampus(id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }

  /**
   * Update campus status to be close
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  rejectCampus(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.CRApplicationId
    new CampusModel().rejectCampus(id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }


  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesList(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CampusModel()
      .findAll(['id', 'campusName'])
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Find all campueses with status done
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAllCampuses(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CampusModel()
      .findAllByConditions(['id', 'campusName'], { status: CONFIGURATIONS.CMAPUS_STATUS.DONE })
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  getCampusByProgramDetailId(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CampusModel()
      .getCampusByProgramDetailId(req.params.programDetailId)
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }


}
