import * as express from 'express';
import { CONFIGURATIONS } from '../../base';
import { CRApplicationModel } from '..';
import { ErrorHandler } from '../../base/conf/error-handler';

export class CampusPreRegistrationsController {
  constructor() { }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    let workflow = CONFIGURATIONS.APPLICATION_WORKFLOW.PRE_REGISTRATION;
    let query = req.query;
    new CRApplicationModel().getRegistrationList(workflow, query).then(result => {
      res.json(result);
    }).catch(_error => {
      ErrorHandler.sendServerError(_error, res, next);
    });
  }

  /**
   * Find all workflow states assigned to user
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAllAssignedWFStates(req: express.Request, res: express.Response, next: express.NextFunction) {
    let workflow = CONFIGURATIONS.APPLICATION_WORKFLOW.PRE_REGISTRATION;
    new CRApplicationModel().findAllAssignedWFStates(workflow).then(result => {
      res.json(result);
    }).catch(_error => {
      ErrorHandler.sendServerError(_error, res, next);
    });
  }


  /**
   * Create
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CRApplicationModel().preRegistrationSave(req.body).then(result => {
      res.json(result);
    }).catch(_error => {
      ErrorHandler.sendServerError(_error, res, next);
    });
  }


  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CRApplicationModel().findPreRegistration(req.params.id).then(result => {
      res.json(result);
    }).catch(_error => {
      ErrorHandler.sendServerError(_error, res, next);
    });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findUserId(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CRApplicationModel().findUserId(req.params.id).then(result => {
      res.json(result);
    }).catch(_error => {
      ErrorHandler.sendServerError(_error, res, next);
    });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  activeUser(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CRApplicationModel().activeUser(req.params.id, req.body).then(result => {
      res.json(result);
    }).catch(_error => {
      ErrorHandler.sendServerError(_error, res, next);
    });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  validateUniqueEmail(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CRApplicationModel().validateUniqueEmail(req.body).then(result => {
      // if email exists sends duplicate entry error
      if (result) {
        ErrorHandler.send(ErrorHandler.duplicateEmail, res, next);
      } else {
        res.json(true);
      }
    }).catch(_error => {
      ErrorHandler.sendServerError(_error, res, next);
    });
  }
  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  changeStatus(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CRApplicationModel().changeStatus(req.params.id, { status: req.body.status }).then(result => {
      res.json(result);
    }).catch(_error => {
      ErrorHandler.sendServerError(_error, res, next);
    });
  }
}
