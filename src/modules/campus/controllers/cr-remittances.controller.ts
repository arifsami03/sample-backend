import * as express from 'express';
 
import { CRRemittanceModel } from '..';

export class CRRemittancesController {
  constructor() {}

  /**
   * Create
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  save(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new CRRemittanceModel()
      .save(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }
  /**
   * view
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  view(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new CRRemittanceModel()
      .view(req.params.id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }
}
