
var faker = require('faker/locale/pk');
import { AcademicCalendarModel, AcademicCalendarProgramDetailModel, ProgramDetailModel, ProgramModel } from '../../academic';
import { FeeHeadsModel, ScholarshipModel } from '../../fee-management';
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import { Promise } from 'bluebird';

export class ScholarShipsFaker {


  public educationLevelId;
  public types = ['annual', 'monthly'];
  public percentages = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

  public feeHeadIds = [];


  constructor() {

  }

  public init() {

    return this.getFeeHeadsId().then(() => {


      return this.insertFakeData();
    })



  }

  getFeeHeadsId() {
    return new FeeHeadsModel().findAll().then(feeHeadIds => {

      feeHeadIds.forEach(element => {

        this.feeHeadIds.push(element['id']);

      });


    });
  }


  private insertFakeData() {

    let scholarShipsModel = new ScholarshipModel();
    let scholarships = [
      { title: "Punjab Educational Endowment Fund", abbreviation: "PEEF", type: this.types[Math.floor(Math.random() * this.types.length)], percentage: this.percentages[Math.floor(Math.random() * this.percentages.length)], feeHeadId: [] },
      { title: "Academic Scholarship", abbreviation: "ACAD", type: this.types[Math.floor(Math.random() * this.types.length)], percentage: this.percentages[Math.floor(Math.random() * this.percentages.length)], feeHeadId: [] },
      { title: "Merit Scholarship", abbreviation: "MERIT", type: this.types[Math.floor(Math.random() * this.types.length)], percentage: this.percentages[Math.floor(Math.random() * this.percentages.length)], feeHeadId: [] },
    ]

    return Promise.each(scholarships, item => {


      item['feeHeadId'].push(this.feeHeadIds[Math.floor(Math.random() * this.feeHeadIds.length)])
      item['feeHeadId'].push(this.feeHeadIds[Math.floor(Math.random() * this.feeHeadIds.length)])


      console.log(item);
      return scholarShipsModel.createWithFeeHeads(item);


    })
  }


}
