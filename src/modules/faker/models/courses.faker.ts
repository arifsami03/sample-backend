
var faker = require('faker/locale/pk');
import { CourseModel } from '../../academic';
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import { Promise } from 'bluebird';

export class CoursesFaker {


  public coursesList = [
    { title: "Introduction to Computing", abbreviation: "CMP" , description:'course' },
    { title: "Programming Fundmentals", abbreviation: "cmp" , description:'course' },
    { title: "Discrete Mathematics", abbreviation: "CMP" , description:'course' },
    { title: "Writing Workshop", abbreviation: "EN" , description:'course' },
    { title: "Digital Logic Design", abbreviation: "CMP" , description:'course' },
    { title: "Data Communication and Computer Networks", abbreviation: "CMP" , description:'course' },
    { title: "Database Systems", abbreviation: "CMP" , description:'course' },
    { title: "Software Engineering", abbreviation: "CMP" , description:'course' },
    { title: "Business and Technical Communication", abbreviation: "CMP" , description:'course' },
    { title: "Project Management", abbreviation: "CMP" , description:'course' },
    { title: "Theory of Automata", abbreviation: "CMP" , description:'course' },
    { title: "Data Structure and Algorithms", abbreviation: "CMP" , description:'course' },
    { title: "Database Administration", abbreviation: "CMP" , description:'course' },
    { title: "Claculus-I", abbreviation: "MA" , description:'course' },
    { title: "Calculus-II", abbreviation: "MA" , description:'course' },
    { title: "Basic Electronics", abbreviation: "NS" , description:'course' },
    { title: "Computer Organization and Assembly Language", abbreviation: "CMP" , description:'course' },
    { title: "Linear Algebra", abbreviation: "MA" , description:'course' },
    { title: "Object Oriented Analysis and Design", abbreviation: "CMP" , description:'course' },
    { title: "Enterprise Application Developement (.NET)", abbreviation: "CMP" , description:'course' },
    { title: "Artificial Intelligence", abbreviation: "CMP" , description:'course' },
    { title: "Operating Systems", abbreviation: "cmp" , description:'course' },
    { title: "Mobile Computing", abbreviation: "CMP" , description:'course' },
    { title: "Analysis of Algorithms", abbreviation: "CMP" , description:'course' },
    { title: "Software Quality Assurance", abbreviation: "CMP" , description:'course' },
    { title: "Final Year Project", abbreviation: "FYP" , description:'course' },
    { title: "Assembly Language", abbreviation: "CMP" , description:'course' },
    { title: "Electricity and Megnetism", abbreviation: "NS" , description:'course' },
    { title: "Probability and Statistics", abbreviation: "MA" , description:'course' },
    { title: "Programming Fundamentals Lab", abbreviation: "CMP" , description:'course' },
    { title: "Digital Logic and Design Lab", abbreviation: "CMP" , description:'course' },
    { title: "Object Oriented Programming", abbreviation: "CMP" , description:'course' },
    { title: "Humanities Elective", abbreviation: "HM" , description:'course' },
    { title: "Social Science Elective", abbreviation: "SS" , description:'course' },
    { title: "Object Oriented Programming Labs", abbreviation: "CMP" , description:'course' },
    { title: "Computer Organization and Assembly Language Lab", abbreviation: "CMP" ,description:'course'}
  ]

  constructor() {

  }

  public init() {

    return this.insertFakeData();

  }


  private insertFakeData() {

    let benchMark = new Date();

    let courseModel = new CourseModel();


    return Promise.each((this.coursesList), (item) => {

      return courseModel.create(item)

    });
  }


}
