
var faker = require('faker/locale/pk');
import { ClassesModel, ProgramDetailModel, ProgramModel, CourseModel } from '../../academic';
import { EducationLevelModel } from '../../institute';
import { TimeTableModel, SectionModel, SlotModel, TTDetailModel } from '../../time-table';
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import { Promise } from 'bluebird';
import * as _ from 'lodash';

export class TimeTableFaker {


  public educationLevelId = [];
  public progamIds = [];
  public progamDetailIds = [];
  public days = ['MON', 'TUE', 'WED', 'THR', 'FRI', 'SAT', 'SUN']
  public sectionIds = [];
  public courseIds = [];
  public slotIds = [];


  constructor() {

  }

  public init() {

    return this.getEducationLevelId().then(() => {

      return this.getProgram().then(() => {

        return this.getProgramDetails().then(() => {

          return this.getCourses().then(() => {

            return this.getSections().then(() => {

              return this.getSlots().then(() => {

                return this.insertFakeData();
              })
            })

          })

        });

      })
    })



  }

  getProgram() {
    this.progamIds = [];

    return Promise.each(this.educationLevelId, edId => {

      return new ProgramModel().findAllByConditions(['id'], { educationLevelId: edId }).then((programs) => {

        programs.forEach(element => {

          this.progamIds.push(element['id'])

        });

      })

    })
  }

  getSections() {

    return new SectionModel().findAll().then(sectionIds => {

      sectionIds.forEach(element => {

        this.sectionIds.push(element['id'])

      });

    })
  }
  getCourses() {

    return new CourseModel().findAll().then(courseIds => {

      courseIds.forEach(element => {

        this.courseIds.push(element['id'])

      });

    })
  }
  getSlots() {

    return new SlotModel().findAll().then(slotIds => {

      slotIds.forEach(element => {

        this.slotIds.push(element['id'])

      });

    })
  }

  getProgramDetails() {

    let programDetailModel = new ProgramDetailModel();
    this.progamDetailIds = [];

    return Promise.each(this.progamIds, item => {

      return programDetailModel.findAllByConditions(['id'], { programId: item }).then(programDetaiilIds => {

        programDetaiilIds.forEach(element => {

          this.progamDetailIds.push(element['id']);


        });


      })


    })

  }
  getEducationLevelId() {
    return new EducationLevelModel().findAllByConditions(['id'], { education: 'University' }).then(educationLevelId => {

      educationLevelId.forEach(element => {

        this.educationLevelId.push(element['id']);

      });


    });
  } 
  
  titleCase(str) {
    str = str.toLowerCase().split(' ');                // will split the string delimited by space into an array of words

    for (var i = 0; i < str.length; i++) {               // str.length holds the number of occurrences of the array...
      str[i] = str[i].split('');                    // splits the array occurrence into an array of letters
      str[i][0] = str[i][0].toUpperCase();          // converts the first occurrence of the array to uppercase
      str[i] = str[i].join('');                     // converts the array of letters back into a word.
    }
    return str.join(' ');                              //  converts the array of words back to a sentence.
  }

  private insertFakeData() {
    let classesModel = new ClassesModel();
    let timeTableModel = new TimeTableModel();


    return Promise.each(this.progamDetailIds, pdId => {

      return classesModel.findAllByConditions(['id'], { programDetailId: pdId }).then(classes => {

        let classIds = [];
        classes.forEach(element => {

          classIds.push(element['id'])

        });

        let postData = {
          programDetailId: pdId,
          classId: classIds[Math.floor(Math.random() * classIds.length)],
          title: this.titleCase(faker.lorem.words())
        }
        return timeTableModel.create(postData).then(timeTable => {

          return this.savaTimeTableDetails(timeTable['id'])

        })

      })



    });
  }

  savaTimeTableDetails(ttId) {

    console.log('i am here')
    let slotOrder = 0;
    let tTDetailModel = new TTDetailModel();

    return Promise.each(this.slotIds, slotId => {

      slotOrder++;

      let selDays = [];
      let selSections = [];
      for (let i = 0; i < 3; i++) {

        
        selDays.push(this.days[Math.floor(Math.random() * this.days.length)])
        selSections.push(this.sectionIds[Math.floor(Math.random() * this.sectionIds.length)])


      }
      selDays=_.sortedUniq(selDays);
      selSections=_.sortedUniq(selSections);
      let postData = {
        dayIds: selDays,
        sectionIds: selSections,
        ttDetail: {
          courseId: this.courseIds[Math.floor(Math.random() * this.courseIds.length)],
          slotId: slotId,
          slotOrder: slotOrder,
          timeTableId: ttId
        }

      }
      return tTDetailModel.createByRelatedData(postData);

    })




  }


}
