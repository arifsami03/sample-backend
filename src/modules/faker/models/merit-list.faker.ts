
var faker = require('faker/locale/pk');
import { AcademicCalendarModel, AcademicCalendarProgramDetailModel, ProgramDetailModel, ProgramModel } from '../../academic';
import { FeeHeadsModel, ScholarshipModel } from '../../fee-management';
import { MeritListModel } from '../../admission';
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import { Promise } from 'bluebird';

export class MeritListFaker {


  public educationLevelId;
  public types = ['annual', 'monthly'];
  public percentageFrom = [0, 10, 15, 20, 25, 30, 35, 40, 45, 50];
  public percentageTo = [55, 60, 65, 70, 75, 80, 85, 90, 95, 100];

  public pdIds = [];
  public scholarshipIds = [];


  constructor() {

  }

  public init() {

    return this.getProgramDetails().then(() => {

      return this.getScholarships().then(() => {


        return this.insertFakeData();
      })
    })



  }

  getProgramDetails() {
    return new ProgramDetailModel().findAll(['id']).then(pdIds => {

      pdIds.forEach(element => {

        this.pdIds.push(element['id']);

      });


    });
  }
  getScholarships() {
    return new ScholarshipModel().findAll().then(scholarshipIds => {

      scholarshipIds.forEach(element => {

        this.scholarshipIds.push(element['id']);

      });


    });
  }

  titleCase(str) {
    str = str.toLowerCase().split(' ');                // will split the string delimited by space into an array of words

    for (var i = 0; i < str.length; i++) {               // str.length holds the number of occurrences of the array...
      str[i] = str[i].split('');                    // splits the array occurrence into an array of letters
      str[i][0] = str[i][0].toUpperCase();          // converts the first occurrence of the array to uppercase
      str[i] = str[i].join('');                     // converts the array of letters back into a word.
    }
    return str.join(' ');                              //  converts the array of words back to a sentence.
  }
  private insertFakeData() {

    let meritListModel = new MeritListModel();


    return Promise.each((this.pdIds), (item) => {

      let postData = this.getMeritListCredentials(item);

      return meritListModel.create(postData);


    })
  }

  private getMeritListCredentials(pdId) {

    let item = {};
    item["academicYear"] = faker.phone.phoneNumber('####');
    item["numberOfSeats"] = faker.phone.phoneNumber('####');
    item["percentageFrom"] = this.percentageFrom[Math.floor(Math.random() * this.percentageFrom.length)];
    item["percentageTo"] = this.percentageTo[Math.floor(Math.random() * this.percentageTo.length)];
    item["title"] = this.titleCase(faker.lorem.words());
    item["scholarshipId"] = this.scholarshipIds[Math.floor(Math.random() * this.scholarshipIds.length)];
    item["programDetailId"] = pdId;


    return item;

  }


}
