
import { EligibilityCriteriaFaker } from './eligibility-criteria.faker';
import { UserFaker } from './user.faker';
import { CoursesFaker } from './courses.faker';
import { ClassesFaker } from './classes.faker';
import { AcademicCalendarFaker } from './academic-calendar.faker';
import { RoadMapFaker } from './road-map.faker';
import { LicenseFeeFaker } from './licenseFee.faker';
import { SlotFaker } from './slots.faker';
import { SectionsFaker } from './sections.faker';
import { TimeTableFaker } from './timeTable.faker';
import { PassingYearFaker } from './passing-year.faker';
import { NatureOfFeeHeadFaker } from './nature-of-fee-head.faker';
import { FeeHeadsFaker } from './fee-heads.faker';
import { ScholarShipsFaker } from './scholarships.faker';
import { MeritListFaker } from './merit-list.faker';
import { CampusPreRegistrationFaker } from './campus-pre-registration.faker';
import { CampusPostRegistrationFaker } from './campus-post-registration.faker';

export class BaseFaker {

  constructor() {
    this.init();
  }



  public init() {

    let startTime = new Date();

    // return new UserFaker().init().then(() => {
    //   return new CoursesFaker().init().then(() => {

    //     return new ClassesFaker().init()

    //   }).then(() => {

    //     return new AcademicCalendarFaker().init();

    //   }).then(() => {

    //     return new RoadMapFaker().init().then(() => {

    //       return new LicenseFeeFaker().init().then(() => {

    //         return new SlotFaker().init().then(() => {

    //           return new SectionsFaker().init().then(() => {

    //             return new TimeTableFaker().init().then(() => {

    //               return new PassingYearFaker().init().then(() => {

    //                 return new NatureOfFeeHeadFaker().init().then(() => {

    //                   return new FeeHeadsFaker().init().then(() => {

    //                     return new ScholarShipsFaker().init().then(() => {

    //                       return new MeritListFaker().init().then(() => {
    //                         return new EligibilityCriteriaFaker().init();
    //                       })

    //                     })

    //                   })
    //                 })
    //               })

    //             })

    //           })
    //         })
    //       })

    //     });

    //   }).then(() => {

    return new CampusPreRegistrationFaker().init().then(() => {

      return new CampusPostRegistrationFaker().init();


    });
    //   })
    // }).then(() => {

    //   let endTime = new Date();

    //   console.log('-----------------------------------------------------');
    //   console.log('start on ', startTime);
    //   console.log('end on ', endTime);

    //   console.log('-----------------------------------------------------');
    // })




  }
}
