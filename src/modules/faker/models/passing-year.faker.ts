
var faker = require('faker/locale/pk');
import { ClassesModel, ProgramDetailModel, ProgramModel } from '../../academic';
import { PassingYearModel } from '../../setting';
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import { Promise } from 'bluebird';

export class PassingYearFaker {

  constructor() {

  }

  public init() {

    return this.insertFakeData();

  }




  private insertFakeData() {
    let passingYearModel = new PassingYearModel();

      let postData = [
        { year: "2000", annual: true, supplimentary:false },
        { year: "2001", annual: false, supplimentary:true },
        { year: "2002", annual: true, supplimentary:true },
        { year: "2003", annual: false, supplimentary:false },
        { year: "2004", annual: true, supplimentary:false },
        { year: "2005", annual: false, supplimentary:true },
        { year: "2006", annual: true, supplimentary:false },
        { year: "2007", annual: true, supplimentary:true },
        { year: "2008", annual: false, supplimentary:false },
        { year: "2009", annual: true, supplimentary:false },
        { year: "2010", annual: false, supplimentary:true },
        { year: "2011", annual: true, supplimentary:true },
        { year: "2012", annual: false, supplimentary:false },
        { year: "2013", annual: true, supplimentary:true },
        { year: "2014", annual: false, supplimentary:false },
        { year: "2015", annual: true, supplimentary:false },
        { year: "2016", annual: false, supplimentary:true },
        { year: "2017", annual: true, supplimentary:false },
      ]


      return passingYearModel.sequelizeModel.bulkCreate(postData)


  }


}
