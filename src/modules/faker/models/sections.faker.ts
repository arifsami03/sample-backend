
var faker = require('faker/locale/pk');
import { SectionModel } from '../../time-table';
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import { Promise } from 'bluebird';

export class SectionsFaker {

  constructor() {

  }

  public init() {

    return this.insertFakeData();

  }




  private insertFakeData() {
    let sectionModel = new SectionModel();

      let postData = [
        { title: "Section A", abbreviation: 'A' ,sectionId: 'secA' },
        { title: "Section B", abbreviation: 'B' ,sectionId: 'secB' },
        { title: "Section C", abbreviation: 'C' ,sectionId: 'secC' },
        { title: "Section D", abbreviation: 'D' ,sectionId: 'secD' },
        { title: "Section E", abbreviation: 'E' ,sectionId: 'secE' },
        { title: "Section F", abbreviation: 'F' ,sectionId: 'secF' },
      ]


      return sectionModel.sequelizeModel.bulkCreate(postData)


  }


}
