
var faker = require('faker/locale/pk');
import { UserModel } from '../../security';
import { CRApplicantModel, CRApplicationModel } from '../../campus';
import { ConfigurationModel, WFStateModel } from '../../setting';
import { InstituteModel } from '../../institute';
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import { Promise } from 'bluebird';

export class CampusPreRegistrationFaker {


  public tf = [true, false];
  public maleFemale = ['male', 'female'];
  public applicationType = ['create', 'transfer'];
  public cityIds = [];
  public instituteIds = [];
  public stateId = [];

  constructor() {

  }

  public init() {

    return this.getConfData().then(() => {
      return this.insertFakeData();
    })


  }
  getCities() {

    return new ConfigurationModel().findAllByConditions(['id'], { key: 'city' }).then(cities => {

      cities.forEach(element => {

        this.cityIds.push(element['id'])

      });
    }
    )

  }
  getInstituteTypes() {

    return new InstituteModel().findAll(['id']).then(institutes => {
      institutes.forEach(element => {

        this.instituteIds.push(element['id'])

      });
    })

  }
  getStateId() {

    return new WFStateModel().findAllByConditions(['id'], { WFId: 'pre-registration', state: { [Sequelize.Op.ne]: 'new' } }).then(stateId => {
      stateId.forEach(element => {

        this.stateId.push(element['id'])

      });
    });

  }
  getConfData() {
    return this.getCities().then(() => {
      return this.getInstituteTypes().then(() => {
        return this.getStateId();
      })
    });



  }

  private insertFakeData() {

    let benchMark = new Date();

    let userModel = new UserModel();
    let applicantModel = new CRApplicantModel();
    let CRApplicantionModel = new CRApplicationModel();



    let tmpAry = [];
    for (let x = 0; x < 50; x++) { tmpAry.push(x); }

    return Promise.each((tmpAry), (tmpItem) => {


      let item = this.getApplicant();

      return applicantModel.findByCondition(['email'], { email: item['email'] }).then((user) => {
        if (user) {
          item = this.getApplicant();
        }
        else {
          return applicantModel.create(item).then(applicantId => {

            let user = this.getUser(applicantId);

            return userModel.findByCondition(['username'], { username: applicantId['email'] }).then((userData) => {
              if (userData) {
                item = this.getUser(applicantId);
              }
              else {
                return userModel.create(user).then(userData => {
                  let application = this.getCRApplication(applicantId);
                  return CRApplicantionModel.create(application);
                })
              }
            })

          })
        }
      })
    }).then(() => {
      let benchMark2 = new Date();
      console.log('---------------------------------------------------');
      console.log(benchMark);
      console.log(benchMark2);

    })
  }

  private getApplicant() {

    let item = {};



    let name = [
      { fn: faker.name.firstName(0), ln: faker.name.lastName(1) },
      { fn: faker.name.firstName(1), ln: faker.name.lastName(1) },
    ]

    // let fullName = fn + ' ' + ln;
    let fn = name[Math.floor(Math.random() * name.length)].fn;
    let ln = name[Math.floor(Math.random() * name.length)].ln;
    let fullName = fn + ' ' + ln;

    item["fullName"] = fullName;
    item["mobileNumber"] = faker.phone.phoneNumber('92-###-#######');

    // item["email"] = faker.internet.email().toLowerCase();
    item["email"] = faker.internet.email(fn, ln).toLowerCase();

    item["applicationType"] = this.applicationType[Math.floor(Math.random() * this.applicationType.length)];
    item["cnic"] = faker.phone.phoneNumber('#####-########-#')

    return item;
  }

  private getUser(applicantId) {

    let item = {};
    item["username"] = applicantId['email'];
    item["password"] = '$2b$10$5ToVb1w/tyCmlFSd.yXhUOgUoNm//nj/C1/Pvy7AqlIJ//VM0DcjG';
    item["isActive"] = true;
    item["portal"] = 'registration';
    item["CRApplicantId"] = applicantId['id'];

    return item;
  }
  private getCRApplication(applicantId) {


    let item = {};
    item["CRApplicantId"] = applicantId['id'];
    item["campusName"] = '';
    item["cityId"] = this.cityIds[Math.floor(Math.random() * this.cityIds.length)];
    item["instituteTypeId"] = this.instituteIds[Math.floor(Math.random() * this.instituteIds.length)];
    item["educationLevelId"] = 0;
    item["stateId"] = this.stateId[Math.floor(Math.random() * this.stateId.length)];
    item["applicationType"] = applicantId['applicationType'];

    return item;



  }

}
