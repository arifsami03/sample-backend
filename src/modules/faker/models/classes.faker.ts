
var faker = require('faker/locale/pk');
import { ClassesModel, ProgramDetailModel, ProgramModel } from '../../academic';
import { EducationLevelModel } from '../../institute';
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import { Promise } from 'bluebird';

export class ClassesFaker {


  public educationLevelId;
  public progamIds = [];
  public progamDetailIds = [];


  constructor() {

  }

  public init() {

    return this.getEducationLevelId().then(() => {

      return this.getProgram().then(() => {

        return this.getProgramDetails().then(() => {

          return this.insertFakeData();

        });

      })
    })



  }

  getProgram() {

    return new ProgramModel().findAllByConditions(['id'], { educationLevelId: this.educationLevelId }).then((programs) => {

      programs.forEach(element => {

        this.progamIds.push(element['id'])

      });

    })

  }
  getProgramDetails() {

    let programDetailModel = new ProgramDetailModel();
    this.progamDetailIds = [];

    return Promise.each(this.progamIds, item => {

      return programDetailModel.findAllByConditions(['id'], { programId: item }).then(programDetaiilIds => {

        programDetaiilIds.forEach(element => {

          this.progamDetailIds.push(element['id']);


        });

      })


    })

  }
  getEducationLevelId() {
    return new EducationLevelModel().findByCondition(['id'], { education: 'University' }).then(educationLevelId => {

      this.educationLevelId = educationLevelId['id'];

    });
  }


  private insertFakeData() {
    let classesModel = new ClassesModel();


    return Promise.each(this.progamDetailIds, pdId => {

      let postData = [
        { name: "Semester-I", programDetailId: pdId },
        { name: "Semester-II", programDetailId: pdId },
        { name: "Semester-III", programDetailId: pdId },
        { name: "Semester-IV", programDetailId: pdId },
        { name: "Semester-V", programDetailId: pdId },
        { name: "Semester-VI", programDetailId: pdId },
        { name: "Semester-VII", programDetailId: pdId },
        { name: "Semester-VIII", programDetailId: pdId },
      ]


      return classesModel.sequelizeModel.bulkCreate(postData)


    });
  }


}
