
var faker = require('faker/locale/pk');
import { UserModel } from '../../security'
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import { Promise } from 'bluebird';
import { CONFIGURATIONS } from '../../base';

export class UserFaker {


  public tf = [true, false];
  public maleFemale = ['male', 'female'];

  constructor() {

  }

  public init() {

    return this.insertFakeData();

  }

  private insertFakeData() {

    let userModel = new UserModel();

    let tmpAry = [];
    for (let x = 0; x < 0; x++) { tmpAry.push(x); }

    return Promise.each((tmpAry), (tmpItem) => {


      let item = this.getUser();

      return userModel.findByCondition(['username'], { username: item['username'] }).then((user) => {
        if (user) {
          item = this.getUser();
        }
        else {
          return userModel.create(item);
        }
      })
    }).then(() => {
      console.log('all done');
    })
  }

  private getUser() {

    let item = {};
    item["username"] = faker.internet.email().toLowerCase();
    item["password"] = '$2b$10$5ToVb1w/tyCmlFSd.yXhUOgUoNm//nj/C1/Pvy7AqlIJ//VM0DcjG';
    item["isActive"] = this.tf[Math.floor(Math.random() * this.tf.length)];
    item["portal"] = CONFIGURATIONS.PORTAL.INSTITUTE;
    item["name"] = faker.name.findName();
    item["address"] = faker.address.streetName() + ' , ' + faker.address.streetAddress() + ' , PakistanS';
    item["phoneNumber"] = faker.phone.phoneNumber('##-###-#######');
    item["gender"] = this.maleFemale[Math.floor(Math.random() * this.maleFemale.length)];

    return item;
  }

}
