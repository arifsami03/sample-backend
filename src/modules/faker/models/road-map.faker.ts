
var faker = require('faker/locale/pk');
import { AcademicCalendarModel, AcademicCalendarProgramDetailModel, ProgramDetailModel, ProgramModel, RoadMapModel, RoadMapCourseModel, CourseModel, ClassesModel } from '../../academic';
import { EducationLevelModel } from '../../institute';
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import { Promise } from 'bluebird';

export class RoadMapFaker {


  public educationLevelId;
  public progamIds = [];
  public progamDetailIds = [];
  public academicCalendarIds = [];
  public courseIds = [];
  public natureOfCourse = ['compulsory', 'elective']; // This should come from configuratio table as natureOfCouse is now part of configuration as CRUD : Arif
  public courseType = ['theoratical', 'practical'];
  public creditHour = ['0', '1', '2', '3', '4', '5', '6'];


  constructor() {

  }

  public init() {

    return this.getEducationLevelId().then(() => {

      return this.getProgram().then(() => {

        return this.getProgramDetails().then(() => {

          return this.getAcademicCalendar().then(() => {

            return this.getCourses().then(() => {

              return this.insertFakeData();

            })

          })

        });

      })
    })



  }

  getProgram() {

    return new ProgramModel().findAllByConditions(['id', 'name'], { educationLevelId: this.educationLevelId }).then((programs) => {

      programs.forEach(element => {

        this.progamIds.push({
          id: element['id'],
          name: element['name'],
        })

      });

    })

  }
  getProgramDetails() {

    let programDetailModel = new ProgramDetailModel();
    this.progamDetailIds = [];

    return Promise.each(this.progamIds, item => {

      return programDetailModel.findAllByConditions(['id', 'name'], { programId: item['id'] }).then(programDetaiilIds => {

        programDetaiilIds.forEach(element => {

          this.progamDetailIds.push({
            id: element['id'],
            name: element['name'],
            program: item['name']
          });


        });

      })


    })

  }
  getAcademicCalendar() {

    let academicCalendarModel = new AcademicCalendarModel();


    return academicCalendarModel.findAll(['id']).then(academicCalendarIds => {

      academicCalendarIds.forEach(element => {

        this.academicCalendarIds.push(element['id']);


      });

    })

  }

  getCourses() {

    let coursesModel = new CourseModel();


    return coursesModel.findAll(['id']).then(coursIds => {

      coursIds.forEach(element => {

        this.courseIds.push(element['id']);


      });

    })

  }
  getEducationLevelId() {
    return new EducationLevelModel().findByCondition(['id'], { education: 'University' }).then(educationLevelId => {

      this.educationLevelId = educationLevelId['id'];

    });
  }


  private insertFakeData() {

    let roadMapModel = new RoadMapModel();
    let roadMapCourseModel = new RoadMapCourseModel();
    let classesModel = new ClassesModel();
    let courseOrder = 0;
    let courseCode = 7409;


    return Promise.each(this.progamDetailIds, item => {

      let title = 'Degree Roadmap' + ' - ' + item['program'] + ' - ' + item['name'];

      let postData = {
        title: title,
        programDetailId: item['id'],
        academicCalendarId: this.academicCalendarIds[Math.floor(Math.random() * this.academicCalendarIds.length)]
      }
      return roadMapModel.create(postData).then(roadMap => {

        return classesModel.findAllByConditions(['id'], { programDetailId: item['id'] }).then(classes => {

          return Promise.each(classes, item => {


            let tmpAry = [];
            for (let x = 0; x < 5; x++) { tmpAry.push(x); }

            return Promise.each((tmpAry), (tmpItem) => {

              let courseId = this.courseIds[Math.floor(Math.random() * this.courseIds.length)]
              return roadMapCourseModel.findByCondition(['id'], { classId: item['id'], courseId: courseId }).then(roadMapId => {

                if (roadMapId) {
                  courseId = this.courseIds[Math.floor(Math.random() * this.courseIds.length)]
                }

                else {
                  let roadMapCourse = {

                    roadMapId: roadMap['id'],
                    courseId: courseId,
                    classId: item['id'],
                    courseCode: courseCode++,
                    natureOfCourse: this.natureOfCourse[Math.floor(Math.random() * this.natureOfCourse.length)],
                    courseType: this.courseType[Math.floor(Math.random() * this.courseType.length)],
                    creditHours: this.creditHour[Math.floor(Math.random() * this.creditHour.length)],
                    courseOrder: courseOrder++,

                  }
                  return roadMapCourseModel.create(roadMapCourse);
                }

              })

            })



          })


        })

      });


    })
  }


}
