
var faker = require('faker/locale/pk');
import { UserModel } from '../../security';
import { CRApplicantModel, CRApplicationModel, CRRemittanceModel, CampusInfoModel, CRMOUModel, CRMOUVerificationModel, CampusModel, CPDAccountModel } from '../../campus';
import { ConfigurationModel, WFStateModel, BankModel } from '../../setting';
import { RoleAssignmentModel, RoleModel } from '../../security';
import { ProgramModel, ProgramDetailModel } from '../../academic';
import { InstituteModel, EducationLevelModel, InstituteTypeModel } from '../../institute';
import { ManagementFeeModel } from '../../fee-management'
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import { Promise } from 'bluebird';

export class CampusPostRegistrationFaker {


  public tf = [true, false];
  public maleFemale = ['male', 'female'];
  public applicationType = ['create', 'transfer'];
  public instrumentCategory = ['Bank Draft', 'Cheque', 'Mail Transfer', 'Cash letter of credit', 'Telegraphic Transfer'];
  public academicYear = [2000, 2005, 2007, 2009, 2011, 2012, 2015];
  public academicMonth = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  public buildingType = ['own', 'rented', 'lease']
  public cityIds = [];
  public tehislIds = [];
  public provinceIds = [];
  public instituteIds = [];
  public natureOfWorkIds = [];
  public crApplicationIds = [];
  public finalResult = [];
  public bankIds = [];
  public randConditions = [true, false];
  public stateId;
  public approveStateId;
  public postRegStateId;
  public rejectStateId;
  public countryId;
  public provinceId;
  public cityId;
  public educationLevelId;
  public licenseFeeId;
  public roleId;

  constructor() {

  }

  public init() {

    return this.getConfData().then(() => {
      return this.getListOfPreRegistrations().then(() => {
        return this.getCampusDictionary().then(() => {
          return this.getCampusPrograms().then(() => {
            return this.insertFakeData();
          })
        })

      });
    })

    // this.insertFakeData();

  }

  getCampusDictionary() {

    let instituteTypeModel = new InstituteTypeModel();
    let educationLevelModel = new EducationLevelModel();
    let programModel = new ProgramModel();
    let programDetailModel = new ProgramDetailModel();
    this.finalResult = [];

    return instituteTypeModel.findAll(['id']).then(institutes => {

      return Promise.each(institutes, insItem => {

        return educationLevelModel.findAllByConditions(['id'], { instituteTypeId: insItem['id'] }).then(levels => {

          return Promise.each(levels, levelItam => {

            return programModel.findAllByConditions(['id'], { educationLevelId: levelItam['id'] }).then((programs) => {

              return Promise.each(programs, progItem => {

                return programDetailModel.findAllByConditions(['id'], { programId: progItem['id'] }).then(progDetails => {

                  var pd = [];
                  progDetails.forEach(element => {
                    pd.push(element['id'])

                  });

                  this.finalResult.push(
                    {
                      instituteTypeId: insItem['id'],
                      levels: levelItam['id'],
                      programs: progItem['id'],
                      programDetails: pd,
                    }
                  )

                })

              })

            })

          })


        })

      })


    }).then(() => {
      return this.finalResult;
    })



  }
  programsWithProgramsDetails = [];
  getCampusPrograms() {

    let educationLevelModel = new EducationLevelModel();
    let programModel = new ProgramModel();
    let programDetailModel = new ProgramDetailModel();
    this.programsWithProgramsDetails = [];



    return educationLevelModel.findByCondition(['id'], { education: 'University' }).then(levels => {


      return programModel.findAllByConditions(['id'], { educationLevelId: levels['id'] }).then((programs) => {

        return Promise.each(programs, progItem => {

          return programDetailModel.findAllByConditions(['id'], { programId: progItem['id'] }).then(progDetails => {

            var pd = [];
            progDetails.forEach(element => {
              pd.push(element['id'])

            });

            this.programsWithProgramsDetails.push(
              {
                programs: progItem['id'],
                programDetails: pd,
              }
            )

          })

        })


      })


    })



      .then(() => {
        return this.programsWithProgramsDetails;
      })



  }

  getCities() {

    return new ConfigurationModel().findByCondition(['id'], { key: 'city', value: 'Lahore', parentKeyId: this.provinceId }).then(cities => {

      this.cityId = cities['id'];
    }
    )

  }

  getTehsils() {

    return new ConfigurationModel().findAllByConditions(['id'], { key: 'tehsil', parentKeyId: this.cityId }).then(tehsils => {

      tehsils.forEach(element => {

        this.tehislIds.push(element['id'])

      });
    }
    )

  }
  getCoutryId() {

    return new ConfigurationModel().findByCondition(['id'], { key: 'country', value: 'Pakistan' }).then(country => {

      this.countryId = country['id'];

    }
    )

  }
  getNatureOfWork() {

    return new ConfigurationModel().findAllByConditions(['id'], { key: 'natureOfWork' }).then(natureOfWorks => {

      natureOfWorks.forEach(element => {

        this.natureOfWorkIds.push(element['id'])

      });

    }
    )

  }
  getProvinceList() {

    return new ConfigurationModel().findByCondition(['id'], { key: 'province', value: 'Punjab', parentKeyId: this.countryId }).then(provinces => {

      this.provinceId = provinces['id'];

    }
    )
  }
  getBankList() {

    return new BankModel().findAll(['id']).then(banks => {

      banks.forEach(element => {

        this.bankIds.push(element['id'])

      });

    }
    )
  }

  getInstituteTypes() {

    return new InstituteModel().findAll(['id']).then(institutes => {
      institutes.forEach(element => {

        this.instituteIds.push(element['id'])

      });
    })

  }
  getSubmitStateId() {

    return new WFStateModel().findByCondition(['id'], { WFId: 'pre-registration', state: 'approve' }).then(stateId => {
      this.stateId = stateId['id'];
    });

  }
  getPostRegSubmitStateId() {

    return new WFStateModel().findByCondition(['id'], { WFId: 'post-registration', state: 'done' }).then(stateId => {
      this.postRegStateId = stateId['id'];
    });

  }
  getApproveStateId() {

    return new WFStateModel().findByCondition(['id'], { WFId: 'pre-registration', state: 'approve' }).then(stateId => {

      return new WFStateModel().findByCondition(['id'], { WFId: 'pre-registration', state: 'reject' }).then(rejectStateId => {

        this.approveStateId = stateId['id'];

        this.rejectStateId = rejectStateId['id'];

      })
    });

  }
  getEducationLevelId() {
    return new EducationLevelModel().findByCondition(['id'], { education: 'University' }).then(eduId => {

      return new ManagementFeeModel().findByCondition(['id'], { educationLevelId: eduId['id'] }).then(lId => {
        this.educationLevelId = eduId['id'];
        this.licenseFeeId = lId['id'];
      })

    })
  }
  getConfData() {

    return this.getCoutryId().then(() => {
      return this.getInstituteTypes().then(() => {
        return this.getSubmitStateId().then(() => {
          return this.getApproveStateId().then(() => {
            return this.getProvinceList().then(() => {
              return this.getNatureOfWork().then(() => {
                return this.getBankList().then(() => {
                  return this.getCities().then(() => {
                    return this.getTehsils().then(() => {
                      return this.getRoleId().then(() => {
                        return this.getPostRegSubmitStateId().then(() => {
                          return this.getEducationLevelId().then(() => {
                            return this.getInstrumentTypeId();
                          })
                        })
                      })
                    })
                  })
                });
              });
            });
          });
        });
      })
    })




  }
  public instrumentTypeIds = [];
  getInstrumentTypeId() {

    return new ConfigurationModel().findAllByConditions(['id'], { key: 'instrumentType' }).then(instrument => {

      instrument.forEach(element => {

        this.instrumentTypeIds.push(element['id'])

      });
    }
    )

  }

  private getRoleId() {

    return new RoleModel().findByCondition(['id'], { name: 'Applicant' }).then(roleId => {

      this.roleId = roleId['id'];

    }
    )

  }
  private getApplicant() {

    let item = {};


    item["address"] = faker.address.streetName() + ' , ' + faker.address.streetAddress() + ' , Pakistan';
    item["countryId"] = this.countryId;
    item["provinceId"] = this.provinceId;
    item["cityId"] = this.cityId;
    item["tehsilId"] = this.tehislIds[Math.floor(Math.random() * this.tehislIds.length)];

    item["natureOfWorkId"] = this.natureOfWorkIds[Math.floor(Math.random() * this.natureOfWorkIds.length)];
    item["nearestBankId"] = this.bankIds[Math.floor(Math.random() * this.bankIds.length)];
    item["approxMonthlyIncome"] = faker.phone.phoneNumber('#####');
    item["references"] = [];

    let tmpAry = [];
    for (let x = 0; x < 5; x++) { tmpAry.push(x); }
    for (var i = 0; i < tmpAry[Math.floor(Math.random() * tmpAry.length)]; i++) {
      let ref = this.getReference();
      item["references"].push(ref);
    }




    return item;
  }
  private getReference() {

    let item = {};


    item["id"] = '';
    let name = [
      { fn: faker.name.firstName(0), ln: faker.name.lastName(1) },
      { fn: faker.name.firstName(1), ln: faker.name.lastName(1) },
    ]
    let fn = name[Math.floor(Math.random() * name.length)].fn;
    let ln = name[Math.floor(Math.random() * name.length)].ln;
    let fullName = fn + ' ' + ln;

    item["refName"] = fullName;
    item["refMobileNumber"] = faker.phone.phoneNumber('92-###-#######');
    item["refCountryCode"] = '92';
    item["refAddress"] = faker.address.streetName() + ' , ' + faker.address.streetAddress() + ' , Pakistan';


    return item;
  }





  getListOfPreRegistrations() {
    return new CRApplicationModel().findAllByConditions(['id'], { stateId: this.stateId }).then(crApplicationIds => {

      crApplicationIds.forEach(element => {

        this.crApplicationIds.push(element['id']);

      });

    });

  }

  insertFakeData() {

    let CRApplicantionModel = new CRApplicationModel();
    let roleAssignmentModel = new RoleAssignmentModel();
    let userModel = new UserModel();

    return Promise.each(this.crApplicationIds, crApplicationId => {


      return CRApplicantionModel.update(crApplicationId, { stateId: this.approveStateId }).then(() => {

        return userModel.findByCondition(['id'], { CRApplicantId: crApplicationId }).then(userId => {

          if (userId) {
            return roleAssignmentModel.create({
              roleId: this.roleId,
              parent: 'user',
              parentId: userId['id']
            }).then(() => {

              return this.savePersonalInfo(crApplicationId).then(() => {

                return this.saveRemittance(crApplicationId).then(() => {

                  return this.saveCampusInfo(crApplicationId).then(() => {
                    return this.saveCampusMOU(crApplicationId).then(() => {
                      return this.saveCampusMOUVerification(crApplicationId).then(() => {
                        return this.saveCreateCampus(crApplicationId).then(() => {
                          return this.saveCampusAccountInfo(crApplicationId)
                        })
                      })
                    })
                  });

                });
              })
            })
          }


        })

      })






    })

  }

  savePersonalInfo(crApplicationId) {

    let applicant = this.getApplicant();
    let postData = {
      personalInfo: applicant,
      deleteRefernceIds: []
    }

    return new CRApplicationModel().findByCondition(['CRApplicantId'], { id: crApplicationId }).then(applicantId => {

      return new CRApplicantModel().updateCRApplicantPersonalInfo(applicantId['CRApplicantId'], postData);

    })


  }

  getRemittance(crApplicationId) {
    let item = {};

    item['id'] = crApplicationId;
    item["amount"] = faker.phone.phoneNumber('#####');
    item["branchName"] = faker.address.streetName();
    item["instrumentCategory"] = this.instrumentCategory[Math.floor(Math.random() * this.instrumentCategory.length)];
    item["bankId"] = this.bankIds[Math.floor(Math.random() * this.bankIds.length)];
    item["instrumentNumber"] = faker.phone.phoneNumber('#####');
    item["purpose"] = 'Application Form';
    item["instrumentDate"] = faker.date.past(10);
    item["currentDate"] = faker.date.past(10);






    return item;
  }

  saveRemittance(crApplicationId) {

    let remittance = this.getRemittance(crApplicationId);

    return new CRRemittanceModel().create(remittance);

  }

  titleCase(str) {
    str = str.toLowerCase().split(' ');                // will split the string delimited by space into an array of words

    for (var i = 0; i < str.length; i++) {               // str.length holds the number of occurrences of the array...
      str[i] = str[i].split('');                    // splits the array occurrence into an array of letters
      str[i][0] = str[i][0].toUpperCase();          // converts the first occurrence of the array to uppercase
      str[i] = str[i].join('');                     // converts the array of letters back into a word.
    }
    return str.join(' ');                              //  converts the array of words back to a sentence.
  }

  private getCampusCredentials() {

    let item = {};
    item["applicationType"] = "create";
    item["bankBranch"] = this.randConditions[Math.floor(Math.random() * this.randConditions.length)];
    item["buildingAvailable"] = true;
    item["buildingType"] = this.buildingType[Math.floor(Math.random() * this.buildingType.length)];
    item["cafeteria"] = this.randConditions[Math.floor(Math.random() * this.randConditions.length)];
    item["campusName"] = this.titleCase(faker.lorem.words());
    item["stateId"] = this.postRegStateId;
    item["campusType"] = true;
    item["coverdArea"] = faker.phone.phoneNumber('#####');
    item["deletedAffiliationIds"] = [];
    item["healthClinic"] = this.randConditions[Math.floor(Math.random() * this.randConditions.length)];
    item["labsQuantity"] = this.randConditions[Math.floor(Math.random() * this.randConditions.length)];
    item["library"] = this.randConditions[Math.floor(Math.random() * this.randConditions.length)];
    item["mosque"] = this.randConditions[Math.floor(Math.random() * this.randConditions.length)];
    item["playGround"] = this.randConditions[Math.floor(Math.random() * this.randConditions.length)];
    item["swimmingPool"] = this.randConditions[Math.floor(Math.random() * this.randConditions.length)];
    item["transport"] = this.randConditions[Math.floor(Math.random() * this.randConditions.length)];
    item["openArea"] = faker.phone.phoneNumber('#####');
    item["roomsQuantity"] = 5;
    item["washroomsQuantity"] = 5;
    item["tentativeSessionStart"] = faker.date.past(10);
    item["campusPrograms"] = [];
    item["affiliations"] = [];
    item["deletedAffiliationIds"] = [];
    item["addresses"] = [
      {
        id: null,
        address: faker.address.streetName() + ' , ' + faker.address.streetAddress() + ' , Pakistan',
        latitude: 31.55162278314177,
        longitude: 74.28474404199221
      }
    ];


    let insTypeId = this.finalResult[Math.floor(Math.random() * this.finalResult.length)].instituteTypeId;
    item["instituteTypeId"] = insTypeId;
    let levelId;
    this.finalResult.forEach(element => {

      if (element['instituteTypeId'] == item["instituteTypeId"]) {
        levelId = element['levels'];
        item["educationLevelId"] = levelId;
        let pd = element['programDetails'][Math.floor(Math.random() * element['programDetails'].length)]
        item["campusPrograms"].push({
          programId: element['programs'],
          programDetails: [pd]
        })

      }

    });







    return item;

  }

  saveCampusInfo(crApplicantionId) {
    let camp = this.getCampusCredentials()
    return new CampusInfoModel().updateCampusInfo(crApplicantionId, camp);
  }

  private getCampusMOUCredentials(crApplicationId) {

    let item = {};
    item["CRApplicationId"] = crApplicationId;
    item["MOUDate"] = faker.date.past(10);
    item["campusTitle"] = this.titleCase(faker.lorem.words());
    item["purpose"] = faker.lorem.words();
    item["educationLevelId"] = this.educationLevelId;
    item["licenseFeeId"] = this.licenseFeeId;
    item["academicMonth"] = this.academicMonth[Math.floor(Math.random() * this.academicMonth.length)];
    item["academicYear"] = this.academicYear[Math.floor(Math.random() * this.academicYear.length)];

    return item;

  }
  public status = ['Active', 'In Active', 'Online', 'Offline', 'Pending', 'Approved'];
  MOU_VERIFICATION_PURPOSES = ['Application Form', 'Registration Form', 'MOU Fee'];
  private getCampusMOUVerificationCredentials(crApplicationId) {

    let item = {};
    item["CRApplicationId"] = crApplicationId;
    item["instrumentTypeId"] = this.instrumentTypeIds[Math.floor(Math.random() * this.instrumentTypeIds.length)];
    item["currentDate"] = faker.date.past(10);
    item["instrumentDate"] = faker.date.past(10);
    item["instrumentNumber"] = faker.phone.phoneNumber('#####');
    item["bankId"] = this.bankIds[Math.floor(Math.random() * this.bankIds.length)];
    item["bankName"] = this.titleCase(faker.lorem.words());
    item["amount"] = 5000;
    item["status"] = this.status[Math.floor(Math.random() * this.status.length)];
    item["purpose"] = this.MOU_VERIFICATION_PURPOSES[Math.floor(Math.random() * this.MOU_VERIFICATION_PURPOSES.length)];
    item["account"] = this.titleCase(faker.lorem.words());
    item["remarks"] = this.titleCase(faker.lorem.words());

    return item;

  }
  private getCreateCampusCredentials(crApplicationId) {

    let item = {};
    item["CRApplicationId"] = crApplicationId;
    item["codeNumber"] = faker.phone.phoneNumber('#####');
    item["campusName"] = this.titleCase(faker.lorem.words());
    item["campusId"] = this.titleCase(faker.lorem.words());
    item["email"] = faker.internet.email().toLowerCase();
    item["mobileNumber"] = faker.phone.phoneNumber('92-###-#######');
    item["address"] = faker.address.streetName() + ' , ' + faker.address.streetAddress() + ' , Pakistan';
    item["academicMonth"] = this.academicMonth[Math.floor(Math.random() * this.academicMonth.length)];
    item["academicYear"] = this.academicYear[Math.floor(Math.random() * this.academicYear.length)];
    item["faxNumber"] = faker.phone.phoneNumber('#####');
    item["status"] = 'done';



    return item;

  }

  saveCampusMOU(crApplicantionId) {

    let campMOU = this.getCampusMOUCredentials(crApplicantionId);
    return new CRMOUModel().create(campMOU);

  }
  saveCampusMOUVerification(crApplicantionId) {

    let campMOUVerification = this.getCampusMOUVerificationCredentials(crApplicantionId);
    return new CRMOUVerificationModel().create(campMOUVerification);

  }
  saveCreateCampus(crApplicantionId) {

    let createCamp = this.getCreateCampusCredentials(crApplicantionId);
    let campusProgram = [];

    this.programsWithProgramsDetails.forEach(element => {


      let pd = element['programDetails'][Math.floor(Math.random() * element['programDetails'].length)]
      campusProgram.push({
        programId: element['programs'],
        programDetails: [pd]
      })



    });
    var postData = {
      campus: createCamp,
      campusProgram: campusProgram
    }
    return new CampusModel().create(postData.campus).then(res => {

      return new CampusModel().saveCRProgram(res.id, postData.campusProgram);

    });


  }
  challanTypes = ['Approved', 'Cheque', 'Credit Card', 'Cash', 'Two Copy Chalan'];

  private getCampusAccountInfoCredentials() {

    let item = {};
    item["bankId"] = this.bankIds[Math.floor(Math.random() * this.bankIds.length)];
    item["branch"] = this.titleCase(faker.lorem.words());
    item["account"] = this.titleCase(faker.lorem.words());
    item["challanType"] = this.challanTypes[Math.floor(Math.random() * this.challanTypes.length)];





    return item;

  }

  saveCampusAccountInfo(crApplicantionId) {

    let programDetailId = [];
    this.programsWithProgramsDetails.forEach(element => {
      let pd = element['programDetails'][Math.floor(Math.random() * element['programDetails'].length)]
      programDetailId.push(pd);

    });


    return new CampusModel().findWithRelatedData(crApplicantionId).then(result => {



      var postData = {
        campusId: result['campus']['id'],
        accountInfo: this.getCampusAccountInfoCredentials(),
        programDetailIds: programDetailId
      }
      return new CPDAccountModel().create(postData)





    })

  }
}
