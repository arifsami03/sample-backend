
var faker = require('faker/locale/pk');
import { EducationLevelModel } from '../../institute';
import { ManagementFeeModel } from '../../fee-management';
import { Promise } from 'bluebird';

export class LicenseFeeFaker {


  public educationLevelId = [];
  public progamIds = [];
  public progamDetailIds = [];


  constructor() {

  }

  public init() {

    return this.getEducationLevelId().then(() => {

      return this.insertFakeData();

    })



  }



  getEducationLevelId() {
    return new EducationLevelModel().findAll().then(educationLevelId => {

      educationLevelId.forEach(element => {

        this.educationLevelId.push(element['id']);

      });


    });
  }


  private insertFakeData() {
    let licenseFeeModel = new ManagementFeeModel();


    return Promise.each(this.educationLevelId, edId => {


      let tmpAry = [];
      for (let x = 0; x < 3; x++) { tmpAry.push(x); }

      return Promise.each((tmpAry), (tmpItem) => {
        let postData = {
          educationLevelId: edId,
          amount: faker.phone.phoneNumber('#####')
        }
        return licenseFeeModel.create(postData);



      })

    });
  }


}
