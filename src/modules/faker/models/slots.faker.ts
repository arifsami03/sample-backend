
var faker = require('faker/locale/pk');
import { ClassesModel, ProgramDetailModel, ProgramModel } from '../../academic';
import { SlotModel } from '../../time-table';
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import { Promise } from 'bluebird';

export class SlotFaker {

  constructor() {

  }

  public init() {

    return this.insertFakeData();

  }




  private insertFakeData() {
    let slotModel = new SlotModel();

      let postData = [
        { fromTime: "08:00", toTime: '08:45' },
        { fromTime: "08:45", toTime: '09:30' },
        { fromTime: "09:30", toTime: '10:15' },
        { fromTime: "10:15", toTime: '11:00' },
        { fromTime: "11:00", toTime: '11:45' },
        { fromTime: "11:45", toTime: '12:30' },
      ]


      return slotModel.sequelizeModel.bulkCreate(postData)


  }


}
