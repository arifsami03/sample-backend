
var faker = require('faker/locale/pk');
import {  ProgramModel, CourseModel } from '../../academic';
import { PassingYearModel } from '../../setting';
import { FeeHeadsModel, ScholarshipModel } from '../../fee-management';
import { MeritListModel, EligibilityCriteriaModel } from '../../admission';
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import { Promise } from 'bluebird';

export class EligibilityCriteriaFaker {


  public percentageFrom = [0, 10, 15, 20, 25, 30, 35, 40, 45, 50];
  public percentageTo = [55, 60, 65, 70, 75, 80, 85, 90, 95, 100];
  public academicYear = [2000, 2005, 2007, 2009, 2011, 2012, 2015];
  public academicMonth = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  public pdIds = [];
  public scholarshipIds = [];
  public courseIds = [];
  public passingYearIds = [];


  constructor() {

  }

  public init() {

    return this.getProgramDetails().then(() => {

      return this.getScholarships().then(() => {

        return this.getCourses().then(() => {

          return this.getPassingYears().then(() => {


            return this.insertFakeData();
          })
        })
      })
    })



  }

  getProgramDetails() {
    return new ProgramModel().findAll(['id']).then(pdIds => {

      pdIds.forEach(element => {

        this.pdIds.push(element['id']);

      });


    });
  }
  getScholarships() {
    return new ScholarshipModel().findAll().then(scholarshipIds => {

      scholarshipIds.forEach(element => {

        this.scholarshipIds.push(element['id']);

      });


    });
  }
  getCourses() {

    return new CourseModel().findAll(['id']).then(courseIds => {

      courseIds.forEach(element => {

        this.courseIds.push(element['id']);

      });


    });
  }
  getPassingYears() {

    return new PassingYearModel().findAll(['id']).then(passingYearIds => {

      passingYearIds.forEach(element => {

        this.passingYearIds.push(element['id']);

      });


    });
  }

  titleCase(str) {
    str = str.toLowerCase().split(' ');                // will split the string delimited by space into an array of words

    for (var i = 0; i < str.length; i++) {               // str.length holds the number of occurrences of the array...
      str[i] = str[i].split('');                    // splits the array occurrence into an array of letters
      str[i][0] = str[i][0].toUpperCase();          // converts the first occurrence of the array to uppercase
      str[i] = str[i].join('');                     // converts the array of letters back into a word.
    }
    return str.join(' ');                              //  converts the array of words back to a sentence.
  }
  private insertFakeData() {

    let eligibilityCriteriaModel = new EligibilityCriteriaModel();


    return Promise.each((this.pdIds), (item) => {

      let postData = this.getEligibilityCriteriaCredentials(item);

      return eligibilityCriteriaModel.create(postData);


    })
  }

  private getEligibilityCriteriaCredentials(pdId) {

    let preReqPrograms = [];
    for (let i = 0; i < this.pdIds.length; i++) {

      if (this.pdIds[i] != pdId) {

        preReqPrograms.push(this.pdIds[i])
      }

    }

    let item = {};
    item["deletedPassingYearIds"] = [];
    item["deletedPreReqIds"] = [];
    item["deletedSubjectIds"] = [];
    item["ecPassingYear"] = [];
    item["ecPassingYear"].push(this.passingYearIds[Math.floor(Math.random() * this.passingYearIds.length)])
    item["ecPassingYear"].push(this.passingYearIds[Math.floor(Math.random() * this.passingYearIds.length)])
    item["ecSubject"] = [];
    item["ecSubject"].push(this.courseIds[Math.floor(Math.random() * this.courseIds.length)])
    item["ecSubject"].push(this.courseIds[Math.floor(Math.random() * this.courseIds.length)])
    item["ecPreReqProgram"] = [];
    item["ecPreReqProgram"].push(preReqPrograms[Math.floor(Math.random() * preReqPrograms.length)])
    item["ecPreReqProgram"].push(preReqPrograms[Math.floor(Math.random() * preReqPrograms.length)])
    let myEligibility =
      {
        'academicMonth': this.academicMonth[Math.floor(Math.random() * this.academicMonth.length)],
        'academicYear': this.academicYear[Math.floor(Math.random() * this.academicYear.length)],
        'ageFromMM': Math.floor(Math.random() * 12) + 1,
        'ageFromYY': Math.floor(Math.random() * 15) + 1,
        'ageToMM': Math.floor(Math.random() * 12) + 1,
        'ageToYY': Math.floor(Math.random() * 20) + 15,
        'programId': pdId,
        'requiredMarksFigure': faker.phone.phoneNumber('###'),
        'requiredMarksPercent': this.percentageTo[Math.floor(Math.random() * this.percentageTo.length)],
        'totalMarks': faker.phone.phoneNumber('###'),

      }


    item["eligibilityCriteria"] = myEligibility



    return item;

  }


}
