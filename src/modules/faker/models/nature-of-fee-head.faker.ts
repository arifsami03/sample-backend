
var faker = require('faker/locale/pk');
import { ClassesModel, ProgramDetailModel, ProgramModel } from '../../academic';
import { ConfigurationModel } from '../../setting';
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import { Promise } from 'bluebird';

export class NatureOfFeeHeadFaker {

  constructor() {

  }

  public init() {

    return this.insertFakeData();

  }




  private insertFakeData() {
    let confModel = new ConfigurationModel();

      let postData = [
        
        { key: "natureOfFeeHeads", value: 'Commission'},
        { key: "natureOfFeeHeads", value: 'Expense'},
        { key: "natureOfFeeHeads", value: 'Revenue'},
      ]


      return confModel.sequelizeModel.bulkCreate(postData)


  }


}
