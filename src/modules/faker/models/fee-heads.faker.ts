
var faker = require('faker/locale/pk');
import { AcademicCalendarModel, AcademicCalendarProgramDetailModel, ProgramDetailModel, ProgramModel } from '../../academic';
import { ConfigurationModel } from '../../setting';
import {  FeeHeadsModel } from '../../fee-management';
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import { Promise } from 'bluebird';

export class FeeHeadsFaker {


  public educationLevelId;
  public progamIds = [];
  public natureOfFeeHeadIds = [];


  constructor() {

  }

  public init() {

    return this.getNatureOfFeeHeads().then(() => {


      return this.insertFakeData();

    })



  }



  getNatureOfFeeHeads() {
    return new ConfigurationModel().findAllByConditions(['id'], { key: 'natureOfFeeHeads' }).then(confIds => {

      confIds.forEach(element => {

        this.natureOfFeeHeadIds.push(element['id']);

      });


    });
  }


  private insertFakeData() {

    let feeHeadsModel = new FeeHeadsModel();

    let feeHeads = [
      { title: "Registration Fee", abbreviation: "REG", code: '021', natureOfFeeHeadsId: this.natureOfFeeHeadIds[Math.floor(Math.random() * this.natureOfFeeHeadIds.length)] },
      { title: "Tuition Fee", abbreviation: "TUIT", code: '023', natureOfFeeHeadsId: this.natureOfFeeHeadIds[Math.floor(Math.random() * this.natureOfFeeHeadIds.length)] },
      { title: "Admission Fee", abbreviation: "ADM", code: '04', natureOfFeeHeadsId: this.natureOfFeeHeadIds[Math.floor(Math.random() * this.natureOfFeeHeadIds.length)] },
      { title: "Library Fee", abbreviation: "LIB", code: '12', natureOfFeeHeadsId: this.natureOfFeeHeadIds[Math.floor(Math.random() * this.natureOfFeeHeadIds.length)] },
      { title: "Transport Fee", abbreviation: "TRANS", code: '089', natureOfFeeHeadsId: this.natureOfFeeHeadIds[Math.floor(Math.random() * this.natureOfFeeHeadIds.length)] },
      { title: "Prospectus Fee", abbreviation: "PROS", code: '52', natureOfFeeHeadsId: this.natureOfFeeHeadIds[Math.floor(Math.random() * this.natureOfFeeHeadIds.length)] },
    ]

    return feeHeadsModel.sequelizeModel.bulkCreate(feeHeads);
  }


}
