
var faker = require('faker/locale/pk');
import { AcademicCalendarModel, AcademicCalendarProgramDetailModel, ProgramDetailModel, ProgramModel } from '../../academic';
import { EducationLevelModel } from '../../institute';
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import { Promise } from 'bluebird';

export class AcademicCalendarFaker {


  public educationLevelId;
  public progamIds = [];
  public progamDetailIds = [];


  constructor() {

  }

  public init() {

    return this.getEducationLevelId().then(() => {

      return this.getProgram().then(() => {

        return this.getProgramDetails().then(() => {

          return this.insertFakeData();

        });

      })
    })



  }

  getProgram() {

    return new ProgramModel().findAllByConditions(['id'], { educationLevelId: this.educationLevelId }).then((programs) => {

      programs.forEach(element => {

        this.progamIds.push(element['id'])

      });

    })

  }
  getProgramDetails() {

    let programDetailModel = new ProgramDetailModel();
    this.progamDetailIds = [];

    return Promise.each(this.progamIds, item => {

      return programDetailModel.findAllByConditions(['id'], { programId: item }).then(programDetaiilIds => {

        programDetaiilIds.forEach(element => {

          this.progamDetailIds.push(element['id']);


        });

      })


    })

  }
  getEducationLevelId() {
    return new EducationLevelModel().findByCondition(['id'], { education: 'University' }).then(educationLevelId => {

      this.educationLevelId = educationLevelId['id'];

    });
  }


  private insertFakeData() {

    let academicCalendarModel = new AcademicCalendarModel();
    let academicCalendarProgramDetaillModel = new AcademicCalendarProgramDetailModel();
    let academicCalendar = [
      { title: "Academic Calendar- Jan 2018 Semester", abbreviation: "AC-SP-2018" },
      { title: "Academic Calendar- Summer 2018 Semester", abbreviation: "AC-SU-2018" },
      { title: "Academic Calendar- Jan 2018", abbreviation: "AC-Jan-18" },
      { title: "Academic Calendar- Feb 2018", abbreviation: "AC-Feb-18" },
      { title: "Academic Calendar- Mar 2018", abbreviation: "AC-Mar-18" },
      { title: "Academic Calendar- Apr 2018", abbreviation: "AC-Apr-18" },
      { title: "Academic Calendar- May 2018", abbreviation: "AC-May-18" },
      { title: "Academic Calendar- Jun 2018", abbreviation: "AC-Jun-18" },
      { title: "Academic Calendar- Jul 2018", abbreviation: "AC-Jul-18" },
      { title: "Academic Calendar- Aug 2018", abbreviation: "AC-Aug-18" },
      { title: "Academic Calendar- Sep 2018", abbreviation: "AC-Sep-18" },
      { title: "Academic Calendar- Oct 2018", abbreviation: "AC-Oct-18" },
      { title: "Academic Calendar- Nov 2018", abbreviation: "AC-Nov-18" },
      { title: "Academic Calendar- Dec 2018", abbreviation: "AC-Dec-18" }
    ]

    return Promise.each(academicCalendar, item => {


      return academicCalendarModel.create(item).then(acId => {

        let tmpAry = [];
        for (let x = 0; x < 5; x++) { tmpAry.push(x); }

        return Promise.each((tmpAry), (tmpItem) => {

          let programDetailId = this.progamDetailIds[Math.floor(Math.random() * this.progamDetailIds.length)]

          return academicCalendarProgramDetaillModel.findByCondition(['id'], { academicCalendarId: acId['id'], programDetailId: programDetailId }).then(progId => {
            if (progId) {

            }
            else {
              return academicCalendarProgramDetaillModel.create(
                {
                  academicCalendarId: acId['id'],
                  programDetailId: programDetailId
                }
              )
            }


          })


        })


      }
      );
    })
  }


}
