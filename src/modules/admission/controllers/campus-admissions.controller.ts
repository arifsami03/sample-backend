import * as express from 'express';
import { CampusAdmissionModel } from '..';
import { ErrorHandler } from '../../base/conf/error-handler';

export class CampusAdmissionsController {
  constructor() { }

  /**
   * Get all records
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CampusAdmissionModel()
      .indexWithRelatedData()
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesList(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CampusAdmissionModel()
      .findAll(['id', 'name'])
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Find one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new CampusAdmissionModel()
      .findWithRelatedData(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Find Campus Admission Program Details List
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findCampusAdmissionProgramDetailsList(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new CampusAdmissionModel()
      .findCampusAdmissionProgramDetailsList(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Create new record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {

    new CampusAdmissionModel()
      .createWithRelatedData(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Create new record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  createCAProgramDetail(req: express.Request, res: express.Response, next: express.NextFunction) {

    new CampusAdmissionModel()
      .createCAProgramDetail(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Delete record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new CampusAdmissionModel()
      .deleteWithAssociatedRecords(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Delete record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  deleteCAProgramDetail(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new CampusAdmissionModel()
      .deleteCAProgramDetail(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;
    new CampusAdmissionModel()
      .updateWithRelatedData(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        console.log(err)
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Find related data for deletion
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findRelatedData(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;

    new CampusAdmissionModel().findRelatedData(id).then(result => {
      res.json(result);
    })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
}
