import { Router } from 'express';
import { EligibilityCriteriasController } from '..';

export class EligibilityCriteriaRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new EligibilityCriteriasController();

    this.router.route('/admission/eligibilityCriterias/index').get(controller.index);
    this.router.route('/admission/eligibilityCriterias/find/:id').get(controller.find);
    this.router.route('/admission/eligibilityCriterias/create').post(controller.create);
    this.router.route('/admission/eligibilityCriterias/update/:id').put(controller.update);
    this.router.route('/admission/eligibilityCriterias/delete/:id').delete(controller.delete);
    this.router.route('/admission/eligibilityCriterias/findAttributesList').get(controller.findAttributesList);
    this.router.route('/admission/eligibilityCriterias/findRelatedData/:id').get(controller.findRelatedData);
  }
}
