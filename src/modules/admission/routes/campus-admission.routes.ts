import { Router } from 'express';
import { CampusAdmissionsController } from '..';

export class CampusAdmissionRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new CampusAdmissionsController();

    this.router.route('/admission/campusAdmissions/index').get(controller.index);
    this.router.route('/admission/campusAdmissions/find/:id').get(controller.find);
    this.router.route('/admission/campusAdmissions/findCampusAdmissionProgramDetailsList/:id').get(controller.findCampusAdmissionProgramDetailsList);
    this.router.route('/admission/campusAdmissions/create').post(controller.create);
    this.router.route('/admission/campusAdmissions/createCAProgramDetail').post(controller.createCAProgramDetail);
    this.router.route('/admission/campusAdmissions/update/:id').put(controller.update);
    this.router.route('/admission/campusAdmissions/delete/:id').delete(controller.delete);
    this.router.route('/admission/campusAdmissions/deleteCAProgramDetail/:id').delete(controller.deleteCAProgramDetail);
    this.router.route('/admission/campusAdmissions/findAttributesList').get(controller.findAttributesList);
    this.router.route('/admission/campusAdmissions/findRelatedData/:id').get(controller.findRelatedData);
    
  }
}
