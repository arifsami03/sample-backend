import { Router } from 'express';

import { MeritListController } from '..';

export class MeritListRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new MeritListController();

    this.router.route('/admission/meritLists/list').get(controller.index);
    this.router.route('/admission/meritLists/find/:id').get(controller.find);
    this.router.route('/admission/meritLists/create').post(controller.create);
    this.router.route('/admission/meritLists/update/:id').put(controller.update);
    this.router.route('/admission/meritLists/delete/:id').delete(controller.delete);
  }
}
