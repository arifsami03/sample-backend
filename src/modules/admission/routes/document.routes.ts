import { Router } from 'express';

import { DocumentController } from '..';

export class DocumentRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new DocumentController();

    this.router.route('/admission/documents/index').get(controller.index);
    this.router.route('/admission/documents/find/:id').get(controller.find);
    this.router.route('/admission/documents/findByDocFor/:docFor').get(controller.findByDocFor);
    this.router.route('/admission/documents/create').post(controller.create);
    this.router.route('/admission/documents/update/:id').put(controller.update);
    this.router.route('/admission/documents/delete/:id').delete(controller.delete);
  }
}
