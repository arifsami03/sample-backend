import { Router } from 'express';

import {
  EligibilityCriteriaRoute,
  MeritListRoute,
  DocumentRoute,
  CampusAdmissionRoute
} from '../..';

/**
 *
 *
 * @class AdmissionBaseRoute
 */
export class AdmissionBaseRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class BaseRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.initAll();
  }

  /**
   * init all routes
   */
  public initAll() {
    new EligibilityCriteriaRoute(this.router);
    new MeritListRoute(this.router);
    new DocumentRoute(this.router);
    new CampusAdmissionRoute(this.router);
    
  }
}
