export * from './routes/base/admission.base.route';

// Eligibility Criteria
export * from './models/schema/eligibility-criteria';
export * from './models/eligibility-criteria.model';
export * from './controllers/eligibility-criterias.controller';
export * from './routes/eligibility-criteria.routes';
export * from './models/schema/ec-pre-req-program';
export * from './models/ec-pre-req-program.model';
export * from './models/schema/ec-subject';
export * from './models/ec-subject.model';
export * from './models/schema/ec-passing-year';
export * from './models/ec-passing-year.model';

// Merit List
export * from './models/schema/merit-list';
export * from './models/merit-list.model';
export * from './controllers/merit-list.controller';
export * from './routes/merit-list.routes';


// Document
export * from './models/schema/document';
export * from './models/document.model';
export * from './controllers/document.controller';
export * from './routes/document.routes';

// Campus Admission
export * from './models/schema/campus-admission';
export * from './models/schema/ca-program-detail';
export * from './models/campus-admission.model';
export * from './models/ca-program-detail.model';
export * from './controllers/campus-admissions.controller';
export * from './routes/campus-admission.routes';
