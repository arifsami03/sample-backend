import { BaseModel } from '../../base';
import { MeritList } from '..';
import { Scholarship } from '../../fee-management'
import { ProgramDetail } from '../../academic';
import * as _ from 'lodash';

export class MeritListModel extends BaseModel {
  constructor() {
    super(MeritList);
  }

  findAllWithAssociatedRecord() {
    let includeObj = [
      { model: ProgramDetail, as: 'programDetail', attributes: ['id', 'name'], where: BaseModel.cb(), required: false },
      { model: Scholarship, as: 'scholarship', attributes: ['id', 'title'], where: BaseModel.cb(), required: false }
    ];

    return this.findAll(['id', 'title'], null, includeObj);
  }

  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Merit List';
    retResult['records'] = [];

    return this.findAllByConditions(['id', 'title'], condition).then((pdRes) => {


      _.each(pdRes, item => { retResult['records'].push(item['dataValues']) });

      return retResult;

    });
  }
}
