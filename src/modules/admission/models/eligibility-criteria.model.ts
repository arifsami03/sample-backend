import { BaseModel, CONFIGURATIONS } from '../../base';
import { EligibilityCriteria, ECPreReqProgramModel, ECSubjectModel, ECPassingYearModel } from '..';
import { ProgramModel, Program } from '../../academic';
import { Promise } from 'bluebird';
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';


export class EligibilityCriteriaModel extends BaseModel {
  constructor() {
    super(EligibilityCriteria);
  }

  private ecPYModel = new ECPassingYearModel();
  private ecPRPModel = new ECPreReqProgramModel();
  private esSModel = new ECSubjectModel();

  findECWithProgram() {

    let progModel = new ProgramModel();

    let finalResult = [];
    return this.findAll(['id', 'programId', 'ageFromYY', 'ageFromMM', 'ageToYY', 'ageToMM', 'requiredMarksFigure', 'requiredMarksPercent', 'totalMarks']).then(res => {

      return Promise.each(res, (item) => {

        return progModel.findByCondition(['id', 'name'], { id: item['programId'] }).then(programTitle => {
          finalResult.push({
            targetProgramTitle: programTitle.name,
            ageFromYY: item['ageFromYY'],
            ageFromMM: item['ageFromMM'],
            ageToYY: item['ageToYY'],
            ageToMM: item['ageToMM'],
            requiredMarksFigure: item['requiredMarksFigure'],
            requiredMarksPercent: item['requiredMarksPercent'],
            totalMarks: item['totalMarks'],
            id: item['id'],
          })
        })

      })





    }).then(() => {
      return finalResult;
    })
  }

  create(postData) {
    let ecPreReq = new ECPreReqProgramModel();
    let ecSubject = new ECSubjectModel();
    let ecPassingYear = new ECPassingYearModel();
    return super.create(postData.eligibilityCriteria).then(ecResult => {

      let ecPreReqData = [];

      postData.ecPreReqProgram.forEach(element => {
        ecPreReqData.push({
          programId: element,
          eligibilityCriteriaId: ecResult['id'],
          createdBy: CONFIGURATIONS.identity.userId,
          updatedBy: CONFIGURATIONS.identity.userId
        })
      });

      return ecPreReq.sequelizeModel.bulkCreate(ecPreReqData).then(() => {

        let ecSubjectData = [];

        postData.ecSubject.forEach(element => {
          ecSubjectData.push({
            courseId: element,
            eligibilityCriteriaId: ecResult['id'],
            createdBy: CONFIGURATIONS.identity.userId,
            updatedBy: CONFIGURATIONS.identity.userId
          })
        });

        return ecSubject.sequelizeModel.bulkCreate(ecSubjectData).then(() => {


          let ecPassingYearData = [];

          postData.ecPassingYear.forEach(element => {
            ecPassingYearData.push({
              passingYearId: element,
              eligibilityCriteriaId: ecResult['id'],
              createdBy: CONFIGURATIONS.identity.userId,
              updatedBy: CONFIGURATIONS.identity.userId
            })


          });
          return ecPassingYear.sequelizeModel.bulkCreate(ecPassingYearData);

        })
      });

    })
  }

  find(id) {
    let ecPreReqProgram = [];
    let ecSubject = [];
    let ecPassingYear = [];

    let resultData = {

    };
    let ecPreReqModel = new ECPreReqProgramModel();
    let ecSubjectModel = new ECSubjectModel();
    let ecPassingYearModel = new ECPassingYearModel();


    return super.find(id).then(ecResult => {
      resultData['eligibilityCriteria'] = ecResult;

      return ecPreReqModel.findAllByConditions(['programId'], { eligibilityCriteriaId: id }).then(ecPreReqResult => {

        return Promise.each(ecPreReqResult, (item) => {
          ecPreReqProgram.push(item['programId'])
        }
        )
      }).then(() => {

        return ecSubjectModel.findAllByConditions(['courseId'], { eligibilityCriteriaId: id }).then(ecSubjectResult => {

          return Promise.each(ecSubjectResult, (item) => {
            ecSubject.push(item['courseId'])
          })
        })

      }).then(() => {

        return ecPassingYearModel.findAllByConditions(['passingYearId'], { eligibilityCriteriaId: id }).then(ecPassingYearResult => {

          return Promise.each(ecPassingYearResult, (item) => {
            ecPassingYear.push(item['passingYearId'])
          })
        })

      });



    }).then(() => {
      resultData['ecPreReq'] = ecPreReqProgram;
      resultData['ecSubject'] = ecSubject;
      resultData['ecPassingYear'] = ecPassingYear;
      return resultData;
    })
  }

  update(id, postData) {

    let ecPreReq = new ECPreReqProgramModel();
    let ecSubject = new ECSubjectModel();
    let ecPassingYear = new ECPassingYearModel();

    return super.update(id, postData.eligibilityCriteria).then(() => {

      return ecPreReq.deleteByConditions({ eligibilityCriteriaId: id, programId: { [Sequelize.Op.in]: postData['deletedPreReqIds'] } }).then(() => {

        return ecSubject.deleteByConditions({ eligibilityCriteriaId: id, courseId: { [Sequelize.Op.in]: postData['deletedSubjectIds'] } }).then(() => {

          return ecPassingYear.deleteByConditions({ eligibilityCriteriaId: id, passingYearId: { [Sequelize.Op.in]: postData['deletedPassingYearIds'] } }).then(() => {

            return Promise.each(postData['ecPreReqProgram'], (item) => {

              return ecPreReq.findByCondition(['id'], { eligibilityCriteriaId: id, programId: item }).then(ecRes => {
                if (ecRes == null) {
                  let obj = {
                    eligibilityCriteriaId: id, programId: item
                  }
                  return ecPreReq.create(obj);
                }

              })

            }).then(() => {

              return Promise.each(postData['ecSubject'], (item) => {

                return ecSubject.findByCondition(['id'], { eligibilityCriteriaId: id, courseId: item }).then(ecSubj => {
                  if (ecSubj == null) {
                    let obj = {
                      eligibilityCriteriaId: id, courseId: item
                    }
                    return ecSubject.create(obj);
                  }

                })

              }).then(() => {
                return Promise.each(postData['ecPassingYear'], (item) => {

                  return ecPassingYear.findByCondition(['id'], { eligibilityCriteriaId: id, passingYearId: item }).then(ecPass => {
                    if (ecPass == null) {
                      let obj = {
                        eligibilityCriteriaId: id, passingYearId: item
                      }
                      return ecPassingYear.create(obj);
                    }

                  })

                })
              })
            })


          })

        })

      })

    })

  }



  deleteWithAssociatedRecords(id) {

    let ecPreReqModel = new ECPreReqProgramModel();
    let ecSubjectModel = new ECSubjectModel();
    let ecPassingYearModel = new ECPassingYearModel();

    return ecPreReqModel.deleteByConditions({ eligibilityCriteriaId: id }).then(() => {
      return ecSubjectModel.deleteByConditions({ eligibilityCriteriaId: id }).then(() => {
        return ecPassingYearModel.deleteByConditions({ eligibilityCriteriaId: id }).then(() => {
          return super.delete(id);
        })

      })
    })

  }

  deleteByConditionsWithAssociatedRecords(condition) {
    let ecPreReqModel = new ECPreReqProgramModel();
    let ecSubjectModel = new ECSubjectModel();
    let ecPassingYearModel = new ECPassingYearModel();

    return this.findAllByConditions(['id'], condition).then(ecs => {

      return Promise.each(ecs, ec => {

        return ecPreReqModel.deleteByConditions({ eligibilityCriteriaId: ec['id'] }).then(() => {

          return ecSubjectModel.deleteByConditions({ eligibilityCriteriaId: ec['id'] }).then(() => {

            return ecPassingYearModel.deleteByConditions({ eligibilityCriteriaId: ec['id'] }).then(() => {

              return super.delete(ec['id']);

            })

          })

        })

      })
    })

  }

  findRelatedData(id) {

    let retResult = {};

    return super.find(id, ['id'], [{ model: Program, as: 'program', attributes: ['id', ['name', 'title']] }]).then((itRes) => {

      retResult = itRes['program']['dataValues'];
      retResult['label'] = 'Eligibility Criteria For Program';
      retResult['children'] = [];

      return this.findChildrenForDel(itRes['dataValues']).then(res => {

        retResult['children'] = res;

      });

    }).then(() => {
      return retResult;
    })
  }

  private findChildrenForDel(item) {

    let itRes = [];

    return this.ecPYModel.findRelatedDataWithCondition({ eligibilityCriteriaId: item['id'] }).then(ecPYRes => {

      itRes.push(ecPYRes);

      return this.ecPRPModel.findRelatedDataWithCondition({ eligibilityCriteriaId: item['id'] }).then(ecPRPRes => {

        itRes.push(ecPRPRes);

        return this.esSModel.findRelatedDataWithCondition({ eligibilityCriteriaId: item['id'] }).then(esSRes => {

          itRes.push(esSRes);

        })

      })

    }).then(() => {
      return itRes;
    });
  }

}
