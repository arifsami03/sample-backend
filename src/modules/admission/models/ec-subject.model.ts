import { BaseModel } from '../../base';
import { ECSubject } from '..';
import { Course } from '../../academic';
import * as _ from 'lodash';

export class ECSubjectModel extends BaseModel {
  constructor() {
    super(ECSubject);
  }

  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Course';
    retResult['records'] = [];

    return this.findAllByConditions(['id'], condition, [{ model: Course, as: 'course', attributes: ['id', 'title'] }]).then((ttdCRes) => {

      _.each(ttdCRes, item => { retResult['records'].push(item['course']) });

      return retResult;

    });
  }
}
