import { BaseModel } from '../../base';
import { CAProgramDetail } from '..';
import * as _ from 'lodash';
import { ProgramDetail } from '../../academic';

export class CAProgramDetailModel extends BaseModel {
  constructor() {
    super(CAProgramDetail);
  }

  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Program Detail';
    retResult['records'] = [];

    return this.findAllByConditions(['id'], condition,[{model:ProgramDetail,as :'programDetail',attributes:['id',['name','title']]}]).then((cacRes) => {

      _.each(cacRes, item => { retResult['records'].push(item['programDetail']) });

      return retResult;

    });
  }
}
