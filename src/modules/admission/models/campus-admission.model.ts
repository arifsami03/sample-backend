import { BaseModel } from '../../base';
import { CampusAdmission, CAProgramDetail, CAProgramDetailModel } from '..';
import { Campus, CampusProgramDetailModel, CampusProgramModel } from '../../campus';
import { ProgramDetail, Program } from '../../academic';
import { Promise } from 'bluebird';
import { Sequelize } from 'sequelize-typescript';

export class CampusAdmissionModel extends BaseModel {
  constructor() {
    super(CampusAdmission);
  }

  private capdModel = new CAProgramDetailModel();

  indexWithRelatedData() {

    return this.sequelizeModel.findAll({
      attributes: ['id', 'campusId', 'admissionMonth', 'admissionYear', 'isActive'],

      include: [
        {
          model: Campus,
          as: 'campus',
          attributes: ['id', 'campusName']
        },
        {
          model: CAProgramDetail,
          as: 'caprogramDetails',
          attributes: ['id', 'campusAdmissionId', 'programDetailId'],
          include: [
            {
              model: ProgramDetail,
              as: 'programDetail',
              attributes: ['id', 'name']
            },
          ]

        },
      ]
    });

  }



  /**
   * Function to create Campus Admissions. 
   * @param item Model of CampusAdmission
   */
  createWithRelatedData(item) {

    // Step1: Iterate each campus ids
    // Step2: iterate each program detail ids
    // Step3: check if program detail belongs to campus
    // Step4: if not skip it
    // Step5: else check if program detail already exists in regarding campus with same month and year admission
    // Step6: if exist skip
    // Step7: if not then insert.

    let campusProgramDetailModel = new CampusProgramDetailModel();
    let campusProgramModel = new CampusProgramModel();
    let caProgramDetailModel = new CAProgramDetailModel();

    // Iterate each campus id
    return Promise.each(item['campusIds'], camId => {

      // Get Campus Program against campus id
      return campusProgramModel.findAllByConditions(['id'], { campusId: camId }).then(campusProgramIdsRes => {

        let programIds = [];
        let programDeatilsIds = [];

        //Push all the Id in array of ProgramIds
        campusProgramIdsRes.forEach(progItem => {

          programIds.push(progItem['id'])

        });

        // Get List of all the Program Details againgst all thhe campus program Id
        return campusProgramDetailModel.findAllByConditions(['programDetailId'], { campusProgramId: { [Sequelize.Op.in]: programIds } }).then(cpdRes => {

          //Push all the Id in array of ProgramDetailsIds
          cpdRes.forEach(progDetailItem => {

            programDeatilsIds.push(progDetailItem['programDetailId'])

          });

          //Iterate each Program detail Id
          return Promise.each(item['programDetailIds'], pdId => {

            // Iterate Campus Program Details
            return Promise.each(programDeatilsIds, cpdResItem => {

              // Check if program detail belongs to campus
              if (cpdResItem == pdId) {

                // Get all the campus Admission Ids against the campus id
                return this.findByCondition(['id', 'admissionMonth', 'admissionYear'], { campusId: camId, admissionMonth: item['admissionMonth'], admissionYear: item['admissionYear'] }).then(campusAdmissionIdsRes => {

                  // Check if Admission is available against the Campus
                  if (campusAdmissionIdsRes) {

                    return caProgramDetailModel.findByCondition(['id'], { programDetailId: pdId, campusAdmissionId: campusAdmissionIdsRes['id'] }).then(pdRes => {

                      if (pdRes) {

                      }
                      else {

                        let postCAProgramDetailData = {

                          campusAdmissionId: campusAdmissionIdsRes['id'],
                          programDetailId: pdId,

                        }

                        return caProgramDetailModel.create(postCAProgramDetailData);
                      }

                    })


                  }

                  else {

                    let postCampusAdmissionData = {
                      admissionMonth: item['admissionMonth'],
                      admissionYear: item['admissionYear'],
                      isActive: item['isActive'],
                      campusId: camId
                    }
                    return this.create(postCampusAdmissionData).then((createdCampusAdmissionId) => {
                      let postCAProgramDetailData = {

                        campusAdmissionId: createdCampusAdmissionId['id'],
                        programDetailId: pdId,

                      }

                      return caProgramDetailModel.create(postCAProgramDetailData);
                    })

                  }

                });

              }

            });
          });
        });

      });




    });

  }



  deleteWithAssociatedRecords(id) {

    let caProgramDetailModel = new CAProgramDetailModel();

    return caProgramDetailModel.deleteByConditions({ campusAdmissionId: id }).then(() => {

      return this.delete(id);

    })
  }

  findCampusAdmissionProgramDetailsList(id) {

    let caProgramDetailModel = new CAProgramDetailModel();

    return caProgramDetailModel.findAllByConditions(['id', 'programDetailId'], { campusAdmissionId: id },
      [
        {
          model: ProgramDetail,
          as: 'programDetail',
          attributes: ['id', 'name'],
          include: [
            {
              model: Program,
              as: 'program',
              attributes: ['id', 'name']
            },
          ]
        }]

    );

  }

  findWithRelatedData(id) {
    return this.find(id, ['id', 'campusId', 'admissionMonth', 'admissionYear', 'isActive'],
      [
        {
          model: Campus,
          as: 'campus',
          attributes: ['id', 'campusName']
        },
        {
          model: CAProgramDetail,
          as: 'caprogramDetails',
          attributes: ['id', 'campusAdmissionId', 'programDetailId'],
          include: [
            {
              model: ProgramDetail,
              as: 'programDetail',
              attributes: ['id', 'name'],
              include: [
                {
                  model: Program,
                  as: 'program',
                  attributes: ['id', 'name']
                },
              ]
            },
          ]

        },
      ]
    );
  }

  deleteCAProgramDetail(id) {
    let caProgramDetailModel = new CAProgramDetailModel();

    return caProgramDetailModel.delete(id)
  }

  createCAProgramDetail(item) {

    let caProgramDetailModel = new CAProgramDetailModel();
    return caProgramDetailModel.create(item);

  }

  updateWithRelatedData(id, item) {
    let campusAdmissionModel = new CampusAdmissionModel();

    return campusAdmissionModel.findByCondition(['id'], { admissionMonth: item['admissionMonth'], admissionYear: item['admissionYear'], campusId: item['campusId'], id: { [Sequelize.Op.ne]: id } }).then(caRes => {

      if (caRes) {
        return null;
      }
      else {
        return campusAdmissionModel.update(id, item);
      }

    })
  }

  findRelatedData(id) {

    let retResult = {};

    return this.find(id, ['id'], [{model: Campus, as: 'campus', attributes: ['id', ['campusName', 'title']]}]).then((itRes) => {

      retResult = itRes['campus']['dataValues'];
      retResult['label'] = 'Admission of Campus';
      retResult['children'] = [];

      return this.findChildrenForDel(itRes['dataValues']).then(res => {

        retResult['children'] = res;

      });

    }).then(() => {
      return retResult;
    })
  }

  private findChildrenForDel(item) {

    let itRes = [];

    return this.capdModel.findRelatedDataWithCondition({ campusAdmissionId: item['id'] }).then(capdRes => {

      itRes.push(capdRes);

    }).then(() => {
      return itRes;
    });
  }
}
