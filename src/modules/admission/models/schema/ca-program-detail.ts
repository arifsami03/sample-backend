import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import { ProgramDetail } from '../../../academic';
import { CampusAdmission } from '../..';

@Table({ timestamps: true })
export class CAProgramDetail extends Model<CAProgramDetail> {

  @ForeignKey(() => CampusAdmission)
  @Column campusAdmissionId: number;

  @ForeignKey(() => ProgramDetail)
  @Column programDetailId: number;

  
  @Column createdBy: number;
  @Column updatedBy: number;

  @BelongsTo(() => CampusAdmission, {
    foreignKey: 'campusAdmissionId'
  })
  campusAdmission: CampusAdmission;

  @BelongsTo(() => ProgramDetail, {
    foreignKey: 'programDetailId'
  })
  programDetail: ProgramDetail;

  
}