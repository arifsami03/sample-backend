import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class Document extends Model<Document> {

  @Column docFor: string;
  @Column title: string;
  @Column docType: string;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

}
