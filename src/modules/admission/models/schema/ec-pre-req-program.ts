import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Program } from '../../../academic';
import { EligibilityCriteria } from './eligibility-criteria';

@Table({ timestamps: true })
export class ECPreReqProgram extends Model<ECPreReqProgram> {

  @ForeignKey(() => Program)
  @Column programId: number;

  @ForeignKey(() => EligibilityCriteria)
  @Column eligibilityCriteriaId: number;

  @Column deleted: boolean;

  @Column createdBy: number;
  @Column updatedBy: number;

  @BelongsTo(() => Program, {
    foreignKey: 'programId'
  })
  program: Program

  @BelongsTo(() => EligibilityCriteria, {
    foreignKey: 'eligibilityCriteriaId'
  })
  eligibilityCriteria: EligibilityCriteria
}
