import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { PassingYear } from '../../../setting';
import { EligibilityCriteria } from './eligibility-criteria';

@Table({ timestamps: true })
export class ECPassingYear extends Model<ECPassingYear> {

  @ForeignKey(() => PassingYear)
  @Column passingYearId: number;

  @ForeignKey(() => EligibilityCriteria)
  @Column eligibilityCriteriaId: number;

  @Column deleted: boolean;

  @Column createdBy: number;
  @Column updatedBy: number;

  @BelongsTo(() => PassingYear, {
    foreignKey: 'passingYearId'
  })
  passingYear: PassingYear

  @BelongsTo(() => EligibilityCriteria, {
    foreignKey: 'eligibilityCriteriaId'
  })
  eligibilityCriteria: EligibilityCriteria
}
