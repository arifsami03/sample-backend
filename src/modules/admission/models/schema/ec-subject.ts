import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Course } from '../../../academic';
import { EligibilityCriteria } from './eligibility-criteria';

@Table({ timestamps: true })
export class ECSubject extends Model<ECSubject> {

  @ForeignKey(() => Course)
  @Column courseId: number;

  @ForeignKey(() => EligibilityCriteria)
  @Column eligibilityCriteriaId: number;

  @Column deleted: boolean;

  @Column createdBy: number;
  @Column updatedBy: number;

  @BelongsTo(() => Course, {
    foreignKey: 'courseId'
  })
  course: Course

  @BelongsTo(() => EligibilityCriteria, {
    foreignKey: 'eligibilityCriteriaId'
  })
  eligibilityCriteria: EligibilityCriteria
}
