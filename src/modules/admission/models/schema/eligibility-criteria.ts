import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Program } from '../../../academic';

@Table({ timestamps: true })
export class EligibilityCriteria extends Model<EligibilityCriteria> {

  @ForeignKey(() => Program)
  @Column programId: number;
  @Column ageFromYY: number;
  @Column ageFromMM: number;
  @Column ageToYY: number;
  @Column ageToMM: number;
  @Column requiredMarksFigure: number;
  @Column requiredMarksPercent: number;
  @Column totalMarks: number;
  @Column academicYear: number;
  @Column academicMonth: number;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

  @BelongsTo(() => Program, {
    foreignKey: 'programId'
  })
  program: Program
}
