import { Table, Column, Model, ForeignKey, BelongsTo,HasMany } from 'sequelize-typescript';

import { Campus } from '../../../campus';
import { CAProgramDetail } from '../..';

@Table({ timestamps: true })
export class CampusAdmission extends Model<CampusAdmission> {

  @ForeignKey(() => Campus)
  @Column campusId: number;

  
  @Column admissionMonth: number;
  @Column admissionYear: number;
  @Column isActive: boolean;


  @Column createdBy: number;
  @Column updatedBy: number;

  @BelongsTo(() => Campus, {
    foreignKey: 'campusId'
  })
  campus: Campus;

  @HasMany(()=>CAProgramDetail,{

    foreignKey:'campusAdmissionId'
  })
  caprogramDetails:CAProgramDetail


}