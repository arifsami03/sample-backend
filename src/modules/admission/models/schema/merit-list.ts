import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Scholarship } from '../../../fee-management';
import { ProgramDetail } from '../../../academic';

@Table({ timestamps: true })
export class MeritList extends Model<MeritList> {
  @ForeignKey(() => ProgramDetail)
  @Column
  programDetailId: number;
  @Column academicYear: number;
  @Column title: string;
  @Column percentageFrom: number;
  @Column percentageTo: number;

  @ForeignKey(() => Scholarship)
  @Column
  scholarshipId: number;
  @Column numberOfSeats: number;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

  /**
   * BelongsTo Relationships
   */
  @BelongsTo(() => ProgramDetail, { foreignKey: 'programDetailId' })
  programDetail: ProgramDetail;

  @BelongsTo(() => Scholarship, { foreignKey: 'scholarshipId' })
  scholarship: Scholarship;
}
