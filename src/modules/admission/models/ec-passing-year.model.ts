import { BaseModel } from '../../base';
import { ECPassingYear } from '..';
import { PassingYear } from '../../setting';
import * as _ from 'lodash';

export class ECPassingYearModel extends BaseModel {
  constructor() {
    super(ECPassingYear);
  }

  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Passing Year';
    retResult['records'] = [];

    return this.findAllByConditions(['id'], condition, [{ model: PassingYear, as: 'passingYear', attributes: ['id', ['year', 'title']] }]).then((ttdCRes) => {

      _.each(ttdCRes, item => { retResult['records'].push(item['passingYear']) });

      return retResult;

    });
  }
}
