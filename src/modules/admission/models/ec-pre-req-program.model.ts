import { BaseModel } from '../../base';
import { ECPreReqProgram } from '..';
import { Program } from '../../academic';
import * as _ from 'lodash';

export class ECPreReqProgramModel extends BaseModel {
  constructor() {
    super(ECPreReqProgram);
  }

  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Pre-requisite Program';
    retResult['records'] = [];

    return this.findAllByConditions(['id'], condition, [{ model: Program, as: 'program', attributes: ['id', ['name', 'title']] }]).then((ecpreReqPRes) => {

      _.each(ecpreReqPRes, item => { retResult['records'].push(item['program']) });

      return retResult;

    });
  }
}
