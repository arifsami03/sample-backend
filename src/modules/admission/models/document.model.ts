import { BaseModel } from '../../base';
import { Document } from '..';
import { Sequelize } from 'sequelize-typescript';


export class DocumentModel extends BaseModel {
  constructor() {
    super(Document);
  }
  list(){
    return this.sequelizeModel.findAll({
      attributes: [[Sequelize.fn('DISTINCT', Sequelize.col('docFor')), 'docFor'],[Sequelize.fn('COUNT', 'docFor'), 'docForCount']],
      group: ['docFor'],
    })
  }
}
