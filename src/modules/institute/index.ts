export * from './routes/base/institutes.base.route';

/**
 * institue
 */
export * from './routes/institute.route';
export * from './controllers/institutes.controller';
export * from './models/institute.model';
export * from './models/schema/institute';

/**
 * Education Level
 */
export * from './routes/education-levels.routes';
export * from './controllers/education-levels.controller';
export * from './models/education-level.model';
export * from './models/schema/education-level';

/**
 * Institute Type
 */
export * from './routes/institute-types.routes';
export * from './controllers/institute-types.controller';
export * from './models/institute-type.model';
export * from './models/schema/institute-type';



/**
 * Campus Academic Calendar
 */
export * from './routes/campus-academic-calendar.routes';
export * from './controllers/campus-academic-calendar.controller';
export * from './models/campus-academic-calendar.model';
export * from './models/schema/campus-academic-calendar';

