import * as express from "express";


import { InstituteTypeModel } from '..';
import { ErrorHandler } from "../../base/conf/error-handler";

export class InstituteTypesController {
  constructor() { }

  /**
   * Get All Recrods
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {

    new InstituteTypeModel().findAll().then(result => {

      res.send(result);

    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Get All Recrods with id, name attributes only
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesList(req: express.Request, res: express.Response, next: express.NextFunction) {

    new InstituteTypeModel().findAll(['id', 'name']).then(result => {

      res.send(result);

    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Find single record
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;

    new InstituteTypeModel().find(id).then(result => {

      res.json(result);

    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Create record
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {

    new InstituteTypeModel().create(req.body).then(result => {

      res.json(result);

    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Delete one record
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;

    new InstituteTypeModel().deleteOne(id).then(result => {
      res.json(result);
    })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update one record
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {

    let item = req.body;
    let id = req.params.id;

    new InstituteTypeModel().update(id, item).then(result => {

      res.json(result);

    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }
}
