import * as express from 'express';
 
import { ErrorHandler } from '../../base/conf/error-handler';

import { InstituteModel } from '..';

export class InstitutesController {
  constructor() { }

  /**
   * Index
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    let key = req.params.key;

    new InstituteModel()
      .findInstitute()
      .then(result => {
        // ErrorHandler.sendServerError('err', res, next);
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    let item = req.body;

    new InstituteModel()
      .updateInstitute(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
}
