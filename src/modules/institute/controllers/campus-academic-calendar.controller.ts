import * as express from 'express';

import { ErrorHandler } from '../../base/conf/error-handler';
import { CampusAcademicCalendarModel } from '..';

export class CampusAcademicCalendarController {
  constructor() { }

  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CampusAcademicCalendarModel()
      .list()
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Find single record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new CampusAcademicCalendarModel()
      .find(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Find single record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findCampusByACPD(req: express.Request, res: express.Response, next: express.NextFunction) {
    let acpdId = req.params.acpdId;

    new CampusAcademicCalendarModel()
      .findCampusByACPD(acpdId)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }


  /**
   * Find  records on basis of campus id
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findWithAcademicCalendars(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new CampusAcademicCalendarModel()
      .findWithAcademicCalendars(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Create record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  createWithCampusAndAcademicCalendarProgramDetails(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CampusAcademicCalendarModel()
      .createWithCampusAndAcademicCalendarProgramDetails(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Create record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  updateWithCampusAndAcademicCalendarProgramDetails(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CampusAcademicCalendarModel()
      .updateWithCampusAndAcademicCalendarProgramDetails(req.params.id, req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Delete one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  deleteAll(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new CampusAcademicCalendarModel()
      .deleteByConditions({ campusId: id })
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Delete one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new CampusAcademicCalendarModel()
      .delete(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;

    new CampusAcademicCalendarModel()
      .update(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
  * Find single record
  *
  * @param req express.Request
  * @param res express.Response
  * @param next express.NextFunction
  */
  findByCampusAndAcademicCalendarProgramDetail(req: express.Request, res: express.Response, next: express.NextFunction) {
    let campusId = req.params.campusId;
    let academicCalendarProgramDetailId = req.params.academicCalendarProgramDetailId;


    new CampusAcademicCalendarModel()
      .findByCondition(['id'], { campusId: campusId, academicCalendarProgramDetailId: academicCalendarProgramDetailId })
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  
}
