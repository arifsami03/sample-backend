import { CONFIGURATIONS } from '../../base/conf/configurations';
import { Promise } from 'bluebird';
import * as _ from 'lodash';
import { BaseModel } from '../../base';

import { CampusAcademicCalendar } from '..';
import { AcademicCalendar, AcademicCalendarProgramDetail, ProgramDetail, Program, AcademicCalendarProgramDetailModel } from '../../academic';
import { Campus, CampusModel, CampusProgramModel, CampusProgramDetail } from '../../campus';


export class CampusAcademicCalendarModel extends BaseModel {
  constructor() {
    super(CampusAcademicCalendar);
  }
  list() {
    // finding the campus using group by and their count respectively
    return this.sequelizeModel
      .findAndCountAll({
        attributes: ['campusId'], where: { deleted: false },
        group: ['campusId']
      })
      .then(result => {
        // initialize empty list to push data
        let listOfDataAndCount = [];
        let arrayOfCampusIds = result['rows'];
        let counts = result['count'];
        let counter = 0;
        // iterate array of campus
        return Promise.each(arrayOfCampusIds, campusIdObj => {
          // finding the campus against campus
          return new CampusModel().findByCondition(['id', 'campusName'], { id: campusIdObj['campusId'] }).then(campus => {
            // pushing desired data into the array to return this is the array we want to return
            listOfDataAndCount.push({ id: campus['id'], campusName: campus['campusName'], count: counts[counter]['count'] });

            counter++;
          });
        }).then(finalResult => {
          return listOfDataAndCount;
        });
      });
  }
  /**
   * creating campus acdemic calendar with assocaited campus and academic calendar program details
   * @param item campus academic calendar program detail data
   */
  createWithCampusAndAcademicCalendarProgramDetails(item) {
    let academicCalendarProgramDetailIds: number[] = item['academicCalendarProgramDetailId'];
    let campusIds: number[] = item['campusId'];
    let mapDates = item['mapDates'];

    let campusProgramModel = new CampusProgramModel();
    let academicCalendarProgramDetailModel = new AcademicCalendarProgramDetailModel();

    // iteration of campuses ids
    return Promise.each(campusIds, campusId => {

      // iteration of academic calendar program details ids
      return Promise.each(academicCalendarProgramDetailIds, (academicCalendarProgramDetailId, index) => {

        // checking if record already exists
        return this.findByCondition(['id'], {

          campusId: campusId,
          academicCalendarProgramDetailId: academicCalendarProgramDetailId

        }).then(_result => {

          if (!_result) {
            // create or update only those with associated campus academic calendar program detail

            let isExists = false;

            return campusProgramModel.findAllByConditions(['id', 'campusId', 'programId'], { campusId: campusId }, [
              { model: CampusProgramDetail, as: 'campusProgramDetails', attributes: ['programDetailId'] }
            ]).then(result => {


              return Promise.each(result, campusProgram => {
                return Promise.each(campusProgram['campusProgramDetails'], campusProgramDetail => {
                  if (!isExists) {
                    return academicCalendarProgramDetailModel.findByCondition(['programDetailId'], { programDetailId: campusProgramDetail['programDetailId'] }).then(pdId => {
                      if (pdId) {
                        isExists = true;
                      }
                    })
                  }
                });

              }).then(() => {
                if (isExists) {
                  return this.create({

                    campusId: campusId,
                    academicCalendarProgramDetailId: academicCalendarProgramDetailId

                  }).then(() => {

                    // now although we are getting the start and end date from here but,
                    //  we are entering it in Academic calendar Program Detail

                    return new AcademicCalendarProgramDetailModel().update(academicCalendarProgramDetailId, {

                      startDate: mapDates[index]['startDate'],
                      endDate: mapDates[index]['endDate'],
                      updatedBy: CONFIGURATIONS.identity.userId

                    });

                  });
                }
              });

            })
          }
        });
      });
    });
  }

  findWithAcademicCalendars(id) {
    let acpdAttr = ['id', 'academicCalendarId', 'programDetailId', 'startDate', 'endDate'];
    let includeObj = [
      { model: Campus, as: 'campus', attributes: ['id', 'campusName'], where: BaseModel.cb(), required: false },
      {
        model: AcademicCalendarProgramDetail, as: 'academicCalendarProgramDetail', attributes: acpdAttr, where: BaseModel.cb(), required: false,
        include: [
          { model: AcademicCalendar, as: 'academicCalendar', attributes: ['id', 'title'], where: BaseModel.cb(), required: false },
          {
            model: ProgramDetail, as: 'programDetail', attributes: ['id', 'name'], where: BaseModel.cb(), required: false,
            include: [{ model: Program, as: 'program', attributes: ['id', 'name'], where: BaseModel.cb(), required: false }
            ]
          }
        ]
      }
    ];

    return this.findAllByConditions(['id', 'campusId', 'academicCalendarProgramDetailId'], { campusId: id }, includeObj);
  }

  /**
   * Finding the academic calendar program detail with assocaited campus
   *  on the basis of the acpdId
   * @param acpdId {number}
   */
  findCampusByACPD(acpdId) {
    let includeObj = [{ model: Campus, as: 'campus', attributes: ['id', 'campusName'], where: BaseModel.cb(), required: false }];

    return this.findAllByConditions(['id', 'academicCalendarProgramDetailId'], { academicCalendarProgramDetailId: acpdId }, includeObj);
  }

  /**
   * Update program details against academic calendar
   * @param _id campus id
   * @param item
   */
  updateWithCampusAndAcademicCalendarProgramDetails(_id, item) {
    let campusProgramModel = new CampusProgramModel();
    let academicCalendarProgramDetailModel = new AcademicCalendarProgramDetailModel();

    let includeObj = [
      { model: AcademicCalendarProgramDetail, as: 'academicCalendarProgramDetail', attributes: ['id', 'academicCalendarId', 'programDetailId'], where: BaseModel.cb(), required: false }
    ];

    return this.findAllByConditions(['id', 'academicCalendarProgramDetailId'], { campusId: _id }, includeObj)
      .then(_acResult => {
        // getting existing ids
        let academicCalendarProgramDetailIds: number[] = [];
        if (_acResult) {
          _acResult.forEach(element => {
            academicCalendarProgramDetailIds.push(element.academicCalendarProgramDetailId);
          });
        }

        let existingProgramDetailIds = academicCalendarProgramDetailIds;
        let inComingProgramDetailIds: number[] = item['academicCalendarProgramDetailId'];

        // Matching and adding new one (programDetailId)
        let addCount = 0;
        inComingProgramDetailIds.forEach(InComingPdi => {
          let exist: boolean = false;
          // Matching
          existingProgramDetailIds.forEach(existingPdi => {
            if (InComingPdi === existingPdi) {
              exist = true;
            }
          });
          if (!exist) {
            // create or update only those with associated campus academic calendar program detail

            let isExists = false;

            return campusProgramModel.findAllByConditions(['id', 'campusId', 'programId'], { campusId: _id }, [
              { model: CampusProgramDetail, as: 'campusProgramDetails', attributes: ['programDetailId'] }
            ]).then(result => {


              return Promise.each(result, campusProgram => {
                return Promise.each(campusProgram['campusProgramDetails'], campusProgramDetail => {
                  if (!isExists) {
                    return academicCalendarProgramDetailModel.findByCondition(['programDetailId'], { programDetailId: campusProgramDetail['programDetailId'] }).then(pdId => {
                      if (pdId) {
                        isExists = true;
                      }
                    })
                  }
                });

              }).then(() => {
                if (isExists) {
                  // Adding
                  let acpdItem = {
                    campusId: _id,
                    academicCalendarProgramDetailId: InComingPdi
                    // startDate: item['mapDates'][addCount]['startDate'],
                    // endDate: item['mapDates'][addCount]['endDate']
                  };
                  return this.create(acpdItem).then(acpd_result => {
                    // now although we are getting the start and end date from here but,
                    //  we are entering it in Academic calendar Program Detail
                    return new AcademicCalendarProgramDetailModel().update(InComingPdi, {
                      startDate: item['mapDates'][addCount]['startDate'],
                      endDate: item['mapDates'][addCount]['endDate'],
                      updatedBy: CONFIGURATIONS.identity.userId

                    });
                  });
                }
              });
            })
          }
          addCount++;
        });
        // Matching and updating new one (programDetailId)
        let updateCount = 0;
        inComingProgramDetailIds.forEach(InComingPdi => {
          let exist: boolean = false;
          // Matching
          for (let i = 0; i < existingProgramDetailIds.length; i++) {
            if (InComingPdi === existingProgramDetailIds[i]) {
              // Updating
              let acpdItem = {
                campusId: _id,
                academicCalendarProgramDetailId: InComingPdi
              };
              acpdItem = BaseModel.extendItem(acpdItem, false);
              this.sequelizeModel.update(acpdItem, {
                where: {
                  campusId: _id,
                  academicCalendarProgramDetailId: InComingPdi
                }
              });
              break;
            }
          }
          updateCount++;
        });

        // Matching and deleteing previous one  (programDetailId)
        existingProgramDetailIds.forEach(existingPdi => {
          let exist: boolean = false;
          // Matching
          inComingProgramDetailIds.forEach(InComingPdi => {
            if (InComingPdi === existingPdi) {
              exist = true;
            }
          });
          if (!exist) {
            return this.deleteByConditions({
              campusId: _id,
              academicCalendarProgramDetailId: existingPdi
            }).then(acpd_result => {
              return acpd_result;
            });
          }
        });
        return _acResult;
      });
  }


}
