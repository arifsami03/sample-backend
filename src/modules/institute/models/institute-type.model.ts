import { BaseModel } from '../../base';
import { InstituteType } from '../';
import {  CRApplication } from '../../campus';
import { EducationLevel} from '../';

export class InstituteTypeModel extends BaseModel {
  constructor() {
    super(InstituteType);
  }

  /**
   * Check for associated records and then delete or returns associated records
   * 
   * @param id 
   */
  deleteOne(id) {

    return this.checkForChildren(id).then(res => {
      if (res['children'].length > 0) {

        return res;

      } else {

        return this.delete(id);

      }
    });

  }

  /**
   * Check if children exists
   * 
   * @param id 
   */
  checkForChildren(id) {
    let retResult = {};

    let includeObj = [
      { model: EducationLevel, as: 'educationLevels', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: CRApplication, as: 'crApplications', attributes: ['id'], where: BaseModel.cb(), required: false }
    ];

    return this.find(id, ['id', ['name', 'title']], includeObj).then(res => {

      retResult['id'] = res['dataValues']['id'];
      retResult['label'] = 'Institute Type';
      retResult['title'] = res['dataValues']['title'];
      retResult['children'] = [];

      if (res['educationLevels'].length > 0) {
        retResult['children'].push({ title: 'Level of Education' });
      }

      if (res['crApplications'].length > 0) {
        retResult['children'].push({ title: 'Campus Register Applications' });
      }

      return retResult;
    })
  }

  // deleteWithAssociatedRecords(id) {
  //   return new CRApplicationModel().deleteByConditionsWithAssociatedRecords({ instituteTypeId: id }).then(() => {
  //     return new EducationLevelModel().deleteByConditionsWithAssociatedRecords({ instituteTypeId: id }).then(() => {
  //       return this.delete(id);
  //     })
  //   })

  // }

  // deleteByConditionsWithAssociatedRecords(condition) {


  //   return this.findAllByConditions(['id'], condition).then(instituteTypes => {
  //     let crAppModel = new CRApplicationModel();
  //     let eduLevelModel = new EducationLevelModel();

  //     return Promise.each(instituteTypes, instituteType => {

  //       return crAppModel.deleteByConditionsWithAssociatedRecords({ instituteTypeId: instituteType['id'] }).then(() => {

  //         return eduLevelModel.deleteByConditionsWithAssociatedRecords({ instituteTypeId: instituteType['id'] }).then(() => {

  //           return this.delete(instituteType['id']);

  //         })
  //       })
  //     })
  //   })
  // }
}
