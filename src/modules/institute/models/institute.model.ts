import { Sequelize } from 'sequelize-typescript';
import { BaseModel } from '../../base';
import { Institute } from '..';
import { ConfigurationModel } from '../../setting';
import { Promise } from 'bluebird';


export class InstituteModel extends BaseModel {
  constructor() {
    super(Institute);
  }

  public findInstitute() {

    let mcModel = new ConfigurationModel();

    return super.findByCondition([
      'id', 'name', 'logo', 'abbreviation', 'website', 'officeEmail', 'headOfficeLocation', 'latitude', 'longitude',
      'establishedScince', 'focalPerson', 'Description','instituteTypeId'], {}).then((data) => {

        let result = data;
        result['dataValues']['contactNumbers'] = [];
        result['dataValues']['affiliation'] = [];


        return mcModel.findAllByConditions(
          ['id', 'key', 'value'],
          { [Sequelize.Op.or]: [{ key: 'instituteContact' }, { key: 'instituteAffiliation' }] }
        ).then((metaData) => {
          metaData.forEach(meta => {
            if (meta.key == 'instituteContact') {
              result['dataValues']['contactNumbers'].push({ id: meta.id, contact: meta.value });
            }
            else if (meta.key == 'instituteAffiliation') {
              result['dataValues']['affiliation'].push({ id: meta.id, affiliationa: meta.value });
            }
          });
          return result;
        })

      });

  }

  public updateInstitute(id, item) {

    return this.update(id, item['institute']).then(result => {

      return this.createContacts(item);

    }).then(res => {

      return this.createAffiliations(item);

    })
      .then(result => {

        return this.updateContacts(item);

      })
      .then(res => {

        return this.updateAffiliations(item);

      })
      .then(res => {

        return this.deleteContacts(item);

      })
      .then(res => {

        return this.deleteAffiliations(item);

      })
  }

  /**
   * Function to add bulk of contacts in  Configuration
   * @param item post Data of Institute
   */
  private createContacts(item) {

    let mcModel = new ConfigurationModel();

    var conatacts = [];

    for (var i = 0; i < item['institute']['contactNumbers'].length; i++) {

      if (item['institute']['contactNumbers'][i].contact != '' && item['institute']['contactNumbers'][i].id == '') {
        conatacts.push({
          key: 'instituteContact',
          value:item['institute']['contactNumbers'][i].countryCode + '-'+ item['institute']['contactNumbers'][i].contact
        })
      }
    }
    return mcModel.sequelizeModel.bulkCreate(conatacts);
  }


  /**
   * Function to add bulk of Affiliations in Configuration
   * @param item post Data of Institute
   */
  private createAffiliations(item) {

    let mcModel = new ConfigurationModel();
    var affiliations = [];

    for (var j = 0; j < item['institute']['affiliation'].length; j++) {
      if (item['institute']['affiliation'][j].affiliationa != '' && item['institute']['affiliation'][j].id == '') {
        affiliations.push({
          key: 'instituteAffiliation',
          value: item['institute']['affiliation'][j].affiliationa
        })
      }
    }

    return mcModel.sequelizeModel.bulkCreate(affiliations);
  }

  /**
   * Function to delete Contacts from configuration
   * @param item post Data of Institute
   */
  private deleteContacts(item) {

    return new Promise((resolve, reject) => {

      let mcModel = new ConfigurationModel();

      if (item['deletedContactIds'].length != 0) {

        return mcModel.deleteByConditions({ id: item['deletedContactIds'] }).then(res => {
          resolve(res);
        });

      }
      else {
        resolve(true);
      }
    })
  }

  /**
   * Function to delete Affiliations from configuration
   * @param item  post Data of Institute
   */
  private deleteAffiliations(item) {

    return new Promise((resolve, reject) => {

      let mcModel = new ConfigurationModel();

      if (item['deletedAffiliationIds'].length != 0) {

        return mcModel.deleteByConditions({ id: item['deletedAffiliationIds'] }).then(res => {
          resolve(res);
        });
      }
      else {
        resolve(true);
      }
    })
  }

  /**
   * update Contacts of Institute
   * @param item Post Data of Institute
   */
  private updateContacts(item) {

    let mcModel = new ConfigurationModel();

    return Promise.each(item['institute']['contactNumbers'], (contactItem) => {

      if (contactItem['id'] != '') {
        return mcModel.update(contactItem['id'], { value:contactItem['countryCode'] +'-'+  contactItem['contact'] }).then(res => {
        });
      }

    })

  }

  /**
   * Function to Update Institute
   * @param item Post Data of Institute
   */
  private updateAffiliations(item) {

    let mcModel = new ConfigurationModel();

    return Promise.each(item['institute']['affiliation'], (affiliationItem) => {

      if (affiliationItem['id'] != '') {
        return mcModel.update(affiliationItem['id'], { value: affiliationItem['affiliationa'] }).then(res => {
        });
      }

    })

  }

  

}
