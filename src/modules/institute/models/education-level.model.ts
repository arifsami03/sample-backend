import { BaseModel } from '../../base';
import { EducationLevel, InstituteType } from '../';
import { CRApplication, CRMOU } from '../../campus';
import {  Program } from '../../academic';
import { LicenseFee } from '../../fee-management';
export class EducationLevelModel extends BaseModel {
  constructor() {
    super(EducationLevel);
  }


  /**
  * Find all records
  */
  findAllWithAssociations() {
    let includeObj = [{ model: InstituteType, as: 'instituteType', attributes: ['id', 'name'], where: BaseModel.cb(), required: false }];

    return this.findAll(['id', 'education', 'instituteTypeId'], null, includeObj);
  }

  /**
   * Check for associated records and then delete or returns associated records
   * 
   * @param id 
   */
  deleteOne(id) {

    return this.checkForChildren(id).then(res => {

      if (res['children'].length > 0) {

        return res;

      } else {

        return this.delete(id);
      }
    })

  }

  /**
   * Check if children exists
   * 
   * @param id 
   */
  checkForChildren(id) {
    let retResult = {};

    let includeObj = [
      { model: Program, as: 'programs', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: CRApplication, as: 'crApplications', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: CRMOU, as: 'crmou', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: LicenseFee, as: 'managementFees', attributes: ['id'], where: BaseModel.cb(), required: false }
    ];

    return this.find(id, ['id', ['education', 'title']], includeObj).then(res => {

      retResult['id'] = res['dataValues']['id'];
      retResult['label'] = 'Level of Education';
      retResult['title'] = res['dataValues']['title'];
      retResult['children'] = [];

      if (res['programs'].length > 0) {
        retResult['children'].push({ title: 'Programs' });
      }

      if (res['crApplications'].length > 0) {
        retResult['children'].push({ title: 'Campus Register Applications' });
      }

      if (res['crmou'].length > 0) {
        retResult['children'].push({ title: 'Campus Register Application\'s MOU' });
      }

      if (res['managementFees'].length > 0) {
        retResult['children'].push({ title: 'Management Fees' });
      }
      return retResult;
    })
  }

  // deleteByConditionsWithAssociatedRecords(condition) {

  //   return this.findAllByConditions(['id'], condition).then(educationLevels => {

  //     let crAppModel = new CRApplicationModel();
  //     let progModel = new ProgramModel();
  //     let crMouModel = new CRMOUModel();
  //     let managementFeeModel = new ManagementFeeModel();
  //     return Promise.each(educationLevels, educationLevel => {

  //       return crAppModel.deleteByConditionsWithAssociatedRecords({ educationLevelId: educationLevel['id'] }).then(() => {

  //         return progModel.deleteByConditionsWithAssociatedRecords({ educationLevelId: educationLevel['id'] }).then(() => {

  //           return crMouModel.deleteByConditions({ educationLevelId: educationLevel['id'] }).then(() => {

  //             return managementFeeModel.deleteByConditionsWithAssociatedRecords({ educationLevelId: educationLevel['id'] }).then(() => {

  //               return this.delete(educationLevel['id']);

  //             })

  //           })

  //         })

  //       })

  //     })

  //   })

  // }
}
