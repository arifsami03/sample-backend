import { Table, Column, Model, HasMany } from 'sequelize-typescript';
import { EducationLevel } from './education-level';
import { CRApplication } from '../../../campus';

@Table({ timestamps: true })
export class InstituteType extends Model<InstituteType> {

  @Column name: string;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @HasMany(() => EducationLevel, { foreignKey: 'instituteTypeId' })
  educationLevels: EducationLevel[]


  @HasMany(() => CRApplication, { foreignKey: 'educationLevelId', constraints: false })
  crApplications: CRApplication[];
}
