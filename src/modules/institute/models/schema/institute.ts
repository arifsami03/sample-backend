import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class Institute extends Model<Institute> {
  @Column name: string;
  @Column logo: string;
  @Column abbreviation: string;
  @Column website: string;
  @Column officeEmail: string;
  @Column headOfficeLocation: string;
  @Column latitude: number;
  @Column longitude: number;
  @Column instituteTypeId: number;
  @Column establishedScince: string;
  @Column focalPerson: string;
  @Column Description: string;
  @Column deleted: boolean;


}
