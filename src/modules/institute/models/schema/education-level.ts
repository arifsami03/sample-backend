import { Table, Column, Model, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { InstituteType } from '../..';
import { Program } from '../../../academic';
import { CRApplication, CRMOU } from '../../../campus';
import { LicenseFee } from '../../../fee-management';

@Table({ timestamps: true })
export class EducationLevel extends Model<EducationLevel> {
  @Column education: string;

  @ForeignKey(() => InstituteType)
  @Column instituteTypeId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => InstituteType, { foreignKey: 'instituteTypeId' })
  instituteType: InstituteType;


  @HasMany(() => Program, { foreignKey: 'educationLevelId' })
  programs: Program[];

  @HasMany(() => CRMOU, { foreignKey: 'educationLevelId' })
  crmou: CRMOU[];

  @HasMany(() => LicenseFee, { foreignKey: 'educationLevelId' })
  managementFees: LicenseFee[];

  @HasMany(() => CRApplication, { foreignKey: 'educationLevelId', constraints: false })
  crApplications: CRApplication[];

}
