import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { AcademicCalendarProgramDetail } from '../../../academic';
import { Campus } from '../../../campus';

@Table({ timestamps: true })
export class CampusAcademicCalendar extends Model<CampusAcademicCalendar> {
  @ForeignKey(() => Campus)
  @Column
  campusId: number;

  @ForeignKey(() => AcademicCalendarProgramDetail)
  @Column
  academicCalendarProgramDetailId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * Belongs To Relationships
   */
  @BelongsTo(() => Campus, { foreignKey: 'campusId' })
  campus: Campus;
  @BelongsTo(() => AcademicCalendarProgramDetail, { foreignKey: 'academicCalendarProgramDetailId' })
  academicCalendarProgramDetail: AcademicCalendarProgramDetail;
}
