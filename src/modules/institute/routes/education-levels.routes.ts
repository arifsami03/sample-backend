import { Router } from 'express';

import { EducationLevelsController } from '..';

export class EducationLevelsRoute {

  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {

    let controller = new EducationLevelsController();

    this.router.route('/institute/educationLevels/index').get(controller.index);
    this.router.route('/institute/educationLevels/find/:id').get(controller.find);
    this.router.route('/institute/educationLevels/create').post(controller.create);
    this.router.route('/institute/educationLevels/update/:id').put(controller.update);
    this.router.route('/institute/educationLevels/delete/:id').delete(controller.delete);
    this.router.route('/institute/educationLevels/findAttributesList').get(controller.findAttributesList);
    this.router.route('/institute/educationLevels/findAllEducationLevels/:instituteTypeId').get(controller.findAllEducationLevels);
  }
}
