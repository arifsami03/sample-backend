import { Request, Response, Router } from 'express';

import { InstitutesController } from '..';

/**
 * / route
 *
 * @class InstituteEditRoute
 */
export class InstituteRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class InstituteEditRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class InstituteEditRoute
   * @method create
   *
   */
  public create() {
    let controller = new InstitutesController();

    this.router.route("/institute/institutes/index").get(controller.index);

    this.router.route("/institute/institutes/update/:id").put(controller.update);

  }
}
