import { Router } from 'express';

import { CampusAcademicCalendarController } from '..';

export class CampusAcademicCalendarRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new CampusAcademicCalendarController();

    this.router.route('/institute/campusAcademicCalendars/index').get(controller.index);
    this.router.route('/institute/campusAcademicCalendars/findWithAcademicCalendars/:id').get(controller.findWithAcademicCalendars);
    this.router.route('/institute/campusAcademicCalendars/findCampusByACPD/:acpdId').get(controller.findCampusByACPD);
    this.router.route('/institute/campusAcademicCalendars/findByCampusAndAcademicCalendarProgramDetail/:campusId/:academicCalendarProgramDetailId').get(controller.findByCampusAndAcademicCalendarProgramDetail);
    this.router.route('/institute/campusAcademicCalendars/createWithCampusAndAcademicCalendarProgramDetails').post(controller.createWithCampusAndAcademicCalendarProgramDetails);
    this.router.route('/institute/campusAcademicCalendars/updateWithCampusAndAcademicCalendarProgramDetails/:id').put(controller.updateWithCampusAndAcademicCalendarProgramDetails);
    this.router.route('/institute/campusAcademicCalendars/update/:id').put(controller.update);
    this.router.route('/institute/campusAcademicCalendars/deleteAll/:id').delete(controller.deleteAll);
    this.router.route('/institute/campusAcademicCalendars/delete/:id').delete(controller.delete);
  }
}
