import { Router } from 'express';

import { InstituteTypesController } from '..';

export class InstituteTypesRoute {

  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {

    let controller = new InstituteTypesController();

    this.router.route('/institute/instituteTypes/index').get(controller.index);
    this.router.route('/institute/instituteTypes/findAttributesList').get(controller.findAttributesList);
    this.router.route('/institute/instituteTypes/find/:id').get(controller.find);
    this.router.route('/institute/instituteTypes/create').post(controller.create);
    this.router.route('/institute/instituteTypes/update/:id').put(controller.update);
    this.router.route('/institute/instituteTypes/delete/:id').delete(controller.delete);

  }
}
