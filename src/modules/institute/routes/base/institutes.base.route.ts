import { Router } from 'express';
import {
  InstituteRoute,
  EducationLevelsRoute,
  InstituteTypesRoute,
  CampusAcademicCalendarRoute,
} from '../..';

/**
 *
 *
 * @class InstituteEditRoute
 */
export class InstitutesBaseRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class BaseRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.initAll();
  }

  /**
   * init all routes
   */
  public initAll() {
    new InstituteRoute(this.router);
    new EducationLevelsRoute(this.router);
    new InstituteTypesRoute(this.router);
    new CampusAcademicCalendarRoute(this.router);
  }
}
