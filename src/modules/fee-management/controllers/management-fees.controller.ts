import * as express from "express";


import { ErrorHandler } from "../../base/conf/error-handler";
import { ManagementFeeModel } from '..';
import { CONFIGURATIONS } from "../../base";

export class ManagementFeesController {
  constructor() { }

  /**
   * Get All Recrods
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {

    new ManagementFeeModel().getAllManagementFees(['id', 'educationLevelId', 'feeType', 'amount']).then(result => {
      res.send(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Find One Record
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new ManagementFeeModel()
      .findWithAssociation(id, ['id', 'educationLevelId', 'feeType', 'amount'])
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Find latest Fee (amount) by educationLevelId
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findFee(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;
    let applicationForm = CONFIGURATIONS.FEE_TYPE.applicationForm;
    let attributes = ['id', 'amount'];

    new ManagementFeeModel().findFee(attributes, { educationLevelId: id, feeType: applicationForm }).then(result => {
      res.json(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Create
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {
    new ManagementFeeModel()
      .create(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Delete
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new ManagementFeeModel()
      .deleteOne(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
 * Find List
 *
 * @param req express.Request
 * @param res express.Response
 * @param next express.NextFunction
 */
  findAttributesList(req: express.Request, res: express.Response, next: express.NextFunction) {
    
    new ManagementFeeModel()
      .findAllByConditions(['id', 'amount'], null)
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
 * Find List By Education Level Id
 *
 * @param req express.Request
 * @param res express.Response
 * @param next express.NextFunction
 */
  findByEducationLevelId(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new ManagementFeeModel()
      .findAllByConditions(['id', 'amount'], { educationLevelId: id })
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }



  /**
   * Update
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;
    new ManagementFeeModel()
      .update(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

}
