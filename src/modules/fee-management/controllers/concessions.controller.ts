import * as express from 'express';

import { ErrorHandler } from '../../base/conf/error-handler';
import { ConcessionModel } from '..';


export class ConcessionsController {

  /** 
   * Constructor
  */
  constructor() { }


  /**
   * Get all records
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {

    new ConcessionModel().findAll().then(result => {

      res.send(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Find one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findWithFeeHeads(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;

    new ConcessionModel().findWithFeeHeads(id).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Create new record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  createWithFeeHeads(req: express.Request, res: express.Response, next: express.NextFunction) {

    new ConcessionModel().createWithFeeHeads(req.body).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Delete record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  deleteWithFeeHeads(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;

    new ConcessionModel().deleteWithFeeHeads(id).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * updateWithFeeHeads record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  updateWithFeeHeads(req: express.Request, res: express.Response, next: express.NextFunction) {

    let item = req.body;
    let id = req.params.id;

    new ConcessionModel().updateWithFeeHeads(id, item).then(result => {

      res.json(result);

    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Delete concession fee head
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  deleteConcessionFeeHead(req: express.Request, res: express.Response, next: express.NextFunction) {

    let concessionId = req.params.concessionId;
    let feeHeadId = req.params.feeHeadId;

    new ConcessionModel()
      .deleteConcessionFeeHead(concessionId, feeHeadId)
      .then(result => {

        res.json(result);

      })
      .catch(err => {

        ErrorHandler.sendServerError(err, res, next);

      });
  }

}
