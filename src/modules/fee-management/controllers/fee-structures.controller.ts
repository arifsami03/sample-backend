import * as express from 'express';
 

import { FeeStructureModel, FeeStructureFeeHeadsModel } from '..';
import { ErrorHandler } from '../../base/conf/error-handler';

export class FeeStructuresController {
  constructor() { }

  /**
   * Get all records
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    new FeeStructureModel()
      .findAllWithProgramsAndProgramDetails()
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesList(req: express.Request, res: express.Response, next: express.NextFunction) {
    new FeeStructureModel()
      .findAll(['id', 'title'])
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Find one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new FeeStructureModel()
      .findWithAssociations(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Create new record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {
    new FeeStructureModel()
      .create(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        console.log('err: ', err);
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Delete record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new FeeStructureModel()
      .delete(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;
    new FeeStructureModel().update(id, item).then(result => {
      res.json(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }


  /**
   * ********************************************************************************************************************************
   * This section is for fee structure fee head
   */


  /**
   * Get All Fee Heads for a Fee Structure
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAllFeeHeads(req: express.Request, res: express.Response, next: express.NextFunction) {
    new FeeStructureModel().findAllFeeHeads(req.params.id).then(result => {
      res.send(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Get Fee Head for a Fee Structure
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findFeeHead(req: express.Request, res: express.Response, next: express.NextFunction) {
    new FeeStructureFeeHeadsModel()
      .findByCondition(['id', 'feeStructureId', 'feeHeadsId', 'typeOfFeeHeadId', 'amount', 'managementFeeFormat', 'managementFeeAmount'], { id: req.params.id })
      .then(result => {
        res.send(result);
      }).catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
  * Get All Fee Heads which are not add to current fee structure
  *
  * @param req express.Request
  * @param res express.Response
  * @param next express.NextFunction
  */
  findAllRemainingFeeHeads(req: express.Request, res: express.Response, next: express.NextFunction) {
    /**
     * As feeHeadsId is optional for fetching remaining feeHeads so set it null first 
     */
    let feeHeadsId = null;
    if (req.params.feeHeadsId) {
      feeHeadsId = req.params.feeHeadsId;
    }
    new FeeStructureModel()
      .findAllRemainingFeeHeads(req.params.id, feeHeadsId).then(result => {
        res.send(result);
      }).catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
  * Create record i.e add fee head to fee structure
  *
  * @param req express.Request
  * @param res express.Response
  * @param next express.NextFunction
  */
  createFeeHead(req: express.Request, res: express.Response, next: express.NextFunction) {
    new FeeStructureFeeHeadsModel().create(req.body).then(result => {
      res.json(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Update record i.e fee-structure-fee-head
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  updateFeeHead(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;
    new FeeStructureFeeHeadsModel().update(id, item).then(result => {
      res.json(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Delete record i.e fee-structure-fee-head
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  deleteFeeHead(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new FeeStructureFeeHeadsModel().delete(id).then(result => {
      res.json(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

}
