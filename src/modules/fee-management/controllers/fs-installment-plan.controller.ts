import * as express from 'express';


import { FSInstallmentPlanModel } from '..';
import { ErrorHandler } from '../../base/conf/error-handler';

export class FSInstallmentPlansController {
  constructor() { }

  /**
   * Find all Installment Plans of a Fee Structure
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAll(req: express.Request, res: express.Response, next: express.NextFunction) {
    let feeStructureId = req.params.feeStructureId;
    new FSInstallmentPlanModel().findAllByConditions(['id', 'title', 'dueDayOfMonth'], { feeStructureId: feeStructureId }).then(result => {
      res.send(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Find one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new FSInstallmentPlanModel()
      .find(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Create new record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {
    new FSInstallmentPlanModel()
      .create(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        console.log('err: ', err);
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Delete record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new FSInstallmentPlanModel()
      .delete(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;
    new FSInstallmentPlanModel().update(id, item).then(result => {
      res.json(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }



}
