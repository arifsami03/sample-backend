import * as express from 'express';

import { ErrorHandler } from '../../base/conf/error-handler';
import { ConcessionFeeHeadModel } from '..';


export class ConcessionFeeHeadsController {

  /** 
   * Constructor
  */
  constructor() { }


  /**
   * Get all records
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {

    new ConcessionFeeHeadModel().findAll().then(result => {

      res.send(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Find one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findByConcession(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.concessionId;

    new ConcessionFeeHeadModel().findByConcession(id).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }


 
}
