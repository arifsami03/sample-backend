import * as express from 'express';
import { ErrorHandler } from '../../base/conf/error-handler';
import { ScholarshipModel } from '..';

export class ScholarshipController {
  constructor() { }

  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   * 
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {

    // new ScholarshipModel().findWithPagination(req.query).then(result => {
    //   res.send(result);
    // }).catch(err => {
    //   ErrorHandler.sendServerError(err, res, next);
    // });
    new ScholarshipModel().findAll(['id', 'title', 'abbreviation']).then(result => {
      res.send(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesList(req: express.Request, res: express.Response, next: express.NextFunction) {
    new ScholarshipModel()
      .findAll(['id', 'title', 'abbreviation'])
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Get All Recrods By perecentage range
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findByPercentage(req: express.Request, res: express.Response, next: express.NextFunction) {
    let fromPercentage = req.params.fromPercentage;
    let toPercentage = req.params.toPercentage;
    new ScholarshipModel()
      .findByPercentage(fromPercentage, toPercentage)
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Find single record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new ScholarshipModel()
      .find(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        res.json(err);
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Create record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {
    new ScholarshipModel()
      .createWithFeeHeads(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        console.log('err', err);

        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Delete one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new ScholarshipModel()
      .deleteOne(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Delete scholarship fee head
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  deleteScholarshipFeeHead(req: express.Request, res: express.Response, next: express.NextFunction) {
    let scholarshipId = req.params.scholarshipId;
    let feeHeadId = req.params.feeHeadId;

    new ScholarshipModel()
      .deleteScholarshipFeeHead(scholarshipId, feeHeadId)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;

    new ScholarshipModel()
      .updateWithFeeHeads(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

}
