import { BaseModel } from '../../base';
import { ScholarshipFeeHead } from '..';
import { Scholarship } from './schema/scholarship';
import * as _ from 'lodash';
import { FeeHeads } from './schema/fee-heads';

export class ScholarshipFeeHeadModel extends BaseModel {
  constructor() {
    super(ScholarshipFeeHead);
  }

  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Sholarship';
    retResult['records'] = [];

    return this.findAllByConditions(['id'], condition, [{ model: Scholarship, as: 'scholarship', attributes: ['id', 'title'] }]).then((sfhRes) => {

      _.each(sfhRes, item => { retResult['records'].push(item['scholarship']) });

      return retResult;

    });
  }

  findRelatedDataWithConditionWRTFeeHeads(condition) {

    let retResult = {};
    retResult['label'] = 'Fee Heads';
    retResult['records'] = [];

    return this.findAllByConditions(['id'], condition, [{ model: FeeHeads, as: 'feeHeads', attributes: ['id', 'title'] }]).then((sfhRes) => {

      _.each(sfhRes, item => { retResult['records'].push(item['feeHeads']) });

      return retResult;

    });
  }

}
