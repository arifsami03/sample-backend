import { BaseModel } from '../../base';
import { FeeHeads } from '..';
import { ConcessionFeeHeadModel } from './concession-fee-head.model';
import { ScholarshipFeeHeadModel } from './scholarship-fee-head.model';
import { Promise } from 'bluebird';
import { ConcessionFeeHead } from './schema/concession-fee-head';
import { ScholarshipFeeHead } from './schema/scholarship-fee-head';
import { Concession } from './schema/concession';
import { Scholarship } from './schema/scholarship';

export class FeeHeadsModel extends BaseModel {
  constructor() {
    super(FeeHeads);
  }

  // deleteWithAssociatedRecords(id) {
  //   return new ConcessionFeeHeadModel().deleteByConditions({ feeHeadId: id }).then(() => {
  //     return new ScholarshipFeeHeadModel().deleteByConditions({ feeHeadId: id }).then(() => {
  //       return this.delete(id);
  //     })
  //   })
  // }
  // deleteByConditionsWithAssociatedRecord(condition) {

  //   return this.findAllByConditions(['id'], condition).then(feeHeads => {

  //     return Promise.each(feeHeads, feeHead => {

  //       return new ConcessionFeeHeadModel().deleteByConditions({ feeHeadId: feeHead['id'] }).then(() => {
  //         return new ScholarshipFeeHeadModel().deleteByConditions({ feeHeadId: feeHead['id'] }).then(() => {
  //           return this.delete(feeHead['id']);
  //         })
  //       })

  //     })

  //   })

  // }

  /**
   * Check for associated records and then delete or returns associated records
   * 
   * @param id 
   */
  deleteOne(id) {

    return this.checkForChildren(id).then(res => {

      if (res['children'].length > 0) {

        return res;

      } else {

        return this.delete(id);
      }
    })

  }

  /**
   * Check if children exists
   * 
   * @param id 
   */
  checkForChildren(id) {
    let retResult = {};

    let includeObj = [
      { model: ConcessionFeeHead, as: 'concessionFeeHeads', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: ScholarshipFeeHead, as: 'scholarshipFeeHeads', attributes: ['id'], where: BaseModel.cb(), required: false }
    ];

    return super.find(id, ['id', 'title'], includeObj).then(res => {

      retResult['id'] = res['dataValues']['id'];
      retResult['label'] = 'Fee Heads';
      retResult['title'] = res['dataValues']['title'];
      retResult['children'] = [];

      if (res['concessionFeeHeads'].length > 0) {
        retResult['children'].push({ title: 'Concessions' });
      }

      if (res['scholarshipFeeHeads'].length > 0) {
        retResult['children'].push({ title: 'Scholarships' });
      }

      return retResult;
    })
  }
}
