import { Sequelize } from 'sequelize-typescript';
import { ScholarshipFeeHeadModel } from './scholarship-fee-head.model';
import { Promise } from 'bluebird';
import { BaseModel } from '../../base';
import { Scholarship, ScholarshipFeeHead, FeeHeads } from '../';
import {  MeritList } from '../../admission';

export class ScholarshipModel extends BaseModel {
  constructor() {
    super(Scholarship);
  }


  /**
   * Creating Fee Heads against scholarship
   * @param item
   */
  createWithFeeHeads(item) {
    let scholarshipItem = {
      title: item['title'],
      abbreviation: item['abbreviation'],
      type: item['type'],
      percentage: item['percentage']
    };

    return super.create(scholarshipItem).then(sResult => {
      if (sResult) {
        let sFeeHeadModel = new ScholarshipFeeHeadModel();

        return Promise.each(item['feeHeadId'], Fhid => {
          let sfhItem = {
            scholarshipId: sResult['id'],
            feeHeadId: Fhid
          };

          return sFeeHeadModel.create(sfhItem);
        }).then(() => {
          return sResult;
        });
      }
    });
  }

  /**
   * Update Fee Haeds against scholarship
   * @param _id scholarship id
   * @param item
   */
  updateWithFeeHeads(_id, item) {
    let scholarshipItem = {
      title: item['title'],
      abbreviation: item['abbreviation'],
      type: item['type'],
      percentage: item['percentage']
    };
    return super.update(_id, scholarshipItem).then(() => {
      return new ScholarshipFeeHeadModel().findAllByConditions(['feeHeadId'], { scholarshipId: _id }).then(_sResult => {

        let existingfeeHeadIds = _sResult;
        let inComingfeeHeadIds: number[] = item['feeHeadId'];

        // Matching and adding new one (feeHeadId)
        inComingfeeHeadIds.forEach(InComingFhi => {
          let exist: boolean = false;
          // Matching
          existingfeeHeadIds.forEach(existingFhi => {
            if (InComingFhi === existingFhi['feeHeadId']) {
              exist = true;
            }
          });
          if (!exist) {
            // Adding
            let sfhItem = {
              scholarshipId: _id,
              feeHeadId: InComingFhi
            };
            return new ScholarshipFeeHeadModel().create(sfhItem).then(sfh_result => {
              return sfh_result;
            });
          }
        });

        // Matching and deleteing previous one  (feeHeadId)
        existingfeeHeadIds.forEach(existingFhi => {
          let exist: boolean = false;
          // Matching
          inComingfeeHeadIds.forEach(InComingFhi => {
            if (InComingFhi === existingFhi['dataValues']['feeHeadId']) {
              exist = true;
            }
          });
          if (!exist) {
            // Deleting
            return new ScholarshipFeeHeadModel().deleteByConditions({ feeHeadId: existingFhi['dataValues']['feeHeadId'], scholarshipId: _id }).then(sfh_result => {
              return sfh_result;
            });
          }
        });
        return _sResult;
      });
    });
  }

  /**
   * find scholarship in by percentage range
   * @param fromPercentage {number}
   * @param toPercentage {number}
   */

  public findByPercentage(fromPercentage: number, toPercentage: number) {
    return this.findAllByConditions(['id', 'title'], { percentage: { [Sequelize.Op.between]: [fromPercentage, toPercentage] } });
  }

  /**
   * find data with Fee Haeds
   * @param _id scholarship id
   */
  public find(_id) {
    this.openConnection();
    return this.sequelizeModel.find({
      where: { id: _id },
      include: [
        {
          model: ScholarshipFeeHead,
          as: 'scholarshipFeeHeads',
          include: [
            {
              model: FeeHeads,
              as: 'feeHeads',
              attributes: ['id', 'title']
            }
          ]
        }
      ]
    });
  }

  /**
   * Delete Scholarship Fee Head
   * @param _id scholarship id
   */
  public deleteScholarshipFeeHead(_scholarshipId, _feeHeadId) {
    return new ScholarshipFeeHeadModel().deleteByConditions({
      scholarshipId: _scholarshipId,
      feeHeadId: _feeHeadId
    });
  }

  public findWithPagination(qryString) {

    //TODO:medium: Need to implement parcing condition from query string at some common place from where system can automatically 
    //create condition on the bases of provided data. Instead of repeating this method everywher.

    let condition = {}
    let order;
    if (qryString['search']) {
      condition = {
        [Sequelize.Op.or]: [
          {
            title: { [Sequelize.Op.like]: '%' + qryString['search'] + '%' }
          },
          {
            abbreviation: { [Sequelize.Op.like]: '%' + qryString['search'] + '%' }
          }
        ]
      }
    }
    if (qryString['sortAttribute'] && qryString['sortDirection']) {
      order = [[qryString['sortAttribute'], qryString['sortDirection']]];
    }

    return super.findAndCountAll(['id', 'title', 'abbreviation'], condition, null, order, qryString);
  }


  deleteWithAssociatedRecords(id) {
    return new ScholarshipFeeHeadModel().deleteByConditions({ scholarshipId: id }).then(() => {
      return this.delete(id);
    })
  }

  // deleteByConditionsWithAssociatedRecord(condition) {

  //   return this.findAllByConditions(['id'], condition).then(scholarships => {

  //     return Promise.each(scholarships, scholarship => {

  //       return new MeritListModel().deleteByConditions({ scholarshipId: scholarship['id'] }).then(() => {

  //         return this.delete(scholarship['id']);

  //       })

  //     })

  //   })

  // }

  /**
   * Check for associated records and then delete or returns associated records
   * 
   * @param id 
   */
  deleteOne(id) {

    return this.checkForChildren(id).then(res => {

      if (res['children'].length > 0) {

        return res;

      } else {

        return this.deleteWithAssociatedRecords(id);
      }
    })

  }

  /**
   * Check if children exists
   * 
   * @param id 
   */
  checkForChildren(id) {
    let retResult = {};

    let includeObj = [
      { model: MeritList, as: 'meritLists', attributes: ['id'], where: BaseModel.cb(), required: false },
    ];

    return super.find(id, ['id', 'title'], includeObj).then(res => {

      retResult['id'] = res['dataValues']['id'];
      retResult['label'] = '';
      retResult['title'] = res['dataValues']['title'];
      retResult['children'] = [];

      if (res['meritLists'].length > 0) {
        retResult['children'].push({ title: 'Merit Lists' });
      }

      return retResult;
    })
  }
}
