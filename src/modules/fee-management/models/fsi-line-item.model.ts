import { BaseModel } from '../../base';
import { FeeStructure } from '..';

export class FSILineItemModel extends BaseModel {
  constructor() {
    super(FeeStructure);
  }

}
