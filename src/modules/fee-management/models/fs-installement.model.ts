import { BaseModel } from '../../base';
import { FSInstallement } from '..';

export class FSInstallementModel extends BaseModel {
  constructor() {
    super(FSInstallement);
  }

}
