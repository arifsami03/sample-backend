import { BaseModel } from '../../base';
import { FSCFeeHead } from '..';

export class FSCFeeHeadModel extends BaseModel {
  constructor() {
    super(FSCFeeHead);
  }

}
