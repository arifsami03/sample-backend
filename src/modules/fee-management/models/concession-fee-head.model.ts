import { BaseModel } from '../../base';
import { ConcessionFeeHead, FeeHeads } from '..';
import { Concession } from './schema/concession';
import * as _ from 'lodash';

export class ConcessionFeeHeadModel extends BaseModel {
  constructor() {
    super(ConcessionFeeHead);
  }

  findByConcession(concessionId) {
    return this.findAllByConditions(null, { consessionId: concessionId }, [{ model: FeeHeads, as: 'feeHead', attributes: ['id', 'code', 'title', 'abbreviation'] }])
  }

  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Concession';
    retResult['records'] = [];

    return this.findAllByConditions(['id'], condition, [{ model: Concession, as: 'consession', attributes: ['id', 'title'] }]).then((cfhRes) => {

      _.each(cfhRes, item => { retResult['records'].push(item['consession']) });

      return retResult;

    });
  }

  findRelatedDataWithConditionWRTFeeHeads(condition) {

    let retResult = {};
    retResult['label'] = 'Fee Heads';
    retResult['records'] = [];

    return this.findAllByConditions(['id'], condition, [{ model: FeeHeads, as: 'feeHead', attributes: ['id', 'title'] }]).then((cfhRes) => {

      _.each(cfhRes, item => { retResult['records'].push(item['feeHead']) });

      return retResult;

    });
  }

}
