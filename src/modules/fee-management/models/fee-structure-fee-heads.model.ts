import { BaseModel } from '../../base';
import { FeeStructureFeeHeads } from '..';

export class FeeStructureFeeHeadsModel extends BaseModel {
  constructor() {
    super(FeeStructureFeeHeads);
  }

}
