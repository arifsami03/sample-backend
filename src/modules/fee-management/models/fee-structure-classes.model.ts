import { BaseModel } from '../../base';
import { FeeStructureClasses } from '..';

export class FeeStructureClassesModel extends BaseModel {
  constructor() {
    super(FeeStructureClasses);
  }

}
