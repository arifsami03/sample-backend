import { BaseModel } from '../../base';
import { LicenseFee } from '..';
import { EducationLevel, InstituteType } from '../../institute';
import { CRMOUModel, CRMOU } from '../../campus';
import { Promise } from 'bluebird';

export class ManagementFeeModel extends BaseModel {
  constructor() {
    super(LicenseFee);
  }

  findWithAssociation(id, attributes) {

    let includeObj = [
      {
        model: EducationLevel, as: 'educationLevel', attributes: ['id', 'education'], where: BaseModel.cb(), required: false,
        include: [{ model: InstituteType, as: 'instituteType', attributes: ['id', 'name'], where: BaseModel.cb(), required: false }]
      }
    ];

    return this.findByCondition(attributes, { id: id }, includeObj);
  }

  findFee(attributes, condition) {
    return this.findByCondition(attributes, condition);
  }

  getAllManagementFees(attributes) {

    let includeObj = [
      {
        model: EducationLevel, as: 'educationLevel', attributes: ['id', 'education'], where: BaseModel.cb(), required: false,
        include: [{ model: InstituteType, as: 'instituteType', attributes: ['id', 'name'], where: BaseModel.cb(), required: false }]
      }
    ];

    return this.findAll(attributes, null, includeObj);
  }

  // deleteWithAssociatedRecords(id) {
  //   return new CRMOUModel().deleteByConditions({ licenseFeeId: id }).then(() => {
  //     return this.delete(id);
  //   })
  // }
  // deleteByConditionsWithAssociatedRecords(condition) {

  //   return this.findAllByConditions(['id'], condition).then(licenseFees => {
  //     let crMouModel = new CRMOUModel();

  //     return Promise.each(licenseFees, licenseFee => {

  //       return crMouModel.deleteByConditions({ licenseFeeId: licenseFee['id'] }).then(() => {
  //         return this.delete(licenseFee['id']);
  //       })

  //     })

  //   })

  // }

  /**
   * Check for associated records and then delete or returns associated records
   * 
   * @param id 
   */
  deleteOne(id) {

    return this.checkForChildren(id).then(res => {

      if (res['children'].length > 0) {

        return res;

      } else {

        return this.delete(id);
      }
    })

  }

  /**
   * Check if children exists
   * 
   * @param id 
   */
  checkForChildren(id) {
    let retResult = {};

    let includeObj = [
      { model: CRMOU, as: 'crMOU', attributes: ['id'], where: BaseModel.cb(), required: false },
    ];

    return super.find(id, ['id', ['feeType', 'title']], includeObj).then(res => {

      retResult['id'] = res['dataValues']['id'];
      retResult['label'] = 'Management Fee';
      retResult['title'] = res['dataValues']['title'];
      retResult['title'] = retResult['title'] ? retResult['title'].toUpperCase() : '';
      retResult['children'] = [];

      if (res['crMOU'].length > 0) {
        retResult['children'].push({ title: 'Campus Register Application MOU' });
      }

      return retResult;
    })
  }
}
