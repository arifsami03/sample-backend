import { CONFIGURATIONS } from '../../base/conf/configurations';
import { BaseModel, Connection } from '../../base';
import { FeeStructure, FeeStructureClasses, FeeStructureFeeHeads, FeeStructureClassesModel, FeeStructureFeeHeadsModel, FeeHeadsModel, FeeHeads, } from '..';
import { Promise } from 'bluebird';
import { Sequelize } from 'sequelize-typescript';
import { Program, ProgramDetail } from '../../academic';
import { Configuration } from '../../setting';
import * as _ from 'lodash';

export class FeeStructureModel extends BaseModel {
  constructor() {
    super(FeeStructure);
  }

  findAllFeeHeads(id) {
    let attributes = ['id', 'feeHeadsId', 'typeOfFeeHeadId', 'amount', 'managementFeeFormat', 'managementFeeAmount'];

    let includeObj = [
      { model: FeeHeads, as: 'feeHead', attributes: ['id', 'title'], where: BaseModel.cb(), required: false },
      { model: Configuration, as: 'typeOfFeeHead', attributes: ['id', 'value'], where: BaseModel.cb(), required: false }
    ];

    return new FeeStructureFeeHeadsModel().findAllByConditions(attributes, { feeStructureId: id }, includeObj);
  }

  // Get All Fee Heads which are not add to current fee structure and include current fee head if any
  findAllRemainingFeeHeads(id, feeHeadsId) {
    let existedFeeHeadIds = [];
    return new FeeStructureFeeHeadsModel().findAllByConditions(['id', 'feeHeadsId'], { feeStructureId: id, feeHeadsId: { [Sequelize.Op.not]: feeHeadsId } }).then(result => {
      return Promise.each(result, item => {
        existedFeeHeadIds.push(item['feeHeadsId']);
      }).then(() => {
        return new FeeHeadsModel().findAllByConditions(['id', 'title'], { id: { [Sequelize.Op.notIn]: existedFeeHeadIds } });
      });

    });

  }

  findWithAssociations(id) {

    let attributes = ['id', 'title', 'programId', 'programDetailId'];
    let includeObj = [{ model: FeeStructureClasses, as: 'classes', attributes: ['id', 'classId'], where: BaseModel.cb(), required: false }];

    return this.findByCondition(attributes, { id: id }, includeObj);
  }

  findAllWithProgramsAndProgramDetails() {
    let includeObj = [
      { model: Program, as: 'program', attributes: ['id', 'name'], where: BaseModel.cb(), required: false },
      { model: ProgramDetail, as: 'programDetail', attributes: ['id', 'name'], where: BaseModel.cb(), required: false }
    ];

    return this.findAll(['id', 'title', 'programId', 'programDetailId'], null, includeObj);
  }

  create(item) {

    //TODO:high: remove this manual connection opening
    this.connection = new Connection().createConnection();

    return this.connection.transaction(t => {
      item = BaseModel.extendItem(item, true);
      return FeeStructure.create(item, { transaction: t }).then(result => {

        return Promise.each(item.classes, element => {

          if (element['classId']) {
            return FeeStructureClasses.create({ feeStructureId: result.id, classId: element['classId'], createdBy: CONFIGURATIONS.identity.userId, updatedBy: CONFIGURATIONS.identity.userId, }, { transaction: t });
          }

        }).then(() => {
          return result;
        });
      })
    });
  }

  private deleteFeeStructureClasses(classIds: number[]) {
    return new Promise((resolve, reject) => {
      if (classIds.length > 0) {
        return new FeeStructureClassesModel().deleteByConditions({ id: { [Sequelize.Op.in]: classIds } }).then(() => {
          return resolve(true);
        });
      } else {
        return resolve(true);
      }
    });
  }

  private updateFeeStructureClasses(feeStructureId, classes) {
    return new Promise((resolve, reject) => {
      if (classes.length > 0) {
        let feeStructureClassesModel = new FeeStructureClassesModel();
        return Promise.each(classes, element => {
          if (!element['id'] && element['classId']) {
            return feeStructureClassesModel.create({ feeStructureId: feeStructureId, classId: element['classId'] });
          } else if (element['id'] && element['classId']) {
            return feeStructureClassesModel.update(element['id'], { classId: element['classId'] });
          }
        }).then(() => {
          return resolve(true);
        })
      } else {
        return resolve(true);
      }
    });
  }

  update(id, item) {

    return super.update(id, item).then(() => {

      return this.deleteFeeStructureClasses(item.deletedClassIds).then(() => {

        return this.updateFeeStructureClasses(id, item.classes);

      });
    });

  }

  delete(id) {
    return this.connection.transaction(t => {

      return FeeStructureClasses.destroy({ where: { feeStructureId: id }, transaction: t }).then(() => {
        return FeeStructureFeeHeads.destroy({ where: { feeStructureId: id }, transaction: t }).then(() => {
          return FeeStructure.destroy({ where: { id: id }, transaction: t });
        });
      });

    });
  }
  deleteWithAssociatedRecords(id) {
    return new FeeStructureClassesModel().deleteByConditions({ feeStructureId: id }).then(() => {
      return new FeeStructureFeeHeadsModel().deleteByConditions({ feeStructureId: id }).then(() => {
        return this.delete(id);
      })
    })
  }

  deleteByConditionsWithAssociatedRecords(condition) {

    return this.findAllByConditions(['id'], condition).then(feeStructures => {
      let fscModel = new FeeStructureClassesModel();
      let fsFeeHeadModel = new FeeStructureFeeHeadsModel();
      return Promise.each(feeStructures, feeStructure => {

        return fscModel.deleteByConditions({ feeStructureId: feeStructure['id'] }).then(() => {
          return fsFeeHeadModel.deleteByConditions({ feeStructureId: feeStructure['id'] }).then(() => {
            return this.delete(feeStructure['id']);
          })
        })

      })
    })

  }

  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Fee Structure';
    retResult['records'] = [];

    return this.findAllByConditions(['id', 'title'], condition).then((pdRes) => {

      _.each(pdRes, item => { retResult['records'].push(item['dataValues']) });

      return retResult;

    });
  }

}
