import { BaseModel } from '../../base';
import { FS_InstallmentPlan } from '..';

export class FSInstallmentPlanModel extends BaseModel {
  constructor() {
    super(FS_InstallmentPlan);
  }

}
