import { Promise } from 'bluebird';
import { FSCFeeHead, FSCFeeHeadModel, FSCampus, FeeHeads, FeeStructure } from '..';
import { BaseModel } from '../../base';
import { ErrorHandler } from '../../base/conf/error-handler';
import { Campus } from '../../campus';

export class FSCampusModel extends BaseModel {
  constructor() {
    super(FSCampus);
  }

  findAllWithCampusAndFeeStructuer() {
    let includeObj = [
      { model: FeeStructure, as: 'feeStructure', attributes: ['id', 'title'], where: BaseModel.cb(), required: false },
      { model: Campus, as: 'campus', attributes: ['id', 'campusName'], where: BaseModel.cb(), required: false }
    ];

    return this.findAll(['id', 'feeStructureId', 'campusId'], null, includeObj);
  }

  findWithRelations(id) {

    let FSCampusAttr = ['id', 'feeStructureId', 'campusId'];
    let FSCFeeHeadAttr = ['id', 'feeHeadId', 'typeOfFeeHeadId', 'amount', 'managementFeeType', 'managementFeeAmount'];

    let includeObj = [
      {
        model: FSCFeeHead, as: 'fscFeeHeads', attributes: FSCFeeHeadAttr, where: BaseModel.cb(), required: false,
        include: [{ model: FeeHeads, as: 'feeHead', attributes: ['id', 'title'], where: BaseModel.cb(), required: false }]
      }
    ];

    return this.findByCondition(FSCampusAttr, { id: id }, includeObj);
  }

  create(item) {
    return super.findByCondition(['id'], { feeStructureId: item.feeStructureId, campusId: item.campusId }).then(record => {
      if (record) {
        return ErrorHandler.duplicateEntry;
      } else {
        let fscFeeHeadModel = new FSCFeeHeadModel();
        let fsCampusItem = { feeStructureId: item.feeStructureId, campusId: item.campusId };
        return super.create(fsCampusItem).then(result => {
          return Promise.each(item.fscFeeHeads, fscFeeHead => {
            let fscFeeHeadItem = {
              FSCId: result.id, feeHeadId: fscFeeHead['feeHeadId'], typeOfFeeHeadId: fscFeeHead['typeOfFeeHeadId'], amount: fscFeeHead['amount'],
              managementFeeType: fscFeeHead['managementFeeType'], managementFeeAmount: fscFeeHead['managementFeeAmount']
            };
            return fscFeeHeadModel.create(fscFeeHeadItem);
          })
        })
      }
    })

  }

  update(id, item) {
    let fscFeeHeadModel = new FSCFeeHeadModel();
    let fsCampusItem = { feeStructureId: item.feeStructureId, campusId: item.campusId };
    return super.update(id, fsCampusItem).then(result => {
      return Promise.each(item.fscFeeHeads, fscFeeHead => {
        let fscFeeHeadItem = {
          feeHeadId: fscFeeHead['feeHeadId'], typeOfFeeHeadId: fscFeeHead['typeOfFeeHeadId'], amount: fscFeeHead['amount'],
          managementFeeType: fscFeeHead['managementFeeType'], managementFeeAmount: fscFeeHead['managementFeeAmount']
        };
        return fscFeeHeadModel.update(fscFeeHead['id'], fscFeeHeadItem);
      })
    })
  }

  delete(id) {
    // First delete its child records from FSCFeeHead and then delete record from FSCampus
    return new FSCFeeHeadModel().deleteByConditions({ FSCId: id }).then(() => {
      return super.delete(id);
    })
  }

}
