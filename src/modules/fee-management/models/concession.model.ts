import { Promise } from 'bluebird';
import { BaseModel } from '../../base';
import { ConcessionFeeHeadModel, Concession, ConcessionFeeHead, FeeHeads } from '..';

export class ConcessionModel extends BaseModel {
  constructor() {
    super(Concession);
  }

  private cfhModel = new ConcessionFeeHeadModel();

  /**
   * Find concessions with assigned fee heads
   *
   * @param id number
   */
  public findWithFeeHeads(id: number) {
    return super.find(id, { id: id }, [
      {
        model: ConcessionFeeHead,
        as: 'concessionFeeHeads',
        include: [
          {
            model: FeeHeads,
            as: 'feeHead',
            attributes: ['id', 'title', 'abbreviation', 'code']
          }
        ]
      }
    ]);
  }

  /**
   * Create Concession with posted fee heads.
   *
   * @param item object
   */
  public createWithFeeHeads(item) {
    let concessionItem = {
      title: item['title'],
      MCConcessionTypeId: item['MCConcessionTypeId'],
      MCConcessionCategoryId: item['MCConcessionCategoryId'],
      concessionFormat: item['concessionFormat'],
    };

    return super.create(concessionItem).then(result => {
      if (result) {
        let CFHModel = new ConcessionFeeHeadModel();

        return Promise.each(item['concessionFeeHeads'], CFHItemRes => {
          let CFHItem = { consessionId: result['id'], feeHeadId: CFHItemRes['feeHeadId'], concessionAmount: CFHItemRes['concession'] };

          return CFHModel.create(CFHItem);
        }).then(() => {
          return result;
        });
      }
    });
  }

  /**
   * Update Concession with Fee heads.
   *
   * @param id number
   * @param item object
   */
  public updateWithFeeHeads(id: number, item) {
    let concessionItem = {
      title: item['title'],
      MCConcessionTypeId: item['MCConcessionTypeId'],
      MCConcessionCategoryId: item['MCConcessionCategoryId'],
      concessionFormat: item['concessionFormat'],
    };

    return super.update(id, concessionItem).then(result => {
      let CFHModel = new ConcessionFeeHeadModel();

      return CFHModel.findAllByConditions(['id', 'feeHeadId'], { consessionId: id }).then(existingFeeHeads => {
        let postedFeeHeads = item['concessionFeeHeads'];

        // Create New FeeHeads if Posted
        return Promise.each(postedFeeHeads, postedItem => {
          let existInDb: boolean = false;

          existingFeeHeads.forEach(existingItem => {
            if (postedItem['feeHeadId'] === existingItem['feeHeadId']) {
              existInDb = true;
            }
          });
          let CFHItem = { consessionId: id, feeHeadId: postedItem['feeHeadId'], concessionAmount: postedItem['concession'] };

          if (!existInDb) {

            return CFHModel.create(CFHItem);
          }
        })
          .then(() => {
            // Updating fee heads
            return Promise.each(postedFeeHeads, postedItem => {

              existingFeeHeads.forEach(existingItem => {
                if (postedItem['feeHeadId'] === existingItem['feeHeadId']) {
                  let CFHItem = { consessionId: id, feeHeadId: postedItem['feeHeadId'], concessionAmount: postedItem['concession'] };

                  return CFHModel.update(existingItem['id'], CFHItem);

                }
              });

            })


          })
          .then(() => {
            // Delete Saved Fee Heads if not posted.
            return Promise.each(existingFeeHeads, existingItem => {
              let existInPostedData: boolean = false;

              postedFeeHeads.forEach(postedItem => {
                if (postedItem['feeHeadId'] === existingItem['feeHeadId']) {
                  existInPostedData = true;
                };
              });

              if (!existInPostedData) {
                return CFHModel.delete(existingItem['id']);
              }
            });
            // return result;
          });
      });
    });
  }

  /**
   * Deleting consession with associated fee heads
   * @param consessionId number
   */
  public deleteWithFeeHeads(_consessionId: number) {
    // First Deleting associated ConcessionFeeHead record

    return new ConcessionFeeHeadModel().deleteByConditions({ consessionId: _consessionId }).then(() => {
      // now deleting concession
      return this.delete(_consessionId);
    });
  }

  /**
   * Delete Concession Fee Head
   * @param _id concession id
   */
  public deleteConcessionFeeHead(_concessionId, _feeHeadId) {

    return new ConcessionFeeHeadModel().deleteByConditions({
      consessionId: _concessionId,
      feeHeadId: _feeHeadId

    });
  }

  findRelatedData(id) {

    let retResult = {};

    return this.find(id, ['id', 'title']).then((itRes) => {

      retResult = itRes['dataValues'];
      retResult['label'] = 'Concession';
      retResult['children'] = [];

      return this.findChildrenForDel(itRes['dataValues']).then(res => {

        retResult['children'] = res;

      });

    }).then(() => {
      return retResult;
    })
  }

  private findChildrenForDel(item) {

    let itRes = [];

    return this.cfhModel.findRelatedDataWithConditionWRTFeeHeads({ consessionId: item['id'] }).then(cfhRes => {

      itRes.push(cfhRes);

    }).then(() => {
      return itRes;
    });
  }
}
