import { Table, Column, Model, ForeignKey, BelongsTo, HasOne } from 'sequelize-typescript';
import { FeeStructure } from '../..';
import { FeeHeads } from '../..';
import { Configuration } from '../../../setting';

@Table({ timestamps: true })
export class FeeStructureFeeHeads extends Model<FeeStructureFeeHeads> {

  @ForeignKey(() => FeeStructure)
  @Column feeStructureId: number;

  @ForeignKey(() => FeeHeads)
  @Column feeHeadsId: number;

  @ForeignKey(() => Configuration)
  @Column typeOfFeeHeadId: number;

  @Column amount: number;

  @Column managementFeeFormat: string;

  @Column managementFeeAmount: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => FeeHeads, {
    foreignKey: 'feeHeadsId'
  })
  feeHead: FeeHeads;

  @BelongsTo(() => Configuration, {
    foreignKey: 'typeOfFeeHeadId'
  })
  typeOfFeeHead: Configuration;

}
