import { Table, Column, Model, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';

import { ConcessionFeeHead } from '../../';
import { Configuration } from '../../../setting';

@Table({ timestamps: true })
export class Concession extends Model<Concession> {

  @Column title: string;

  @ForeignKey(() => Configuration)
  @Column MCConcessionTypeId: number;

  @ForeignKey(() => Configuration)
  @Column MCConcessionCategoryId: number;

  @Column concessionFormat: string;

  @Column deleted: boolean;

  @Column createdBy: number;
  @Column updatedBy: number;

  @BelongsTo(() => Configuration, {
    foreignKey: 'MCConcessionTypeId'
  })
  MCConcessionType: Configuration;

  @BelongsTo(() => Configuration, {
    foreignKey: 'MCConcessionCategoryId'
  })
  MCConcessionCategory: Configuration;

  @HasMany(() => ConcessionFeeHead, {
    foreignKey: 'consessionId'
  })
  concessionFeeHeads: ConcessionFeeHead[];
}