import { Table, Column, Model, BelongsTo, ForeignKey } from 'sequelize-typescript';
import { } from './fs-installement';
import { FeeHeads, FSInstallement } from '../..';

@Table({ timestamps: true })
export class FSILineItem extends Model<FSILineItem> {

  @ForeignKey(() => FSInstallement)
  @Column FSInstallementId: number;

  @ForeignKey(() => FeeHeads)
  @Column feeHeadId: number;

  @Column installementFormat: string;

  @Column amount: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;


  @BelongsTo(() => FSInstallement, {
    foreignKey: 'FSInstallementId'
  })
  fsInstallement: FSInstallement;


  @BelongsTo(() => FeeHeads, {
    foreignKey: 'FeeHeadId'
  })
  feeHead: FeeHeads;
}
