import { Table, Column, Model, ForeignKey } from 'sequelize-typescript';

import { FeeStructure } from '../..';
import { Classes } from '../../../academic';

@Table({ timestamps: true })
export class FeeStructureClasses extends Model<FeeStructureClasses> {

  @ForeignKey(() => FeeStructure)
  @Column feeStructureId: number;

  @ForeignKey(() => Classes)
  @Column classId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

}
