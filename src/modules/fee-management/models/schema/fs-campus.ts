import { Table, Column, Model, BelongsTo, ForeignKey, HasMany } from 'sequelize-typescript';

import { FeeStructure, FSCFeeHead } from '../..';
import { Campus } from '../../../campus';

@Table({ timestamps: true })
export class FSCampus extends Model<FSCampus> {

  @ForeignKey(() => FeeStructure)
  @Column feeStructureId: number;

  @ForeignKey(() => Campus)
  @Column campusId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => FeeStructure, {
    foreignKey: 'feeStructureId'
  })
  feeStructure: FeeStructure;


  @BelongsTo(() => Campus, {
    foreignKey: 'campusId'
  })
  campus: Campus;

  @HasMany(() => FSCFeeHead, {
    foreignKey: 'FSCId'
  })
  fscFeeHeads: FSCFeeHead[]
}
