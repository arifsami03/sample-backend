import { Table, Column, Model, HasMany } from 'sequelize-typescript';
import { ScholarshipFeeHead, FeeHeads } from '../..';
import { MeritList } from '../../../admission';

@Table({ timestamps: true })
export class Scholarship extends Model<Scholarship> {
  @Column title: string;
  @Column abbreviation: string;
  @Column type: string;
  @Column percentage: number;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

  /**
   * Has Many Relationships
   */
  @HasMany(() => ScholarshipFeeHead, { foreignKey: 'scholarshipId' })
  scholarshipFeeHeads: ScholarshipFeeHead[];

  @HasMany(() => MeritList, { constraints: false, foreignKey: 'scholarshipId' })
  meritLists: MeritList[];
}
