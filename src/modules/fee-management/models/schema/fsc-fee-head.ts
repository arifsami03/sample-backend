import { Table, Column, Model, BelongsTo, ForeignKey } from 'sequelize-typescript';

import { FeeHeads, FSCampus } from '../..';
import { Configuration } from '../../../setting';

@Table({ timestamps: true })
export class FSCFeeHead extends Model<FSCFeeHead> {

  @ForeignKey(() => FSCampus)
  @Column FSCId: number;

  @ForeignKey(() => FeeHeads)
  @Column feeHeadId: number;

  @ForeignKey(() => Configuration)
  @Column typeOfFeeHeadId: number;

  @Column amount: number;

  @Column managementFeeType: string;

  @Column managementFeeAmount: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => FSCampus, {
    foreignKey: 'FSCId'
  })
  fsCampus: FSCampus;

  @BelongsTo(() => FeeHeads, {
    foreignKey: 'feeHeadId'
  })
  feeHead: FeeHeads;


  @BelongsTo(() => Configuration, {
    foreignKey: 'typeOfFeeHeadId'
  })
  typeOfFeeHead: Configuration;
}
