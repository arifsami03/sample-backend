import { Table, Column, Model, BelongsTo, ForeignKey, HasMany } from 'sequelize-typescript';

import { Program, ProgramDetail } from '../../../academic';
import { FeeStructureClasses, FeeStructureFeeHeads } from '../..';

@Table({ timestamps: true })
export class FeeStructure extends Model<FeeStructure> {

  @Column title: string;

  @ForeignKey(() => Program)
  @Column programId: number;

  @ForeignKey(() => ProgramDetail)
  @Column programDetailId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @HasMany(() => FeeStructureClasses, {
    foreignKey: 'feeStructureId'
  })
  classes: FeeStructureClasses;

  @HasMany(() => FeeStructureFeeHeads, {
    foreignKey: 'feeStructureId'
  })
  feeHeads: FeeStructureFeeHeads;

  @BelongsTo(() => Program, {
    foreignKey: 'programId'
  })
  program: Program;


  @BelongsTo(() => ProgramDetail, {
    foreignKey: 'programDetailId'
  })
  programDetail: ProgramDetail;
}
