import { Table, Column, Model, BelongsTo, ForeignKey, HasMany } from 'sequelize-typescript';
import { FeeStructure, FSILineItem } from '../..';


@Table({ timestamps: true })
export class FSInstallement extends Model<FSInstallement> {

  @ForeignKey(() => FeeStructure)
  @Column feeStructureId: number;

  @Column title: string;

  @Column dueDate: Date;

  @Column expiryDate: Date;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @HasMany(() => FSILineItem, {
    foreignKey: 'FSInstallementId'
  })
  lineItems: FSILineItem[];

  @BelongsTo(() => FeeStructure, {
    foreignKey: 'feeStructureId'
  })
  feeStructure: FeeStructure;

}
