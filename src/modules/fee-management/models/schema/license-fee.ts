import { Table, Column, Model, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { EducationLevel } from '../../../institute';
import { CRMOU } from '../../../campus';



@Table({ timestamps: true })
export class LicenseFee extends Model<LicenseFee> {

  @ForeignKey(() => EducationLevel)
  @Column educationLevelId: number;

  @Column feeType: string;

  @Column amount: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => EducationLevel, { foreignKey: 'educationLevelId' })
  educationLevel: EducationLevel;

  @HasMany(() => CRMOU, { foreignKey: 'educationLevelId' })
  crMOU: CRMOU[];
}
