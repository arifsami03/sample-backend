import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import { Concession, FeeHeads } from '../..';

@Table({ timestamps: true })
export class ConcessionFeeHead extends Model<ConcessionFeeHead> {

  @ForeignKey(() => Concession)
  @Column consessionId: number;

  @ForeignKey(() => FeeHeads)
  @Column feeHeadId: number;

  @Column concessionAmount: number;

  @Column deleted: boolean;

  @Column createdBy: number;
  @Column updatedBy: number;

  @BelongsTo(() => Concession, {
    foreignKey: 'consessionId'
  })
  consession: Concession;

  @BelongsTo(() => FeeHeads, {
    foreignKey: 'feeHeadId'
  })
  feeHead: FeeHeads;
}