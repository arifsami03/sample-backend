import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Scholarship, FeeHeads } from '../..';

@Table({ timestamps: true })
export class ScholarshipFeeHead extends Model<ScholarshipFeeHead> {
  @ForeignKey(() => Scholarship)
  @Column
  scholarshipId: number;

  @ForeignKey(() => FeeHeads)
  @Column
  feeHeadId: number;

  @Column deleted: boolean;

  @Column createdBy: number;
  @Column updatedBy: number;

  @BelongsTo(() => FeeHeads, { foreignKey: 'feeHeadId' })
  feeHeads: FeeHeads;

  @BelongsTo(() => Scholarship, { foreignKey: 'scholarshipId' })
  scholarship: Scholarship;
}
