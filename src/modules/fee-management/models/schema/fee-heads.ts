import { Table, Column, Model, Unique, BelongsTo, HasMany } from 'sequelize-typescript';
import { Configuration } from '../../../setting';
import { ConcessionFeeHead } from './concession-fee-head';
import { ScholarshipFeeHead } from './scholarship-fee-head';



@Table({ timestamps: true })
export class FeeHeads extends Model<FeeHeads> {

  @Unique
  @Column code: string;

  @Column title: string;

  @Column abbreviation: string;

  @Column natureOfFeeHeadsId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => Configuration, { foreignKey: 'natureOfFeeHeadsId', constraints: false })
  natureOfFeeHeads: Configuration;

  @HasMany(() => ConcessionFeeHead, { foreignKey: 'feeHeadId', })
  concessionFeeHeads: ConcessionFeeHead[];

  @HasMany(() => ScholarshipFeeHead, { foreignKey: 'feeHeadId', })
  scholarshipFeeHeads: ScholarshipFeeHead[];

}
