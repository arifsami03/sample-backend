import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { FeeStructure } from '../..';

@Table({ timestamps: true })
export class FS_InstallmentPlan extends Model<FS_InstallmentPlan> {

  @ForeignKey(() => FeeStructure)
  @Column feeStructureId: number;

  @Column title: string;

  @Column dueDayOfMonth: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => FeeStructure, {
    foreignKey: 'feeStructureId'
  })
  feeStructure: FeeStructure;

}
