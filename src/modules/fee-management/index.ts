export * from './routes/base/fee-management.base.route';

// Fee Heads
export * from './models/schema/fee-heads';
export * from './models/fee-heads.model';
export * from './controllers/fee-heads.controller';
export * from './routes/fee-heads.routes';

// Fee Structure
export * from './models/schema/fee-structure';
export * from './models/fee-structure.model';
export * from './controllers/fee-structures.controller';
export * from './routes/fee-structure.routes';

// Fee Structure Classes
export * from './models/schema/fee-structure-classes';
export * from './models/fee-structure-classes.model';

// Fee Structure Fee Heads
export * from './models/schema/fee-structure-fee-heads';
export * from './models/fee-structure-fee-heads.model';


// Concession
export * from './models/schema/concession';
export * from './models/schema/concession-fee-head';
export * from './models/concession.model';
export * from './models/concession-fee-head.model';
export * from './controllers/concession-fee-heads.controller';
export * from './routes/concession-fee-heads.routes';
export * from './controllers/concessions.controller';
export * from './routes/concession.routes';

// Scholarship
export * from './models/schema/scholarship';
export * from './models/scholarship.model';
export * from './controllers/scholarship.controller';
export * from './routes/scholarship.routes';

// ScholarshipFeeHead
export * from './models/schema/scholarship-fee-head';
export * from './models/scholarship-fee-head.model';
export * from './controllers/scholarship-fee-head.controller';
export * from './routes/scholarship-fee-head.routes';


// Fee Structure Mapping
export * from './models/schema/fs-campus';
export * from './models/schema/fsc-fee-head';
export * from './models/fs-campus.model';
export * from './models/fsc-fee-head.model';
export * from './controllers/fs-campus.controller';
export * from './routes/fs-campus.routes';

//Fee Installement
export * from './models/schema/fs-installement';
export * from './models/schema/fsi-line-item';
export * from './models/fs-installement.model';
export * from './models/fsi-line-item.model';
export * from './controllers/fs-installements.controller';
export * from './routes/fs-installements.routes';

// Fee Structure Installment Plan
export * from './models/schema/fs-installment-plan';
export * from './models/fs-installment-plan.model';
export * from './controllers/fs-installment-plan.controller';
export * from './routes/fs-installment-plan.routes';

/**
 * Management Fee
 */
export * from './routes/management-fees.route';
export * from './controllers/management-fees.controller';
export * from './models/management-fee.model';
export * from './models/schema/license-fee';
