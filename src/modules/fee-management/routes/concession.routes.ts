import { Router } from 'express';

import { ConcessionsController } from '..';

export class ConcessionRoute {

  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {

    let controller = new ConcessionsController();

    this.router.route('/feeManagement/concessions/index').get(controller.index);
    this.router.route('/feeManagement/concessions/findWithFeeHeads/:id').get(controller.findWithFeeHeads);
    this.router.route('/feeManagement/concessions/createWithFeeHeads').post(controller.createWithFeeHeads);
    this.router.route('/feeManagement/concessions/updateWithFeeHeads/:id').put(controller.updateWithFeeHeads);
    this.router.route('/feeManagement/concessions/deleteWithFeeHeads/:id').delete(controller.deleteWithFeeHeads);
    this.router.route('/feeManagement/concessions/deleteConcessionFeeHead/:concessionId/:feeHeadId').delete(controller.deleteConcessionFeeHead);
  }
}
