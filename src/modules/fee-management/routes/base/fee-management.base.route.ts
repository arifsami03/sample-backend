import { Router } from 'express';

import {

  FeeHeadsRoute,
  FeeStructureRoute,
  FSCampusRoute,
  ConcessionRoute,
  ScholarshipRoute,
  ScholarshipFeeHeadRoute,
  ManagementFeesRoute,
  FSInstallementsRoute,
  FSInstallmentPlanRoute,
  ConcessionFeeHeadsRoute
} from '../..';

/**
 *
 *
 * @class FeeManagementBaseRoute
 */
export class FeeManagementBaseRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class BaseRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.initAll();
  }

  /**
   * init all routes
   */
  public initAll() {

    new FeeHeadsRoute(this.router);
    new FeeStructureRoute(this.router);
    new ConcessionRoute(this.router);
    new ScholarshipRoute(this.router);
    new ScholarshipFeeHeadRoute(this.router);
    new FSCampusRoute(this.router);
    new FSInstallementsRoute(this.router);
    new FSInstallmentPlanRoute(this.router);
    new ConcessionFeeHeadsRoute(this.router);
    new ManagementFeesRoute(this.router);
  }
}
