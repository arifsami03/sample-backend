import { Router } from 'express';

import { ConcessionFeeHeadsController } from '..';

export class ConcessionFeeHeadsRoute {

  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {

    let controller = new ConcessionFeeHeadsController();

    this.router.route('/feeManagement/concessionFeeHeads/findByConcession/:concessionId').get(controller.findByConcession);

  }
}
