import { Router } from 'express';
import { FeeStructuresController } from '..';

export class FeeStructureRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new FeeStructuresController();

    this.router.route('/feeManagement/feeStructures/index').get(controller.index);
    this.router.route('/feeManagement/feeStructures/find/:id').get(controller.find);
    this.router.route('/feeManagement/feeStructures/create').post(controller.create);
    this.router.route('/feeManagement/feeStructures/update/:id').put(controller.update);
    this.router.route('/feeManagement/feeStructures/delete/:id').delete(controller.delete);
    this.router.route('/feeManagement/feeStructures/findAttributesList').get(controller.findAttributesList);

    /**
     * This section is for fee-structure-fee-heads
     */
    this.router.route('/feeManagement/feeStructures/findAllFeeHeads/:id').get(controller.findAllFeeHeads);
    this.router.route('/feeManagement/feeStructures/findFeeHead/:id').get(controller.findFeeHead);
    this.router.route('/feeManagement/feeStructures/findAllRemainingFeeHeads/:id/:feeHeadsId?').get(controller.findAllRemainingFeeHeads); // feeHeadsId is optional for create scenario
    this.router.route('/feeManagement/feeStructures/createFeeHead').post(controller.createFeeHead);
    this.router.route('/feeManagement/feeStructures/updateFeeHead/:id').put(controller.updateFeeHead);
    this.router.route('/feeManagement/feeStructures/deleteFeeHead/:id').delete(controller.deleteFeeHead);
  }
}
