import { Router } from 'express';

import { ScholarshipFeeHeadController } from '..';

export class ScholarshipFeeHeadRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new ScholarshipFeeHeadController();

    this.router.route('/feeManagement/scholarshipFeeHeads/index').get(controller.index);
    this.router.route('/feeManagement/scholarshipFeeHeads/find/:id').get(controller.find);
    this.router.route('/feeManagement/scholarshipFeeHeads/create').post(controller.create);
    this.router.route('/feeManagement/scholarshipFeeHeads/update/:id').put(controller.update);
    this.router.route('/feeManagement/scholarshipFeeHeads/delete/:id').delete(controller.delete);
  }
}
