import { Router } from 'express';
import { FeeHeadsController } from '..';

export class FeeHeadsRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new FeeHeadsController();

    this.router.route('/feeManagement/feeHeads/index').get(controller.index);
    this.router.route('/feeManagement/feeHeads/find/:id').get(controller.find);
    this.router.route('/feeManagement/feeHeads/create').post(controller.create);
    this.router.route('/feeManagement/feeHeads/update/:id').put(controller.update);
    this.router.route('/feeManagement/feeHeads/delete/:id').delete(controller.delete);
    this.router.route('/feeManagement/feeHeads/findAttributesList').get(controller.findAttributesList);
  }
}
