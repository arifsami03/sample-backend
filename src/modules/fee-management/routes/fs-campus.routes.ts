import { Router } from 'express';
import { FSCampusController } from '..';

export class FSCampusRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new FSCampusController();

    this.router.route('/feeManagement/fsCampus/index').get(controller.index);
    this.router.route('/feeManagement/fsCampus/find/:id').get(controller.find);
    this.router.route('/feeManagement/fsCampus/create').post(controller.create);
    this.router.route('/feeManagement/fsCampus/update/:id').put(controller.update);
    this.router.route('/feeManagement/fsCampus/delete/:id').delete(controller.delete);
  }
}
