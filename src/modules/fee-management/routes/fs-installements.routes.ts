import { Router } from 'express';
import { FSInstallementsController } from '..';

export class FSInstallementsRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new FSInstallementsController();

    this.router.route('/feeManagement/fsInstallements/find/:id').get(controller.find);
    this.router.route('/feeManagement/fsInstallements/create').post(controller.create);
    this.router.route('/feeManagement/fsInstallements/update/:id').put(controller.update);
    this.router.route('/feeManagement/fsInstallements/delete/:id').delete(controller.delete);
  }
}
