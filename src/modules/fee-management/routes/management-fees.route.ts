import { Router } from 'express';
import { ManagementFeesController } from '..';

export class ManagementFeesRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new ManagementFeesController();

    this.router.route('/feeManagement/managementFees/index').get(controller.index);

    this.router.route('/feeManagement/managementFees/find/:id').get(controller.find);

    this.router.route('/feeManagement/managementFees/findFee/:id').get(controller.findFee);

    this.router.route('/feeManagement/managementFees/findAttributesList').get(controller.findAttributesList);

    this.router.route('/feeManagement/managementFees/findByEducationLevelId/:id').get(controller.findByEducationLevelId);


    this.router.route('/feeManagement/managementFees/create').post(controller.create);

    this.router.route('/feeManagement/managementFees/delete/:id').delete(controller.delete);

    this.router.route('/feeManagement/managementFees/update/:id').put(controller.update);

  }
}
