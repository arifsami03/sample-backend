import { Router } from 'express';

import { ScholarshipController } from '..';

export class ScholarshipRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new ScholarshipController();

    this.router.route('/feeManagement/scholarships/index').get(controller.index);
    this.router.route('/feeManagement/scholarships/findAttributesList').get(controller.findAttributesList);
    this.router.route('/feeManagement/scholarships/findByPercentage/:fromPercentage/:toPercentage').get(controller.findByPercentage);
    this.router.route('/feeManagement/scholarships/find/:id').get(controller.find);
    this.router.route('/feeManagement/scholarships/create').post(controller.create);
    this.router.route('/feeManagement/scholarships/update/:id').put(controller.update);
    this.router.route('/feeManagement/scholarships/delete/:id').delete(controller.delete);
    this.router.route('/feeManagement/scholarships/deleteScholarshipFeeHead/:scholarshipId/:feeHeadId').delete(controller.deleteScholarshipFeeHead);
  }
}
