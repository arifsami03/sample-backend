import { Router } from 'express';
import { FSInstallmentPlansController } from '..';

export class FSInstallmentPlanRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new FSInstallmentPlansController();

    this.router.route('/feeManagement/fsInstallmentPlans/findAll/:feeStructureId').get(controller.findAll);
    this.router.route('/feeManagement/fsInstallmentPlans/find/:id').get(controller.find);
    this.router.route('/feeManagement/fsInstallmentPlans/create').post(controller.create);
    this.router.route('/feeManagement/fsInstallmentPlans/update/:id').put(controller.update);
    this.router.route('/feeManagement/fsInstallmentPlans/delete/:id').delete(controller.delete);
  }
}
