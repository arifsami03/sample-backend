import { BaseModel } from '../../base';
import { CampusDefaultUser } from '..';
import { Role } from '../../security';
import { Promise } from 'bluebird';
import { Sequelize } from 'sequelize-typescript';

export class CampusDefaultUserModel extends BaseModel {
  constructor() {
    super(CampusDefaultUser);
  }

  indexWithRelatedData(){

    return this.sequelizeModel.findAll({
      attributes: ['id', 'userName', 'abbreviation', 'roleId'],

      include: [
        {
          model: Role,
          as: 'role',
          attributes: ['id', 'name']
        },
        
      ]
    });

  }


}
