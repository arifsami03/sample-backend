import { BaseModel, CONFIGURATIONS } from '../../base';
import { WFStateFlow, EmailTemplateAssignmentModel, EmailTemplateAssignment } from '..';
import { WFStateModel } from '..';
import { Promise } from 'bluebird';
import { RoleWFStateFlowModel } from '../../security';
import { Sequelize } from 'sequelize-typescript';

export class WFStateFlowModel extends BaseModel {
  constructor() {
    super(WFStateFlow);
  }

  /**
   * Creating program details against academic calendar
   * @param item
   */
  createWithEmailTemplateAssignment(item) {
    return super.create(item).then(workFlowStateResult => {
      if (workFlowStateResult) {
        let emailTemplateAssignmentModel = new EmailTemplateAssignmentModel();

        return Promise.each(item['emailTemplateList'], emailTemplate => {
          let emailTemplateItem = {
            parent: CONFIGURATIONS.ENUMS.ETAParents.stateflow,
            parentId: workFlowStateResult['id'],
            emailTemplateId: emailTemplate['emailTemplateId']
          };

          return emailTemplateAssignmentModel.create(emailTemplateItem);
        }).then(() => {
          return workFlowStateResult;
        });
      }
    });
  }
  /**
   * Update program details against academic calendar
   * @param _id academic calendar id
   * @param item
   */
  updateWithEmailTemplateAssignment(_id, item) {
    return super.update(_id, item).then(() => {
      let existingEmailTemplateIds: any[] = [];
      let inComingEmailTemplateIds: number[] = item['emailTemplateList'];
      return new EmailTemplateAssignmentModel().findAll(null, { parent: CONFIGURATIONS.ENUMS.ETAParents.stateflow, parentId: _id }).then(emailTemplateResult => {
        if (emailTemplateResult) {
          emailTemplateResult.forEach(element => {
            existingEmailTemplateIds.push({ emailTemplateId: element['emailTemplateId'] });
          });
        }

        // Matching and adding new one (programDetailId)
        inComingEmailTemplateIds.forEach(InComingEtid => {
          let exist: boolean = false;
          // Matching
          existingEmailTemplateIds.forEach(existingEtid => {
            if (InComingEtid['emailTemplateId'] === existingEtid['emailTemplateId']) {
              exist = true;
            }
          });
          if (!exist) {
            // Adding
            let emailTemplateItem = {
              parent: CONFIGURATIONS.ENUMS.ETAParents.stateflow,
              parentId: _id,
              emailTemplateId: InComingEtid['emailTemplateId']
            };
            return new EmailTemplateAssignmentModel().create(emailTemplateItem).then(emailTemplate_result => {
              return emailTemplate_result;
            });
          }
        });

        // Matching and deleteing previous one  (programDetailId)
        existingEmailTemplateIds.forEach(existingEtid => {
          let exist: boolean = false;
          // Matching
          inComingEmailTemplateIds.forEach(InComingEtid => {
            if (InComingEtid['emailTemplateId'] === existingEtid['emailTemplateId']) {
              exist = true;
            }
          });
          if (!exist) {
            // Deleting
            return new EmailTemplateAssignmentModel()
              .deleteByConditions({
                emailTemplateId: existingEtid['emailTemplateId'],
                parent: CONFIGURATIONS.ENUMS.ETAParents.stateflow,
                parentId: _id
              })
              .then(emailTemplate_result => {
                return emailTemplate_result;
              });
          }
        });
        return emailTemplateResult;
      });
    });
  }
  findWFStateFlow(id: string) {
    let wfStateModel = new WFStateModel();

    return this.findAllByConditions(['id', 'fromId', 'toId', 'description'], { WFId: id }).then(flows => {
      
      var myflows = [];
      
      return Promise.each(flows, flowItem => {
        
        return wfStateModel.findByCondition(['state', 'title'], { id: flowItem['fromId'] }).then(fromFlow => {

          return wfStateModel.findByCondition(['state', 'title'], { id: flowItem['toId'] }).then(result => {
            myflows.push({
              id: flowItem['id'],
              fromId: flowItem['fromId'],
              toId: flowItem['toId'],
              from: fromFlow['title'],
              to: result['title'],
              description: flowItem['description']
            });
          });
        });
      }).then(() => {
        return myflows;
      });
    });
  }
  // checkRecordExists(id:string){
  //   return this.findAllByConditions(['id','title','state','description'],{state:id});
  // }

  // {
  //   attributes: ['id', 'fromId', 'toId', 'description'],
  //   where: { id: id },
  //   include: [
  //     {
  //       model: EmailTemplateAssignment,
  //       as: 'emailTemplateList'
  //     }
  //   ]
  // }
  findById(id: string) {
    this.openConnection();
    return this.sequelizeModel.find({
      attributes: ['id', 'fromId', 'toId', 'description'],
      where: { id: id },
      include: [
        {
          model: EmailTemplateAssignment,
          as: 'emailTemplateList',
          attributes: ['emailTemplateId']
        }
      ]
    });
  }

  getFlowByWFId(id: string) {
    
    return this.findAllByConditions(['id', 'fromId', 'toId', 'description'], { WFId: id }).then(flows => {
    
      var myflows = [];
      let wfStateModel = new WFStateModel();

      return Promise.each(flows, flowItem => {
        return wfStateModel.findByCondition(['state','title'], { id: flowItem['fromId'] }).then(fromFlow => {
          return wfStateModel.findByCondition(['state','title'], { id: flowItem['toId'] }).then(result => {
            myflows.push({
              id: flowItem['id'],
              fromId: flowItem['fromId'],
              toId: flowItem['toId'],
              from: fromFlow['title'] + ' ( ' + fromFlow['state'] + ' ) ',
              to: result['title'] + ' ( ' + result['state'] + ' ) ',
              description: flowItem['description']
            });
          });
        });
      }).then(() => {
        return myflows;
      });
    });
  }

  delete(id: number) {
    return new RoleWFStateFlowModel().deleteByConditions({ WFStateFlowId: id }).then(() => {
      return super.delete(id);
    });
  }

  deleteByStateId(stateId) {
    //Delete WF State Flow associated with the Work Flow State
    return super.findAllByConditions(['id'], { [Sequelize.Op.or]: [{ fromId: stateId }, { toId: stateId }] }).then(wfStateFlowIds => {
      let ids = [];
      for (let c = 0; c < wfStateFlowIds.length; c++) {
        ids.push(wfStateFlowIds[c]['id']);
      }

      return this.deleteMultiple(ids);
    });
  }

  deleteByWorkFlow(wfid) {
    //Delete WF State Flow associated with the Work Flow State
    return super.findAllByConditions(['id'], { WFId: wfid }).then(wfStateFlowIds => {
      let ids = [];
      for (let c = 0; c < wfStateFlowIds.length; c++) {
        ids.push(wfStateFlowIds[c]['id']);
      }
      return this.deleteMultiple(ids);
    });
  }

  deleteMultiple(ids) {
    //Delete Role WF State Flow associated with the WF State Flow
    return new RoleWFStateFlowModel().deleteByConditions({ WFStateFlowId: ids }).then(() => {
      //Delete WF State Flow
      return super.deleteByConditions({ id: ids });
    });
  }
}
