import { BaseModel } from '../../base';
import { EmailTemplate, EmailTemplateAssignmentModel } from '..';



export class EmailTemplateModel extends BaseModel {
  constructor() {
    super(EmailTemplate);
  }
  deleteDeletable(id) {
    return this.find(id, ['deletable']).then(result => {
      
      if (result['deletable'] === true || result['deletable'] === null) {
        return new EmailTemplateAssignmentModel().deleteByConditions({ emailtemplateId: id }).then(() => {
          return this.delete(id);
        });
      }
    });
  }
}
