import { BaseModel } from '../../base';
import { Attachment, AttachedFile, AttachmentModel } from '..';
import { Promise } from 'bluebird';
const multer = require('multer');
const path = require('path');
const fs = require('fs');

export class AttachedFileModel extends BaseModel {
  constructor() {
    super(AttachedFile);
  }

  findByAttachment(queryParams) {
    let attachmentModel = new AttachmentModel();
    return attachmentModel.findByCondition(['id'], { parent: queryParams['parent'], parentId: queryParams['parentId'] }).then(_result => {
      if (_result) {
        return this.sequelizeModel.findAndCountAll({
          attributes: ['id', 'originalName', 'createdAt', 'size', 'type'],
          order: [['createdAt', 'DESC']],
          where: { attachmentId: _result['id'] }
        });
      }

      return _result;
    });
  }
  deleteWithFile(id: number) {
    // Assuming that 'path/file.txt' is a regular file.
    let attachedFileModel = new AttachedFileModel();
    return attachedFileModel.find(id, ['path', 'name']).then(_result => {
      fs.unlink(_result['path'], err => {
        if (err) throw err;
        return attachedFileModel.delete(id);
      });
      return _result;
    });
  }
  download(id: number) {
    let attachedFileModel = new AttachedFileModel();
    return attachedFileModel.find(id, ['path', 'name']);
  }
}
