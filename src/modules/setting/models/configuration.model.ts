import { Promise } from 'bluebird';
import { BaseModel } from '../../base';
import { Configuration, ConfigurationOption, ConfigurationOptionModel } from '..';
import { Sequelize } from 'sequelize-typescript';
import { CRMOUVerificationModel } from '../../campus';

export class ConfigurationModel extends BaseModel {

  /**
   * To delete configuration recursively we need to feth all children ids and save them.
   * So we can delete those records when we have all the ids first.
   */
  private confIdsForDel = [];
  /**
   * To avoid opening database connection each time when recursivly called in deletion process
   */
  private confModel;
  private confOptionModel;


  constructor() {
    super(Configuration);
  }

  /**
   * Create Configuration data with its Options if received.
   * @param item 
   */
  createWithOptions(item) {
    return super.create(item).then((result) => {
      if (item.configurationOptions.length > 0) {
        return Promise.each(item.configurationOptions, option => {
          // if (option['value']) {
          let configurationOptionModel = new ConfigurationOptionModel();
          option['configurationId'] = result.id;
          return configurationOptionModel.create(option);
          // }
        }).then(() => {
          return result;
        });

      } else {
        return result;
      }
    });

  }

  /**
   * Update Configuration data and its Options
   * @param id 
   * @param item 
   */
  updateWithOptions(id, item) {
    return super.update(id, item).then((result) => {
      if (item.configurationOptions.length > 0) {

        let configurationOptionModel = new ConfigurationOptionModel();
        return Promise.each(item.configurationOptions, option => {
          if (option['id']) {
            return configurationOptionModel.update(option['id'], option);
          } else if (option['value']) {
            option['configurationId'] = id;
            return configurationOptionModel.create(option);
          }
        });
      } else {
        return result;
      }
    })

  }

  /**
   * Find single record with its options
   * @param attributes
   * @param conditions
   */
  findWithOptions(attributes, condition) {
    let includeObj = [{ model: ConfigurationOption, as: 'configurationOptions', attributes: ['id', 'configurationId', 'key', 'value'], where: BaseModel.cb(), required: false }];

    return this.findByCondition(attributes, condition, includeObj);
  }

  // /**
  //  * Delete Configuration and also its associations.
  //  * @param id 
  //  */
  // deleteWithAssociations(id) {
  //   return super.findAllByConditions(['id'], { parentKeyId: id }).then((children) => {

  //     let toDelete = [];
  //     toDelete.push(id);
  //     return Promise.each(children, child => {
  //       return this.deleteWithAssociations(child['id']);
  //     }).then(() => {
  //       return new ConfigurationOptionModel().deleteByConditions({ configurationId: { [Sequelize.Op.in]: toDelete } }).then(() => {
  //         return super.deleteByConditions({ id: { [Sequelize.Op.in]: toDelete } });
  //       });
  //     });


  //     //   if (children.length > 0) {
  //     //     return Promise.each(children, child => {
  //     //       return this.deleteWithAssociations(child['id']);
  //     //     });
  //     //   } else {
  //     //     return new ConfigurationOptionModel().deleteByConditions({ configurationId: { [Sequelize.Op.in]: toDelete } }).then(() => {
  //     //       return super.deleteByConditions({ id: { [Sequelize.Op.in]: toDelete } });
  //     //     });
  //     //   }

  //   });
  // }

  /**
   * Delete configuration with configuration option and all chldren nested
   * 
   * @param id number
   * @param key string
   */
  public delete(id) {

    this.confModel = new ConfigurationModel();
    this.confOptionModel = new ConfigurationOptionModel();

    return this.confModel.find(id, ['id']).then(cmRes => {

      this.confIdsForDel.push(cmRes['id']);

      return this.findChildrenRecursively(id);

    }).then(() => {

      /**
       * To reverse order because it contais data parent first then child
       * and we need to delete children first and then parent.
       */
      this.confIdsForDel.reverse();

      return this.confOptionModel.deleteByConditions({ configurationId: { [Sequelize.Op.in]: this.confIdsForDel } }).then(() => {
        return super.deleteByConditions({ id: { [Sequelize.Op.in]: this.confIdsForDel } });
      })

    })

  }

  /**
   * Recursively find children for deletion
   * 
   * @param parentKeyId number
   */
  public findChildrenRecursively(parentKeyId) {

    return this.confModel.findAllByConditions(['id'], { parentKeyId: parentKeyId }).then(cmRes => {

      return Promise.each(cmRes, (item) => {

        this.confIdsForDel.push(item['id']);

        return this.findChildrenRecursively(item['id']);

      })

    })
  }

  /**
   * Find All Cities of a country
   * @param id 
   */
  findAllCities(id) {
    let cities = [];

    // First find all provinces of the current country
    return super.findAllByConditions(['id', 'key', 'value'], { parentKeyId: id }).then(provinces => {
      return Promise.each(provinces, province => {
        return super.findAllByConditions(['id', 'key', 'value'], { parentKeyId: province['id'] }).then(_cities => {
          _cities.forEach(city => {
            cities.push(city);
          });
        });
      }).then(() => {
        return cities;
      });
    });
  }
  deleteWithAssociatedRecords(id: number) {
    return this.findByCondition(['key'], { id: id }).then(result => {
      if (result['key'] === "instrumentType") {
        return new CRMOUVerificationModel().deleteByConditions({ instrumentTypeId: id }).then(() => {
          return this.delete(id);
        })
      }
      else {
        return this.delete(id);
      }
    })

  }
}
