import { Promise } from 'bluebird';

import { BaseModel } from '../../base';
import { ESQuestion, ESQuestionOption } from '..';
import { ESQuestionOptionModel } from '..';

export class ESQuestionModel extends BaseModel {
  constructor() {
    super(ESQuestion);
  }

  findAllESQuestions(ESSectionId: number) {
    let attributes = ['id', 'question', 'countField', 'remarksField', 'countField', 'optionType'];
    let includeObj = [{ model: ESQuestionOption, as: 'options', attributes: ['id', 'optionString'], where: BaseModel.cb(), required: false }];

    return this.findAllByConditions(attributes, { ESSectionId: ESSectionId }, includeObj);
  }

  create(req) {
    let item = req;
    return super.create(item).then((result) => {
      return new Promise((resolve, reject) => {
        if (item.options.length > 0) {
          let options = [];
          for (let i = 0; i < item.options.length; i++) {
            options.push({ ESQuestionId: result.id, optionString: item.options[i] });
          };
          return ESQuestionOption.bulkCreate(options).then(() => {
            return resolve(result);
          });
        } else {
          return resolve(result);
        }
      });


    });
  }

  deleteWithOptions(id) {
    return new ESQuestionOptionModel().deleteByConditions({ ESQuestionId: id }).then(() => {
      return super.delete(id);
    });
  }

  update(id, req) {
    let item = req;

    return super.findByCondition(['id', 'optionType'], { id: id }).then(previousResult => {

      return super.update(id, item).then(() => {

        return new Promise((resolve, reject) => {

          if (previousResult.optionType != item.optionType) {
            let options = [];
            for (let i = 0; i < item.options.length; i++) {
              options.push({ ESQuestionId: id, optionString: item.options[i] });
            };
            return new ESQuestionOptionModel().deleteByConditions({ ESQuestionId: id }).then(() => {

              return ESQuestionOption.bulkCreate(options).then(() => {
                return resolve(true);

              });
            });

          } else {
            return resolve(true);
          }

        });

      });

    });
  }

}
