import { BaseModel } from '../../base';
import { WorkFlowLog, WFStateModel } from '..';
import { Promise } from 'bluebird';

export class WorkFlowLogModel extends BaseModel {
  constructor() {
    super(WorkFlowLog);
  }

  getLog(parent: string, parentId: number) {

    return this.findAllByConditions(['id', 'fromStateId', 'toStateId'], { parent: parent, parentId: parentId }).then(logs => {

      var logList = [];

      return Promise.each(logs, (logItem) => {

        return new WFStateModel().findByCondition(['state', 'title'], { id: logItem['fromStateId'] }).then(fromState => {

          return new WFStateModel().findByCondition(['state', 'title'], { id: logItem['toStateId'] }).then(toState => {

            logList.push({
              id: logItem['id'],
              from: fromState['title'],
              to: toState['title'],
            })
          })


        });

      }).then(() => {
        return logList;
      })

    })

  }
}
