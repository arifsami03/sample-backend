import { EmailTemplate } from '..';
import { Promise } from 'bluebird';
import * as nodemailer from 'nodemailer';

import { BaseModel } from '../../base';
import { OutgoingMailServer } from '..';

export class OutGoingMailServerModel extends BaseModel {
  constructor() {
    super(OutgoingMailServer);
  }

  public sendEmailByEmailTemplate(_id: number) {
    let attributes = ['id', 'username', 'password', 'smtpServer', 'smtpPort', 'connectionSecurity'];
    let includeObj = [{ model: EmailTemplate, as: 'emailTemplates', where: BaseModel.cb({ id: 1 }), required: false }];

    return this.findByCondition(attributes, { id: _id }, includeObj)
      .then(_result => {
        if (_result) {
          let _from = _result['username'];
          let _fromPassword = _result['password'];
          let _to = 'umair.rauf@mekdiz.com';
          let _smtpServer = _result['smtpServer'];
          let _port = _result['smtpPort'];
          let _connectionSecurity = _result['connectionSecurity'];
          let _subject = _result['emailTemplates'][0]['subject'];
          let _text = _result['emailTemplates'][0]['emailBody'];
          let _html = '';
          return this.sendEmail(_from, _fromPassword, _to, _smtpServer, _port, _connectionSecurity, _subject, _text, _html);
        }
      });
  }

  /**
   *
   * @param _from sender email address
   * @param _fromPassword sender password
   * @param _to receiver email adresses
   * @param _subject subject of email
   * @param _text text of email
   * @param _html html in email
   */
  private sendEmail(_from: string, _fromPassword: string, _to: string, _smtpServer: string, _port: number, _connectionSecurity: string, _subject: string, _text: string, _html: string) {
    /**
     * preRegistrationDetail finding the one record of pre regitsrtaion form on basis of id
     * @param _id
     */

    return new Promise((resolve, reject) => {
      nodemailer.createTestAccount((err, account) => {
        let transporter = nodemailer.createTransport({
          // from: 'noreply@gmail.com',
          host: _smtpServer, // hostname
          secureConnection: _connectionSecurity, // use SSL
          port: _port, // port for secure SMTP
          transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts
          auth: {
            user: _from,
            pass: _fromPassword
          }
        });
        let mailOptions = {
          from: _from, // sender address
          to: _to, // list of receivers
          subject: _subject, // Subject line
          text: _text, // plain text body
          html: _html
        };

        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            reject({ error: 'Something went wrong.', detail: error });
          } else {
            resolve({ success: 'Sent', detail: info });
          }
        });
      });
    });
  }
}
