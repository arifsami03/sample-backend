import { BaseModel } from '../../base';
import { ESSection, ESQuestionModel, ESQuestionOptionModel } from '..';
import { Promise } from 'bluebird';
import { Sequelize } from 'sequelize-typescript';

export class ESSectionModel extends BaseModel {
  constructor() {
    super(ESSection);
  }

  findAllESSections(ESId: number) {
    return this.findAllByConditions(['id', 'title'], { ESId: ESId });
  }

  /**
   * To delete a section, first delete his questions and to delete questions, first delete his options
   * @param id 
   */
  deleteWithQuestions(id) {
    let esQuestionModel = new ESQuestionModel();

    return esQuestionModel.findAllByConditions(['id'], { ESSectionId: id }).then((esQuestions) => {

      return Promise.each(esQuestions, esQuestion => {
        return esQuestionModel.deleteWithOptions(esQuestion['id']);
      }).then(() => {
        return super.delete(id);
      });

    })

  }
}
