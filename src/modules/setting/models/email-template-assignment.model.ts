import { BaseModel } from '../../base';
import {  EmailTemplateAssignment } from '..';

export class EmailTemplateAssignmentModel extends BaseModel {
  constructor() {
    super(EmailTemplateAssignment);
  }
}
