import { BaseModel } from '../../base';
import { WFState, WFStateFlowModel } from '..';
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';

export class WFStateModel extends BaseModel {
  constructor() {
    super(WFState);
  }

  /**
   * Function to get List Of Work Flow States
   * @param id Work Flow ID
   */

  findWFState(id: string) {
    return this.findAllByConditions(['id', 'title', 'state', 'description'], { WFId: id });
  }

  /**
   * Function to Check wheather the state already exists or not
   * @param id State Id
   * @param WFId Work Flow Id
   */
  checkRecordExists(id: string, WFId: string) {
    return this.findAllByConditions(['id', 'title', 'state', 'description'], { state: id, WFId: WFId });
  }

  /**
   * Function to Find Work Flow State for Edit Purpose
   * @param id State Id
   */
  findById(id: string) {
    return this.findByCondition(['id', 'title', 'state', 'description'], { id: id });
  }

  delete(id) {
    return new WFStateFlowModel().deleteByStateId(id).then(() => {
      return super.delete(id);
    });
  }

  deleteByWorkFlow(wfid) {
    //First Delete WF State Flow associated with the Work Flow State
    return new WFStateFlowModel().deleteByWorkFlow(wfid).then(res => {
      return super.deleteByConditions({ WFId: wfid });
    });
  }
}
