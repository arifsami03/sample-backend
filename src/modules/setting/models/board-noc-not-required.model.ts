import { BaseModel } from '../../base';
import { BoardNOCNotRequired, Board, BoardModel } from '..';
import { Sequelize } from 'sequelize-typescript';
import { Promise } from 'bluebird';
import { ErrorHandler } from '../../base/conf/error-handler';

export class BoardNOCNotRequiredModel extends BaseModel {
  constructor() {
    super(BoardNOCNotRequired);
  }

  // TODO: low have to change this function to send data in proper way but for now emergency i've codded it in this way
  findAllNOCNotRequiredBoards(id) {
    return new BoardNOCNotRequiredModel().findAllByConditions(['id'], { boardId: id }, [
      { model: Board, as: 'boardNOC', where: BaseModel.cb(), required: false }
    ]);
  }


  findAllBoardsToAddNOC(id) {
    return super.findAllByConditions(['id', 'boardId', 'boardNOCNotRequiredId'], { boardId: id }).then((result) => {
      let existingIds = [];
      existingIds.push(id); // also do not show current board to select NOC for current board
      result.forEach(element => {
        existingIds.push(element['boardNOCNotRequiredId']);
      });
      return new BoardModel().findAllByConditions(['id', 'name'], { id: { [Sequelize.Op.notIn]: existingIds } });
    });
  }

}
