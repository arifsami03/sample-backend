import * as _ from 'lodash';
import { CONFIGURATIONS } from '../../base';
import { WorkFlowModel, WFStateFlowModel, WFStateModel, EmailTemplateAssignmentModel, EmailSenderModel, } from '..';
import { RoleWFStateFlowModel, RoleAssignmentModel, UserModel, UserGroupModel } from '../../security';
import { Sequelize } from 'sequelize-typescript';
import { CRApplicationModel } from '../../campus/models/cr-application.model';
import { WorkFlowLogModel } from '..';



export class DocStateManagerModel {
  constructor() {

  }

  /**
   * TODO:high: need to optimize this function
   * @param item 
   */
  getListOfActions(item) {
    let userId = item['userId'];
    let stateId = item['stateId'];
    let WFId = item['WFId'];

    let userModel = new UserModel();
    let roleAssignment = new RoleAssignmentModel();
    let roleWFStateFlow = new RoleWFStateFlowModel();
    let wfState = new WFStateModel();
    let wfStateFlow = new WFStateFlowModel();
    let userGroupModel = new UserGroupModel();

    // Check if its a super user or not
    // In case of super user display state flows anyway.
    return userModel.findUserOnly(userId, ['isSuperUser']).then(userRes => {

      if (userRes['isSuperUser']) {

        return wfStateFlow.findAllByConditions(['toId'], { fromId: stateId, WFId: WFId }).then(toFlows => {

          // To store all the state on which login User can move.
          let toFlowsIds = [];
          toFlows.forEach(flow => toFlowsIds.push(flow['toId']));

          return wfState.findAllByConditions(['id', 'title', 'state'], { id: { [Sequelize.Op.in]: toFlowsIds } });

        })

      } else {

        // Get All the List of Roles Assigned to User.
        return roleAssignment.findAllByConditions(['roleId'], { parentId: userId, parent: 'user' }).then(rolesList => {

          // To store all the roles assign to User.
          let roleIds = [];
          rolesList.forEach(role => roleIds.push(role['roleId']));

          return userGroupModel.findAllByConditions(['id', 'groupId'], { userId: userId }).then((userGroups) => {

            let groupIds = [];
            userGroups.forEach(item => groupIds.push(item['groupId']));

            return roleAssignment.findAllByConditions(['roleId'], { parentId: { [Sequelize.Op.in]: groupIds }, parent: 'group' }).then(rolesList => {

              rolesList.forEach(role => roleIds.push(role['roleId']));

              roleIds = _.sortedUniq(roleIds);

              // Get List of all the work flow state based on permissions.
              return roleWFStateFlow.findAllByConditions(['WFStateFlowId'], { roleId: { [Sequelize.Op.in]: roleIds } }).then(wfStateFlows => {

                // To store all the roles assign to User.        
                let wfStateFlowIds = [];
                wfStateFlows.forEach(flow => wfStateFlowIds.push(flow['WFStateFlowId']));


                return wfStateFlow.findAllByConditions(['toId'], { id: { [Sequelize.Op.in]: wfStateFlowIds }, fromId: stateId, WFId: WFId }).then(toFlows => {

                  // To store all the state on which login User can move.
                  let toFlowsIds = [];
                  toFlows.forEach(flow => toFlowsIds.push(flow['toId']));

                  return wfState.findAllByConditions(['id', 'title', 'state'], { id: { [Sequelize.Op.in]: toFlowsIds } });

                })
              })

            })

          })



        })
      }
    })


  }

  /**
   * Update state id of document
   * 
   * @param {any} item 
   * @returns 
   * @memberof DocStateManagerModel
   */
  public updateStateId(item) {

    let wfState = new WFStateModel();
    let isRuleExists: boolean = false;
    let jumpWFId;
    let jumpWFState;

    return this.updateStateIdByJumpFlow(item).then(res1 => {

      CONFIGURATIONS.WF_JUMPs.forEach(condition => {

        if (condition['from']['wf'] == item['WFId'] && condition['from']['state'] == item['state']) {

          isRuleExists = true;
          jumpWFId = condition['to']['wf'];
          jumpWFState = condition['to']['state'];

        }

      });

      if (isRuleExists) {

        return wfState.findByCondition(['id'], { WFId: jumpWFId, state: jumpWFState }).then(wfStateId => {
          item['id'] = wfStateId['id'];
          item['stateId'] = wfStateId['id'];
          return this.updateStateIdByJumpFlow(item);
        })
      }
      else {
        return res1;
      }
    })
  }

  updateStateIdByJumpFlow(item) {

    let retRes;
    let campusModel = new CRApplicationModel();

    return campusModel.findByCondition(['id', 'performedStates'], { id: item['parentId'] }).then(crApplicationData => {

      let performedStates;

      performedStates = crApplicationData.performedStates ? crApplicationData.performedStates : '';

      performedStates += '{' + item['previousStateId'] + '},';

      return campusModel.update(item['parentId'], { stateId: item['id'], performedStates: performedStates }).then(res => {

        retRes = res;

        let log = { parent: item['parent'], parentId: item['parentId'], fromStateId: item['previousStateId'], toStateId: item['id'] }

        return this.createLog(log).then(() => {

          // Asynchronous/Parallel call for sending email because email sending will take some time and we don't want uset to wait for long.
          let arg = { fromStateId: item['previousStateId'], toStateId: item['stateId'], CRApplicationId: item['parentId'] }

          this.sendEmail(arg);

          return retRes;

        }).then(() => {
          // Return result
          return retRes;
        })

      })

    });
  }


  /**
   * Create log 
   * logItem : { parent: string, parentId: number, fromStateId: number, toStateId: number }
   * 
   * @param logItem object
   */
  public createLog(logItem: object) {
    return new WorkFlowLogModel().create(logItem);
  }

  /**
   * Just provide workflow id (name), from state, to state names and email will be sent if attached any.
   * 
   * @param WFId string
   * @param fromState string
   * @param toState string
   * @param CRApplicationId number
   */
  public findAndSendAttachedEmails(WFId, fromState, toState, CRApplicationId) {

    return this.getStateIds(WFId, fromState, toState).then(stateFlowRes => {

      let item = {
        fromStateId: stateFlowRes['fromStateId'],
        toStateId: stateFlowRes['toStateId'],
        CRApplicationId: CRApplicationId
      }
      return this.sendEmail(item);
    })
  }

  /**
   * Find state ids by providing names of WFId, fromState and toState
   * 
   * @param WFId string
   * @param fromState string
   * @param toState string
   */
  public getStateIds(WFId, fromState, toState) {

    let wfStateModel = new WFStateModel();

    return wfStateModel.findByCondition(['id'], { WFId: WFId, state: fromState }).then(fromRes => {

      return wfStateModel.findByCondition(['id'], { WFId: WFId, state: toState }).then(toRes => {

        return { fromStateId: fromRes['id'], toStateId: toRes['id'] }

      })
    })
  }

  /**
   * Send email on state change if any email template is attached.
   * 
   * Now here we need to get email template ids (which may be multiple) and then send those ids to EmailSenderModel class
   * Rest will be handled by EmailSenderModel class.
   * 
   * @param item Object
   */
  public sendEmail(item) {

    let fromId = item['fromStateId'];
    let toId = item['toStateId'];


    // Find WFStateFlow id to find attached email templates.
    return new WFStateFlowModel().findByCondition(['id'], { fromId: fromId, toId: toId }).then(wfStatFlowRes => {

      if (wfStatFlowRes) {

        // Find assigned/attached email templates
        return new EmailTemplateAssignmentModel().findAllByConditions(['emailTemplateId'], { parent: CONFIGURATIONS.ENUMS.ETAParents.stateflow, parentId: wfStatFlowRes['id'] }).then(etRes => {

          if (etRes) {

            let CRApplicationId = item['CRApplicationId'];
            let emailTemplateIds = _.map(etRes, 'emailTemplateId');
            let dataOptions = [{ tbl: 'campus', recId: CRApplicationId }];

            return new EmailSenderModel().sendEmails(emailTemplateIds, dataOptions);
          }

        })
      }
    })
  }

  /**
   * Function to get State Id of Work Flow State.
   * 
   * @param postData WorkFlow Id and State.
   */
  getStateId(item: any) {
    let WFId = item['WFId'];
    let state = item['state'];
    return new WFStateModel().findByCondition(['id'], { WFId: WFId, state: state });
  }

  /**
   * Function to get State of Work Flow State.
   * @param postData WorkFlow Id and State.
   */

  getState(id: number) {
    return new WFStateModel().findByCondition(['state'], { id: id });
  }

  /**
   * Function to get Current State Id of Campus. 
   * @param postData includes CRApplicationId.
   */
  getCurrentStateId(postData: any) {
    // let CRApplicationId = postData['CRApplicationId'];
    // return new CRApplicationModel().findByCondition(['stateId'], { id: CRApplicationId });

    // changes made by Arif as data is received as {CRApplicationId: CRApplicationId}
    let condition = { id: postData['CRApplicationId'] };
    return new CRApplicationModel().findByCondition(['stateId'], condition);

  }
}
