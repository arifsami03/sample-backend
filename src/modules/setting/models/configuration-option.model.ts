import { BaseModel } from '../../base';
import { ConfigurationOption } from '..';

export class ConfigurationOptionModel extends BaseModel {
  constructor() {
    super(ConfigurationOption);
  }
}
