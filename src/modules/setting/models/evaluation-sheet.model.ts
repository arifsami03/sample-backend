import { EvaluationSheet, ESCategory, ESSectionModel,ESQuestionModel } from '..';
import { BaseModel } from '../../base';
import { Promise } from 'bluebird';
import { Sequelize } from 'sequelize-typescript';

export class EvaluationSheetModel extends BaseModel {
  constructor() {
    super(EvaluationSheet);
  }

  /**
   * Create evaluation sheet and if created evaluation sheet is requested to be active then deactivate all evaluation sheets regarding
   * current ESCategory and ESForm
   * @param item 
   */
  create(item) {
    if (item.active) {
      return super.findAllByConditions(['id'], { active: true, ESCategoryId: item.ESCategoryId, ESForm: item.ESForm }).then(savedES => {
        return Promise.each(savedES, evaluationSheet => {
          return super.update(evaluationSheet['id'], { active: false });
        }).then(() => {
          return super.create(item);
        });
      });

    } else {
      return super.create(item)
    }

  }

  /**
   * Update evaluation sheet and if updated evaluation sheet is requested to be active then deactivate all evaluation sheets regarding
   * current ESCategory and ESForm
   * @param item 
   */
  update(id, item) {
    if (item.active) {
      return super.findAllByConditions(['id'], { id: { [Sequelize.Op.not]: id }, active: true, ESCategoryId: item.ESCategoryId, ESForm: item.ESForm }).then(savedES => {
        return Promise.each(savedES, evaluationSheet => {
          return super.update(evaluationSheet['id'], { active: false });
        }).then(() => {
          return super.update(id, item);
        });
      });

    } else {
      return super.update(id, item)
    }

  }

  deleteWithSections(id) {
    let esSectionModel = new ESSectionModel();

    return esSectionModel.findAllByConditions(['id'], { ESId: id }).then((esSections) => {
      return Promise.each(esSections, esSection => {
        return esSectionModel.deleteWithQuestions(esSection['id']);
      }).then(() => {
        return super.delete(id);
      });
    });
  }

  findAllWithESCategory() {
    let attributes = ['id', 'title', 'ESCategoryId', 'ESForm', 'active'];
    let includeObj = [{ model: ESCategory, as: 'esCategory', attributes: ['id', 'name'], where: BaseModel.cb(), required: false }];

    return this.findAll(attributes, null, includeObj);
  }

  findWithESCategory(id) {
    let attributes = ['id', 'title', 'ESCategoryId', 'ESForm', 'active'];
    let includeObj = [{ model: ESCategory, as: 'esCategory', attributes: ['id', 'name'], where: BaseModel.cb(), required: false }];

    return this.findByCondition(attributes, { id: id }, includeObj);
  }

  findCampusEvaluationSheet() {
    let result;
    let esModel = new EvaluationSheetModel();
    let esSectionModel = new ESSectionModel();

    // find evaluation sheet for campus with status 'draft'
    return esModel.findByCondition(['id', 'title'],
      { ESForm: 'campus-application-form', active: true }).then(evaluationSheet => {
        if (evaluationSheet) {

          result = evaluationSheet['dataValues'];

          // find all sections for evaluation sheet
          return esSectionModel.findAllByConditions(['id', 'title'], { ESId: evaluationSheet.id }).then(esSections => {
            let esQuestionModel = new ESQuestionModel();
            let sections = [];

            // find all qeustions for each founded section
            return Promise.each(esSections, esSection => {
              return esQuestionModel.findAllByConditions(['id', 'question', 'countField', 'remarksField', 'optionType'], { ESSectionId: esSection['id'] }).then(esQuestions => {
                let section = { id: esSection['id'], title: esSection['title'], questions: esQuestions };
                sections.push(section);

              })

            }).then(() => {
              result.sections = sections;
              return result;
            })
          })
        } else {
          return null;
        }
      });
  }

}
