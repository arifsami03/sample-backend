import { BaseModel } from '../../base';
import { Attachment, AttachedFile } from '..';
import { Promise } from 'bluebird';
import { ErrorHandler } from '../../base/conf/error-handler';

const multer = require('multer');
const path = require('path');

export class AttachmentModel extends BaseModel {
  upload: any;
  constructor() {
    super(Attachment);
  }
  fileUpload(item) {
    return this.findByCondition(['id'], { parent: item['body'].parent, parentId: item['body'].parentId }).then(_result => {
      if (!_result) {
        return this.create(item['body']).then(createdAttachmentResult => {
          return this.createAttachedFile(createdAttachmentResult['id'], item['file']);
        });
      } else {
        return this.createAttachedFile(_result['id'], item['file']);
      }
    });
  }
  createAttachedFile(attachmentId: number, itemFile: any) {
    this.openConnection();
    let attachedFileItem = {
      attachmentId: attachmentId,
      originalName: itemFile['originalname'],
      name: itemFile['filename'],
      path: itemFile['path'],
      size: itemFile['size'],
      type: itemFile['mimetype']
    };
    attachedFileItem = BaseModel.extendItem(attachedFileItem,true);
    return AttachedFile.create(attachedFileItem);
  }

  /**
   *
   * Configuring multer
   * @param {any} queryParams
   * @memberof AttachmentModel
   */
  configureMulter(queryParams) {
    
    let dir = './uploads/' + queryParams['parent'] + '/' + queryParams['parentId'];
    // Set The Storage Engine
    let storage = multer.diskStorage({
      destination: dir,
      filename: function(req, file, cb) {
        cb(null, queryParams['parentId'] + '-' + Date.now() + path.extname(file.originalname));
      }
    });
    let _fileSize: number = queryParams['fileSize'];
    let _fileFilter: string = queryParams['fileFilter'];
    // Init Upload
    this.upload = multer({
      storage: storage,
      limits: { fileSize: _fileSize * 1048576 },
      fileFilter: function(req, file, cb) {
        let isAllow = false;
        // extension of existing file
        const ext = path.extname(file.originalname).replace(' ', '');
        const mimetype = file.mimetype;
        // remove spaces
        _fileFilter = _fileFilter.replace(' ', '');
        let _fileFilterArray = _fileFilter.split(',');

        _fileFilterArray.forEach(element => {
          if (ext === element) {
            isAllow = true;
            return;
          }
        });

        if (isAllow || _fileFilter === 'undefined') {
          return cb(null, true);
        } else {
          cb({ code: 'INVALID_FILE_TYPE' });
        }
      }
    }).single('file');
  }
  uploader(req, res, err, next) {
    if (err) {
      if (err.code === 'INVALID_FILE_TYPE') {
        ErrorHandler.sendFileTypeError(err, res, next);
      } else if (err.code === 'LIMIT_FILE_SIZE') {
        console.log(err);
        ErrorHandler.sendFileSizeError(err, res, next);
      }
    } else {
      if (req['file'] == undefined) {
        res.send({ msg: 'Error: No File Selected!' });
      } else {
        this.fileUpload(req)
          .then(result => {
            res.send(result);
          })
          .catch(err => {
            ErrorHandler.sendServerError(err, res, next);
          });
      }
    }
  }
}
