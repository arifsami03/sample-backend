import { BaseModel } from '../../base';
import { ESCategory, EvaluationSheetModel } from '..';
import { Promise } from 'bluebird';

export class ESCategoryModel extends BaseModel {
  constructor() {
    super(ESCategory);
  }

  deleteWithEvaluationSheets(id) {
    let evaluationSheetModel = new EvaluationSheetModel();

    return evaluationSheetModel.findAllByConditions(['id'], { ESCategoryId: id }).then((evaluationSheets) => {
      return Promise.each(evaluationSheets, evaluationSheet => {
        return evaluationSheetModel.deleteWithSections(evaluationSheet['id']);
      }).then(() => {
        return super.delete(id);
      })
    });
  }

  findCategoriesByES(){
    let evaluationSheetModel = new EvaluationSheetModel();
    return  evaluationSheetModel.findAllByConditions(['id','title','ESCategoryId','ESForm'],{active:true,ESForm:'application-form'},[{model:ESCategory,as: 'esCategory'}])
  }
  
}
