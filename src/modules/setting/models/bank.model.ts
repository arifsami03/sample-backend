import { BaseModel } from '../../base';
import { Bank } from '..';

export class BankModel extends BaseModel {
  constructor() {
    super(Bank);
  }
}
