import { BaseModel } from '../../base';
import { ESQuestionOption } from '..';

export class ESQuestionOptionModel extends BaseModel {
  constructor() {
    super(ESQuestionOption);
  }

}
