import { BaseModel } from '../../base';
import { Board, BoardNOCNotRequiredModel } from '..';
import { Sequelize } from 'sequelize-typescript';

export class BoardModel extends BaseModel {
  constructor() {
    super(Board);
  }

}
