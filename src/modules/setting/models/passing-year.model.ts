import { BaseModel } from '../../base';
import { PassingYear } from '..';
import { Sequelize } from 'sequelize-typescript';

export class PassingYearModel extends BaseModel {
  constructor() {
    super(PassingYear);
  }

  getPassingYearNotSelected(selectedPassingYear){
    return this.findAllByConditions(['id', 'year'], { id: { [Sequelize.Op.notIn]: selectedPassingYear }});
  }
  
}
