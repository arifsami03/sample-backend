import { Table, Column, Model, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';

import { ESSection, ESQuestionOption } from '../..'

@Table({ timestamps: true })
export class ESQuestion extends Model<ESQuestion> {

  @Column question: string;

  @ForeignKey(() => ESSection)
  @Column ESSectionId: number;


  @Column countField: boolean;

  @Column remarksField: boolean;

  @Column optionType: string;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => ESSection, {
    foreignKey: 'ESSectionId'
  })
  esSection: ESSection;

  @HasMany(() => ESQuestionOption, {
    foreignKey: 'ESQuestionId'
  })
  options: ESQuestionOption[]
}
