import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class ESCategory extends Model<ESCategory> {

  @Column name: string;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

}
