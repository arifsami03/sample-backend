import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class Board extends Model<Board> {
  @Column name: string;
  @Column abbreviation: string;
  @Column mobileNumber: string;
  @Column website: string;
  @Column address: string;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;
}
