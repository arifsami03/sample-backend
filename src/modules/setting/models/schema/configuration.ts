import { Table, Column, Model, HasMany } from 'sequelize-typescript';
import { ConfigurationOption } from '../..'


@Table({ timestamps: true })
export class Configuration extends Model<Configuration> {

  @Column key: string;

  @Column value: string;

  @Column parentKey: string;

  @Column parentKeyId: number;

  @Column deleted: boolean;
  @Column isSingle: boolean;
  
  @Column label: string;

  @Column createdBy: number;

  @Column updatedBy: number;

  @HasMany(() => ConfigurationOption, {
    foreignKey: 'configurationId'
  })
  configurationOptions: ConfigurationOption[]

}
