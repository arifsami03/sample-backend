import { Table, Column, Model, BelongsTo, ForeignKey } from 'sequelize-typescript';

import { Configuration } from '../..'

@Table({ timestamps: true })
export class ConfigurationOption extends Model<ConfigurationOption> {

  @ForeignKey(() => Configuration)
  @Column configurationId: number;

  @Column key: string;

  @Column value: string;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => Configuration, {
    foreignKey: 'configurationId'
  })
  configuration: Configuration;

}
