import { Table, Column, Model, HasMany } from 'sequelize-typescript';
import { EmailTemplate } from '../..';

@Table({ timestamps: true })
export class OutgoingMailServer extends Model<OutgoingMailServer> {
  @Column name: string;
  @Column description: string;
  @Column isDefault: boolean;
  @Column smtpServer: string;
  @Column smtpPort: number;
  @Column connectionSecurity: string;
  @Column ownerId: number;
  @Column username: string;
  @Column password: string;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

  @HasMany(() => EmailTemplate, { foreignKey: 'outGoingMailServerId' })
  emailTemplates: EmailTemplate[];
}
