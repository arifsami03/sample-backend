import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class PassingYear extends Model<PassingYear> {
  @Column year: number;
  @Column annual: boolean;
  @Column supplimentary: boolean;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;
}
