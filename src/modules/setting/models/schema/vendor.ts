import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class Vendor extends Model<Vendor> {
  @Column name: string;
  @Column mobileNumber: string;
  @Column website: string;
  @Column address: string;
  @Column email: string;
  @Column contactPerson: string;
  @Column salesTaxNumber: string;
  @Column faxNumber: string;
  @Column ntnNumber: string;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

}
