import { Table, Column, Model, ForeignKey } from 'sequelize-typescript';
import { WorkFlow } from '../..';

@Table({ timestamps: true })
export class WFState extends Model<WFState> {
  @Column state: string;
  @Column title: string;
  @Column description: string;
  @ForeignKey(() => WorkFlow)
  @Column
  WFId: string;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;
}
