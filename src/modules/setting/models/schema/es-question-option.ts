import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import { ESQuestion } from '../..'

@Table({ timestamps: true })
export class ESQuestionOption extends Model<ESQuestionOption> {

  @Column optionString: string;

  @ForeignKey(() => ESQuestion)
  @Column ESQuestionId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => ESQuestion, {
    foreignKey: 'ESQuestionId'
  })
  esQuestion: ESQuestion;
}
