import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Board } from './board'
@Table({ timestamps: true })
export class BoardNOCNotRequired extends Model<BoardNOCNotRequired> {

  @ForeignKey(() => Board)
  @Column boardId: number;

  @ForeignKey(() => Board)
  @Column boardNOCNotRequiredId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;


  @BelongsTo(() => Board, {
    foreignKey: 'boardId'
  })
  board: Board;

  @BelongsTo(() => Board, {
    foreignKey: 'boardNOCNotRequiredId'
  })
  boardNOC: Board;
}
