import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { OutgoingMailServer } from '../..';

@Table({ timestamps: true })
export class EmailTemplate extends Model<EmailTemplate> {
  @Column name: string;
  @Column subject: string;

  @Column emailBody: boolean;
  @ForeignKey(() => OutgoingMailServer)
  @Column
  outGoingMailServerId: number;

  @Column to: string;
  @Column cc: string;
  @Column bcc: string;
  @Column code: string;
  @Column deletable: boolean;

  @Column deleted: boolean;

  @Column createdBy: number;
  @Column updatedBy: number;

  @BelongsTo(() => OutgoingMailServer, { foreignKey: 'outGoingMailServerId' })
  outGoingMailServer: OutgoingMailServer;
}
