import { Table, Column, Model, ForeignKey } from 'sequelize-typescript';

@Table({ timestamps: true })
export class WorkFlowLog extends Model<WorkFlowLog> {

  @Column parent: string;
  @Column parentId: number;
  @Column fromStateId: number;
  @Column toStateId: number;

  @Column deleted: boolean;

  @Column createdBy: number;
  @Column updatedBy: number;
}
