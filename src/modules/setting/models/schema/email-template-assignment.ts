import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { EmailTemplate } from '../..';

@Table({ timestamps: true })
export class EmailTemplateAssignment extends Model<EmailTemplateAssignment> {
  @Column parent: string;

  @Column parentId: number;

  @ForeignKey(() => EmailTemplate)
  @Column
  emailTemplateId: number;

  @Column deleted: boolean;

  @Column createdBy: number;
  @Column updatedBy: number;
}
