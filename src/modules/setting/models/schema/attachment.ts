import { Table, Column, Model, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { AttachedFile } from '../..';

@Table({ timestamps: true })
export class Attachment extends Model<Attachment> {
  @Column parent: string;
  @Column parentId: number;
  @Column description: string;
  @Column title: string;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

  @HasMany(() => AttachedFile, { foreignKey: 'attachmentId' })
  attachedFiles: AttachedFile[];
}
