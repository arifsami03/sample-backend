import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import { EvaluationSheet } from './evaluation-sheet'

@Table({ timestamps: true })
export class ESSection extends Model<ESSection> {
  @Column title: string;

  @ForeignKey(() => EvaluationSheet)
  @Column ESId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => EvaluationSheet, {
    foreignKey: 'ESId'
  })
  evaluationSheet: EvaluationSheet;
}
