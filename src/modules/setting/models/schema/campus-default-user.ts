import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import { Role } from '../../../security'

@Table({ timestamps: true })
export class CampusDefaultUser extends Model<CampusDefaultUser> {

  @Column userName: string;

  @Column abbreviation: string;

  @Column roleId: number;


  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => Role, {
    foreignKey: 'roleId'
  })
  role: Role;
}
