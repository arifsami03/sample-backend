import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import { ESCategory } from './es-category'

@Table({ timestamps: true })
export class EvaluationSheet extends Model<EvaluationSheet> {

  @Column title: string;

  @Column ESForm: string;

  @Column active: number;

  @Column ESCategoryId: string;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => ESCategory, {
    foreignKey: 'ESCategoryId'
  })
  esCategory: ESCategory;
}
