import { Table, Column, Model, ForeignKey, HasMany } from 'sequelize-typescript';
import { WorkFlow, WFState, EmailTemplateAssignment } from '../..';

@Table({ timestamps: true })
export class WFStateFlow extends Model<WFStateFlow> {
  @Column description: string;
  @ForeignKey(() => WorkFlow)
  @Column
  WFId: string;

  @ForeignKey(() => WFState)
  @Column
  fromId: number;

  @ForeignKey(() => WFState)
  @Column
  toId: number;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

  // HasMany Relationship
  //TODO:low why we need to do this what if parent is changed.
  @HasMany(() => EmailTemplateAssignment, {
    constraints: false,
    foreignKey: 'parentId',
    scope: {
      parent: 'stateflow'
    }
  })
  emailTemplateList: EmailTemplateAssignment[];
}
