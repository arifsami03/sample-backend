import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Attachment } from '../..';

@Table({ timestamps: true })
export class AttachedFile extends Model<AttachedFile> {
  @ForeignKey(() => Attachment)
  @Column
  attachmentId: number;

  @Column title: string;
  @Column originalName: string;
  @Column name: string;
  @Column path: string;
  @Column size: string;
  @Column type: string;
  @Column description: string;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

  @BelongsTo(() => Attachment, { foreignKey: 'attachmentId' })
  attachment: Attachment;
}
