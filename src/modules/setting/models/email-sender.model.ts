import * as nodemailer from 'nodemailer';
import { Promise } from 'bluebird';
import { EmailTemplateModel, OutgoingMailServer, OutGoingMailServerModel } from '..';
import { CRApplicationModel, CRApplicantModel } from '../../campus';
import { UserModel } from '../../security';
import { CONFIGURATIONS, BaseModel } from '../../base';

/**
 * Send email.
 *
 * @class EmailSenderModel
 */
export class EmailSenderModel {
  /**
   * Need this as a class property to save variable data values and use it in scope.
   *
   */
  private variableData = {};

  /**
   * A very responsible funciton be there always whenever you need :D
   *
   */
  constructor() { }

  /**
   * Find email templats on the bases of provided ids and send emails
   *
   * @param emailTemplateIds number array
   * @param options object array
   */
  public sendEmails(emailTemplateIds, options) {

    
    let includeObj = [{ model: OutgoingMailServer, as: 'outGoingMailServer', where: BaseModel.cb(), required: true }];

    return new EmailTemplateModel().findAllByConditions(null, { id: emailTemplateIds }, includeObj)
      .then(result => {

        if (result && result.length) {
          let emailTemplates = result;
          // checking if outGoingMailServer exist for this email template
          if (result['outGoingMailServer']) {
            // Get data on the bases of provided options
            return this.getDataByOptions(options).then(() => {
              // Replace provided variables with actual values and send email
              return this.prepareAndSendEmails(result);
            });
          } else {
            return new OutGoingMailServerModel().sequelizeModel
              .find({
                where: { isDefault: true }
              })
              .then(result => {
                if (result) {
                  for (let i = 0; i < emailTemplates.length; i++) {
                    emailTemplates[i]['outGoingMailServer'] = result;
                  }

                  // Get data on the bases of provided options
                  return this.getDataByOptions(options).then(() => {
                    // Replace provided variables with actual values and send email
                    return this.prepareAndSendEmails(emailTemplates);
                  });
                }
              });
          }
        }
        return result;
      });
  }

  /**
   * Get Data by provided options
   *
   * @param options object array
   */
  private getDataByOptions(options) {
    if (options.length) {
      return Promise.each(options, opt => {
        if (opt['tbl'] == 'campus') {
          return this.getCampusData(opt['recId']);
        }
      });
    }
  }

  /**
   * Get campus data
   *
   * @param id number
   */
  private getCampusData(id) {
   
    return new CRApplicationModel().find(id, ['CRApplicantId', 'campusName']).then(campRes => {
            
      this.variableData['campus-info'] = campRes;

      return new CRApplicantModel().find(campRes['CRApplicantId'], ['fullName', 'email']).then(coRes => {
        this.variableData['campus-owner-info'] = coRes;
      });
    });
  }

  /**
   * Prepare template and send email
   *
   * @param emailTemplates object array
   */
  prepareAndSendEmails(emailTemplates) {
    return Promise.each(emailTemplates, item => {
      
      
        let from = item['outGoingMailServer']['username'];
        let fromPassword = item['outGoingMailServer']['password'];
        let smtpServer = item['outGoingMailServer']['smtpServer'];
        let port = item['outGoingMailServer']['smtpPort'];
        let connectionSecurity = item['outGoingMailServer']['connectionSecurity'];

        // Replace variables
        let etVar = CONFIGURATIONS.EmailTemplateVar;

        let toEmail = item['to'].replace(etVar.CRApplicantEmail, this.variableData['campus-owner-info']['email']);

        let subject = item['subject'].replace(etVar.CRApplicantName, this.variableData['campus-owner-info']['fullName']);
        subject = subject.replace(etVar.campusName, this.variableData['campus-info']['campusName']);

        let html = item['emailBody'].replace(etVar.CRApplicantName, this.variableData['campus-owner-info']['fullName']);
        html = html.replace(etVar.CRApplicantUsername, this.variableData['campus-owner-info']['email']);
        html = html.replace(etVar.campusName, this.variableData['campus-info']['campusName']);
        html = html.replace(etVar.homeUrl, CONFIGURATIONS.environment.frontendURL);

        let text = '';

        // Now everything is prepared so send email
        return this.sendEmail(from, fromPassword, toEmail, smtpServer, port, connectionSecurity, subject, text, html).catch(error => {
          console.log(error);

          //TODO:low: If email sending is faild then inform somebody, maintain some log or do something.
        });
      

    });
  }
  /**
   * The following prinness method is the one which will actually send email.
   *
   * @param from string
   * @param fromPassword string
   * @param toEmail string
   * @param smtpServer string
   * @param port number
   * @param connectionSecurity string
   * @param subject string
   * @param text string
   * @param html string
   */
  private sendEmail(from: string, fromPassword: string, toEmail: string, smtpServer: string, port: number, connectionSecurity: string, subject: string, text: string, html: string) {
    return new Promise((resolve, reject) => {
      nodemailer.createTestAccount((err, account) => {
        let transporter = nodemailer.createTransport({
          host: smtpServer,
          secureConnection: connectionSecurity,
          port: port,
          transportMethod: 'SMTP',
          auth: { user: from, pass: fromPassword }
        });

        // Mail Options
        let mailOptions = { from: from, to: toEmail, subject: subject, text: text, html: html };

        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            reject({ error: 'Something went wrong.', detail: error });
          } else {
            resolve({ success: 'Sent', detail: info });
          }
        });
      });
    });
  }

  /**
   * Find email templats on the bases of provided ids and send emails
   *
   * @param emailTemplateIds number array
   * @param options object array
   */
  public sendEmailsByCode(emailTemplateCode, variableData) {
    let includeObj = [{ model: OutgoingMailServer, as: 'outGoingMailServer', where: BaseModel.cb(), required: false }];

    return new EmailTemplateModel().findAllByConditions(['id'], { code: emailTemplateCode }, includeObj)
      .then(result => {
        if (result && result.length) {
          let emailTemplates = result;
          // checking if outGoingMailServer exist for this email template
          if (result['outGoingMailServer']) {
            return this.prepareAndSendEmailByCode(emailTemplates, variableData);
          } else {
            return new OutGoingMailServerModel().sequelizeModel
              .find({
                where: { isDefault: true }
              })
              .then(result => {
                if (result) {
                  for (let i = 0; i < emailTemplates.length; i++) {
                    emailTemplates[i]['outGoingMailServer'] = result;
                  }
                  // Passing template and for replacing variable in this function
                  return this.prepareAndSendEmailByCode(emailTemplates, variableData);
                }
              });
          }
        }
      });
  }

  /**
   * Prepare template and send email
   *
   * @param emailTemplates object array
   */
  prepareAndSendEmailByCode(emailTemplates, variableData) {
    return Promise.each(emailTemplates, item => {
      let from = item['outGoingMailServer']['username'];
      let fromPassword = item['outGoingMailServer']['password'];
      let smtpServer = item['outGoingMailServer']['smtpServer'];
      let port = item['outGoingMailServer']['smtpPort'];
      let connectionSecurity = item['outGoingMailServer']['connectionSecurity'];
      // Replace variables
      let toEmail = item['to'];
      let subject = item['subject'];
      let html = item['emailBody'];
      if (variableData && variableData.length > 0) {
        variableData.forEach(item => {
          toEmail = toEmail.replace(item['variableName'], item['variableValue']);
          html = html.replace(item['variableName'], item['variableValue']);
          subject = subject.replace(item['variableName'], item['variableValue']);
        });
      }
      let text = '';

      // Now everything is prepared so send email
      return this.sendEmail(from, fromPassword, toEmail, smtpServer, port, connectionSecurity, subject, text, html).catch(error => {
        console.log(error);

        //TODO:low: If email sending is faild then inform somebody, maintain some log or do something.
      });
    });
  }
}
