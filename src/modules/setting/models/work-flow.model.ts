import { BaseModel } from '../../base';
import { WorkFlow, WFStateFlowModel, WFStateModel } from '..';

export class WorkFlowModel extends BaseModel {
  constructor() {
    super(WorkFlow);
  }

  /**
   * Function to get All Work Flows
   * @param id Work Flow Id
   */
  findWorkFlow(id: string) {
    return this.findAllByConditions(['WFId', 'title', 'description'], { WFId: id })
  }


  delete(id: string){
    
    //First Delete Associations, associated with the Work Flow.
    return new WFStateModel().deleteByWorkFlow(id).then(result => {

      return super.deleteByConditions({WFId:id});
      
    })
  }


}
