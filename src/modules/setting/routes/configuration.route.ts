import { Request, Response, Router } from 'express';

import { ConfigurationsController } from '..';

/**
 * / route
 *
 * @class Configuration
 */
export class ConfigurationRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class Configuration
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class Configuration
   * @method create
   *
   */
  public create() {
    let controller = new ConfigurationsController();

    this.router.route('/settings/configurations/index/:key').get(controller.index);
    this.router.route('/settings/configurations/getAllSingleConf').get(controller.getAllSingleConf);
    this.router.route('/settings/configurations/city').get(controller.findCities);
    this.router.route('/settings/configurations/bank').get(controller.findBanks);
    this.router.route('/settings/configurations/view/:id').get(controller.find);
    this.router.route('/settings/configurations/update/:id').put(controller.update);
    this.router.route('/settings/configurations/create').post(controller.create);
    this.router.route('/settings/configurations/delete/:id').delete(controller.delete);
    this.router.route('/settings/configurations/findAttributesList/:key').get(controller.findAttributesList);
    this.router.route('/settings/configurations/findChildren/:id').get(controller.findChildren);
    this.router.route('/settings/configurations/findAllCities/:id').get(controller.findAllCities);
    this.router.route('/settings/configurations/findConfiguration').get(controller.findConfiguration);
  }
}
