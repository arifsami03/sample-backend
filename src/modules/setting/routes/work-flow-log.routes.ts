import { Router, Request, Response } from 'express';
import { WorkFlowLogController } from '..';

export class WorkFlowLogRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new WorkFlowLogController();

    this.router.route('/settings/workFlowLog/getLog/:parent/:parentId').get(controller.find);

  }
}
