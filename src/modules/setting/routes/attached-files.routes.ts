import { Router } from 'express';

import { AttachedFilesController } from '..';

export class AttachedFilesRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new AttachedFilesController();

    this.router.route('/settings/attachedFiles/list/:parent/:parentId').get(controller.list);
    this.router.route('/settings/attachedFiles/delete/:id').delete(controller.delete);
    this.router.route('/settings/attachedFiles/download').post(controller.download);
  }
}
