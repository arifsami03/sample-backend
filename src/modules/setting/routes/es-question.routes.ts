import { Router } from 'express';
import { ESQuestionsController } from '..';

export class ESQuestionsRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new ESQuestionsController();

    this.router.route('/settings/esQuestions/index').get(controller.index);
    this.router.route('/settings/esQuestions/find/:id').get(controller.find);
    this.router.route('/settings/esQuestions/create').post(controller.create);
    this.router.route('/settings/esQuestions/update/:id').put(controller.update);
    this.router.route('/settings/esQuestions/delete/:id').delete(controller.delete);
    this.router.route('/settings/esQuestions/findAttributesList').get(controller.findAttributesList);
    this.router.route('/settings/esQuestions/findAllESQuestions/:ESId').get(controller.findAllESQuestions);
  }
}
