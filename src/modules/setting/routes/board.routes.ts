import { Router } from 'express';
import { BoardsController } from '..';

export class BoardRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new BoardsController();

    this.router.route('/settings/boards/index').get(controller.index);
    this.router.route('/settings/boards/find/:id').get(controller.find);
    this.router.route('/settings/boards/create').post(controller.create);
    this.router.route('/settings/boards/update/:id').put(controller.update);
    this.router.route('/settings/boards/delete/:id').delete(controller.delete);
    this.router.route('/settings/boards/findAttributesList').get(controller.findAttributesList);

    this.router.route('/settings/boards/addNOCNotRequired').post(controller.addNOCNotRequired);
    this.router.route('/settings/boards/deleteNOCNotRequuired/:id').delete(controller.deleteNOCNotRequuired);
    this.router.route('/settings/boards/findAllNOCNotRequiredBoards/:id').get(controller.findAllNOCNotRequiredBoards);
    this.router.route('/settings/boards/findAllBoardsToAddNOC/:id').get(controller.findAllBoardsToAddNOC);


  }
}
