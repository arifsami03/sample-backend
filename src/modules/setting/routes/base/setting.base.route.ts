import { Router } from 'express';

import {
  ConfigurationRoute,
  BanksRoute,
  OutGoingMailServersRoute,
  EmailTemplatesRoute,
  WorkFlowsRoute,
  WFStateRoute,
  WFStateFlowRoute,
  EvaluationSheetsRoute,
  ESSectionsRoute,
  DocStateManagerRoute,
  ESQuestionsRoute,
  WorkFlowLogRoute,
  EmailTemplateAssignmentsRoute,
  AttachmentsRoute,
  AttachedFilesRoute,
  ESCategoryRoute,
  PassingYearRoute,
  BoardRoute,
  VendorRoute,
  CampusDefaultUsersRoute
} from '../..';

/**
 *
 *
 * @class SettingBaseRoute
 */
export class SettingBaseRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class BaseRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.initAll();
  }

  /**
   * init all routes
   */
  public initAll() {
    new ConfigurationRoute(this.router);
    new BanksRoute(this.router);
    new OutGoingMailServersRoute(this.router);
    new EmailTemplatesRoute(this.router);
    new WorkFlowsRoute(this.router);
    new WFStateRoute(this.router);
    new WFStateFlowRoute(this.router);
    new EvaluationSheetsRoute(this.router);
    new ESSectionsRoute(this.router);
    new DocStateManagerRoute(this.router);
    new ESQuestionsRoute(this.router);
    new EmailTemplateAssignmentsRoute(this.router);
    new WorkFlowLogRoute(this.router);
    new AttachmentsRoute(this.router);
    new AttachedFilesRoute(this.router);
    new ESCategoryRoute(this.router);
    new PassingYearRoute(this.router);
    new BoardRoute(this.router);
    new VendorRoute(this.router);
    new CampusDefaultUsersRoute(this.router);
  }
}
