import { Router, Request, Response } from 'express';
import { WorkFlowsController } from '..';

export class WorkFlowsRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new WorkFlowsController();

    this.router.route('/settings/workFlows/index').get(controller.index);
    this.router.route('/settings/workFlows/find/:id').get(controller.find);
    this.router.route('/settings/workFlows/checkRecordExists/:id').get(controller.checkRecordExists);

    this.router.route('/settings/workFlows/create').post(controller.create);
    this.router.route('/settings/workFlows/update/:id').put(controller.update);
    this.router.route('/settings/workFlows/delete/:id').delete(controller.delete);
  }
}
