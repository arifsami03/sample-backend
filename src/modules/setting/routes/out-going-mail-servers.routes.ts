import { Router } from 'express';

import { OutGoingMailServersController } from '..';

export class OutGoingMailServersRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new OutGoingMailServersController();

    this.router.route('/settings/outGoingMailServers/index').get(controller.index);
    this.router.route('/settings/outGoingMailServers/find/:id').get(controller.find);
    this.router.route('/settings/outGoingMailServers/sendEmailByEmailTemplate/:id').get(controller.sendEmailByEmailTemplate);
    this.router.route('/settings/outGoingMailServers/create').post(controller.create);
    this.router.route('/settings/outGoingMailServers/update/:id').put(controller.update);
    this.router.route('/settings/outGoingMailServers/delete/:id').delete(controller.delete);
    this.router.route('/settings/outGoingMailServers/findAttributesList').get(controller.findAttributesList);
  }
}
