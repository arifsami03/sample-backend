import { Router } from 'express';
import { BanksController } from '..';

export class BanksRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new BanksController();

    this.router.route('/settings/banks/index').get(controller.index);
    this.router.route('/settings/banks/find/:id').get(controller.find);
    this.router.route('/settings/banks/create').post(controller.create);
    this.router.route('/settings/banks/update/:id').put(controller.update);
    this.router.route('/settings/banks/delete/:id').delete(controller.delete);
    this.router.route('/settings/banks/findAttributesList').get(controller.findAttributesList);
  }
}
