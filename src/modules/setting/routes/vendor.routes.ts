import { Router } from 'express';
import { VendorsController } from '..';

export class VendorRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new VendorsController();

    this.router.route('/settings/vendors/index').get(controller.index);
    this.router.route('/settings/vendors/find/:id').get(controller.find);
    this.router.route('/settings/vendors/create').post(controller.create);
    this.router.route('/settings/vendors/update/:id').put(controller.update);
    this.router.route('/settings/vendors/delete/:id').delete(controller.delete);
    this.router.route('/settings/vendors/findAttributesList').get(controller.findAttributesList);
  }
}
