import { Router, Request, Response } from 'express';
import { WFStateFlowsController } from '..';

export class WFStateFlowRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new WFStateFlowsController();

    this.router.route('/settings/wfStateFlows/index').get(controller.index);
    this.router.route('/settings/wfStateFlows/list/:id').get(controller.find);
    this.router.route('/settings/wfStateFlows/findById/:id').get(controller.findById);
    this.router.route('/settings/wfStateFlows/getFlowByWFId/:id').get(controller.getFlowByWFId);

    this.router.route('/settings/wfStateFlows/create').post(controller.create);
    this.router.route('/settings/wfStateFlows/update/:id').put(controller.update);
    this.router.route('/settings/wfStateFlows/delete/:id').delete(controller.delete);
  }
}
