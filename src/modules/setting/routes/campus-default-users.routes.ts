import { Router } from 'express';
import { CampusDefaultUsersController } from '..';

export class CampusDefaultUsersRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new CampusDefaultUsersController();

    this.router.route('/settings/campusDefaultUsers/index').get(controller.index);
    this.router.route('/settings/campusDefaultUsers/find/:id').get(controller.find);
    this.router.route('/settings/campusDefaultUsers/create').post(controller.create);
    this.router.route('/settings/campusDefaultUsers/update/:id').put(controller.update);
    this.router.route('/settings/campusDefaultUsers/delete/:id').delete(controller.delete);
    this.router.route('/settings/campusDefaultUsers/findAttributesList').get(controller.findAttributesList);
  }
}
