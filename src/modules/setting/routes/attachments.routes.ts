import { Router } from 'express';

import { AttachmentsController } from '..';

export class AttachmentsRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new AttachmentsController();

    this.router.route('/settings/attachments/fileUpload/:parent/:parentId/:fileFilter/:fileSize').post(controller.fileUpload);
  }
}
