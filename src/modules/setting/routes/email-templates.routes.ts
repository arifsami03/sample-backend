import { Router } from 'express';

import { EmailTemplatesController } from '..';

export class EmailTemplatesRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new EmailTemplatesController();

    this.router.route('/settings/emailTemplates/index').get(controller.index);
    this.router.route('/settings/emailTemplates/findAttributesList').get(controller.findAttributesList);
    this.router.route('/settings/emailTemplates/find/:id').get(controller.find);
    this.router.route('/settings/emailTemplates/create').post(controller.create);
    this.router.route('/settings/emailTemplates/update/:id').put(controller.update);
    this.router.route('/settings/emailTemplates/delete/:id').delete(controller.delete);
  }
}
