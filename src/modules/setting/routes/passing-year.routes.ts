import { Router } from 'express';
import { PassingYearsController } from '..';

export class PassingYearRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new PassingYearsController();

    this.router.route('/settings/passingYears/index').get(controller.index);
    this.router.route('/settings/passingYears/find/:id').get(controller.find);
    this.router.route('/settings/passingYears/create').post(controller.create);
    this.router.route('/settings/passingYears/update/:id').put(controller.update);
    this.router.route('/settings/passingYears/delete/:id').delete(controller.delete);
    this.router.route('/settings/passingYears/findAttributesList').get(controller.findAttributesList);
    this.router.route('/settings/passingYears/getPassingYearNotSelected').post(controller.getPassingYearNotSelected);
  }
}
