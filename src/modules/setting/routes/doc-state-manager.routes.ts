import { Router } from 'express';
import { DocStateManagersController } from '..';

export class DocStateManagerRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new DocStateManagersController();

    this.router.route('/settings/docStateManagers/getListOfActions').post(controller.getListOfActions);
    this.router.route('/settings/docStateManagers/updateStateId').post(controller.updateStateId);
    this.router.route('/settings/docStateManagers/getStateId').post(controller.getStateId);
    this.router.route('/settings/docStateManagers/getState/:id').get(controller.getState);
    this.router.route('/settings/docStateManagers/getCurrentStateId').post(controller.getCurrentStateId);

  }
}
