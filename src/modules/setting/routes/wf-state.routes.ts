import { Router, Request, Response } from 'express';
import { WFStatesController } from '..';

export class WFStateRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new WFStatesController();

    this.router.route('/settings/wfStates/index').get(controller.index);
    this.router.route('/settings/wfStates/list/:id').get(controller.find);
    this.router.route('/settings/wfStates/findById/:id').get(controller.findById);
    this.router.route('/settings/wfStates/checkRecordExists/:id').put(controller.checkRecordExists);
    this.router.route('/settings/wfStates/create').post(controller.create);
    this.router.route('/settings/wfStates/update/:id').put(controller.update);
    this.router.route('/settings/wfStates/delete/:id').delete(controller.delete);
    this.router.route('/settings/wfStates/findWorkflowStates/:id').get(controller.findWorkflowStates);

  }
}
