import { Router } from 'express';
import { EvaluationSheetsController } from '..';

export class EvaluationSheetsRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new EvaluationSheetsController();

    this.router.route('/settings/evaluationSheets/index').get(controller.index);
    this.router.route('/settings/evaluationSheets/find/:id').get(controller.find);
    this.router.route('/settings/evaluationSheets/findCampusEvaluationSheet').get(controller.findCampusEvaluationSheet);
    this.router.route('/settings/evaluationSheets/create').post(controller.create);
    this.router.route('/settings/evaluationSheets/update/:id').put(controller.update);
    this.router.route('/settings/evaluationSheets/delete/:id').delete(controller.delete);
    this.router.route('/settings/evaluationSheets/findAttributesList').get(controller.findAttributesList);
    this.router.route('/settings/evaluationSheets/findAttributesListByCat/:catId').get(controller.findAttributesListByCat);
  }
}
