import { Router } from 'express';

import { EmailTemplateAssignmentsController } from '..';

export class EmailTemplateAssignmentsRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new EmailTemplateAssignmentsController();

    this.router.route('/settings/emailTemplateAssignments/index').get(controller.index);
    this.router.route('/settings/emailTemplateAssignments/find/:id').get(controller.find);
    this.router.route('/settings/emailTemplateAssignments/create').post(controller.create);
    this.router.route('/settings/emailTemplateAssignments/update/:id').put(controller.update);
    this.router.route('/settings/emailTemplateAssignments/delete/:id').delete(controller.delete);
  }
}
