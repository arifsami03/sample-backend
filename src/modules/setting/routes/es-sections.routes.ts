import { Router } from 'express';
import { ESSectionsController } from '..';

export class ESSectionsRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new ESSectionsController();

    this.router.route('/settings/esSections/index').get(controller.index);
    this.router.route('/settings/esSections/find/:id').get(controller.find);
    this.router.route('/settings/esSections/create').post(controller.create);
    this.router.route('/settings/esSections/update/:id').put(controller.update);
    this.router.route('/settings/esSections/delete/:id').delete(controller.delete);
    this.router.route('/settings/esSections/findAttributesList').get(controller.findAttributesList);
    this.router.route('/settings/esSections/findAllESSections/:ESId').get(controller.findAllESSections);
  }
}
