import { Router } from 'express';
import { ESCategoriesController } from '..';

export class ESCategoryRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new ESCategoriesController();

    this.router.route('/settings/esCategories/index').get(controller.index);
    this.router.route('/settings/esCategories/find/:id').get(controller.find);
    this.router.route('/settings/esCategories/create').post(controller.create);
    this.router.route('/settings/esCategories/update/:id').put(controller.update);
    this.router.route('/settings/esCategories/delete/:id').delete(controller.delete);
    this.router.route('/settings/esCategories/findAttributesList').get(controller.findAttributesList);
    this.router.route('/settings/esCategories/findCategoriesByES').get(controller.findCategoriesByES);
  }
}
