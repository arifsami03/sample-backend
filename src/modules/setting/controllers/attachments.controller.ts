import * as express from 'express';
 

import { ErrorHandler } from '../../base/conf/error-handler';
import { AttachmentModel } from '..';

export class AttachmentsController {
  constructor() {}

  /**
   * Create record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  fileUpload(req: express.Request, res: express.Response, next: express.NextFunction) {
    let attachmentModel = new AttachmentModel();

    attachmentModel.configureMulter(req['params']);

    attachmentModel.upload(req, res, err => {
      return attachmentModel.uploader(req, res, err, next);
    });
  }
}
