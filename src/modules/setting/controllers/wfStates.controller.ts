import * as express from 'express';


import { WFStateModel } from '..';
import { ErrorHandler } from '../../base/conf/error-handler';
import { Sequelize } from 'sequelize-typescript';

export class WFStatesController {
  constructor() { }

  /**
   * get all records
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    new WFStateModel()
      .findAll(['WFId', 'title', 'description'])
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Find All States against workflow
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findWorkflowStates(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new WFStateModel().findAllByConditions(['id', 'title'], { WFId: id, state: { [Sequelize.Op.not]: 'new' } }).then(result => {
      res.send(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }
  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesList(req: express.Request, res: express.Response, next: express.NextFunction) {
    new WFStateModel()
      .findAll(['id', 'name'])
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Find one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new WFStateModel()
      .findWFState(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Find one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findById(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new WFStateModel()
      .findById(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }



  /**
   * create new record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {

    new WFStateModel()
      .create(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * delete record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new WFStateModel()
      .delete(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * update record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;
    new WFStateModel()
      .update(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });

  }

  /**
  * Check Record Exsist or not
  * 
  * @param req express.Request
  * @param res express.Response
  * @param next express.NextFunction
  */
  checkRecordExists(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    let item = req.body;
    new WFStateModel()
      .checkRecordExists(id, item.WFId)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }


}
