import * as express from 'express';


import { DocStateManagerModel, WFStateModel } from '..';
import { ErrorHandler } from '../../base/conf/error-handler';

export class DocStateManagersController {
  constructor() { }

  /**
   * get all records
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  getListOfActions(req: express.Request, res: express.Response, next: express.NextFunction) {
    new DocStateManagerModel()
      .getListOfActions(req.body)
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  updateStateId(req: express.Request, res: express.Response, next: express.NextFunction) {

    new DocStateManagerModel().updateStateId(req.body).then(result => {
      res.json(result);
    })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }


  /**
  * Get State ID
  *
  * @param req express.Request
  * @param res express.Response
  * @param next express.NextFunction
  */
  getStateId(req: express.Request, res: express.Response, next: express.NextFunction) {

    new DocStateManagerModel()
      .getStateId(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
  * Get State
  *
  * @param req express.Request
  * @param res express.Response
  * @param next express.NextFunction
  */
  getState(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new DocStateManagerModel()
      .getState(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
  * Get State ID
  *
  * @param req express.Request
  * @param res express.Response
  * @param next express.NextFunction
  */
  getCurrentStateId(req: express.Request, res: express.Response, next: express.NextFunction) {

    new DocStateManagerModel()
      .getCurrentStateId(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }


}
