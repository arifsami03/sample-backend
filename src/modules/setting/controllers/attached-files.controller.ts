import * as express from 'express';
 

import { ErrorHandler } from '../../base/conf/error-handler';
import { AttachedFileModel } from '..';
const path = require('path');

export class AttachedFilesController {
  constructor() {}

  /**
   * Find single record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  list(req: express.Request, res: express.Response, next: express.NextFunction) {
    let queryParams = req.params;
    new AttachedFileModel()
      .findByAttachment(queryParams)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Delete one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new AttachedFileModel()
      .deleteWithFile(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Download one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  download(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.body.id;

    new AttachedFileModel()
      .download(id)
      .then(result => {
        let filePath = path.resolve(result['path']);
        res.sendFile(filePath);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
}
