import * as express from 'express';


import { BoardModel, BoardNOCNotRequiredModel } from '..';
import { ErrorHandler } from '../../base/conf/error-handler';
import { CustomSort } from '../../base/conf/custom-sort';

export class BoardsController {
  constructor() { }

  /**
   * Get all records
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    new BoardModel()
      .findAll()
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesList(req: express.Request, res: express.Response, next: express.NextFunction) {
    new BoardModel()
      .findAll(['id', 'name'])
      .then(result => {
        result.sort(CustomSort.sortByName);
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Find one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new BoardModel()
      .find(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Create new record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new BoardModel()
      .create(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Delete record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new BoardModel()
      .delete(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;
    new BoardModel()
      .update(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Add a NOC board (not required) against a board 
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  addNOCNotRequired(req: express.Request, res: express.Response, next: express.NextFunction) {
    new BoardNOCNotRequiredModel()
      .create(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Deletes a Board NOC (not required) against a board
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  deleteNOCNotRequuired(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new BoardNOCNotRequiredModel()
      .delete(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Finds NOC (not required) board list against a board
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAllNOCNotRequiredBoards(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new BoardNOCNotRequiredModel()
      .findAllNOCNotRequiredBoards(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Find list of boards to add them as NOC (i.e the boards which are not already added as NOC)
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAllBoardsToAddNOC(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new BoardNOCNotRequiredModel()
      .findAllBoardsToAddNOC(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

}
