import * as express from 'express';
 

import { WorkFlowLogModel } from '..';
import { ErrorHandler } from '../../base/conf/error-handler';

export class WorkFlowLogController {
  constructor() { }


  /**
   * Find one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let parent = req.params.parent;
    let parentId = req.params.parentId;

    new WorkFlowLogModel()
      .getLog(parent,parentId)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }


 
}
