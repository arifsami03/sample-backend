import * as express from 'express';

import { ErrorHandler } from '../../base/conf/error-handler';
import { CustomSort } from '../../base/conf/custom-sort';

import { ConfigurationModel, ConfigurationOption } from '..';

export class ConfigurationsController {
  constructor() { }

  /**
   * Index
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let key = req.params.key;

    new ConfigurationModel()
      .findAllByConditions(['id', 'key', 'value'], { key: key }, [{
        model: ConfigurationOption, as: 'configurationOptions',
        attributes: ['id', 'key', 'value']
      }])
      .then(result => {
        result.sort(CustomSort.sortByValue);
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * getAllSingleConf
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  getAllSingleConf(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {

    new ConfigurationModel()
      .findAllByConditions(['id', 'key', 'value','label'], { isSingle: true }, [{
        model: ConfigurationOption, as: 'configurationOptions',
        attributes: ['id', 'key', 'value']
      }])
      .then(result => {
        result.sort(CustomSort.sortByValue);
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * findAttributesList
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesList(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let key = req.params.key;

    new ConfigurationModel()
      .findAllByConditions(['id', 'key', 'value'], { key: key }, [{
        model: ConfigurationOption, as: 'configurationOptions',
        attributes: ['id', 'key', 'value']
      }])
      .then(result => {
        result.sort(CustomSort.sortByValue);
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * findChildren of configuration
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findChildren(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let id = req.params.id;

    new ConfigurationModel()
      .findAllByConditions(['id', 'key', 'value'], { parentKeyId: id })
      .then(result => {
        result.sort(CustomSort.sortByValue);
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   *  Find All Cities of a country
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAllCities(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new ConfigurationModel().findAllCities(id)
      .then(result => {
        result.sort(CustomSort.sortByValue);
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Find One
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let id = req.params.id;

    new ConfigurationModel()
      .findWithOptions(['id', 'key', 'value', 'parentKey', 'parentKeyId'], { id: id })
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Find Remmitance
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findConfiguration(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {

    new ConfigurationModel()
      .findWithOptions(['id', 'key', 'value', 'parentKey', 'parentKeyId'], { key : 'showRemittance', isSingle: true })
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * findCities
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findCities(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new ConfigurationModel().findAll(['id', 'value'], { key: 'city' }).then(result => {
      if (result) {
        result.sort(CustomSort.sortByValue);
        res.json(result);
      } else {
        res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
      }
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * getBanks
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findBanks(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new ConfigurationModel()
      .findAll(['id', 'value'], { key: 'bank' })
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let id = req.params.id;
    let item = req.body;

    new ConfigurationModel()
      .updateWithOptions(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Create
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new ConfigurationModel()
      .createWithOptions(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Delete
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {

    new ConfigurationModel().deleteWithAssociatedRecords(req.params.id).then(result => {
      res.json(result);
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }
}
