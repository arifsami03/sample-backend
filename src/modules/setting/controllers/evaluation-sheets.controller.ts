import * as express from 'express';


import { EvaluationSheetModel } from '..';
import { ErrorHandler } from '../../base/conf/error-handler';

export class EvaluationSheetsController {
  constructor() { }

  /**
   * Get all records
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    new EvaluationSheetModel().findAllWithESCategory().then(result => {
      res.send(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesList(req: express.Request, res: express.Response, next: express.NextFunction) {
    new EvaluationSheetModel().findAll(['id', 'title']).then(result => {
      res.send(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }
    /**
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findCampusEvaluationSheet(req: express.Request, res: express.Response, next: express.NextFunction) {
    new EvaluationSheetModel()
      .findCampusEvaluationSheet()
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        ErrorHandler.sendServerError(_error, res, next);
      });
  }

  /**
   * Find list by category
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesListByCat(req: express.Request, res: express.Response, next: express.NextFunction) {
    let catId = req.params.catId;
    new EvaluationSheetModel().findAllByConditions(['id', 'title'], { ESCategoryId: catId }).then(result => {
      res.send(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Find one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new EvaluationSheetModel().findWithESCategory(id).then(result => {
      res.json(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Create new record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new EvaluationSheetModel().create(req.body).then(result => {
      res.json(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Delete record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new EvaluationSheetModel().deleteWithSections(id).then(result => {
      res.json(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Update record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;
    new EvaluationSheetModel().update(id, item).then(result => {
      res.json(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

}
