import { BaseModel } from '../../base';
import { AcademicCalendarProgramDetail, AcademicCalendar, ProgramDetail, Program, CACTerm, ProgramModel } from '..';
import { Sequelize } from 'sequelize-typescript';
import { CampusProgramDetailModel, CampusProgramDetail, CampusProgramModel } from '../../campus';
import { CampusAcademicCalendarModel } from '../../institute';
import { CACTermModel } from './cac-term.model';
import { Promise } from 'bluebird';

export class AcademicCalendarProgramDetailModel extends BaseModel {
  constructor() {
    super(AcademicCalendarProgramDetail);
  }
  private cacModel = new CampusAcademicCalendarModel();
  private cacTermModel = new CACTermModel();

  findAttributesList() {
    let includeObj = [
      { model: AcademicCalendar, as: 'academicCalendar', attributes: ['id', 'title'], where: BaseModel.cb(), required: false },
      {
        model: ProgramDetail, as: 'programDetail', attributes: ['id', 'name'], where: BaseModel.cb(), required: false,
        include: [{ model: Program, as: 'program', attributes: ['id', 'name'], where: BaseModel.cb(), required: false }]
      }
    ];

    return this.findAll(['id', 'startDate', 'endDate'], {}, includeObj);
  }
  findByProgramDetail(_programDetailId) {
    let includeObj = [{ model: AcademicCalendar, as: 'academicCalendar', attributes: ['id', 'title'], where: BaseModel.cb(), required: false }];

    return this.findAllByConditions(['id', 'startDate', 'endDate'], { programDetailId: _programDetailId }, includeObj)
      .then(result => {
        let academicCalendar = [];
        result.forEach(element => {

          academicCalendar.push(element['academicCalendar'])
        });
        return academicCalendar;

      });
  }
  findByCampusProgramDetails(campusIds: number[]) {
    let programDetaiIds = [];
    return new CampusProgramModel().findAllByConditions(['id', 'campusId', 'programId'], { campusId: { [Sequelize.Op.in]: campusIds } },
      [
        {
          model: CampusProgramDetail,
          as: 'campusProgramDetails',
          attributes: ['programDetailId']
          // attributes : [[Sequelize.fn('DISTINCT', Sequelize.col('programDetailId')), 'programDetailId']]
        }
      ]
    ).then(campusPrograms => {
      campusPrograms.forEach(campusProgram => {
        campusProgram['campusProgramDetails'].forEach(campusProgramDetail => {
          programDetaiIds.push(campusProgramDetail['programDetailId'])
        });
      });
      programDetaiIds = this.removeDuplicateValuesInArray(programDetaiIds);

      let includeObj = [
        { model: AcademicCalendar, as: 'academicCalendar', attributes: ['id', 'title'], where: BaseModel.cb(), required: false },
        {
          model: ProgramDetail, as: 'programDetail', attributes: ['id', 'name'], where: BaseModel.cb(), required: false,
          include: [{ model: Program, as: 'program', attributes: ['id', 'name'], where: BaseModel.cb(), required: false }]
        }
      ];

      return this.findAllByConditions(['id', 'startDate', 'endDate'], { programDetailId: programDetaiIds }, includeObj);
    })

  }

  findAttributes(id) {

    let includeObj = [
      { model: AcademicCalendar, as: 'academicCalendar', attributes: ['id', 'title'], where: BaseModel.cb(), required: false },
      {
        model: ProgramDetail, as: 'programDetail', attributes: ['id', 'name'], where: BaseModel.cb(), required: false,
        include: [{ model: Program, as: 'program', attributes: ['id', 'name'], where: BaseModel.cb(), required: false }]
      },
      { model: CACTerm, as: 'cacTerms', where: BaseModel.cb(), required: false }
    ];
    return this.find(id, ['id', 'startDate', 'endDate'], includeObj);
  }

  removeDuplicateValuesInArray(arr) {
    let unique_array = arr.filter(function (elem, index, self) {
      return index == self.indexOf(elem);
    });
    return unique_array
  }

  deleteWithAssociatedRecords(id) {

    return new CampusAcademicCalendarModel().deleteByConditions({ academicCalendarProgramDetailId: id }).then(() => {
      return new CACTermModel().deleteByConditions({ academicCalendarProgramDetailId: id }).then(() => {
        return this.delete(id);
      })
    })
  }
  deleteByConditionsWithAssociatedRecords(condition) {


    return this.findAllByConditions(['id'], condition).then(acpds => {
      let campusModel = new CampusAcademicCalendarModel();
      let cacTermModel = new CACTermModel();
      return Promise.each(acpds, acpd => {

        return campusModel.deleteByConditions({ academicCalendarProgramDetailId: acpd['id'] }).then(() => {

          return cacTermModel.deleteByConditions({ academicCalendarProgramDetailId: acpd['id'] }).then(() => {

            return this.delete(acpd['id']);

          })

        })

      })
    })

  }
  
}
