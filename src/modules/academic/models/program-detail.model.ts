import { BaseModel } from '../../base';
import * as _ from 'lodash';
import { ClassesModel, ProgramDetail, Program } from '..';
import { EducationLevel, InstituteType } from '../../institute';
import { MeritList } from '../../admission';
import { CampusProgramModel } from '../../campus';
import { CAProgramDetailModel } from '../../admission';
import { Sequelize } from 'sequelize-typescript';
import { CampusProgramDetailModel, CampusProgramDetail, CPDAccount, CRProgramDetail } from '../../campus';
import { FeeStructure } from '../../fee-management';
import { CampusTimeTable, TimeTable } from '../../time-table';
import { RoadMap } from './schema/roadmap';
import { AcademicCalendarProgramDetail } from './schema/academic-calendar-program-detail';
import { Classes } from './schema/classes';

export class ProgramDetailModel extends BaseModel {
  constructor() {
    super(ProgramDetail);
  }


  /**
   * it will find all programs
   */
  // index() {
  //   this.openConnection();
  //   var programDetails = [];
  //   return this.findAll(['id', 'name', 'programId']).then(res => {
  //     return Promise.each(res, resItem => {
  //       return new ProgramModel().findByCondition(['name'], { id: resItem['programId'] }).then(finalRes => {
  //         programDetails.push({
  //           id: resItem['id'],
  //           name: resItem['name'],
  //           program: finalRes
  //         });
  //       });
  //     }).then(() => {
  //       return programDetails;
  //     });
  //   });
  // }

  index() {
    let includeObj = [
      {
        model: Program, as: 'program', attributes: ['id', 'name', 'educationLevelId'], where: BaseModel.cb(), required: false,
        include: [
          {
            model: EducationLevel, as: 'educationLevel', attributes: ['id', 'education', 'instituteTypeId'], where: BaseModel.cb(), required: false,
            include: [{ model: InstituteType, as: 'instituteType', attributes: ['id', 'name'], where: BaseModel.cb(), required: false }]
          }
        ]
      }
    ];

    return this.findAll(['id', 'name', 'programId'], null, includeObj);
  }

  findAttributesList() {

    let includeObj = [
      {
        model: Program, as: 'program', attributes: ['id', 'name', 'educationLevelId'], where: BaseModel.cb(), required: false,
        include: [{
          model: EducationLevel, as: 'educationLevel', attributes: ['id', 'education', 'instituteTypeId'], where: BaseModel.cb(), required: false,
          include: [{ model: InstituteType, as: 'instituteType', attributes: ['id', 'name'], where: BaseModel.cb(), required: false }]
        }]
      }
    ];

    return this.findAll(['id', 'name', 'programId'], null, includeObj);
  }

  find(id) {
    //this.openConnection();
    return this.findByCondition(['id', 'name', 'programId', 'abbreviation'], { id: id }).then(res => {
      return res;
    });
  }

  findProgramDetailslistNotInCampusAdmission(campusId, campusAdmissionId) {

    let caProgramDetailModel = new CAProgramDetailModel();

    let campusProgramModel = new CampusProgramModel();
    let campusProgramDetailModel = new CampusProgramDetailModel();
    let campusAdmissionPDId = [];
    let programIds = [];
    let programDeatilsIds = [];



    return caProgramDetailModel.findAllByConditions(['programDetailId'], { campusAdmissionId: campusAdmissionId }).then(pdIdsRes => {

      pdIdsRes.forEach(item => {

        campusAdmissionPDId.push(item['programDetailId'])

      });

      return campusProgramModel.findAllByConditions(['id'], { campusId: campusId }).then(campusProgramIdsRes => {



        //Push all the Id in array of ProgramIds
        campusProgramIdsRes.forEach(progItem => {

          programIds.push(progItem['id'])

        });

        // Get List of all the Program Details againgst all thhe campus program Id
        return campusProgramDetailModel.findAllByConditions(['programDetailId'], { campusProgramId: { [Sequelize.Op.in]: programIds } }).then(cpdRes => {

          //Push all the Id in array of ProgramDetailsIds
          cpdRes.forEach(progDetailItem => {

            programDeatilsIds.push(progDetailItem['programDetailId'])

          });

          programDeatilsIds = _.difference(programDeatilsIds, campusAdmissionPDId);


        })
      });

    }).then(() => {
      return new ProgramDetailModel().findAllByConditions(['id', 'name'], { id: { [Sequelize.Op.in]: programDeatilsIds } }, {
        model: Program,
        as: 'program',
        attributes: ['id', 'name']
      })

    });
  }

  getProgramProgramDetails(id) {
    return new ProgramDetailModel()
      .findAllByConditions(['id', 'name'], { programId: id });
  }

  getProgramDetails() {
    return this.index();
  }

  // getProgramDetails() {
  //   var programDetailsList = [];
  //   return this.findAll(['id', 'name', 'programId']).then(programDetails => {
  //     return Promise.each(programDetails, programDetailItem => {
  //       return new ProgramModel()
  //         .findByCondition(['name', 'educationLevelId'], { id: programDetailItem['programId'] })
  //         .then(program => {
  //           return new EducationLevelModel()
  //             .findByCondition(['education'], { id: program['educationLevelId'] })
  //             .then(educationLevel => {
  //               programDetailsList.push({
  //                 id: programDetailItem['id'],
  //                 name: educationLevel['education'] + '-' + program['name'] + '-' + programDetailItem['name']
  //               });
  //             });
  //         });
  //     }).then(() => {
  //       return programDetailsList;
  //     });
  //   });
  // }

  getClassesByProgramDetail(id) {

    let classModel = new ClassesModel();

    return classModel.findAllByConditions(['id', 'name'], { programDetailId: id });
  }

  // deleteWithAssociatedRecords(id) {
  //   return new MeritListModel().deleteByConditions({ programDetailId: id }).then(() => {

  //     return new RoadMapModel().deleteByConditionsWithAssociatedRecords({ programDetailId: id }).then(() => {

  //       return new AcademicCalendarProgramDetailModel().deleteByConditionsWithAssociatedRecords({ programDetailId: id }).then(() => {

  //         return new CampusProgramDetailModel().deleteByConditions({ programDetailId: id }).then(() => {

  //           return new CPDAccountModel().deleteByConditions({ programDetailId: id }).then(() => {

  //             return new CRProgramDetailModel().deleteByConditions({ programDetailId: id }).then(() => {

  //               return new FeeStructureModel().deleteByConditionsWithAssociatedRecords({ programDetailId: id }).then(() => {

  //                 return new CampusTimeTableModel().deleteByConditionsWithAssociatedRecords({ programDetailId: id }).then(() => {

  //                   return new TimeTableModel().deleteByConditionsWithAssociatedRecords({ programDetailId: id }).then(() => {

  //                     return new ClassesModel().deleteByConditionsWithAssociatedRecords({ programDetailId: id }).then(() => {

  //                       return this.delete(id);

  //                     })

  //                   })


  //                 })

  //               })

  //             })

  //           })

  //         })

  //       })

  //     })

  //   })
  // }
  // deleteByConditionsWithAssociatedRecords(condition) {
  //   return this.findAllByConditions(['id'], condition).then(programDetails => {

  //     let meritListModel = new MeritListModel();
  //     let roadMapModel = new RoadMapModel();
  //     let acpdModel = new AcademicCalendarProgramDetailModel();
  //     let cpdModel = new CampusProgramDetailModel();
  //     let cpdAcountModel = new CPDAccountModel();
  //     let crpdModel = new CRProgramDetailModel();
  //     let fsModel = new FeeStructureModel();
  //     let cttModel = new CampusTimeTableModel();
  //     let ttModel = new TimeTableModel();
  //     let classesModel = new ClassesModel();
  //     return Promise.each(programDetails, programDetail => {

  //       return meritListModel.deleteByConditions({ programDetailId: programDetail['id'] }).then(() => {

  //         return roadMapModel.deleteByConditionsWithAssociatedRecords({ programDetailId: programDetail['id'] }).then(() => {

  //           return acpdModel.deleteByConditionsWithAssociatedRecords({ programDetailId: programDetail['id'] }).then(() => {

  //             return cpdModel.deleteByConditions({ programDetailId: programDetail['id'] }).then(() => {

  //               return cpdAcountModel.deleteByConditions({ programDetailId: programDetail['id'] }).then(() => {

  //                 return crpdModel.deleteByConditions({ programDetailId: programDetail['id'] }).then(() => {

  //                   return fsModel.deleteByConditionsWithAssociatedRecords({ programDetailId: programDetail['id'] }).then(() => {

  //                     return cttModel.deleteByConditionsWithAssociatedRecords({ programDetailId: programDetail['id'] }).then(() => {

  //                       return ttModel.deleteByConditionsWithAssociatedRecords({ programDetailId: programDetail['id'] }).then(() => {

  //                         return classesModel.deleteByConditionsWithAssociatedRecords({ programDetailId: programDetail['id'] }).then(() => {

  //                           return this.delete(programDetail['id']);

  //                         })

  //                       })


  //                     })

  //                   })

  //                 })

  //               })

  //             })

  //           })

  //         })

  //       })

  //     })
  //   })

  // }

  /**
   * Check for associated records and then delete or returns associated records
   * 
   * @param id 
   */
  deleteOne(id) {

    return this.checkForChildren(id).then(res => {

      if (res['children'].length > 0) {

        return res;

      } else {

        return this.delete(id);
      }
    })

  }

  /**
   * Check if children exists
   * 
   * @param id 
   */
  checkForChildren(id) {
    let retResult = {};

    let includeObj = [
      { model: MeritList, as: 'meritLists', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: RoadMap, as: 'roadMaps', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: AcademicCalendarProgramDetail, as: 'acProgramDetails', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: CampusProgramDetail, as: 'campusProgramDetails', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: CPDAccount, as: 'cpdAccounts', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: CRProgramDetail, as: 'crProgramDetails', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: FeeStructure, as: 'feeStructures', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: CampusTimeTable, as: 'campusTimeTables', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: TimeTable, as: 'timeTables', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: Classes, as: 'classes', attributes: ['id'], where: BaseModel.cb(), required: false }

    ];

    return super.find(id, ['id', ['name', 'title']], includeObj).then(res => {

      retResult['id'] = res['dataValues']['id'];
      retResult['label'] = 'Program Detail';
      retResult['title'] = res['dataValues']['title'];
      retResult['children'] = [];

      if (res['meritLists'].length > 0) {
        retResult['children'].push({ title: 'Merit Lists' });
      }

      if (res['roadMaps'].length > 0) {
        retResult['children'].push({ title: 'Roadmaps' });
      }

      if (res['acProgramDetails'].length > 0) {
        retResult['children'].push({ title: 'Academic Calendar Program Details' });
      }

      if (res['campusProgramDetails'].length > 0) {
        retResult['children'].push({ title: 'Campus Program Details' });
      }

      if (res['cpdAccounts'].length > 0) {
        retResult['children'].push({ title: 'Campus Program Detail Accounts' });
      }

      if (res['crProgramDetails'].length > 0) {
        retResult['children'].push({ title: 'Campus Register Application Program Detais' });
      }

      if (res['feeStructures'].length > 0) {
        retResult['children'].push({ title: 'Fee Structures' });
      }

      if (res['campusTimeTables'].length > 0) {
        retResult['children'].push({ title: 'Campus Time Tables' });
      }

      if (res['timeTables'].length > 0) {
        retResult['children'].push({ title: 'Time Tables' });
      }

      if (res['classes'].length > 0) {
        retResult['children'].push({ title: 'Classes' });
      }

      return retResult;
    })
  }

}
