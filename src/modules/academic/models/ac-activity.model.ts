import { BaseModel } from '../../base';
import { ACActivity, AcademicCalendar, Activity } from '..';

export class ACActivityModel extends BaseModel {
  constructor() {
    super(ACActivity);
  }

  findAttributesList() {
    return this.sequelizeModel.findAll({
      attributes: ['id', 'startDate', 'endDate'],
      include: [
        {
          model: AcademicCalendar,
          as: 'academicCalendar',
          attributes: ['id', 'title']
        },
        {
          model: Activity,
          as: 'activity',
          attributes: ['id', 'name'],
        }
      ]
    });
  }
  findByActivity(_activityId) {
    return this.sequelizeModel.findAll({
      where: { activityId: _activityId },
      attributes: ['id'],
      include: [
        {
          model: AcademicCalendar,
          as: 'academicCalendar',
          attributes: ['id', 'title']
        },
      ]
    }).then(result => {
      let academicCalendar = [];
      result.forEach(element => {

        academicCalendar.push(element['academicCalendar'])
      });
      return academicCalendar;

    });
  }

  findAttributes(id) {

    return this.sequelizeModel.find({
      where: { id: id },
      attributes: ['id', 'startDate', 'endDate'],
      include: [
        {
          model: AcademicCalendar,
          as: 'academicCalendar',
          attributes: ['id', 'title']
        },
        {
          model: Activity,
          as: 'activity',
          attributes: ['id', 'name'],

        },

      ]
    });
  }
}
