import { Table, Column, Model, ForeignKey, HasMany, BelongsTo, HasOne } from 'sequelize-typescript';
import { AcademicCalendar, ProgramDetail, CACTerm } from '../..';
import { CampusAcademicCalendar } from '../../../institute';

@Table({ timestamps: true })
export class AcademicCalendarProgramDetail extends Model<AcademicCalendarProgramDetail> {
  @ForeignKey(() => AcademicCalendar)
  @Column
  academicCalendarId: number;

  @ForeignKey(() => ProgramDetail)
  @Column
  programDetailId: number;

  @Column startDate: Date;

  @Column endDate: Date;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * BelongsTo relationships
   */
  @BelongsTo(() => ProgramDetail, { foreignKey: 'programDetailId' })
  programDetail: ProgramDetail;

  @BelongsTo(() => AcademicCalendar, { foreignKey: 'academicCalendarId' })
  academicCalendar: AcademicCalendar;

  /**
   * HasMany Relationships
   */
  @HasMany(() => CACTerm, { foreignKey: 'academicCalendarProgramDetailId' })
  cacTerms: CACTerm[]
  @HasMany(() => CampusAcademicCalendar, { foreignKey: 'academicCalendarProgramDetailId' })
  campusAcademicCalendars: CampusAcademicCalendar[]
}
