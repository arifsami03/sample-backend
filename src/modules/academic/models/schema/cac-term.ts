import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { AcademicCalendarProgramDetail } from '../..';

@Table({ timestamps: true })
export class CACTerm extends Model<CACTerm> {
  @ForeignKey(() => AcademicCalendarProgramDetail)
  @Column
  academicCalendarProgramDetailId: number;

  @Column startDate: Date;

  @Column endDate: Date;

  @Column title: String;

  @Column abbreviation: String;

  @Column status: Boolean;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * Belongs To Relationships
   */
  @BelongsTo(() => AcademicCalendarProgramDetail, { foreignKey: 'academicCalendarProgramDetailId' })
  academicCalendarProgramDetail: AcademicCalendarProgramDetail;
}
