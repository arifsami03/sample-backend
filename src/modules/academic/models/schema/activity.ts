import { Table, Column, Model} from 'sequelize-typescript';

@Table({ timestamps: true })
export class Activity extends Model<Activity> {

  @Column title: String;

  @Column startDate: Date;

  @Column endDate: Date;

  @Column deleted: boolean;
  
  @Column createdBy: number;

  @Column updatedBy: number;


}
