import { Table, Column, Model, HasMany } from 'sequelize-typescript';
import { RoadMapPreRequisiteCourse } from '../..';
import { RoadMapCourse } from './roadmap-course';
import { TTDetail, CTTDetail } from '../../../time-table';
import { ECSubject } from '../../../admission';

@Table({ timestamps: true })
export class Course extends Model<Course> {
  @Column title: string;
  @Column abbreviation: string;
  @Column description: string;
  @Column deleted: boolean;
  @Column createdBy: number;
  @Column updatedBy: number;

  @HasMany(() => RoadMapCourse, { foreignKey: 'courseId' })
  roadMapCourses: RoadMapCourse[];

  @HasMany(() => RoadMapPreRequisiteCourse, { constraints: false, foreignKey: 'courseId' })
  roadMapPreRequisiteCourses: RoadMapPreRequisiteCourse[];

  @HasMany(() => TTDetail, { foreignKey: 'courseId' })
  ttDetails: TTDetail[];

  @HasMany(() => CTTDetail, { constraints: false, foreignKey: 'courseId' })
  cttDetails: CTTDetail[];

  @HasMany(() => ECSubject, { foreignKey: 'courseId' })
  ecSubjects: ECSubject[];
}
