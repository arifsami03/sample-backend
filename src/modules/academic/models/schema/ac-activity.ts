import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { AcademicCalendar, Activity } from '../..';

@Table({ timestamps: true })
export class ACActivity extends Model<ACActivity> {
  @ForeignKey(() => AcademicCalendar)
  @Column
  academicCalendarId: number;

  @ForeignKey(() => Activity)
  @Column
  activityId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * BelongsTo relationships
   */
  @BelongsTo(() => Activity, { foreignKey: 'activityId' })
  programDetail: Activity;

  @BelongsTo(() => AcademicCalendar, { foreignKey: 'academicCalendarId' })
  academicCalendar: AcademicCalendar;

}
