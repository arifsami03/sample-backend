import { Table, Column, Model, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { RoadMap, Classes, Course, RoadMapPreRequisiteCourse } from '../..';
import { Configuration } from '../../../setting';

@Table({ timestamps: true })
export class RoadMapCourse extends Model<RoadMapCourse> {
  @ForeignKey(() => RoadMap)
  @Column
  roadMapId: number;

  @ForeignKey(() => Course)
  @Column
  courseId: number;

  @ForeignKey(() => Classes)
  @Column
  classId: number;

  @Column courseCode: string;

  @ForeignKey(() => Configuration)
  @Column natureOfCourseId: number;

  @Column courseType: string;

  @Column courseOrder: number;

  @Column creditHours: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => RoadMap, { foreignKey: 'roadMapId' })
  roadMap: RoadMap;
  @BelongsTo(() => Classes, { foreignKey: 'classId' })
  class: Classes;
  @BelongsTo(() => Course, { foreignKey: 'courseId' })
  course: Course;
  @BelongsTo(() => Configuration, { foreignKey: 'natureOfCourseId' })
  natureOfCourse: Configuration;
  /**
   * Has Many Relationships
   */

  @HasMany(() => RoadMapPreRequisiteCourse, {
    constraints: false,
    foreignKey: 'roadMapCourseId'
  })
  preReqCoursesList: RoadMapPreRequisiteCourse[];

  @HasMany(() => RoadMapCourse, {
    constraints: false,
    foreignKey: 'classId'
  })
  roadMapCourses: RoadMapCourse[];
}
