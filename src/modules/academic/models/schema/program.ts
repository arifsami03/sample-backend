import { Table, Column, Model, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { EducationLevel } from './../../../institute/';
import { EligibilityCriteria, ECPreReqProgram } from '../../../admission';
import { ProgramDetail } from './program-detail';
import { CampusProgram, CRProgram } from '../../../campus';

@Table({ timestamps: true })
export class Program extends Model<Program> {
  @Column name: string;

  @Column abbreviation: string;

  @ForeignKey(() => EducationLevel)
  @Column educationLevelId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => EducationLevel, {
    foreignKey: 'educationLevelId'
  })
  educationLevel: EducationLevel;

  @HasMany(() => EligibilityCriteria, { foreignKey: 'programId' })
  eligibilityCriteria: EligibilityCriteria[];

  @HasMany(() => ProgramDetail, { foreignKey: 'programId' })
  programDetails: ProgramDetail[];

  @HasMany(() => ECPreReqProgram, { foreignKey: 'programId' })
  ecPreReqPrograms: ECPreReqProgram[];

  @HasMany(() => CampusProgram, { foreignKey: 'programId' })
  campusPrograms: CampusProgram[];

  @HasMany(() => CRProgram, { foreignKey: 'programId' })
  crPrograms: CRProgram[];

}
