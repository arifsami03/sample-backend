import { Table, Column, Model, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { RoadMapCourse, ProgramDetail } from '../..';
import { TimeTable, CampusTimeTable } from '../../../time-table';

@Table({ timestamps: true })
export class Classes extends Model<Classes> {
  @Column name: string;

  @ForeignKey(() => ProgramDetail)
  @Column
  programDetailId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => ProgramDetail, {
    foreignKey: 'programDetailId'
  })
  programDetail: ProgramDetail;

  /**
   * Has Many
   */
  @HasMany(() => RoadMapCourse, { foreignKey: 'classId' })
  roadMapCourses: RoadMapCourse[];

  @HasMany(() => TimeTable, { foreignKey: 'classId' })
  timeTables: TimeTable[];

  @HasMany(() => CampusTimeTable, { foreignKey: 'classId' })
  campusTimeTables: CampusTimeTable[];
}
