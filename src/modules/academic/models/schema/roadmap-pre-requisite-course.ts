import { Table, Column, Model, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { RoadMap, Course, RoadMapCourse } from '../..';

@Table({ timestamps: true })
export class RoadMapPreRequisiteCourse extends Model<RoadMapPreRequisiteCourse> {
  @ForeignKey(() => RoadMapCourse)
  @Column
  roadMapCourseId: number;

  @ForeignKey(() => Course)
  @Column
  courseId: number;

  @Column deleted: boolean;

  @BelongsTo(() => Course, { foreignKey: 'courseId' })
  course: Course;
}
