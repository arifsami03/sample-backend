import { Table, Column, Model, HasMany, BelongsToMany } from 'sequelize-typescript';
import { AcademicCalendarProgramDetail, Activity ,ACActivity} from '../..';

@Table({ timestamps: true })
export class AcademicCalendar extends Model<AcademicCalendar> {
  @Column title: string;
  @Column abbreviation: string;
  @Column description: string;
  
  @Column deleted: boolean;

  
  @Column createdBy: number;
  @Column updatedBy: number;

  /**
   * Has Many Relationships
   */
  @HasMany(() => AcademicCalendarProgramDetail, { foreignKey: 'academicCalendarId' })
  academicCalendarProgramDetails: AcademicCalendarProgramDetail[];

  @HasMany(() => ACActivity, { foreignKey: 'academicCalendarId' })
  activitiesList: ACActivity[];

  @HasMany(() => Activity, { foreignKey: 'academicCalendarId' })
  activities: Activity[];
}
