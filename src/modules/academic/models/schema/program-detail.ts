import { Table, Column, Model, ForeignKey, BelongsTo, BelongsToMany, HasMany } from 'sequelize-typescript';
import { Program, Classes } from '../..';
import { MeritList } from '../../../admission';
import { RoadMap } from './roadmap';
import { AcademicCalendarProgramDetail } from './academic-calendar-program-detail';
import { CampusProgramDetail, CPDAccount, CRProgramDetail } from '../../../campus';
import { FeeStructure } from '../../../fee-management';
import { CampusTimeTable, TimeTable } from '../../../time-table';

@Table({ timestamps: true })
export class ProgramDetail extends Model<ProgramDetail> {
  @Column name: string;

  @Column abbreviation: string;

  @ForeignKey(() => Program)
  @Column
  programId: number;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => Program, {
    foreignKey: 'programId'
  })
  program: Program;

  @HasMany(() => MeritList, { foreignKey: 'programDetailId' })
  meritLists: MeritList[];

  @HasMany(() => RoadMap, { foreignKey: 'programDetailId' })
  roadMaps: RoadMap[];

  @HasMany(() => AcademicCalendarProgramDetail, { foreignKey: 'programDetailId' })
  acProgramDetails: AcademicCalendarProgramDetail[];

  @HasMany(() => CampusProgramDetail, { foreignKey: 'programDetailId' })
  campusProgramDetails: CampusProgramDetail[];

  @HasMany(() => CPDAccount, { foreignKey: 'programDetailId' })
  cpdAccounts: CPDAccount[];

  @HasMany(() => CRProgramDetail, { foreignKey: 'programDetailId' })
  crProgramDetails: CRProgramDetail[];

  @HasMany(() => FeeStructure, { foreignKey: 'programDetailId' })
  feeStructures: FeeStructure[];

  @HasMany(() => CampusTimeTable, { foreignKey: 'programDetailId' })
  campusTimeTables: CampusTimeTable[];

  @HasMany(() => TimeTable, { foreignKey: 'programDetailId' })
  timeTables: TimeTable[];

  @HasMany(() => Classes, { foreignKey: 'programDetailId' })
  classes: Classes[];

}
