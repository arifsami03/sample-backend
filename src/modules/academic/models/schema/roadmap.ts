import { Table, Column, Model, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { ProgramDetail, RoadMapCourse } from '../..';

@Table({ timestamps: true })
export class RoadMap extends Model<RoadMap> {
  @Column title: string;

  @ForeignKey(() => ProgramDetail)
  @Column
  programDetailId: number;

  @Column academicYear: number;

  @Column category: string;

  @Column deleted: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * Belongs to Relationships
   */
  @BelongsTo(() => ProgramDetail, { foreignKey: 'programDetailId' })
  programDetail: ProgramDetail;


  /**
   * Has Many Relationships
   */
  @HasMany(() => RoadMapCourse, { foreignKey: 'roadMapId' })
  roadMapCourses: RoadMapCourse[]
}
