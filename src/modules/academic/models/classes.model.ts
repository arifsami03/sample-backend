import { BaseModel } from '../../base';
import { Promise } from 'bluebird';
import { Classes, Program, RoadMapCourseModel } from '..';
import { ProgramDetailModel, ProgramModel } from '..'
import { EducationLevelModel, InstituteTypeModel, EducationLevel, InstituteType } from '../../institute'
import { Sequelize } from 'sequelize-typescript';
import * as _ from 'lodash';
import { TimeTableModel, CampusTimeTableModel, TimeTable, CampusTimeTable } from '../../time-table';
import { RoadMapCourse } from './schema/roadmap-course';

export class ClassesModel extends BaseModel {
  constructor() {
    super(Classes);
  }


  // No longer in use as we return classes with include statement direct from controller. Arif
  getClasses() {

    var classesList = [];
    return this.findAll(['id', 'name', 'programDetailId']).then(classes => {

      return Promise.each(classes, (classItem) => {

        return new ProgramDetailModel().findByCondition(['name', 'programId'], { id: classItem['programDetailId'] }).then(programDetail => {
          return new ProgramModel().findByCondition(['name', 'educationLevelId'], { id: programDetail['programId'] }).then(program => {
            return new EducationLevelModel().findByCondition(['education', 'instituteTypeId'], { id: program['educationLevelId'] }).then(educationLevel => {
              return new InstituteTypeModel().findByCondition(['name'], { id: educationLevel['instituteTypeId'] }).then(instituteType => {
                classesList.push({
                  id: classItem['id'],
                  name: classItem['name'],
                  programDetail: programDetail['name'],
                  program: program['name'],
                  educationLevel: educationLevel['education'],
                  instituteType: instituteType['name'],
                })
              })
            })
          })
        })
      }).then(() => {
        return classesList;
      })

    })
  }

  list() {
    let attributes = [[Sequelize.fn('DISTINCT', Sequelize.col('programDetailId')), 'programDetailId']];
    return this.findAll(attributes)
      .then(result => {
        let programDetailIds = result.map(function (item) {
          return item['programDetailId'];
        });
        return new ProgramDetailModel().findAllByConditions(['id', 'name'], { id: programDetailIds }, [
          {
            model: Classes,
            as: 'classes',
            attributes: ['id', 'name']
          },
          {
            model: Program,
            as: 'program',
            attributes: ['id', 'name'],
            include: [{
              model: EducationLevel,
              as: 'educationLevel',
              attributes: ['id', 'education'],
              include: [{
                model: InstituteType,
                as: 'instituteType',
                attributes: ['id', 'name']
              }]
            }]
          }])

      })
  }

  findWithProgramDetail(programDetailId: number) {
    return new ProgramDetailModel().findByCondition(['id', 'name'], { id: programDetailId }, [
      {
        model: Classes,
        as: 'classes',
        attributes: ['id', 'name']
      },
      {
        model: Program,
        as: 'program',
        attributes: ['id', 'name'],
        include: [{
          model: EducationLevel,
          as: 'educationLevel',
          attributes: ['id', 'education'],
          include: [{
            model: InstituteType,
            as: 'instituteType',
            attributes: ['id', 'name']
          }]
        }]
      }])

  }

  deleteWithAssociatedRecords(id: number) {
    return new RoadMapCourseModel().deleteByConditionsWithAssociatedRecords({ classId: id }).then(() => {
      return this.delete(id);
    })
  }
  deleteByConditionsWithAssociatedRecords(condition) {

    return this.findAllByConditions(['id'], condition).then(roadMapClasses => {

      let roadMapCourseModel = new RoadMapCourseModel();

      return Promise.each(roadMapClasses, roadMapClass => {

        return roadMapCourseModel.deleteByConditionsWithAssociatedRecords({ classId: roadMapClass['id'] }).then(() => {
          return this.delete(roadMapClass['id']);
        })


      })
    })

  }


  updateByProgramDetail(id: number, programDetailId: number, item) {
    return this.sequelizeModel.update(item, { where: { id: id, programDetailId: programDetailId } })
  }

  /**
   * Check for associated records and then delete or returns associated records
   * 
   * @param id 
   */
  deleteOne(id) {

    return this.checkForChildren(id).then(res => {

      if (res['children'].length > 0) {

        return res;

      } else {

        return this.delete(id);
      }
    })

  }

  /**
   * Check if children exists
   * 
   * @param id 
   */
  checkForChildren(id) {
    let retResult = {};

    let includeObj = [
      { model: RoadMapCourse, as: 'roadMapCourses', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: TimeTable, as: 'timeTables', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: CampusTimeTable, as: 'campusTimeTables', attributes: ['id'], where: BaseModel.cb(), required: false }
    ];

    return super.find(id, ['id', ['name', 'title']], includeObj).then(res => {

      retResult['id'] = res['dataValues']['id'];
      retResult['label'] = 'Class';
      retResult['title'] = res['dataValues']['title'];
      retResult['children'] = [];

      if (res['roadMapCourses'].length > 0) {
        retResult['children'].push({ title: 'Roadmap Courses' });
      }

      if (res['timeTables'].length > 0) {
        retResult['children'].push({ title: 'Time Tables' });
      }

      if (res['campusTimeTables'].length > 0) {
        retResult['children'].push({ title: 'Campus Time Tables' });
      }

      return retResult;
    })
  }
}
