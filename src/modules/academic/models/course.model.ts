import { BaseModel } from '../../base';
import { Course, RoadMapPreRequisiteCourseModel, RoadMapCourseModel } from '..';
import { Sequelize } from 'sequelize-typescript';
import { ECSubjectModel, ECSubject } from '../../admission';
import { CTTDetailModel, TTDetailModel, TTDetail, CTTDetail } from '../../time-table';
import { RoadMapCourse } from './schema/roadmap-course';
import { RoadMapPreRequisiteCourse } from './schema/roadmap-pre-requisite-course';

export class CourseModel extends BaseModel {
  constructor() {
    super(Course);
  }

  findPreReqCourses(postData) {
    return this.findAllByConditions(['id', 'title', 'abbreviation'], { id: { [Sequelize.Op.notIn]: postData } });
  }

  findRoadMapCourses(postData) {
    return this.findAllByConditions(['id', 'title', 'abbreviation'], { id: { [Sequelize.Op.notIn]: postData } });
  }

  getSubjectsNotSelected(selectedSubjects) {
    return this.findAllByConditions(['id', 'title'], { id: { [Sequelize.Op.notIn]: selectedSubjects } });
  }

  // deleteWithAssociatedRecords(id: number) {

  //   return new RoadMapPreRequisiteCourseModel().deleteByConditions({ courseId: id }).then(() => {
  //     return new RoadMapCourseModel().deleteByConditions({ courseId: id }).then(() => {
  //       return new ECSubjectModel().deleteByConditions({ courseId: id }).then(() => {
  //         return new CTTDetailModel().deleteByConditions({ courseId: id }).then(() => {
  //           return new TTDetailModel().deleteByConditions({ courseId: id }).then(() => {
  //             return this.delete(id);
  //           })
  //         })
  //       })
  //     })
  //   })

  // }

  /**
   * Check for associated records and then delete or returns associated records
   * 
   * @param id 
   */
  deleteOne(id) {

    return this.checkForChildren(id).then(res => {

      if (res['children'].length > 0) {

        return res;

      } else {

        return this.delete(id);
      }
    })

  }

  /**
   * Check if children exists
   * 
   * @param id 
   */
  checkForChildren(id) {
    let retResult = {};

    let includeObj = [
      { model: RoadMapCourse, as: 'roadMapCourses', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: RoadMapPreRequisiteCourse, as: 'roadMapPreRequisiteCourses', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: TTDetail, as: 'ttDetails', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: CTTDetail, as: 'cttDetails', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: ECSubject, as: 'ecSubjects', attributes: ['id'], where: BaseModel.cb(), required: false },
    ];

    return super.find(id, ['id', 'title'], includeObj).then(res => {

      retResult['id'] = res['dataValues']['id'];
      retResult['label'] = 'Course';
      retResult['title'] = res['dataValues']['title'];
      retResult['children'] = [];

      if (res['roadMapCourses'].length > 0) {
        retResult['children'].push({ title: 'Roadmap Courses' });
      }

      if (res['roadMapPreRequisiteCourses'].length > 0) {
        retResult['children'].push({ title: 'Roadmap Pre-Requisite Courses' });
      }

      if (res['ttDetails'].length > 0) {
        retResult['children'].push({ title: 'Time Table Details' });
      }

      if (res['cttDetails'].length > 0) {
        retResult['children'].push({ title: 'Campus Time Table Details' });
      }

      if (res['ecSubjects'].length > 0) {
        retResult['children'].push({ title: 'Eligibility Criteria Subjects' });
      }

      return retResult;
    })
  }
}
