import { BaseModel } from '../../base';
import { RoadMapPreRequisiteCourse } from '..';

export class RoadMapPreRequisiteCourseModel extends BaseModel {
  constructor() {
    super(RoadMapPreRequisiteCourse);
  }
}
