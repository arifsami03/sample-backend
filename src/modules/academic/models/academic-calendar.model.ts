import { AcademicCalendarProgramDetailModel } from './academic-calendar-program-detail.model';
import { Promise } from 'bluebird';
import { BaseModel } from '../../base';
import { AcademicCalendar, AcademicCalendarProgramDetail, ProgramDetail, Program } from '..';
import { ACActivityModel } from './ac-activity.model';
import { ACActivity } from './schema/ac-activity';
import { CACTerm } from './schema/cac-term';
import { CampusAcademicCalendar } from '../../institute';
export class AcademicCalendarModel extends BaseModel {
  constructor() {
    super(AcademicCalendar);
  }


  /**
   * Creating program details against academic calendar
   * @param item
   */
  createWithProgramDetails(item) {
    let academisCalendarItem = {
      title: item['title'],
      abbreviation: item['abbreviation'],
      description: item['description']
    };

    return super.create(academisCalendarItem).then(acResult => {
      if (acResult) {
        let acProgramDetailModel = new AcademicCalendarProgramDetailModel();
        let acActivityModel = new ACActivityModel();
        // adding program detail in academic calendar pogram detail
        return Promise.each(item['programDetailId'], pdId => {
          let acpdItem = {
            academicCalendarId: acResult['id'],
            programDetailId: pdId
          };

          return acProgramDetailModel.create(acpdItem);
        }).then(() => {
          return Promise.each(item['activitiesList'], activity => {

            if (activity['activityId']) {
              let acActivityItem = {
                academicCalendarId: acResult['id'],
                activityId: activity['activityId']
              };
              return acActivityModel.create(acActivityItem);
            }

          }).then(() => {
            return acResult;

          })
        });
      }
    });
  }

  /**
   * Update program details against academic calendar
   * @param _id academic calendar id
   * @param item
   */
  updateWithProgramDetails(_id, item) {
    let academisCalendarItem = {
      title: item['title'],
      abbreviation: item['abbreviation'],
      description: item['description']
    };

    return super.update(_id, academisCalendarItem).then(() => {

      return this.find(_id, null, [
        {
         
          model: AcademicCalendarProgramDetail,
          as: 'academicCalendarProgramDetails',
          attributes: ['programDetailId'],
          where: BaseModel.cb(),
          required: false,
        },
        {
          model: ACActivity,
          as: 'activitiesList',
          attributes: ['activityId'],
          where: BaseModel.cb(),
          required: false,

        }
      ]).then(_acResult => {
        let existingProgramDetailIds = _acResult['academicCalendarProgramDetails'];
        let inComingProgramDetailIds: number[] = item['programDetailId'];

        // Matching and adding new one (programDetailId)
        inComingProgramDetailIds.forEach(InComingPdi => {
          let exist: boolean = false;
          // Matching
          existingProgramDetailIds.forEach(existingPdi => {
            if (InComingPdi === existingPdi['dataValues']['programDetailId']) {
              exist = true;
            }
          });
          if (!exist) {
            // Adding
            let acpdItem = {
              academicCalendarId: _acResult['id'],
              programDetailId: InComingPdi
            };
            return new AcademicCalendarProgramDetailModel().create(acpdItem).then(acpd_result => {
              return acpd_result;
            });
          }
        });

        // Matching and deleteing previous one  (programDetailId)
        existingProgramDetailIds.forEach(existingPdi => {
          let exist: boolean = false;
          // Matching
          inComingProgramDetailIds.forEach(InComingPdi => {
            if (InComingPdi === existingPdi['dataValues']['programDetailId']) {
              exist = true;
            }
          });
          if (!exist) {

            return new AcademicCalendarProgramDetailModel().deleteByConditionsWithAssociatedRecords({ programDetailId: existingPdi['dataValues']['programDetailId'], academicCalendarId: _acResult['id'] }).then(acpd_result => {
              return acpd_result;
            });
          }
        });

        /**
         * This is for Activity
         */
        let existingActivityIds = _acResult['activitiesList'];
        let inComingActivityIds: number[] = item['activitiesList'];

        // Matching and adding new one (activityIdId)
        inComingActivityIds.forEach(InComingActivityId => {
          let exist: boolean = false;
          // Matching
          existingActivityIds.forEach(existingActivityId => {
            if (InComingActivityId['activityId'] === existingActivityId['dataValues']['activityId']) {
              exist = true;
            }
          });
          if (!exist) {
            // Adding
            let activityItem = {
              academicCalendarId: _acResult['id'],
              activityId: InComingActivityId['activityId']
            };
            return new ACActivityModel().create(activityItem).then(activity_result => {
              return activity_result;
            });
          }
        });

        // Matching and deleteing previous one  (programDetailId)
        existingActivityIds.forEach(existingActivityId => {
          let exist: boolean = false;
          // Matching
          inComingActivityIds.forEach(InComingActivityId => {
            if (InComingActivityId['activityId'] === existingActivityId['dataValues']['activityId']) {
              exist = true;
            }
          });
          if (!exist) {

            return new ACActivityModel().deleteByConditions({ activityId: existingActivityId['dataValues']['activityId'], academicCalendarId: _acResult['id'] }).then(activity_result => {
              return activity_result;
            });
          }
        });



        return _acResult;
      });
    });
  }

  /**
   * find data with program details
   * @param _id academic calendar id
   */
  public findWithAsscoiatedRecord(_id) {
    return this.find(_id, null, [
      {
        model: AcademicCalendarProgramDetail,
        as: 'academicCalendarProgramDetails',
        where: BaseModel.cb(),
        required: false,
        include: [
          {
            model: ProgramDetail,
            as: 'programDetail',
            attributes: ['id', 'name'],
            include: [
              {
                model: Program,
                as: 'program',
                attributes: ['id', 'name']
              }
            ]
          },

        ]
      },
      {
        model: ACActivity,
        as: 'activitiesList',
        where: BaseModel.cb(),
        required: false,
        attributes: ['id', 'activityId'],
      }
    ])

  }


  deleteWithAssociatedRecords(id) {

    let acpdModel = new AcademicCalendarProgramDetailModel();

    return acpdModel.deleteByConditions({ academicCalendarId: id }).then(() => {

      return this.delete(id);
    })


  }

  /**
   * Check for associated records and then delete or returns associated records
   * 
   * @param id 
   */
  deleteOne(id) {

    return this.checkForChildren(id).then(res => {

      if (res['children'].length > 0) {

        return res;

      } else {

        return this.deleteWithAssociatedRecords(id);
      }
    })

  }

  /**
  * Check if children exists
  * 
  * @param id 
  */
  checkForChildren(id) {
    let retResult = {};

    let includeObj = [
      {
        model: AcademicCalendarProgramDetail, as: 'academicCalendarProgramDetails', attributes: ['id'], where: BaseModel.cb(), required: false,
        include: [
          { model: CACTerm, as: 'cacTerms', attributes: ['id'], where: BaseModel.cb(), required: false, },
          { model: CampusAcademicCalendar, as: 'campusAcademicCalendars', attributes: ['id'], where: BaseModel.cb(), required: false, }
        ]
      },
    ];

    return super.find(id, ['id', 'title'], includeObj).then(res => {

      retResult['id'] = res['dataValues']['id'];
      retResult['label'] = 'Academic Calendar';
      retResult['title'] = res['dataValues']['title'];
      retResult['children'] = [];

      if (res['academicCalendarProgramDetails'].length > 0) {

        for (let index = 0; index < res['academicCalendarProgramDetails'].length; index++) {
          const item = res['academicCalendarProgramDetails'][index];
          if (item['cacTerms'].length > 0) {

            retResult['children'].push({ title: 'CAC Terms' });
            break;
          }
        }


        for (let index = 0; index < res['academicCalendarProgramDetails'].length; index++) {
          const item = res['academicCalendarProgramDetails'][index];
          if (item['campusAcademicCalendars'].length > 0) {

            retResult['children'].push({ title: 'Campus Academic Calendar' });
            break;
          }
        }
      }

      return retResult;
    })
  }
}
