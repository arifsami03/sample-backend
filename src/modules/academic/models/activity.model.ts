import { BaseModel } from '../../base';
import { Activity  } from '..';


export class ActivityModel extends BaseModel {
  constructor() {
    super(Activity);
  }
 
}
