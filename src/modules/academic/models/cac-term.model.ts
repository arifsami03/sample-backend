import * as _ from 'lodash';
import { Sequelize } from 'sequelize-typescript';

import { BaseModel } from '../../base';
import { AcademicCalendar, ProgramDetail, Program, CACTerm, AcademicCalendarProgramDetailModel } from '..';
import { CampusAcademicCalendarModel, CampusAcademicCalendar } from '../../institute';


export class CACTermModel extends BaseModel {
  constructor() {
    super(CACTerm);
  }
  list() {
    return new AcademicCalendarProgramDetailModel().findAll(
      ['id', 'academicCalendarId', 'programDetailId'],

      { startDate: { [Sequelize.Op.not]: null }, endDate: { [Sequelize.Op.not]: null } },

      [
        {
          model: AcademicCalendar,
          as: 'academicCalendar',
          attributes: ['id', 'title']
        },
        {
          model: CACTerm,
          as: 'cacTerms',
          attributes: ['id']
        },
        {
          model: CampusAcademicCalendar,
          as: 'campusAcademicCalendars',
          attributes: ['id']
        },
        {
          model: ProgramDetail,
          as: 'programDetail',
          attributes: ['id', 'name'],
          include: [
            {
              model: Program,
              as: 'program',
              attributes: ['id', 'name']
            }
          ]
        }
      ]

    )
  }

  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Terms';
    retResult['records'] = [];

    return this.findAllByConditions(['id', 'title'], condition).then((cacRes) => {

      _.each(cacRes, item => { retResult['records'].push(item['dataValues']) });

      return retResult;

    });
  }
}
