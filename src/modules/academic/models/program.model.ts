import { BaseModel } from '../../base';
import { Promise } from 'bluebird';
import { Program } from '..';
import { EducationLevel, InstituteType } from '../../institute';
import { ProgramDetailModel } from '..';
import { Sequelize } from 'sequelize-typescript';
import { EligibilityCriteriaModel, ECPreReqProgramModel, EligibilityCriteria, ECPreReqProgram } from '../../admission';
import { CampusProgramModel, CRProgramModel, CampusProgram, CRProgram } from '../../campus';
import { FeeStructureModel } from '../../fee-management';
import { ProgramDetail } from './schema/program-detail';

export class ProgramModel extends BaseModel {
  constructor() {
    super(Program);
  }

  /**
   * it will find all programs
   */

  index() {
    let includeObj = [
      {
        model: EducationLevel, as: 'educationLevel', attributes: ['id', 'education', 'instituteTypeId'], where: BaseModel.cb(), required: false,
        include: [{ model: InstituteType, as: 'instituteType', attributes: ['id', 'name'], where: BaseModel.cb(), required: false }]
      }
    ];

    return this.findAll(['id', 'name', 'educationLevelId'], null, includeObj);
  }

  find(id) {
    //this.openConnection();
    return this.findByCondition(['id', 'name', 'educationLevelId', 'abbreviation'], { id: id });
  }


  getProgramsList() {
    return this.index();
  }



  getAllPrograms(educationLevelId: number, selectedPrograms: number[]) {

    return this.findAllByConditions(['id', 'name'], { id: { [Sequelize.Op.notIn]: selectedPrograms }, educationLevelId: educationLevelId });
  }
  getProgramsWithProgramDetails(educationLevelId: number) {

    let campProgProDetResult = [];
    return this.findAllByConditions(['id', 'name'], { educationLevelId: educationLevelId }).then(res => {
      return Promise.each(res, (ProgramItem) => {

        return new ProgramDetailModel().findAllByConditions(['id', 'name'], { programId: ProgramItem['id'] }).then(_programDetails => {
          campProgProDetResult.push({
            programId: ProgramItem['id'],
            programName: ProgramItem['name'],
            programDetails: _programDetails
          });

        })

      })

    }).then(() => {
      return campProgProDetResult;
    })
  }

  getPreReqNotSelected(selectedPrograms) {
    return this.findAllByConditions(['id', 'name'], { id: { [Sequelize.Op.notIn]: selectedPrograms } });
  }

  // deleteWithAssociatedRecords(id) {
  //   return new EligibilityCriteriaModel().deleteByConditionsWithAssociatedRecords({ programId: id }).then(() => {

  //     return new ProgramDetailModel().deleteByConditionsWithAssociatedRecords({ programId: id }).then(() => {

  //       return new ECPreReqProgramModel().deleteByConditions({ programId: id }).then(() => {

  //         return new CampusProgramModel().deleteByConditionsWithAssociatedRecords({ programId: id }).then(() => {

  //           return new CRProgramModel().deleteByConditions({ programId: id }).then(() => {

  //             return super.delete(id);

  //           })

  //         })

  //       })

  //     })

  //   })
  // }

  // deleteByConditionsWithAssociatedRecords(condition) {



  //   return this.findAllByConditions(['id'], condition).then(programs => {

  //     let ecModel = new EligibilityCriteriaModel();
  //     let pdModel = new ProgramDetailModel();
  //     let ecPreReqProgramModel = new ECPreReqProgramModel();
  //     let cpModel = new CampusProgramModel();
  //     let crpModel = new CRProgramModel();

  //     return Promise.each(programs, program => {

  //       return ecModel.deleteByConditionsWithAssociatedRecords({ programId: program['id'] }).then(() => {

  //         return pdModel.deleteByConditionsWithAssociatedRecords({ programId: program['id'] }).then(() => {

  //           return ecPreReqProgramModel.deleteByConditions({ programId: program['id'] }).then(() => {

  //             return cpModel.deleteByConditionsWithAssociatedRecords({ programId: program['id'] }).then(() => {

  //               return crpModel.deleteByConditions({ programId: program['id'] }).then(() => {

  //                 return this.delete(program['id']);

  //               })

  //             })

  //           })

  //         })

  //       })

  //     })

  //   })

  // }


  /**
   * Check for associated records and then delete or returns associated records
   * 
   * @param id 
   */
  deleteOne(id) {

    return this.checkForChildren(id).then(res => {
      if (res['children'].length > 0) {

        return res;

      } else {

        return this.delete(id);

      }
    });

  }

  /**
   * Check if children exists
   * 
   * @param id 
   */
  checkForChildren(id) {
    let retResult = {};

    let includeObj = [
      { model: EligibilityCriteria, as: 'eligibilityCriteria', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: ProgramDetail, as: 'programDetails', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: ECPreReqProgram, as: 'ecPreReqPrograms', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: CampusProgram, as: 'campusPrograms', attributes: ['id'], where: BaseModel.cb(), required: false },
      { model: CRProgram, as: 'crPrograms', attributes: ['id'], where: BaseModel.cb(), required: false }
    ];

    return super.find(id, ['id', ['name', 'title']], includeObj).then(res => {

      retResult['id'] = res['dataValues']['id'];
      retResult['label'] = 'Program';
      retResult['title'] = res['dataValues']['title'];
      retResult['children'] = [];

      if (res['eligibilityCriteria'].length > 0) {
        retResult['children'].push({ title: 'Eligibility Criteria' });
      }

      if (res['programDetails'].length > 0) {
        retResult['children'].push({ title: 'Program Details' });
      }

      if (res['ecPreReqPrograms'].length > 0) {
        retResult['children'].push({ title: 'Eligibility Criteria Pre-Requisite Programs' });
      }

      if (res['campusPrograms'].length > 0) {
        retResult['children'].push({ title: 'Campus Programs' });
      }

      if (res['crPrograms'].length > 0) {
        retResult['children'].push({ title: 'Campus Register Application Programs' });
      }

      return retResult;
    })
  }
}
