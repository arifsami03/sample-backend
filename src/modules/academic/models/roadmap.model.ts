import { BaseModel } from '../../base';
import { RoadMap, ProgramDetail, AcademicCalendar, RoadMapCourse, Program } from '..';
import { EducationLevel } from '../../institute';
import { Promise } from 'bluebird';
import { RoadMapCourseModel } from './roadmap-course.model';
import * as _ from 'lodash';

export class RoadMapModel extends BaseModel {
  constructor() {
    super(RoadMap);
  }

  private rmCModel = new RoadMapCourseModel();

  /**
   * Creating program details against academic calendar
   * @param item
   */

  index() {
    let attributes = ['id', 'title', 'category', 'academicYear'];
    let includeObj = [
      { model: ProgramDetail, as: 'programDetail', attributes: ['id', 'name'], where: BaseModel.cb(), required: false },
      { model: RoadMapCourse, as: 'roadMapCourses', attributes: ['id', 'creditHours'], where: BaseModel.cb(), required: false }
    ];
    return this.findAll(attributes, null, includeObj);
  }

  find(_id) {
    this.openConnection();
    return this.sequelizeModel.find({
      where: { id: _id },
      include: [
        {
          model: ProgramDetail,
          as: 'programDetail',
          attributes: ['id', 'name'],
          include: [{
            model: Program,
            as: 'program',
            attributes: ['id', 'name', 'educationLevelId'],
            include: [
              {
                model: EducationLevel,
                as: 'educationLevel',
                attributes: ['id', 'education', 'instituteTypeId'],
              }
            ]
          }]
        },

      ]
    });
  }
  deleteWithAssociatedRecords(id) {
    return new RoadMapCourseModel().findAllByConditions(['id'], { roadMapId: id }).then(roadMapCourses => {
      return Promise.each(roadMapCourses, roadMapCourse => {
        return new RoadMapCourseModel().deleteWithAssociatedRecords(roadMapCourse['id'])
      }).then(() => {

        return this.delete(id);

      })
    })
  }

  deleteByConditionsWithAssociatedRecords(condition) {


    return this.findAllByConditions(['id'], condition).then(roadMaps => {
      let roadMapCourseModel = new RoadMapCourseModel();

      return Promise.each(roadMaps, roadMap => {

        return roadMapCourseModel.findAllByConditions(['id'], { roadMapId: roadMap['id'] }).then(roadMapCourses => {
          return Promise.each(roadMapCourses, roadMapCourse => {
            return roadMapCourseModel.deleteWithAssociatedRecords(roadMapCourse['id'])
          }).then(() => {

            return this.delete(roadMap['id']);

          })
        })

      })
    })

  }

  // findRelatedDataWithCondition(condition) {

  //   let retResult = {};
  //   retResult['label'] = 'Roadmap';
  //   retResult['records'] = [];

  //   return this.findAllByConditions(['id', 'title'], condition).then((pdRes) => {


  //     _.each(pdRes, item => { retResult['records'].push(item['dataValues']) });

  //     return retResult;

  //   });
  // }

}
