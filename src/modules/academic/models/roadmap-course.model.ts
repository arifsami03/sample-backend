import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import { BaseModel } from '../../base';
import { RoadMapCourse, RoadMapPreRequisiteCourseModel, Course, Classes, RoadMapPreRequisiteCourse } from '..';
import { Promise } from 'bluebird';
import * as _ from 'lodash';
import { RoadMap } from './schema/roadmap';
import { ClassesModel } from './classes.model';
import { CourseModel } from './course.model';

export class RoadMapCourseModel extends BaseModel {
  constructor() {
    super(RoadMapCourse);
  }
  /**
   * Creating program details against academic calendar
   * @param item
   */

  createWithPreRequisiteCourse(item) {
    let roadMapCourseItem = {
      roadMapId: item['roadMapId'],
      classId: item['classId'],
      courseCode: item['courseCode'],
      courseId: item['courseId'],
      courseOrder: item['courseOrder'],
      courseType: item['courseType'],
      creditHours: item['creditHours'],
      natureOfCourseId: item['natureOfCourseId']
    };

    return super.create(roadMapCourseItem).then(roadMapResult => {
      if (roadMapResult) {
        let roadMapPreReqModel = new RoadMapPreRequisiteCourseModel();
        return Promise.each(item['preReqCoursesList'], _course => {
          if (_course['courseId']) {
            let roadMapPreReqItem = {
              roadMapCourseId: roadMapResult['id'],
              courseId: _course['courseId']
            };

            return roadMapPreReqModel.create(roadMapPreReqItem);
          }
        }).then(() => {
          return roadMapResult;
        });
      }
    });
  }
  /**
   * Update program details against academic calendar
   * @param _id academic calendar id
   * @param item
   */
  updateWithPreRequisiteCourse(_id, item) {
    let roadMapCourseItem = {
      roadMapId: item['roadMapId'],
      classId: item['classId'],
      courseCode: item['courseCode'],
      courseId: item['courseId'],
      courseOrder: item['courseOrder'],
      courseType: item['courseType'],
      creditHours: item['creditHours'],
      natureOfCourseId: item['natureOfCourseId']
    };
    return super.update(_id, roadMapCourseItem).then(() => {
      let existingCourseIds: any[] = [];
      let inComingCourseIds: number[] = [];
      if (item['preReqCoursesList']) {
        inComingCourseIds = item['preReqCoursesList']
      }
      return new RoadMapPreRequisiteCourseModel().findAll(null, { roadMapCourseId: _id }).then(preReqResult => {
        if (preReqResult) {
          preReqResult.forEach(element => {
            existingCourseIds.push({ courseId: element['courseId'] });
          });
        }

        // Matching and adding new one (programDetailId)
        inComingCourseIds.forEach(InComingCId => {
          let exist: boolean = false;
          // Matching
          existingCourseIds.forEach(existingCId => {
            if (InComingCId['courseId'] === existingCId['courseId']) {
              exist = true;
            }
          });
          if (!exist) {
            // Adding
            let roadMapPreReqItem = {
              roadMapCourseId: _id,
              courseId: InComingCId['courseId']
            };
            return new RoadMapPreRequisiteCourseModel().create(roadMapPreReqItem).then(roadMapPreReq_result => {
              return roadMapPreReq_result;
            });
          }
        });

        // Matching and deleteing previous one  (programDetailId)
        existingCourseIds.forEach(existingCId => {
          let exist: boolean = false;
          // Matching
          inComingCourseIds.forEach(InComingCId => {
            if (InComingCId['courseId'] === existingCId['courseId']) {
              exist = true;
            }
          });
          if (!exist) {
            // Deleting
            return new RoadMapPreRequisiteCourseModel()
              .deleteByConditions({
                courseId: existingCId['courseId'],
                roadMapCourseId: _id
              })
              .then(roadMapPreReq_result => {
                return roadMapPreReq_result;
              });
          }
        });
        return preReqResult;
      });
    });
  }
  public findByRoadMap(_roadMapId) {

    let roadmapCourseAttr = ['id', 'roadMapId', 'courseId', 'courseCode', 'courseOrder', 'creditHours', 'courseType'];

    let includeObj = [
      {
        model: RoadMapCourse, as: 'roadMapCourses', attributes: roadmapCourseAttr, where: BaseModel.cb({ roadMapId: _roadMapId }), required: false,
        include: [
          { model: Course, as: 'course', attributes: ['id', 'title', 'abbreviation'], where: BaseModel.cb(), required: false },
          {
            model: RoadMapPreRequisiteCourse, as: 'preReqCoursesList', where: BaseModel.cb(), required: false,
            include: [{ model: Course, as: 'course', attributes: ['id', 'title'], where: BaseModel.cb(), required: false }]
          }
        ]
      }
    ];

    return new ClassesModel().findAll(['id', 'name'], null, includeObj, [['createdAt', 'ASC']]);
  }

  public findCourses(_roadMapId) {

    let arrayOfCourse = [];
    let rmcAttr = [[Sequelize.fn('DISTINCT', Sequelize.col('courseId')), 'courseId']];
    let courseAttr = ['id', 'title', 'abbreviation'];

    return this.findAllByConditions(rmcAttr, { roadMapId: _roadMapId })
      .then(roadMapCourses => {
        roadMapCourses.forEach(element => {
          arrayOfCourse.push(element['courseId']);
        });

        return new CourseModel().findAllByConditions(courseAttr, { id: { [Sequelize.Op.in]: arrayOfCourse } });
      });
  }

  public find(_id) {
    let preReqCoursesList: any[] = [];
    this.openConnection();
    return this.sequelizeModel
      .find({
        where: { id: _id }
      })
      .then(roadMapResult => {
        return new RoadMapPreRequisiteCourseModel().findAll(null, { roadMapCourseId: roadMapResult['id'] }).then(preReqResult => {
          if (preReqResult) {
            preReqResult.forEach(element => {
              preReqCoursesList.push({ courseId: element['courseId'] });
            });
          }
          roadMapResult['dataValues']['preReqCoursesList'] = preReqCoursesList;
          return roadMapResult;
        });
      });
  }

  public validate(item: any) {
    let _errors = [];
    return this.findAllByConditions(['id', 'courseCode', 'courseOrder'], { roadMapId: item['roadMapId'] })
      .then(roadMapCourses => {
        if (roadMapCourses) {
          roadMapCourses.forEach(roadMapCourse => {
            if (roadMapCourse['courseOrder'] == item['courseOrder'] && roadMapCourse['id'] !== item['id']) {
              _errors.push({ code: 'INVALID_COURSE_ORDER', msg: 'Course order should be unique.' });
            }
            if (roadMapCourse['courseCode'] == item['courseCode'] && roadMapCourse['id'] !== item['id']) {
              _errors.push({ code: 'INVALID_COURSE_CODE', msg: 'Course code should be unique.' });
            }

          });
        }

        return _errors;
      });
  }
  deleteWithAssociatedRecords(id) {
    return new RoadMapPreRequisiteCourseModel().deleteByConditions({ roadMapCourseId: id }).then(() => {
      return this.delete(id);
    })
  }

  deleteByConditionsWithAssociatedRecords(condition) {


    return this.findAllByConditions(['id'], condition).then(roadMapCourses => {

      return Promise.each(roadMapCourses, roadMapCourse => {

        let roadMapPreReqCourseModel = new RoadMapPreRequisiteCourseModel();

        return roadMapPreReqCourseModel.deleteByConditions({ roadMapCourseId: roadMapCourse['id'] }).then(() => {
          return this.delete(roadMapCourse['id']);
        })

      })
    })

  }

  findMaxCourseOrder(roadMapId: number) {
    return this.sequelizeModel.max('courseOrder', { where: { roadMapId: roadMapId } });
  }


  findRelatedDataWithCondition(condition) {

    let retResult = {};
    retResult['label'] = 'Roadmap Course';
    retResult['records'] = [];

    return this.findAllByConditions(['id'], condition, { model: Course, as: 'course', attributes: ['id', 'title'] }).then((rmCRes) => {

      _.each(rmCRes, item => { retResult['records'].push(item['dataValues']['course']) });

      return retResult;

    });
  }

  findRelatedDataWithConditionWRTCourse(condition) {

    let retResult = {};
    retResult['label'] = 'Course Roadmap';
    retResult['records'] = [];

    return this.findAllByConditions(['id'], condition, { model: RoadMap, as: 'roadMap', attributes: ['id', 'title'] }).then((rmCRes) => {

      _.each(rmCRes, item => { retResult['records'].push(item['dataValues']['roadMap']) });

      return retResult;

    });
  }
}
