import { Router } from 'express';

import { RoadMapsController } from '..';

export class RoadMapsRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new RoadMapsController();

    this.router.route('/academic/roadMaps/index').get(controller.index);
    this.router.route('/academic/roadMaps/findAttributesList').get(controller.findAttributesList);
    this.router.route('/academic/roadMaps/find/:id').get(controller.find);
    this.router.route('/academic/roadMaps/create').post(controller.create);
    this.router.route('/academic/roadMaps/update/:id').put(controller.update);
    this.router.route('/academic/roadMaps/delete/:id').delete(controller.delete);
  }
}
