import { Router } from 'express';

import { ClassesController } from '..';

export class ClassesRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new ClassesController();

    this.router.route('/academic/classes/index').get(controller.index);
    this.router.route('/academic/classes/findAttributesList').get(controller.findAttributesList);
    this.router.route('/academic/classes/find/:id').get(controller.find);
    this.router.route('/academic/classes/findWithProgramDetail/:programDetailId').get(controller.findWithProgramDetail);
    this.router.route('/academic/classes/create').post(controller.create);
    this.router.route('/academic/classes/updateByProgramDetail/:programDetailId/:id').put(controller.updateByProgramDetail);
    this.router.route('/academic/classes/update/:id').put(controller.update);
    this.router.route('/academic/classes/delete/:id').delete(controller.delete);
    this.router.route('/academic/classes/findProgramDetailsClasses/:id').get(controller.findProgramDetailsClasses);
    this.router.route('/academic/classes/findByProgramDetail/:programDetailId').get(controller.findByProgramDetail);
  }
}
