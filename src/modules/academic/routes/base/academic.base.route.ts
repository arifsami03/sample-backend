import { Router } from 'express';

import {
  ProgramsRoute, ProgramDetailsRoute, ClassesRoute,
  AcademicCalendarsRoute,
  AcademicCalendarProgramDetailsRoute,
  CoursesRoute,
  RoadMapsRoute,
  RoadMapPreRequisiteCoursesRoute,
  RoadMapCoursesRoute,
  CACTermsRoute,
  ActivitiesRoute,
  ACActivitiesRoute
} from '../..';

/**
 *
 *
 * @class AcademicBaseRoute
 */
export class AcademicBaseRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class BaseRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.initAll();
  }

  /**
   * init all routes
   */
  public initAll() {
    new ProgramsRoute(this.router);
    new ProgramDetailsRoute(this.router);
    new CoursesRoute(this.router);
    new ClassesRoute(this.router);
    new AcademicCalendarsRoute(this.router);
    new AcademicCalendarProgramDetailsRoute(this.router);
    new RoadMapsRoute(this.router);
    new RoadMapCoursesRoute(this.router);
    new RoadMapPreRequisiteCoursesRoute(this.router);
    new CACTermsRoute(this.router);
    new ActivitiesRoute(this.router);
    new ACActivitiesRoute(this.router);
  }
}
