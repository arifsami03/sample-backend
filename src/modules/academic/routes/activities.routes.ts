import { Router } from 'express';

import { ActivitiesController } from '..';

export class ActivitiesRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new ActivitiesController();

    this.router.route('/academic/activities/index').get(controller.index);
    this.router.route('/academic/activities/findAttributesList').get(controller.findAttributesList);
    this.router.route('/academic/activities/find/:id').get(controller.find);
    this.router.route('/academic/activities/create').post(controller.create);
    this.router.route('/academic/activities/update/:id').put(controller.update);
    this.router.route('/academic/activities/delete/:id').delete(controller.delete);
  }
}
