import { Router } from 'express';

import { CACTermsController } from '..';

export class CACTermsRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new CACTermsController();

    this.router.route('/academic/cacTerms/index').get(controller.index);
    this.router.route('/academic/cacTerms/find/:id').get(controller.find);
    this.router.route('/academic/cacTerms/findByACPD/:acpdId').get(controller.findByACPD);
    this.router.route('/academic/cacTerms/create').post(controller.create);
    this.router.route('/academic/cacTerms/update/:id').put(controller.update);
    this.router.route('/academic/cacTerms/delete/:id').delete(controller.delete);
  }
}
