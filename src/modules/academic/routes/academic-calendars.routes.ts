import { Router } from 'express';

import { AcademicCalendarsController } from '..';

export class AcademicCalendarsRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new AcademicCalendarsController();

    this.router.route('/academic/academicCalendars/index').get(controller.index);
    this.router.route('/academic/academicCalendars/findAttributesList').get(controller.findAttributesList);
    this.router.route('/academic/academicCalendars/find/:id').get(controller.find);
    this.router.route('/academic/academicCalendars/create').post(controller.create);
    this.router.route('/academic/academicCalendars/update/:id').put(controller.update);
    this.router.route('/academic/academicCalendars/delete/:id').delete(controller.delete);
  }
}
