import { Router } from 'express';

import { AcademicCalendarProgramDetailsController } from '..';

export class AcademicCalendarProgramDetailsRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new AcademicCalendarProgramDetailsController();

    this.router.route('/academic/academicCalendarProgramDetails/index').get(controller.index);
    this.router.route('/academic/academicCalendarProgramDetails/find/:id').get(controller.find);
    this.router.route('/academic/academicCalendarProgramDetails/create').post(controller.create);
    this.router.route('/academic/academicCalendarProgramDetails/update/:id').put(controller.update);
    this.router.route('/academic/academicCalendarProgramDetails/delete/:id').delete(controller.delete);
    this.router.route('/academic/academicCalendarProgramDetails/findAttributesList').get(controller.findAttributesList);
    this.router.route('/academic/academicCalendarProgramDetails/findByCampusProgramDetails').post(controller.findByCampusProgramDetails);
    this.router.route('/academic/academicCalendarProgramDetails/findAttributes/:id').get(controller.findAttributes);
    this.router.route('/academic/academicCalendarProgramDetails/findByProgramDetail/:programDetailId').get(controller.findByProgramDetail);
  }
}
