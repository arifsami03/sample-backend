import { Router } from 'express';
import { ProgramDetailsController } from '..';

export class ProgramDetailsRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new ProgramDetailsController();
    this.router.route('/academic/programDetails/index').get(controller.index);

    this.router.route('/academic/programDetails/findAttributesList').get(controller.findAttributesList);

    this.router.route('/academic/programDetails/findProgramDetailslistNotInCampusAdmission/:campusId/:campusAdmissionId').get(controller.findProgramDetailslistNotInCampusAdmission);

    this.router.route('/academic/programDetails/getProgramDetails').get(controller.getProgramDetails);
    

    this.router.route('/academic/programDetails/getProgramProgramDetails/:id').get(controller.getProgramProgramDetails);

    this.router.route('/academic/programDetails/getClassesByProgramDetail/:id').get(controller.getClassesByProgramDetail);

    this.router.route('/academic/programDetails/find/:id').get(controller.find);

    this.router.route('/academic/programDetails/create').post(controller.create);

    this.router.route('/academic/programDetails/delete/:id').delete(controller.delete);

    this.router.route('/academic/programDetails/update/:id').put(controller.update);

  }
}
