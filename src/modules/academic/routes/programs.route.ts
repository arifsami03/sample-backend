import { Router } from 'express';
import { ProgramsController } from '..';

export class ProgramsRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new ProgramsController();

    this.router.route('/academic/programs/index').get(controller.index);


    this.router.route('/academic/programs/find/:id').get(controller.find);

    this.router.route('/academic/programs/create').post(controller.create);
    this.router.route('/academic/programs/getPreReqNotSelected').post(controller.getPreReqNotSelected);

    this.router.route('/academic/programs/delete/:id').delete(controller.delete);

    this.router.route('/academic/programs/update/:id').put(controller.update);

    this.router.route('/academic/programs/getProgramsList').get(controller.getProgramsList);

    this.router.route('/academic/programs/getProgramsWithProgramDetails/:educationLevelId').get(controller.getProgramsWithProgramDetails);
    this.router.route('/academic/programs/getAllPrograms/:educationLevelId').post(controller.getAllPrograms);

  }
}
