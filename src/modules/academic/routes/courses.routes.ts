import { Router } from 'express';

import { CoursesController } from '..';

export class CoursesRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new CoursesController();

    this.router.route('/academic/courses/index').get(controller.index);
    this.router.route('/academic/courses/findAttributesList').get(controller.findAttributesList);
    this.router.route('/academic/courses/find/:id').get(controller.find);
    this.router.route('/academic/courses/findPreReqCourses').post(controller.findPreReqCourses);
    this.router.route('/academic/courses/findRoadMapCourses').post(controller.findRoadMapCourses);
    this.router.route('/academic/courses/getSubjectsNotSelected').post(controller.getSubjectsNotSelected);
    this.router.route('/academic/courses/create').post(controller.create);
    this.router.route('/academic/courses/update/:id').put(controller.update);
    this.router.route('/academic/courses/delete/:id').delete(controller.delete);
  }
}
