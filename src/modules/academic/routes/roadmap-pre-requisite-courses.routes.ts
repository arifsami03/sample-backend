import { Router } from 'express';

import { RoadMapPreRequisiteCoursesController } from '..';

export class RoadMapPreRequisiteCoursesRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new RoadMapPreRequisiteCoursesController();

    this.router.route('/academic/roadMapPreReuisiteCourses/index').get(controller.index);
    this.router.route('/academic/roadMapPreReuisiteCourses/find/:id').get(controller.find);
    this.router.route('/academic/roadMapPreReuisiteCourses/create').post(controller.create);
    this.router.route('/academic/roadMapPreReuisiteCourses/update/:id').put(controller.update);
    this.router.route('/academic/roadMapPreReuisiteCourses/delete/:id').delete(controller.delete);
  }
}
