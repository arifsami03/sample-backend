import { Router } from 'express';

import { ACActivitiesController } from '..';

export class ACActivitiesRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new ACActivitiesController();

    this.router.route('/academic/acActivities/index').get(controller.index);
    this.router.route('/academic/acActivities/find/:id').get(controller.find);
    this.router.route('/academic/acActivities/create').post(controller.create);
    this.router.route('/academic/acActivities/update/:id').put(controller.update);
    this.router.route('/academic/acActivities/delete/:id').delete(controller.delete);
    this.router.route('/academic/acActivities/findAttributesList').get(controller.findAttributesList);
    this.router.route('/academic/acActivities/findAttributes/:id').get(controller.findAttributes);
    this.router.route('/academic/acActivities/findByActivity/:activityId').get(controller.findByActivity);
  }
}
