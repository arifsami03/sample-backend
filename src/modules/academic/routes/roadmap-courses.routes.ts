import { Router } from 'express';

import { RoadMapCoursesController } from '..';

export class RoadMapCoursesRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new RoadMapCoursesController();

    this.router.route('/academic/roadMapCourses/index').get(controller.index);
    this.router.route('/academic/roadMapCourses/findCourses/:id').get(controller.findCourses);
    this.router.route('/academic/roadMapCourses/find/:id').get(controller.find);
    this.router.route('/academic/roadMapCourses/findMaxCourseOrder/:roadMapId').get(controller.findMaxCourseOrder);
    this.router.route('/academic/roadMapCourses/findByRoadMap/:id').get(controller.findByRoadMap);
    this.router.route('/academic/roadMapCourses/create').post(controller.create);
    this.router.route('/academic/roadMapCourses/update/:id').put(controller.update);
    this.router.route('/academic/roadMapCourses/delete/:id').delete(controller.delete);
    this.router.route('/academic/roadMapCourses/validate').post(controller.validate);
  }
}
