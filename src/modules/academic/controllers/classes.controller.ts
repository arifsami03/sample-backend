import * as express from 'express';

import { ErrorHandler } from '../../base/conf/error-handler';
import { ClassesModel } from '..';

export class ClassesController {
  constructor() { }

  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    // return all related data using base model's include feature
    new ClassesModel().list().then(result => {
      // result.sort(CustomSort.sortByName);
      res.send(result);
    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }
  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesList(req: express.Request, res: express.Response, next: express.NextFunction) {
    new ClassesModel()
      .findAll(['id', 'name'])
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Get All Recrods on the basis of program details
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findByProgramDetail(req: express.Request, res: express.Response, next: express.NextFunction) {
    let _programDetailId = req.params.programDetailId;
    new ClassesModel()
      .findAllByConditions(['id', 'name'], { programDetailId: _programDetailId })
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Get All Recrods on the basis of program details
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findWithProgramDetail(req: express.Request, res: express.Response, next: express.NextFunction) {
    let _programDetailId = req.params.programDetailId;
    new ClassesModel()
      .findWithProgramDetail(_programDetailId)
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Find single record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new ClassesModel()
      .find(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Get all classes for a programDetail
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findProgramDetailsClasses(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new ClassesModel()
      .findAllByConditions(['id', 'name'], { programDetailId: id })
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Create record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {
    new ClassesModel()
      .create(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Delete one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new ClassesModel()
      .deleteOne(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;

    new ClassesModel()
      .update(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Update one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  updateByProgramDetail(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;
    let programDetailId = req.params.programDetailId;

    new ClassesModel()
      .updateByProgramDetail(id, programDetailId, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

}
