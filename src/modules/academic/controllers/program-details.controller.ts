import * as express from 'express';

import { ErrorHandler } from '../../base/conf/error-handler';
import { ProgramDetailModel } from '..';

export class ProgramDetailsController {
  constructor() { }

  /**
   * it will get all Programs
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    new ProgramDetailModel()
      .index()
      .then(programs => {
        res.send(programs);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesList(req: express.Request, res: express.Response, next: express.NextFunction) {
    new ProgramDetailModel()
      .findAttributesList()
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findProgramDetailslistNotInCampusAdmission(req: express.Request, res: express.Response, next: express.NextFunction) {
    let campusId=req.params.campusId;
    let campusAdmissionId=req.params.campusAdmissionId;
    new ProgramDetailModel()
      .findProgramDetailslistNotInCampusAdmission(campusId,campusAdmissionId)
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * it will get one Programs
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new ProgramDetailModel()
      .find(id)
      .then(program => {
        res.send({
          success: 'Programs has been selected successfully',
          data: program
        });
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * it will create new Programs
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new ProgramDetailModel()
      .create(req.body)
      .then(program => {
        res.send({
          success: 'Programs has been selected successfully',
          data: program
        });
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * it will delete Programs
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new ProgramDetailModel()
      .deleteOne(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * it will update Programs
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;
    new ProgramDetailModel()
      .update(id, item)
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  getProgramProgramDetails(req: express.Request, res: express.Response, next: express.NextFunction) {
    new ProgramDetailModel()
      .getProgramProgramDetails(req.params.id)
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  getClassesByProgramDetail(req: express.Request, res: express.Response, next: express.NextFunction) {
    new ProgramDetailModel()
      .getClassesByProgramDetail(req.params.id)
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  getProgramDetails(req: express.Request, res: express.Response, next: express.NextFunction) {
    new ProgramDetailModel()
      .getProgramDetails()
      .then(programs => {
        res.send(programs);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

}
