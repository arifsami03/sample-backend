import * as express from 'express';
 
import { ErrorHandler } from '../../base/conf/error-handler';
import { RoadMapCourseModel } from '..';

export class RoadMapCoursesController {
  constructor() {}

  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    new RoadMapCourseModel()
      .findAll()
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findCourses(req: express.Request, res: express.Response, next: express.NextFunction) {
    let _roadMapId = req.params.id;

    new RoadMapCourseModel()
      .findCourses(_roadMapId)
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Get Max Course Order
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findMaxCourseOrder(req: express.Request, res: express.Response, next: express.NextFunction) {
    let _roadMapId = req.params.roadMapId;

    new RoadMapCourseModel()
      .findMaxCourseOrder(_roadMapId)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Find single record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new RoadMapCourseModel()
      .find(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Find single record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findByRoadMap(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new RoadMapCourseModel()
      .findByRoadMap(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Create record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {
    new RoadMapCourseModel()
      .createWithPreRequisiteCourse(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Delete one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new RoadMapCourseModel()
      .deleteWithAssociatedRecords(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;

    new RoadMapCourseModel()
      .updateWithPreRequisiteCourse(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Validating unique course order
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  validate(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    new RoadMapCourseModel()
      .validate(item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
}
