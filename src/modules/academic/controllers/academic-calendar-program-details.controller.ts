import * as express from 'express';

import { ErrorHandler } from '../../base/conf/error-handler';
import { AcademicCalendarProgramDetailModel } from '..';

export class AcademicCalendarProgramDetailsController {
  constructor() { }

  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    new AcademicCalendarProgramDetailModel()
      .findAll()
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesList(req: express.Request, res: express.Response, next: express.NextFunction) {
    new AcademicCalendarProgramDetailModel()
      .findAttributesList()
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Get All Recrods on the basis of campus program details
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findByCampusProgramDetails(req: express.Request, res: express.Response, next: express.NextFunction) {
    let campusIds = req.body.campusIds;
    new AcademicCalendarProgramDetailModel()
      .findByCampusProgramDetails(campusIds)
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        console.log(err);
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Get All Recrods on the basis of campus  details
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findByProgramDetail(req: express.Request, res: express.Response, next: express.NextFunction) {
    let programDetailId = req.params.programDetailId;
    new AcademicCalendarProgramDetailModel()
      .findByProgramDetail(programDetailId)
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        console.log(err);
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Get Single record with details
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributes(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new AcademicCalendarProgramDetailModel()
      .findAttributes(id)
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Find single record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new AcademicCalendarProgramDetailModel()
      .find(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Create record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {
    new AcademicCalendarProgramDetailModel()
      .create(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Delete one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new AcademicCalendarProgramDetailModel()
      .deleteWithAssociatedRecords(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;

    new AcademicCalendarProgramDetailModel()
      .update(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }


}
