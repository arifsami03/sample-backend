export * from './routes/base/academic.base.route';

/**
 * Program
 */
export * from './routes/programs.route';
export * from './controllers/programs.controller';
export * from './models/program.model';
export * from './models/schema/program';

/**
 * Program Details
 */
export * from './routes/program-details.route';
export * from './controllers/program-details.controller';
export * from './models/program-detail.model';
export * from './models/schema/program-detail';

/**
 * Course
 */
export * from './routes/courses.routes';
export * from './controllers/courses.controller';
export * from './models/course.model';
export * from './models/schema/course';

/**
 * Classes
 */
export * from './routes/classes.routes';
export * from './controllers/classes.controller';
export * from './models/classes.model';
export * from './models/schema/classes';

/**
 * Academic Calendar Program Detail
 */
export * from './routes/academic-calendar-program-details.routes';
export * from './controllers/academic-calendar-program-details.controller';
export * from './models/academic-calendar-program-detail.model';
export * from './models/schema/academic-calendar-program-detail';


/**
 * Academic Calendar
 */
export * from './routes/academic-calendars.routes';
export * from './controllers/academic-calendars.controller';
export * from './models/academic-calendar.model';
export * from './models/schema/academic-calendar';

/**
 * RoadMap
 */
export * from './routes/roadmaps.routes';
export * from './controllers/roadmaps.controller';
export * from './models/roadmap.model';
export * from './models/schema/roadmap';

/**
 * RoadMap Course
 */
export * from './routes/roadmap-courses.routes';
export * from './controllers/roadmap-courses.controller';
export * from './models/roadmap-course.model';
export * from './models/schema/roadmap-course';

/**
 * RoadMap Pre Requisite Course
 */
export * from './routes/roadmap-pre-requisite-courses.routes';
export * from './controllers/roadmap-pre-requisite-courses.controller';
export * from './models/roadmap-pre-requisite-course.model';
export * from './models/schema/roadmap-pre-requisite-course';

/**
 * Academic Calendar Terms
 */
export * from './routes/cac-terms.routes';
export * from './controllers/cac-terms.controller';
export * from './models/cac-term.model';
export * from './models/schema/cac-term';
/**
 * Activity
 */
export * from './routes/activities.routes';
export * from './controllers/activities.controller';
export * from './models/activity.model';
export * from './models/schema/activity';
/**
 * ACActivity
 */
export * from './routes/ac-activities.routes';
export * from './controllers/ac-activities.controller';
export * from './models/ac-activity.model';
export * from './models/schema/ac-activity';
