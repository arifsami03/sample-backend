'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ESQuestionOption', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      ESQuestionId: { type: Sequelize.INTEGER, allowNull: false },
      optionString: { type: Sequelize.STRING, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('ESQuestionOption', ['ESQuestionId'], {
        type: 'foreign key', name: 'FK_ESQuestionOption_ESQuestion',
        references: { table: 'ESQuestion', field: 'id' }
      })
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ESQuestionOption');
  }
};