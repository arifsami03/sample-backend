'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AcademicCalendarProgramDetail', null, {}).then(() => {
      return queryInterface
        .addColumn('AcademicCalendarProgramDetail', 'startDate', {
          type: Sequelize.DATE,
        })
        .then(() => {
          return queryInterface.addColumn('AcademicCalendarProgramDetail', 'endDate', {
            type: Sequelize.DATE,
          });
        });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('AcademicCalendarProgramDetail', 'startDate').then(() => {
      return queryInterface.removeColumn('AcademicCalendarProgramDetail', 'endDate');
    });
  }
};
