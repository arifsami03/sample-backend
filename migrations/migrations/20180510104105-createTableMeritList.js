'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('MeritList', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        programDetailId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        academicYear: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        title: {
          type: Sequelize.STRING,
          allowNull: false
        },
        percentageFrom: {
          type: Sequelize.FLOAT,
          allowNull: false
        },
        percentageTo: {
          type: Sequelize.FLOAT,
          allowNull: false
        },
        scholarshipId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        numberOfSeats: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER },
        updatedBy: { type: Sequelize.INTEGER }
      })
      .then(() => {
        return queryInterface
          .addConstraint('MeritList', ['programDetailId'], {
            type: 'foreign key',
            name: 'FK_MeritList_ProgramDetail',
            references: {
              table: 'ProgramDetail',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          })
          .then(() => {
            return queryInterface.addConstraint('MeritList', ['scholarshipId'], {
              type: 'foreign key',
              name: 'FK_MeritList_Scholarship',
              references: {
                table: 'Scholarship',
                field: 'id'
              },
              onDelete: 'cascade',
              onUpdate: 'cascade'
            });
          });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('MeritList');
  }
};
