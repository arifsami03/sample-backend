'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('CRApplication', 'cityId', {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: false
      })
      .then(() => {
        return queryInterface.addConstraint('CRApplication', ['cityId'], {
          type: 'foreign key',
          name: 'FK_Application_MetaConf',
          references: {
            table: 'MetaConf',
            field: 'id'
          }
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('CRApplication', 'FK_Application_MetaConf')
      .then(() => {
        return queryInterface.removeColumn('CRApplication', 'cityId');
      });
  }
};
