'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Configuration', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      key: { type: Sequelize.STRING(50), allowNull: false, defaultValue: false },
      value: { type: Sequelize.TEXT, allowNull: false, defaultValue: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER },
    }

    ).then(() => {
      return queryInterface.bulkInsert('Configuration', [
        {
          key: 'appName', value: 'Idrak', createdAt: new Date(), updatedAt: new Date()
        }
      ], {});
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Configuration');

  }
};
