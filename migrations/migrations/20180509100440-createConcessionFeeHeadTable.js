'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.createTable('ConcessionFeeHead', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      consessionId: { type: Sequelize.INTEGER, allowNull: false },
      feeHeadId: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {

      return queryInterface.addConstraint('ConcessionFeeHead', ['consessionId'], {
        type: 'foreign key', name: 'FK_ConcessionFeeHead_Concession',
        references: { table: 'Concession', field: 'id' }
      })

    }).then(() => {

      return queryInterface.addConstraint('ConcessionFeeHead', ['feeHeadId'], {
        type: 'foreign key', name: 'FK_ConcessionFeeHead_FeeHead',
        references: { table: 'FeeHeads', field: 'id' }
      })

    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ConcessionFeeHead');
  }
};