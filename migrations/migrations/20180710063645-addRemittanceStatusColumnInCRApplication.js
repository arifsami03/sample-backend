'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface
      .addColumn('CRApplication', 'remittanceStatus', {
        type: Sequelize.BOOLEAN
      })
      .then(() => {
        return queryInterface.sequelize.query(`UPDATE CRApplication SET remittanceStatus = ?`, {
          replacements: [true]
        })
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CRApplication', 'remittanceStatus');
  }
};
