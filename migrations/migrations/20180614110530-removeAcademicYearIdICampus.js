'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Campus', 'academicYearId')
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Campus', 'academicYearId', {
      type: Sequelize.INTEGER,
    })
  }
};
