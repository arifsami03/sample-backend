'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('WorkFlow', {
        WFId: {
          type: Sequelize.STRING,
          allowNull:false,
          primaryKey: true,
          autoIncrement:false
        },
        title: {
          type: Sequelize.STRING,
          allowNull:false,          
        },
        description: {
          type: Sequelize.TEXT,
          allowNull:true,          
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        updatedBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        }
      })
      
  },

  down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('WorkFlow');

  }
};