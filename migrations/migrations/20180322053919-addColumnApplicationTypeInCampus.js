'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
    .addColumn('CRApplication', 'applicationType', {
      type: Sequelize.STRING
    })
    
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CRApplication', 'applicationType')
    
   
  }
};
