'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Configuration', [
      {
        key: 'showRemittance',
        value: '',
        isSingle: true,
        label: 'Show Remittance',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Configuration', { key: 'showRemittance' });
  }
};
