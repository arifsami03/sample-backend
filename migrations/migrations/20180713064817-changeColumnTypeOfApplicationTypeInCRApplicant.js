'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    
      
      return queryInterface.changeColumn('CRApplicant', 'applicationType', {
        type: Sequelize.STRING,
        allowNull: true
      })

    
  },

  down: (queryInterface, Sequelize) => {
      
      return queryInterface.changeColumn('CRApplicant', 'applicationType', {
        type: Sequelize.STRING,
        allowNull: false
      })

  }
};