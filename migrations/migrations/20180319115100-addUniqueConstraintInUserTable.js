'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addConstraint('User', ['username'], {
      type: 'unique',
      name: 'Unique_User_Username'
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('User', 'Unique_User_Username');
  }
};
