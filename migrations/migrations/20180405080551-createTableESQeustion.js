'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ESQuestion', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      question: { type: Sequelize.STRING, allowNull: false },
      ESSectionId: { type: Sequelize.INTEGER, allowNull: false },
      countField: { type: Sequelize.BOOLEAN, allowNull: false },
      remarksField: { type: Sequelize.BOOLEAN, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('ESQuestion', ['ESSectionId'], {
        type: 'foreign key', name: 'FK_ESQuestion_ESSection',
        references: { table: 'ESSection', field: 'id' }
      })
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('ESQuestion', 'FK_ESQuestion_ESSection').then(() => {
      return queryInterface.dropTable('ESQuestion');
    });
  }
};