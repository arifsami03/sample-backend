'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('EmailTemplate', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        name: {
          type: Sequelize.STRING(100),
          allowNull: false,
          defaultValue: false
        },
        subject: {
          type: Sequelize.STRING,
          allowNull: false,
          defaultValue: false
        },
        emailBody: {
          type: Sequelize.TEXT,
          allowNull: false
        },
        outgoingMailServerId: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        updatedBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        }
      })
      .then(() => {
        return queryInterface.addConstraint(
          'EmailTemplate', ['outgoingMailServerId'], {
            type: 'foreign key',
            name: 'FK_EmailTemplate_OutgoingMailServer',
            references: {
              table: 'OutgoingMailServer',
              field: 'id'
            }
          }
        );
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('EmailTemplate', 'FK_EmailTemplate_OutgoingMailServer')
      .then(() => {
        return queryInterface.dropTable('EmailTemplate');
      });
  }
};