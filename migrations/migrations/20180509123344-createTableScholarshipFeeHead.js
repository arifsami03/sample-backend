'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('ScholarshipFeeHead', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        scholarshipId: {
          type: Sequelize.INTEGER
        },
        feeHeadId: {
          type: Sequelize.INTEGER
        },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER },
        updatedBy: { type: Sequelize.INTEGER }
      })
      .then(() => {
        return queryInterface
          .addConstraint('ScholarshipFeeHead', ['scholarshipId'], {
            type: 'foreign key',
            name: 'FK_ScholarshipFeeHead_Scholarship',
            references: {
              table: 'Scholarship',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          })
          .then(() => {
            return queryInterface.addConstraint('ScholarshipFeeHead', ['feeHeadId'], {
              type: 'foreign key',
              name: 'FK_ScholarshipFeeHead_FeeHeads',
              references: {
                table: 'FeeHeads',
                field: 'id'
              },
              onDelete: 'cascade',
              onUpdate: 'cascade'
            });
          });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ScholarshipFeeHead');
  }
};
