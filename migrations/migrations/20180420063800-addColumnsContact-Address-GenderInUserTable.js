'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('User', 'phoneNumber', {
      type: Sequelize.STRING
    }).then(() => {
      return queryInterface.addColumn('User', 'address', {
        type: Sequelize.STRING
      }).then(() => {
        return queryInterface.addColumn('User', 'gender', {
          type: Sequelize.STRING
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('User', 'phoneNumber').then(() => {
      return queryInterface.removeColumn('User', 'address').then(() => {
        return queryInterface.removeColumn('User', 'gender');
      });
    });
  }
};
