'use strict';

module.exports = {
  /**
   * FSI stands for Fee Structure Installement
   */
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('FSILineItem', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      FSInstallementId: { type: Sequelize.INTEGER, allowNull: false },
      feeHeadId: { type: Sequelize.INTEGER, allowNull: false },
      installementFormat: { type: Sequelize.STRING, allowNull: false },
      amount: { type: Sequelize.FLOAT, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('FSILineItem', ['FSInstallementId'], {
        type: 'foreign key', name: 'FK_FSILineItem_FSInstallement',
        references: { table: 'FSInstallement', field: 'id' },
        onDelete: 'cascade', onUpdate: 'cascade'
      }).then(() => {
        return queryInterface.addConstraint('FSILineItem', ['feeHeadId'], {
          type: 'foreign key', name: 'FK_FSILineItem_FeeHeads',
          references: { table: 'FeeHeads', field: 'id' },
          onDelete: 'cascade', onUpdate: 'cascade'
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('FSILineItem');
  }

}