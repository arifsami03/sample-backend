'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('RoadMap', 'FK_RoadMap_AcademicCalendar').then(() => {

    })

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addConstraint('RoadMap', ['academicCalendarId'], {
      type: 'foreign key',
      name: 'FK_RoadMap_AcademicCalendar',
      references: { table: 'AcademicCalendar', field: 'id' },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    });
  }
};
