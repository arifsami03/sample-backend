'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('EmailTemplate', 'to', {
        type: Sequelize.STRING
      })
      .then(() => {
        return queryInterface
          .addColumn('EmailTemplate', 'cc', {
            type: Sequelize.STRING
          })
          .then(() => {
            return queryInterface.addColumn('EmailTemplate', 'bcc', {
              type: Sequelize.STRING
            });
          });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('EmailTemplate', 'to').then(() => {
      return queryInterface.removeColumn('EmailTemplate', 'cc').then(() => {
        return queryInterface.removeColumn('EmailTemplate', 'bcc');
      });
    });
  }
};
