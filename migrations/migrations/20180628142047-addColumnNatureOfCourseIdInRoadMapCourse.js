'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkDelete('RoadMapPreRequisiteCourse', null, {}).then(() => {

      return queryInterface.bulkDelete('RoadMapCourse', null, {}).then(() => {

        return queryInterface.removeColumn('RoadMapCourse', 'natureOfCourse').then(() => {

          return queryInterface.addColumn('RoadMapCourse', 'natureOfCourseId', { type: Sequelize.INTEGER }).then(() => {

            return queryInterface.addConstraint('RoadMapCourse', ['natureOfCourseId'], {
              type: 'foreign key', name: 'FK_RoadMapCourse_Configuration',
              references: { table: 'Configuration', field: 'id' }
            });
          });
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {

    return queryInterface.removeConstraint('RoadMapCourse', 'FK_RoadMapCourse_Configuration').then(() => {

      return queryInterface.removeColumn('RoadMapCourse', 'natureOfCourseId').then(() => {

        return queryInterface.addColumn('RoadMapCourse', 'natureOfCourse', { type: Sequelize.STRING });

      });
    });
  }
};
