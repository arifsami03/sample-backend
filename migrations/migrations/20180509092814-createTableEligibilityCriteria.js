'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('EligibilityCriteria', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      programId: { type: Sequelize.INTEGER, allowNull: false },
      academicCalendarId: { type: Sequelize.INTEGER, allowNull: false },
      ageFromYY: { type: Sequelize.INTEGER, allowNull: false },
      ageFromMM: { type: Sequelize.INTEGER, allowNull: false },
      ageToYY: { type: Sequelize.INTEGER, allowNull: false },
      ageToMM: { type: Sequelize.INTEGER, allowNull: false },
      requiredMarksFigure: { type: Sequelize.FLOAT, allowNull: false },
      requiredMarksPercent: { type: Sequelize.FLOAT, allowNull: false },
      totalMarks: { type: Sequelize.FLOAT, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('EligibilityCriteria', ['programId'], {
        type: 'foreign key', name: 'FK_EligibilityCriteria_Program',
        references: { table: 'Program', field: 'id' }
      }).then(() => {
        return queryInterface.addConstraint('EligibilityCriteria', ['academicCalendarId'], {
          type: 'foreign key', name: 'FK_EligibilityCriteria_AcademicCalendar',
          references: { table: 'AcademicCalendar', field: 'id' }
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('EligibilityCriteria', 'FK_EligibilityCriteria_AcademicCalendar').then(() => {
      return queryInterface.removeConstraint('EligibilityCriteria', 'FK_EligibilityCriteria_Program').then(() => {
        return queryInterface.dropTable('EligibilityCriteria');
      });
    });
  }
};