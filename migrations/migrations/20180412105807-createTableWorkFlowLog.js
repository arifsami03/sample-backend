'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('WorkFlowLog', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        fromStateId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        toStateId: {
          type: Sequelize.INTEGER
        },
        parent: {
          type: Sequelize.STRING
        },
        parentId: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        updatedBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        }
      })

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('WorkFlowLog');

  }
};