'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('LicenseFee', 'feeType', {
      type: Sequelize.STRING,
    }).then(() => {
      return queryInterface.sequelize.query(`UPDATE LicenseFee SET feeType = ?`, {
        replacements: ['license-fee']
      });
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('LicenseFee', 'feeType');
  }
}