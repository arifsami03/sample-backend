'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ConfigurationOption', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      configurationId: { type: Sequelize.INTEGER, allowNull: false },
      key: { type: Sequelize.STRING, allowNull: false },
      value: { type: Sequelize.STRING, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER, allowNull: true },
      updatedBy: { type: Sequelize.INTEGER, allowNull: true }
    }).then(() => {
    }).then(() => {
      return queryInterface.addConstraint('ConfigurationOption', ['configurationId'], {
        type: 'foreign key', name: 'FK_ConfigurationOption_Configuration',
        references: { table: 'Configuration', field: 'id' }
      })
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('ConfigurationOption', 'FK_ConfigurationOption_Configuration').then(() => {
      return queryInterface.dropTable('ConfigurationOption');
    });
  }
};