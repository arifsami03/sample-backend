'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('CACTerm', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        academicCalendarProgramDetailId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },

        startDate: {
          type: Sequelize.DATE,
          allowNull: false
        },
        endDate: {
          type: Sequelize.DATE,
          allowNull: false
        },
        title: {
          type: Sequelize.STRING,
          allowNull: false
        },
        abbreviation: {
          type: Sequelize.STRING,
          allowNull: false
        },
        status: {
          type: Sequelize.BOOLEAN,
          defaultValue: true
        },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER },
        updatedBy: { type: Sequelize.INTEGER }
      })
      .then(() => {
        return queryInterface.addConstraint('CACTerm', ['academicCalendarProgramDetailId'], {
          type: 'foreign key',
          name: 'FK_CACTerm_AcademicCalendarProgramDetail',
          references: {
            table: 'AcademicCalendarProgramDetail',
            field: 'id'
          }
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CACTerm');
  }
};
