'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CampusProgram', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      campusId: { type: Sequelize.INTEGER, allowNull: false },
      programId: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER, allowNull: true },
      updatedBy: { type: Sequelize.INTEGER, allowNull: true }
    }).then(() => {
      return queryInterface.addConstraint('CampusProgram', ['campusId'], {
        type: 'foreign key', name: 'FK_CampusProgram_Campus',
        references: { table: 'Campus', field: 'id' }
      }).then(() => {
        return queryInterface.addConstraint('CampusProgram', ['programId'], {
          type: 'foreign key', name: 'FK_CampusProgram_Program',
          references: { table: 'Program', field: 'id' }
        })
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('CampusProgram', 'FK_CampusProgram_Program').then(() => {
      return queryInterface.removeConstraint('CampusProgram', 'FK_CampusProgram_Campus').then(() => {
        return queryInterface.dropTable('CampusProgram');
      });
    });

  }
};