'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CRMOU', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      CRApplicationId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      MOUDate: {
        type: Sequelize.DATE,
        allowNull: false
      },
      campusTitle: {
        type: Sequelize.STRING,
        allowNull: false
      },
      purpose: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      educationLevelId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      academicYearId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      licenseFeeId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },


      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      createdBy: {
        type: Sequelize.INTEGER
      },
      updatedBy: {
        type: Sequelize.INTEGER
      }

    }).then(() => {
      return queryInterface.addConstraint('CRMOU', ['CRApplicationId'], {
        type: 'foreign key',
        name: 'FK_CRMOU_CRApplication',
        references: {
          table: 'CRApplication',
          field: 'id'
        }
      })
    }).then(() => {
      return queryInterface.addConstraint('CRMOU', ['educationLevelId'], {
        type: 'foreign key',
        name: 'FK_CRMOU_EducationLevel',
        references: {
          table: 'EducationLevel',
          field: 'id'
        }
      })
    }).then(() => {
      return queryInterface.addConstraint('CRMOU', ['academicYearId'], {
        type: 'foreign key',
        name: 'FK_CRMOU_AcademicCalendar',
        references: {
          table: 'AcademicCalendar',
          field: 'id'
        }
      })
    }).then(() => {
      return queryInterface.addConstraint('CRMOU', ['licenseFeeId'], {
        type: 'foreign key',
        name: 'FK_CRMOU_LicenseFee',
        references: {
          table: 'LicenseFee',
          field: 'id'
        }
      })
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('CRMOU', 'FK_CRMOU_CRApplication').then(() => {
      return queryInterface.removeConstraint('CRMOU', 'FK_CRMOU_EducationLevel').then(() => {
        return queryInterface.removeConstraint('CRMOU', 'FK_CRMOU_AcademicCalendar').then(() => {
          return queryInterface.removeConstraint('CRMOU', 'FK_CRMOU_LicenseFee').then(() => {
            return queryInterface.dropTable('CRMOU');
          });
        })
      })
    });
  }
};