'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('AttachedFile', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        attachmentId: { type: Sequelize.INTEGER, allowNull: false },
        originalName: { type: Sequelize.STRING, allowNull: false },
        name: { type: Sequelize.STRING, allowNull: false },
        path: { type: Sequelize.STRING, allowNull: false },
        size: { type: Sequelize.INTEGER, allowNull: false },
        type: { type: Sequelize.STRING, allowNull: false },
        description: { type: Sequelize.STRING },
        title: { type: Sequelize.STRING },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER },
        updatedBy: { type: Sequelize.INTEGER }
      })
      .then(() => {
        return queryInterface.addConstraint('AttachedFile', ['attachmentId'], {
          type: 'foreign key',
          name: 'FK_Attachment_AttachedFile',
          references: { table: 'Attachment', field: 'id' },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('AttachedFile', 'FK_Attachment_AttachedFile').then(() => {
      return queryInterface.dropTable('AttachedFile');
    });
  }
};
