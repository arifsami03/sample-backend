'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CRProgramDetail', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      CRProgramId: { type: Sequelize.INTEGER, allowNull: false },
      programDetailId: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER, allowNull: true },
      updatedBy: { type: Sequelize.INTEGER, allowNull: true }
    }).then(() => {
      return queryInterface.addConstraint('CRProgramDetail', ['CRProgramId'], {
        type: 'foreign key', name: 'FK_CRProgramDetail_CRProgram',
        references: { table: 'CRProgram', field: 'id' }
      }).then(() => {
        return queryInterface.addConstraint('CRProgramDetail', ['programDetailId'], {
          type: 'foreign key', name: 'FK_CRProgramDetail_ProgramDetail',
          references: { table: 'ProgramDetail', field: 'id' }
        })
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('CRProgramDetail', 'FK_CRProgramDetail_ProgramDetail').then(() => {
      return queryInterface.removeConstraint('CRProgramDetail', 'FK_CRProgramDetail_CRProgram').then(() => {
        return queryInterface.dropTable('CRProgramDetail');
      });
    });

  }
};