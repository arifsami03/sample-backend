'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('InstituteType', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: false
      },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('InstituteType');
  }
};
