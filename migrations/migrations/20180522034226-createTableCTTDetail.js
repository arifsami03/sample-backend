'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('CTTDetail', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        campusTimeTableId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        slotId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        slotOrder: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        courseId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER
        },
        updatedBy: {
          type: Sequelize.INTEGER
        }
      })
      .then(() => {
        return queryInterface.addConstraint('CTTDetail', ['campusTimeTableId'], {
          type: 'foreign key',
          name: 'FK_CTTDetail_CampusTimeTable',
          references: {
            table: 'CampusTimeTable',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        })
      })
      // .then(() => {
      //   return queryInterface.addConstraint('CTTDetail', ['slotId'], {
      //     type: 'foreign key',
      //     name: 'FK_TimeTable_Slot',
      //     references: {
      //       table: 'Slot',
      //       field: 'id'
      //     },
      //     onDelete: 'cascade',
      //     onUpdate: 'cascade'
      //   })
      // })
      // .then(() => {
      //   return queryInterface.addConstraint('CTTDetail', ['courseId'], {
      //     type: 'foreign key',
      //     name: 'FK_TimeTable_Course',
      //     references: {
      //       table: 'Course',
      //       field: 'id'
      //     },
      //     onDelete: 'cascade',
      //     onUpdate: 'cascade'
      //   });
      // })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CTTDetail');
  }
};