'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CRAvailableBuilding', 'buildingOwn').then(() => {
      return queryInterface.addColumn('CRAvailableBuilding', 'buildingType', {
        type: Sequelize.STRING
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CRAvailableBuilding', 'buildingType').then(() => {
      return queryInterface.addColumn('CRAvailableBuilding', 'buildingOwn', {
        type: Sequelize.BOOLEAN
      });
    });
  }
};