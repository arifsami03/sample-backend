'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('CRReference', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        CRApplicantId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: false
        },
        fullName: {
          type: Sequelize.STRING(55),
          allowNull: false,
          defaultValue: false
        },
        mobileNumber: {
          type: Sequelize.STRING(20),
          allowNull: false,
          defaultValue: false
        },
        address: {
          type: Sequelize.STRING,
          allowNull: true
        },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER, allowNull: true },
        updatedBy: { type: Sequelize.INTEGER, allowNull: true }
      })
      .then(() => {
        return queryInterface.addConstraint(
          'CRReference',
          ['CRApplicantId'],
          {
            type: 'foreign key',
            name: 'FK_CRApplicantReference_CRApplicant',
            references: {
              table: 'CRApplicant',
              field: 'id'
            }
          }
        );
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint(
        'CRReference',
        'FK_CRApplicantReference_CRApplicant'
      )
      .then(() => {
        return queryInterface.dropTable('CRReference');
      });
  }
};
