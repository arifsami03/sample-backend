'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('CampusAcademicCalendar', null, {}).then(() => {
      return queryInterface.removeColumn('CampusAcademicCalendar', 'startDate').then(() => {
        return queryInterface.removeColumn('CampusAcademicCalendar', 'endDate').then(() => {
          return queryInterface.removeColumn('CampusAcademicCalendar', 'status');
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('CampusAcademicCalendar', 'startDate', {
        type: Sequelize.DATE,
      })
      .then(() => {
        return queryInterface
          .addColumn('CampusAcademicCalendar', 'endDate', {
            type: Sequelize.DATE,
          })
          .then(() => {
            return queryInterface.addColumn('CampusAcademicCalendar', 'status', {
              type: Sequelize.BOOLEAN,
              defaultValue: true
            });
          });
      });
  }
};
