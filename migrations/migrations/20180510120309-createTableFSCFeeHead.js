'use strict';

module.exports = {
  /**
   * FSC stands for Fee Structure Campus Table
   */
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('FSCFeeHead', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      FSCId: { type: Sequelize.INTEGER, allowNull: false },
      feeHeadId: { type: Sequelize.INTEGER, allowNull: false },
      typeOfFeeHeadId: { type: Sequelize.INTEGER, allowNull: false },
      amount: { type: Sequelize.INTEGER, allowNull: false },
      managementFeeType: { type: Sequelize.STRING, allowNull: false },
      managementFeeAmount: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('FSCFeeHead', ['FSCId'], {
        type: 'foreign key', name: 'FK_FSCFeeHead_FSCampus',
        references: { table: 'FSCampus', field: 'id' }
      }).then(() => {
        return queryInterface.addConstraint('FSCFeeHead', ['feeHeadId'], {
          type: 'foreign key', name: 'FK_FSCFeeHead_FeeHeads',
          references: { table: 'FeeHeads', field: 'id' }
        }).then(() => {
          return queryInterface.addConstraint('FSCFeeHead', ['typeOfFeeHeadId'], {
            type: 'foreign key', name: 'FK_FSCFeeHead_Configuration',
            references: { table: 'Configuration', field: 'id' }
          });
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('FSCFeeHead');
  }

}