'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('CRApplication', 'MOUApproveRemarks', {
        type: Sequelize.TEXT
      })
      .then(() => {
        return queryInterface
          .addColumn('CRApplication', 'rejectRemarks', {
            type: Sequelize.TEXT
          })
          
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CRApplication', 'MOUApproveRemarks').then(() => {
      return queryInterface.removeColumn('CRApplication', 'rejectRemarks');
    });
  }
};
