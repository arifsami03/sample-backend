'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CRAAdditionalInfo', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      CRApplicationId: { type: Sequelize.INTEGER, allowNull: false },
      question: { type: Sequelize.STRING, allowNull: false },
      optionType: { type: Sequelize.STRING, allowNull: false },
      answer: { type: Sequelize.STRING, allowNull: false },
      optionsCSV: { type: Sequelize.STRING, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('CRAAdditionalInfo', ['CRApplicationId'], {
        type: 'foreign key', name: 'FK_CRAAdditionalInfo_CRApplication',
        references: {
          table: 'CRApplication', field: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CRAAdditionalInfo');
  }
};