'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CPDAccount', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      campusId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      bankId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      programDetailId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      branch: {
        type: Sequelize.STRING,
        allowNull: false
      },
      account: {
        type: Sequelize.STRING,
        allowNull: false
      },
      challanType: {
        type: Sequelize.STRING,
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      createdBy: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      updatedBy: {
        type: Sequelize.INTEGER,
        allowNull: true
      }
    }).then(() => {
      return queryInterface.addConstraint('CPDAccount', ['campusId'], {
        type: 'foreign key',
        name: 'FK_CPDAccount_Campus',
        references: {
          table: 'Campus',
          field: 'id'
        }
      }).then(() => {
        return queryInterface.addConstraint('CPDAccount', ['programDetailId'], {
          type: 'foreign key',
          name: 'FK_CPDAccount_ProgramDetail',
          references: {
            table: 'ProgramDetail',
            field: 'id'
          }
        })
      }).then(() => {
        return queryInterface.addConstraint('CPDAccount', ['bankId'], {
          type: 'foreign key',
          name: 'FK_CPDAccount_Bank',
          references: {
            table: 'Bank',
            field: 'id'
          }
        })
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('CPDAccount', 'FK_CPDAccount_ProgramDetail').then(() => {
      return queryInterface.removeConstraint('CPDAccount', 'FK_CPDAccount_Campus').then(() => {
        return queryInterface.removeConstraint('CPDAccount', 'FK_CPDAccount_Bank').then(() => {
          return queryInterface.dropTable('CPDAccount');
        });
      });
    });

  }
};