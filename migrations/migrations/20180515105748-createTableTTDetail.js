'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('TTDetail', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        timeTableId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        slotId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        slotOrder: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        courseId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER
        },
        updatedBy: {
          type: Sequelize.INTEGER
        }
      })
      .then(() => {
        return queryInterface
          .addConstraint('TTDetail', ['timeTableId'], {
            type: 'foreign key',
            name: 'FK_TTDetail_TimeTable',
            references: {
              table: 'TimeTable',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          })

      }).then(() => {
        return queryInterface.addConstraint('TTDetail', ['slotId'], {
          type: 'foreign key',
          name: 'FK_TimeTable_Slot',
          references: {
            table: 'Slot',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
      }).then(() => {
        return queryInterface.addConstraint('TTDetail', ['courseId'], {
          type: 'foreign key',
          name: 'FK_TimeTable_Course',
          references: {
            table: 'Course',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
      });;
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('TTDetail');
  }
};