'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('RoadMapPreRequisiteCourse', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        roadMapId: { type: Sequelize.INTEGER, allowNull: false },
        courseId: { type: Sequelize.INTEGER, allowNull: false },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER },
        updatedBy: { type: Sequelize.INTEGER }
      })
      .then(() => {
        return queryInterface.addConstraint('RoadMapPreRequisiteCourse', ['roadMapId'], {
          type: 'foreign key',
          name: 'FK_RoadMapPreRequisiteCourse_RoadMap',
          references: { table: 'RoadMap', field: 'id' },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('RoadMapPreRequisiteCourse', 'FK_RoadMapPreRequisiteCourse_RoadMap')
      .then(() => {
        return queryInterface.dropTable('RoadMapPreRequisiteCourse');
      });
  }
};
