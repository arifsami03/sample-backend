'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`UPDATE CRMOU set  semSession='fall'`)
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`UPDATE CRMOU set  semSession=''`)
  }
};
