'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('RoadMapCourse', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        roadMapId: { type: Sequelize.INTEGER, allowNull: false },
        courseId: { type: Sequelize.INTEGER, allowNull: false },
        classId: { type: Sequelize.INTEGER, allowNull: false },
        courseCode: { type: Sequelize.INTEGER, allowNull: false },
        natureOfCourse: { type: Sequelize.STRING, allowNull: false },
        courseType: { type: Sequelize.STRING, allowNull: false },
        courseOrder: { type: Sequelize.INTEGER },
        creditHours: { type: Sequelize.INTEGER },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER },
        updatedBy: { type: Sequelize.INTEGER }
      })
      .then(() => {
        return queryInterface
          .addConstraint('RoadMapCourse', ['roadMapId'], {
            type: 'foreign key',
            name: 'FK_RoadMapCourse_RoadMap',
            references: { table: 'RoadMap', field: 'id' },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          })
          .then(() => {
            return queryInterface.addConstraint('RoadMapCourse', ['courseId'], {
              type: 'foreign key',
              name: 'FK_RoadMapCourse_Course',
              references: { table: 'Course', field: 'id' },
              onDelete: 'cascade',
              onUpdate: 'cascade'
            });
          })
          .then(() => {
            return queryInterface.addConstraint('RoadMapCourse', ['classId'], {
              type: 'foreign key',
              name: 'FK_RoadMapCourse_Classes',
              references: { table: 'Classes', field: 'id' },
              onDelete: 'cascade',
              onUpdate: 'cascade'
            });
          });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('RoadMapCourse', 'FK_RoadMapCourse_RoadMap').then(() => {
      return queryInterface.removeConstraint('RoadMapCourse', 'FK_RoadMapCourse_Course').then(() => {
        return queryInterface.removeConstraint('RoadMapCourse', 'FK_RoadMapCourse_Classes').then(() => {
          return queryInterface.dropTable('RoadMapCourse');
        });
      });
    });
  }
};
