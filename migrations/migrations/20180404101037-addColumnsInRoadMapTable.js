'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('RoadMap', 'courseOrder', {
        type: Sequelize.INTEGER
      })
      .then(() => {
        return queryInterface.addColumn('RoadMap', 'creditHours', {
          type: Sequelize.INTEGER
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('RoadMap', 'courseOrder').then(() => {
      return queryInterface.removeColumn('RoadMap', 'creditHours');
    });
  }
};
