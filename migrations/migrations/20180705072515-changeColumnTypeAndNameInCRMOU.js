'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn('CRMOU','academicMonth','semSession').then(()=>{
      return queryInterface.changeColumn('CRMOU','semSession',{
        type: Sequelize.STRING,
      })
    })
   
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn('CRMOU','semSession','academicMonth').then(()=>{
      return queryInterface.changeColumn('CRMOU','academicMonth',{
        type: Sequelize.INTEGER,
      })
    })
  }
};
