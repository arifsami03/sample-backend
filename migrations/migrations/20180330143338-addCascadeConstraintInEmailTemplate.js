'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('EmailTemplate', 'FK_EmailTemplate_OutgoingMailServer').then(() => {
      return queryInterface.addConstraint('EmailTemplate', ['outgoingMailServerId'], {
        type: 'foreign key',
        name: 'FK_EmailTemplate_OutgoingMailServer',
        references: {
          table: 'OutgoingMailServer',
          field: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('EmailTemplate', 'FK_EmailTemplate_OutgoingMailServer').then(() => {
      return queryInterface.addConstraint('EmailTemplate', ['outgoingMailServerId'], {
        type: 'foreign key',
        name: 'FK_EmailTemplate_OutgoingMailServer',
        references: {
          table: 'OutgoingMailServer',
          field: 'id'
        },
      });
    });
  }
};