'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('EligibilityCriteria', 'academicMonth', {
      type: Sequelize.INTEGER,
      allowNull: false
    }).then(() => {
      return queryInterface.addColumn('EligibilityCriteria', 'academicYear', {
        type: Sequelize.INTEGER,
        allowNull: false
      })
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('EligibilityCriteria', 'academicMonth').then(() => {
      return queryInterface.removeColumn('EligibilityCriteria', 'academicYear')
    })
  }
};
