'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ConcessionFeeHead', null, {}).then(() => {
      return queryInterface.addColumn('ConcessionFeeHead', 'concessionAmount', {
        type: Sequelize.FLOAT
      })
    });

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('ConcessionFeeHead', 'concessionAmount');
  }
};
