'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('CampusAcademicCalendar', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        campusId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        academicCalendarProgramDetailId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        startDate: {
          type: Sequelize.DATE,
          allowNull: false
        },
        endDate: {
          type: Sequelize.DATE,
          allowNull: false
        },
        status: {
          type: Sequelize.BOOLEAN,
          defaultValue: true

        },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER },
        updatedBy: { type: Sequelize.INTEGER }
      })
      .then(() => {
        return queryInterface
          .addConstraint('CampusAcademicCalendar', ['campusId'], {
            type: 'foreign key',
            name: 'FK_CampusAcademicCalendar_Campus',
            references: {
              table: 'Campus',
              field: 'id'
            },
      
          })
          .then(() => {
            return queryInterface.addConstraint('CampusAcademicCalendar', ['academicCalendarProgramDetailId'], {
              type: 'foreign key',
              name: 'FK_CampusAcademicCalendar_AcademicCalendarProgramDetail',
              references: {
                table: 'AcademicCalendarProgramDetail',
                field: 'id'
              },
            });
          });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CampusAcademicCalendar');
  }
};
