'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CampusDefaultUser', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      userName: { type: Sequelize.STRING, allowNull: false },
      abbreviation: { type: Sequelize.STRING, allowNull: false },
      roleId: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('CampusDefaultUser', ['roleId'], {
        type: 'foreign key', name: 'FK_CampusDefaultUser_Role',
        references: { table: 'Role', field: 'id' },
        onDelete: 'cascade', onUpdate: 'cascade'
      })
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CampusDefaultUser');
  }

}