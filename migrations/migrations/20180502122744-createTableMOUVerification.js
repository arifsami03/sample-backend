'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CRMOUVerification', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      CRApplicationId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      instrumentTypeId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      currentDate: {
        type: Sequelize.DATE,
        allowNull: false
      },
      instrumentDate: {
        type: Sequelize.DATE,
        allowNull: false
      },
      instrumentNumber: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      bankId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      bankName: {
        type: Sequelize.STRING,
        allowNull: false
      },
      amount: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      status: {
        type: Sequelize.STRING,
        allowNull: false
      },
      account: {
        type: Sequelize.STRING,
        allowNull: false
      },
      purpose: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      remarks: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      createdBy: {
        type: Sequelize.INTEGER
      },
      updatedBy: {
        type: Sequelize.INTEGER
      }

    }).then(() => {
      return queryInterface.addConstraint('CRMOUVerification', ['CRApplicationId'], {
        type: 'foreign key',
        name: 'FK_CRMOUVerification_CRApplication',
        references: {
          table: 'CRApplication',
          field: 'id'
        }
      })
    }).then(() => {
      return queryInterface.addConstraint('CRMOUVerification', ['instrumentTypeId'], {
        type: 'foreign key',
        name: 'FK_CRMOUVerification_Configuration',
        references: {
          table: 'Configuration',
          field: 'id'
        }
      })
    }).then(() => {
      return queryInterface.addConstraint('CRMOUVerification', ['bankId'], {
        type: 'foreign key',
        name: 'FK_CRMOUVerification_Bank',
        references: {
          table: 'Bank',
          field: 'id'
        }
      })
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('CRMOUVerification', 'FK_CRMOUVerification_CRApplication').then(() => {
      return queryInterface.removeConstraint('CRMOUVerification', 'FK_CRMOUVerification_Configuration').then(() => {
        return queryInterface.removeConstraint('CRMOUVerification', 'FK_CRMOUVerification_Bank').then(() => {
          return queryInterface.dropTable('CRMOUVerification');
        })
      })
    });
  }
};