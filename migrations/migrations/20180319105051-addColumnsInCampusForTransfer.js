'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('CRApplication', 'website', {
      type: Sequelize.STRING
    }).then(() => {
      return queryInterface.addColumn('CRApplication', 'officialEmail', {
        type: Sequelize.STRING
      }).then(() => {
        return queryInterface.addColumn('CRApplication', 'establishedSince', {
          type: Sequelize.STRING
        })
      }).then(() => {
        return queryInterface.addColumn('CRApplication', 'noOfCampusTransfer', {
          type: Sequelize.INTEGER
        })
      })
    })

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CRApplication', 'establishedSince').then(() => {
      return queryInterface.removeColumn('CRApplication', 'officialEmail').then(() => {
        return queryInterface.removeColumn('CRApplication', 'website').then(() => {
          return queryInterface.removeColumn('CRApplication', 'noOfCampusTransfer')
        })
      })
    })


  }
};