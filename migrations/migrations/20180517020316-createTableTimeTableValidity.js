'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('TimeTableValidity', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        timeTableId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        validFrom: {
          type: Sequelize.DATE,
          allowNull: false
        },
        validTo: {
          type: Sequelize.DATE,
          allowNull: false
        },
        

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER
        },
        updatedBy: {
          type: Sequelize.INTEGER
        }
      })
      .then(() => {
        return queryInterface
          .addConstraint('TimeTableValidity', ['timeTableId'], {
            type: 'foreign key',
            name: 'FK_TimeTableValidity_TimeTable',
            references: {
              table: 'TimeTable',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          })

      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('TimeTableValidity');
  }
};