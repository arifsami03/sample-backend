'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('CRRemittance', {
        id: { type: Sequelize.INTEGER, primaryKey: true },
        instrumentCategory: {
          type: Sequelize.STRING
        },
        currentDate: {
          type: Sequelize.DATE
        },
        bankId: {
          type: Sequelize.INTEGER
        },
        branchName: {
          type: Sequelize.STRING
        },
        instrumentDate: {
          type: Sequelize.DATE
        },
        instrumentNumber: {
          type: Sequelize.INTEGER
        },
        amount: {
          type: Sequelize.FLOAT
        },
        purpose: {
          type: Sequelize.STRING
        },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER, allowNull: true },
        updatedBy: { type: Sequelize.INTEGER, allowNull: true }
      })
      .then(() => {
        return queryInterface.addConstraint('CRRemittance', ['id'], {
          type: 'foreign key',
          name: 'FK_CRRemittance_CRApplication',
          references: {
            table: 'CRApplication',
            field: 'id'
          }
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('CRRemittance', 'FK_CRRemittance_CRApplication')
      .then(() => {
        return queryInterface.dropTable('CRRemittance');
      });
  }
};
