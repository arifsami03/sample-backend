'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('CRRemittance', 'amount', {
      type: Sequelize.FLOAT,
      allowNull: true
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('CRRemittance', 'amount', {
      type: Sequelize.INTEGER
    });
  }
};
