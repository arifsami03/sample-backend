'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('CRAvailableBuilding', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        CRApplicationId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: false
        },
        buildingOwn: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false
        },
        rentAgreementUpTo: {
          type: Sequelize.STRING,
          allowNull: true
        },
        address: {
          type: Sequelize.STRING,
          allowNull: false,
          defaultValue: false
        },
        latitude: { type: Sequelize.FLOAT, allowNull: true },
        longitude: { type: Sequelize.FLOAT, allowNull: true },
        coverdArea: { type: Sequelize.FLOAT, allowNull: true },
        openArea: { type: Sequelize.FLOAT, allowNull: true },
        totalArea: { type: Sequelize.FLOAT, allowNull: true },
        roomsQuantity: { type: Sequelize.INTEGER, allowNull: true },
        washroomsQuantity: { type: Sequelize.INTEGER, allowNull: true },
        teachingStaffQuantity: { type: Sequelize.INTEGER, allowNull: true },
        labsQuantity: { type: Sequelize.INTEGER, allowNull: true },
        nonTeachingStaffQty: { type: Sequelize.INTEGER, allowNull: true },
        studentsQuantity: { type: Sequelize.INTEGER, allowNull: true },
        playGround: { type: Sequelize.BOOLEAN, allowNull: true },
        swimmingPool: { type: Sequelize.BOOLEAN, allowNull: true },
        healthClinic: { type: Sequelize.BOOLEAN, allowNull: true },

        mosque: { type: Sequelize.BOOLEAN, allowNull: true },
        cafeteria: { type: Sequelize.BOOLEAN, allowNull: true },
        transport: { type: Sequelize.BOOLEAN, allowNull: true },
        library: { type: Sequelize.BOOLEAN, allowNull: true },
        bankBranch: { type: Sequelize.BOOLEAN, allowNull: true },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER, allowNull: true },
        updatedBy: { type: Sequelize.INTEGER, allowNull: true }
      })
      .then(() => {
        return queryInterface.addConstraint(
          'CRAvailableBuilding',
          ['CRApplicationId'],
          {
            type: 'foreign key',
            name: 'FK_CRAvailableBuilding_CRApplication',
            references: {
              table: 'CRApplication',
              field: 'id'
            }
          }
        );
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint(
        'CRAvailableBuilding',
        'FK_CRAvailableBuilding_CRApplication'
      )
      .then(() => {
        return queryInterface.dropTable('CRAvailableBuilding');
      });
  }
};
