'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('Vendor', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        name: { type: Sequelize.STRING,  allowNull: false },
        website: { type: Sequelize.STRING, allowNull: false },
        mobileNumber: {type: Sequelize.STRING,allowNull: false },
        address: {type: Sequelize.STRING, allowNull: false },
        email: {type: Sequelize.STRING, allowNull: false },
        contactPerson: {type: Sequelize.STRING, allowNull: false },
        salesTaxNumber: {type: Sequelize.INTEGER, allowNull: false },
        faxNumber: {type: Sequelize.INTEGER, allowNull: false },
        ntnNumber: {type: Sequelize.INTEGER, allowNull: false },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER },
        updatedBy: { type: Sequelize.INTEGER }
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Vendor');
  }
};
