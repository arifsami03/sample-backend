'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    
    return queryInterface.removeColumn('CRApplication', 'tentativeSessionStart').then(() => {
      
      return queryInterface.addColumn('CRApplication', 'tentativeSessionStart', {
        type: Sequelize.DATE,
        allowNull: true
      })

    })
    
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CRApplication', 'tentativeSessionStart').then(() => {
      
      return queryInterface.addColumn('CRApplication', 'tentativeSessionStart', {
        type: Sequelize.STRING,
        allowNull: true
      })

    })
  }
};