'use strict';

module.exports = {
  /**
   * FS stands for Fee Structure
   */
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('FSInstallement', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      feeStructureId: { type: Sequelize.INTEGER, allowNull: false },
      title: { type: Sequelize.STRING, allowNull: false },
      dueDate: { type: Sequelize.DATE, allowNull: false },
      expiryDate: { type: Sequelize.DATE, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('FSInstallement', ['feeStructureId'], {
        type: 'foreign key', name: 'FK_FSInstallement_FeeStructure',
        references: { table: 'FeeStructure', field: 'id' },
        onDelete: 'cascade', onUpdate: 'cascade'
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('FSInstallement');
  }

}