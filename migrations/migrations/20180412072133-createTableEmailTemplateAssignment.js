'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('EmailTemplateAssignment', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        parent: {
          type: Sequelize.STRING,
          allowNull: false
        },
        parentId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        emailTemplateId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER },
        updatedBy: { type: Sequelize.INTEGER }
      })
      .then(() => {
        return queryInterface.addConstraint('EmailTemplateAssignment', ['emailTemplateId'], {
          type: 'foreign key',
          name: 'FK_EmailTemplateAssignment_EmailTemplate',
          references: { table: 'EmailTemplate', field: 'id' }
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('EmailTemplateAssignment', 'FK_EmailTemplateAssignment_EmailTemplate').then(() => {
      return queryInterface.dropTable('EmailTemplateAssignment');
    });
  }
};
