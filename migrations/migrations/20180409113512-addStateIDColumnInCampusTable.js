'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    
    return queryInterface.addColumn('CRApplication', 'stateId', {
      type: Sequelize.INTEGER,
      allowNull: false
    });

  },

  down: (queryInterface, Sequelize) => {
       
        return queryInterface.removeColumn('CRApplication', 'stateId')
      
  }
};