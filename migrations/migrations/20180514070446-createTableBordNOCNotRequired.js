'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('BoardNOCNotRequired', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      boardId: { type: Sequelize.INTEGER, allowNull: false },
      boardNOCNotRequiredId: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('BoardNOCNotRequired', ['boardId'], {
        type: 'foreign key', name: 'FK_BoardNOCNotRequired_Board',
        references: { table: 'Board', field: 'id' }
      }).then(() => {
        return queryInterface.addConstraint('BoardNOCNotRequired', ['boardNOCNotRequiredId'], {
          type: 'foreign key', name: 'FK_BoardNOCNotRequired_BoardNOCNotRequired',
          references: { table: 'Board', field: 'id' }
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('BoardNOCNotRequired');
  }

}