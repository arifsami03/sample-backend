'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('CRApplication', 'status', {
      type: Sequelize.STRING(20)
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CRApplication', 'status');
  }
};
