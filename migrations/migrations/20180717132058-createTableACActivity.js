'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('ACActivity', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        academicCalendarId: {
          type: Sequelize.INTEGER
        },
        activityId: {
          type: Sequelize.INTEGER
        },
        deleted: { type: Sequelize.BOOLEAN, allowNull: false, defaultValue: false },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER },
        updatedBy: { type: Sequelize.INTEGER }
      })
      .then(() => {
        return queryInterface
          .addConstraint('ACActivity', ['academicCalendarId'], {
            type: 'foreign key',
            name: 'FK_ACActivity_AcademicCalendar',
            references: {
              table: 'AcademicCalendar',
              field: 'id'
            }
          })
          .then(() => {
            return queryInterface.addConstraint('ACActivity', ['activityId'], {
              type: 'foreign key',
              name: 'FK_ACActivity_Activity',
              references: {
                table: 'Activity',
                field: 'id'
              }
            });
          });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ACActivity');
  }
};
