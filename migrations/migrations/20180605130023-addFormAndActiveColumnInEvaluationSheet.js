'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('EvaluationSheet', 'ESForm', {
      type: Sequelize.STRING,
    }).then(() => {
      return queryInterface.addColumn('EvaluationSheet', 'active', {
        type: Sequelize.BOOLEAN,
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('EvaluationSheet', 'active').then(() => {
      return queryInterface.removeColumn('EvaluationSheet', 'ESForm');
    });
  }
};
