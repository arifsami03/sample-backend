'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('CRMOUVerification', 'bankName', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('CRMOUVerification', 'bankName', {
      type: Sequelize.STRING,
      allowNull: false
    });
  }
};
