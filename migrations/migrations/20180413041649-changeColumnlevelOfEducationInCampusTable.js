'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CRApplication', 'levelOfEducation').then(() => {
      return queryInterface.addColumn('CRApplication', 'educationLevelId', {
        type: Sequelize.INTEGER, allowNull: false, defaultValue: false
      });

    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CRApplication', 'educationLevelId').then(() => {
      return queryInterface.addColumn('CRApplication', 'levelOfEducation', {
        type: Sequelize.STRING, allowNull: true
      });
    });
  }
};
