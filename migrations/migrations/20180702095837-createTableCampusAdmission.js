'use strict';

module.exports = {
  /**
   * FSI stands for Fee Structure Installement
   */
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CampusAdmission', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      campusId: { type: Sequelize.INTEGER, allowNull: false },
      admissionMonth: { type: Sequelize.INTEGER, allowNull: false },
      admissionYear: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('CampusAdmission', ['campusId'], {
        type: 'foreign key', name: 'FK_CampusAdmission_Campus',
        references: { table: 'Campus', field: 'id' },
        onDelete: 'cascade', onUpdate: 'cascade'
      })
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CampusAdmission');
  }

}