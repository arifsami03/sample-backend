'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('FS_InstallmentPlan', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      feeStructureId: { type: Sequelize.INTEGER, allowNull: false },
      title: { type: Sequelize.STRING, allowNull: false },
      dueDayOfMonth: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('FS_InstallmentPlan', ['feeStructureId'], {
        type: 'foreign key', name: 'FK_FS_InstallmentPlan_FeeStructure',
        references: {
          table: 'FeeStructure', field: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('FS_InstallmentPlan');
  }
};