'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Document', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      docFor: { type: Sequelize.STRING },
      title: { type: Sequelize.STRING },
      docType: { type: Sequelize.STRING },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Document');
  }
};