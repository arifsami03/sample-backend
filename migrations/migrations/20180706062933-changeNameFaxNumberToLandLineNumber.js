'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn('Campus','faxNumber','landLineNumber').then(()=>{
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn('Campus','landLineNumber','faxNumber').then(()=>{
    })
  }
};
