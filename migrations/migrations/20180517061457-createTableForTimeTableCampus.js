'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('TimeTableCampus', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        timeTableValidityId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        campusId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER
        },
        updatedBy: {
          type: Sequelize.INTEGER
        }
      })
      .then(() => {
        return queryInterface
          .addConstraint('TimeTableCampus', ['timeTableValidityId'], {
            type: 'foreign key',
            name: 'FK_TimeTableCampus_TimeTableValidity',
            references: {
              table: 'TimeTableValidity',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          })

      })
      .then(() => {
        return queryInterface
          .addConstraint('TimeTableCampus', ['campusId'], {
            type: 'foreign key',
            name: 'FK_TimeTableCampus_Campus',
            references: {
              table: 'Campus',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          })

      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('TimeTableCampus');
  }
};