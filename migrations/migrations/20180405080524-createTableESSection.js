'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ESSection', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      title: { type: Sequelize.STRING, allowNull: false },
      ESId: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('ESSection', ['ESId'], {
        type: 'foreign key', name: 'FK_ESSection_EvaluationSheet',
        references: { table: 'EvaluationSheet', field: 'id' }
      })
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('ESSection', 'FK_ESSection_EvaluationSheet').then(() => {
      return queryInterface.dropTable('ESSection');
    });
  }
};