'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('FSCFeeHead', 'managementFeeAmount', {
      type: Sequelize.FLOAT,
      allowNull: false
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('FSCFeeHead', 'managementFeeAmount', {
      type: Sequelize.INTEGER,
      allowNull: false
    })
  }
};
