'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('CRMOU', null, {}).then(() => {
      return queryInterface.removeConstraint('CRMOU', 'FK_CRMOU_AcademicCalendar').then(() => {
        return queryInterface.removeColumn('CRMOU', 'academicYearId')
      });
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('CRMOU', 'academicYearId', {
      type: Sequelize.INTEGER,
      allowNull: false
    }).then(() => {
      return queryInterface.addConstraint('CRMOU', ['academicYearId'], {
        type: 'foreign key',
        name: 'FK_CRMOU_AcademicCalendar',
        references: {
          table: 'AcademicCalendar',
          field: 'id'
        }
      })
    });
  }
};
