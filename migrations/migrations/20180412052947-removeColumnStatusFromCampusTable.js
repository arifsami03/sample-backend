'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CRApplication', 'status');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('CRApplication', 'status', {
        type: Sequelize.STRING
      });
      
  }
};
