'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('RoadMap', 'FK_RoadMap_ProgramDetail').then(() => {
      return queryInterface.removeConstraint('RoadMap', 'FK_RoadMap_AcademicCalendar').then(() => {
        return queryInterface.removeConstraint('RoadMap', 'FK_RoadMap_Course').then(() => {
          return queryInterface.removeConstraint('RoadMap', 'FK_RoadMap_Classes').then(() => {
            return queryInterface.dropTable('RoadMap');
          });
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('RoadMap', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        title: {
          type: Sequelize.STRING,
          allowNull: false
        },
        programDetailId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        academicCalendarId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        courseId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        classId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        courseCode: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        natureOfCourse: {
          type: Sequelize.STRING,
          allowNull: false
        },
        courseType: {
          type: Sequelize.STRING,
          allowNull: false
        },
        courseOrder: {
          type: Sequelize.INTEGER,
        },
        creditHours: {
          type: Sequelize.INTEGER,
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER
        },
        updatedBy: {
          type: Sequelize.INTEGER
        }
      })
      .then(() => {
        return queryInterface
          .addConstraint('RoadMap', ['programDetailId'], {
            type: 'foreign key',
            name: 'FK_RoadMap_ProgramDetail',
            references: {
              table: 'ProgramDetail',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          })
          .then(() => {
            return queryInterface.addConstraint('RoadMap', ['academicCalendarId'], {
              type: 'foreign key',
              name: 'FK_RoadMap_AcademicCalendar',
              references: {
                table: 'AcademicCalendar',
                field: 'id'
              },
              onDelete: 'cascade',
              onUpdate: 'cascade'
            });
          })
          .then(() => {
            return queryInterface.addConstraint('RoadMap', ['courseId'], {
              type: 'foreign key',
              name: 'FK_RoadMap_Course',
              references: {
                table: 'Course',
                field: 'id'
              },
              onDelete: 'cascade',
              onUpdate: 'cascade'
            });
          })
          .then(() => {
            return queryInterface.addConstraint('RoadMap', ['classId'], {
              type: 'foreign key',
              name: 'FK_RoadMap_Classes',
              references: {
                table: 'Classes',
                field: 'id'
              },
              onDelete: 'cascade',
              onUpdate: 'cascade'
            });
          });
      });
  }
}