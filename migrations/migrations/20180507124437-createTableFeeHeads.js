'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('FeeHeads', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      code: { type: Sequelize.STRING, allowNull: false },
      title: { type: Sequelize.STRING, allowNull: false },
      abbreviation: { type: Sequelize.STRING, allowNull: false },
      natureOfFeeHeadsId: { type: Sequelize.INTEGER, allowNull: false }, // TODO: high need to disucss can make relation with configuration table
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER, allowNull: true },
      updatedBy: { type: Sequelize.INTEGER, allowNull: true }
    }).then(() => {
      return queryInterface.addConstraint('FeeHeads', ['code'], {
        type: 'unique',
        name: 'Unique_FeeHeads_code'
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('FeeHeads');
  }
};