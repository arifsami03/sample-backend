'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ECPassingYear', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      passingYearId: { type: Sequelize.INTEGER, allowNull: false },
      eligibilityCriteriaId: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('ECPassingYear', ['passingYearId'], {
        type: 'foreign key', name: 'FK_ECPassingYear_PassingYear',
        references: { table: 'PassingYear', field: 'id' }
      }).then(() => {
        return queryInterface.addConstraint('ECPassingYear', ['eligibilityCriteriaId'], {
          type: 'foreign key', name: 'FK_ECPassingYear_EligibilityCriteria',
          references: { table: 'EligibilityCriteria', field: 'id' }
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('ECPassingYear', 'FK_ECPassingYear_EligibilityCriteria').then(() => {
      return queryInterface.removeConstraint('ECPassingYear', 'FK_ECPassingYear_PassingYear').then(() => {
        return queryInterface.dropTable('ECPassingYear');
      });
    });
  }
};