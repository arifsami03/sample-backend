'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    // As cityId in Campus table does not allow null values so first we have to change it then set this
    // column to null as our configuration table is newly created and there is no data to reference.
    return queryInterface.changeColumn('CRApplication', 'cityId', {
      type: Sequelize.INTEGER, allowNull: true
    }).then(() => {
      return queryInterface.sequelize.query(`UPDATE CRApplication SET CRApplication.cityId = NULL;`).then(() => {
        return queryInterface.addConstraint('CRApplication', ['cityId'], {
          type: 'foreign key', name: 'FK_Application_Configuration',
          references: { table: 'Configuration', field: 'id' }
        });
      });
    });

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('CRApplication', 'FK_Application_Configuration').then(() => {
      return true;
      // return queryInterface.changeColumn('CRApplication', 'cityId', {
      //   type: Sequelize.INTEGER,
      //   allowNull: false,
      //   defaultValue: false
      // });
    });
  }
};
