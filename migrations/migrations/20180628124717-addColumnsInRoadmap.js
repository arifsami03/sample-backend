'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Roadmap', 'category', {
      type: Sequelize.STRING,
    }).then(() => {
      return queryInterface.addColumn('Roadmap', 'academicYear', {
        type: Sequelize.INTEGER,
      })
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Roadmap', 'category').then(() => {

      return queryInterface.removeColumn('Roadmap', 'academicYear')

    })

  }
};
