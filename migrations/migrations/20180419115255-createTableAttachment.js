'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Attachment', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      parent: { type: Sequelize.STRING, allowNull: false },
      parentId: { type: Sequelize.INTEGER, allowNull: false },
      title: { type: Sequelize.STRING, allowNull: false },
      description: { type: Sequelize.STRING },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Attachment');
  }
};
