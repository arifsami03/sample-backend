'use strict';

module.exports = {
  /**
   * CBF stands for campus building floor
   */
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CBFRoom', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      CBFloorId: { type: Sequelize.INTEGER, allowNull: false },
      name: { type: Sequelize.STRING, allowNull: false },
      abbreviation: { type: Sequelize.STRING, allowNull: false },
      code: { type: Sequelize.STRING, allowNull: false },
      seatingCapability: { type: Sequelize.INTEGER, allowNull: false },
      classroomType: { type: Sequelize.STRING, allowNull: true },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('CBFRoom', ['CBFloorId'], {
        type: 'foreign key', name: 'FK_CBFRoom_CBFloor',
        references: { table: 'CBFloor', field: 'id' }
      }).then(() => {
        return queryInterface.addConstraint('CBFRoom', ['code'], {
          type: 'unique',
          name: 'Unique_CBFRoom_code'
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CBFRoom');
  }

}