'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Scholarship', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      title: {
        type: Sequelize.STRING,
        allowNull: false
      },
      abbreviation: {
        type: Sequelize.STRING,
        allowNull: false
      },
      type: {
        type: Sequelize.STRING,
        allowNull: false
      },
      percentage: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Scholarship');
  }
};
