'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('CTTDSection', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        CTTDetailId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        sectionId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER
        },
        updatedBy: {
          type: Sequelize.INTEGER
        }
      })
      .then(() => {
        return queryInterface
          .addConstraint('CTTDSection', ['CTTDetailId'], {
            type: 'foreign key',
            name: 'FK_CTTDSection_CTTDetail',
            references: {
              table: 'CTTDetail',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          })

      })
      // .then(() => {
      //   return queryInterface.addConstraint('CTTDSection', ['sectionId'], {
      //     type: 'foreign key',
      //     name: 'FK_TimeTable_Section',
      //     references: {
      //       table: 'Section',
      //       field: 'id'
      //     },
      //     onDelete: 'cascade',
      //     onUpdate: 'cascade'
      //   });
      // })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CTTDSection');
  }
};