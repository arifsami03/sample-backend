'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('TTDDay', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        TTDetailId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        dayId: {
          type: Sequelize.STRING,
          allowNull: false
        },
        

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER
        },
        updatedBy: {
          type: Sequelize.INTEGER
        }
      })
      .then(() => {
        return queryInterface
          .addConstraint('TTDDay', ['TTDetailId'], {
            type: 'foreign key',
            name: 'FK_TTDDay_TTDetail',
            references: {
              table: 'TTDetail',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          })

      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('TTDDay');
  }
};