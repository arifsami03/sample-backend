'use strict';

module.exports = {
  /**
   * CB stands for campus building
   */
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CBFloor', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      CBId: { type: Sequelize.INTEGER, allowNull: false },
      name: { type: Sequelize.STRING, allowNull: false },
      abbreviation: { type: Sequelize.STRING, allowNull: false },
      code: { type: Sequelize.STRING, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('CBFloor', ['CBId'], {
        type: 'foreign key', name: 'FK_CBFloor_CampusBuilding',
        references: { table: 'CampusBuilding', field: 'id' }
      }).then(() => {
        return queryInterface.addConstraint('CBFloor', ['code'], {
          type: 'unique',
          name: 'Unique_CBFloor_code'
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CBFloor');
  }

}