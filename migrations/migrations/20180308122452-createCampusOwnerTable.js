'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CRApplicant', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      fullName: {
        type: Sequelize.STRING(55),
        allowNull: false,
        defaultValue: false
      },
      mobileNumber: { type: Sequelize.STRING(15), allowNull: true },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: false
      },
      applicationType: {
        type: Sequelize.STRING(55),
        allowNull: false,
        defaultValue: false
      },
      ntn: { type: Sequelize.STRING(20), allowNull: true },
      countryId: { type: Sequelize.INTEGER, allowNull: true },
      provinceId: { type: Sequelize.INTEGER, allowNull: true },
      cityId: { type: Sequelize.INTEGER, allowNull: true },
      tehsilId: { type: Sequelize.INTEGER, allowNull: true },
      natureOfWorkId: { type: Sequelize.INTEGER, allowNull: true },
      approxMonthlyIncome: { type: Sequelize.FLOAT, allowNull: true },
      nearestBankId: { type: Sequelize.INTEGER, allowNull: true },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER, allowNull: true },
      updatedBy: { type: Sequelize.INTEGER, allowNull: true }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CRApplicant');
  }
};
