'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('CRMOU', 'academicMonth', {
      type: Sequelize.INTEGER,
      allowNull: false
    }).then(() => {
      return queryInterface.addColumn('CRMOU', 'academicYear', {
        type: Sequelize.INTEGER,
        allowNull: false
      })
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CRMOU', 'academicMonth').then(() => {
      return queryInterface.removeColumn('CRMOU', 'academicYear')
    })
  }
};
