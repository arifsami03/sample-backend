'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('AcademicCalendarProgramDetail', 'FK_AcademicCalendarProgramDetail_AcademicCalendar')
      .then(() => {
        return queryInterface.addConstraint('AcademicCalendarProgramDetail', ['academicCalendarId'], {
          type: 'foreign key',
          name: 'FK_AcademicCalendarProgramDetail_AcademicCalendar',
          references: {
            table: 'AcademicCalendar',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
      })
      .then(() => {
        return queryInterface.removeConstraint('AcademicCalendarProgramDetail', 'FK_AcademicCalendarProgramDetail_ProgramDetail').then(() => {
          return queryInterface.addConstraint('AcademicCalendarProgramDetail', ['programDetailId'], {
            type: 'foreign key',
            name: 'FK_AcademicCalendarProgramDetail_ProgramDetail',
            references: {
              table: 'ProgramDetail',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          });
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('AcademicCalendarProgramDetail', 'FK_AcademicCalendarProgramDetail_AcademicCalendar').then(() => {
      return queryInterface.removeConstraint('AcademicCalendarProgramDetail', 'FK_AcademicCalendarProgramDetail_ProgramDetail');
    });
  }
};
