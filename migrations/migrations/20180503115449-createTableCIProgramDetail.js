'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CampusProgramDetail', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      campusProgramId: { type: Sequelize.INTEGER, allowNull: false },
      programDetailId: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER, allowNull: true },
      updatedBy: { type: Sequelize.INTEGER, allowNull: true }
    }).then(() => {
      return queryInterface.addConstraint('CampusProgramDetail', ['campusProgramId'], {
        type: 'foreign key', name: 'FK_CIProgramDetail_CIProgram',
        references: { table: 'CampusProgram', field: 'id' }
      }).then(() => {
        return queryInterface.addConstraint('CampusProgramDetail', ['programDetailId'], {
          type: 'foreign key', name: 'FK_CIProgramDetail_ProgramDetail',
          references: { table: 'ProgramDetail', field: 'id' }
        })
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('CampusProgramDetail', 'FK_CIProgramDetail_ProgramDetail').then(() => {
      return queryInterface.removeConstraint('CampusProgramDetail', 'FK_CIProgramDetail_CIProgram').then(() => {
        return queryInterface.dropTable('CampusProgramDetail');
      });
    });

  }
};