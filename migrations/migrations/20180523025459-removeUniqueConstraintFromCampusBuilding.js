'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('CampusBuilding', 'Unique_CampusBuilding_code');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addConstraint('CampusBuilding', ['code'], {
      type: 'unique',
      name: 'Unique_CampusBuilding_code'
    });
  }
};
