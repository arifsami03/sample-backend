'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Institute', [
      { 
        name: 'Idrak',
        logo: '',
        abbreviation: 'IDR',
        website: 'www.idrak.edu.pk', 
        officeEmail: 'info@idrak.com', 
        headOfficeLocation: 'Phase 4, Johar town lahore', 
        latitude: '31.481106733193347', 
        longitude: '74.41345557161969', 
        establishedScince:'2017', 
        contactNo: '', 
        affiliation: '', 
        focalPerson: '',
        Description: '', 
        createdAt: new Date(), 
        updatedAt: new Date() }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Institute', null, {});
  }
};
