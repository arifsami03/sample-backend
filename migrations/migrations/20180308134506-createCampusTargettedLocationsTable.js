'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('CRTargettedLocations', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        CRApplicationId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: false
        },
        address: {
          type: Sequelize.STRING,
          allowNull: false,
          defaultValue: false
        },
        latitude: { type: Sequelize.FLOAT, allowNull: true },
        longitude: { type: Sequelize.FLOAT, allowNull: true },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER, allowNull: true },
        updatedBy: { type: Sequelize.INTEGER, allowNull: true }
      })
      .then(() => {
        return queryInterface.addConstraint(
          'CRTargettedLocations',
          ['CRApplicationId'],
          {
            type: 'foreign key',
            name: 'FK_CRTargettedLocations_CRApplication',
            references: {
              table: 'CRApplication',
              field: 'id'
            }
          }
        );
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint(
        'CRTargettedLocations',
        'FK_CRTargettedLocations_CRApplication'
      )
      .then(() => {
        return queryInterface.dropTable('CRTargettedLocations');
      });
  }
};
