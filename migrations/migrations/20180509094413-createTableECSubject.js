'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ECSubject', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      courseId: { type: Sequelize.INTEGER, allowNull: false },
      eligibilityCriteriaId: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('ECSubject', ['courseId'], {
        type: 'foreign key', name: 'FK_ECSubject_Course',
        references: { table: 'Course', field: 'id' }
      }).then(() => {
        return queryInterface.addConstraint('ECSubject', ['eligibilityCriteriaId'], {
          type: 'foreign key', name: 'FK_ECSubject_EligibilityCriteria',
          references: { table: 'EligibilityCriteria', field: 'id' }
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('ECSubject', 'FK_ECSubject_EligibilityCriteria').then(() => {
      return queryInterface.removeConstraint('ECSubject', 'FK_ECSubject_Course').then(() => {
        return queryInterface.dropTable('ECSubject');
      });
    });
  }
};