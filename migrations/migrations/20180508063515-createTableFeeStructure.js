'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('FeeStructure', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      title: { type: Sequelize.STRING, allowNull: false },
      programId: { type: Sequelize.INTEGER, allowNull: false },
      programDetailId: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('FeeStructure', ['programId'], {
        type: 'foreign key', name: 'FK_FeeStructure_Program',
        references: { table: 'Program', field: 'id' }
      }).then(() => {
        return queryInterface.addConstraint('FeeStructure', ['programDetailId'], {
          type: 'foreign key', name: 'FK_FeeStructure_ProgramDetail',
          references: { table: 'ProgramDetail', field: 'id' }
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('FeeStructure', 'FK_FeeStructure_ProgramDetail').then(() => {
      return queryInterface.removeConstraint('FeeStructure', 'FK_FeeStructure_Program').then(() => {
        return queryInterface.dropTable('FeeStructure');
      });
    });
  }
};