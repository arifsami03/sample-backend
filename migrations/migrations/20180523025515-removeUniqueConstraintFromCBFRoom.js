'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('CBFRoom', 'Unique_CBFRoom_code');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addConstraint('CBFRoom', ['code'], {
      type: 'unique',
      name: 'Unique_CBFRoom_code'
    });
  }
};
