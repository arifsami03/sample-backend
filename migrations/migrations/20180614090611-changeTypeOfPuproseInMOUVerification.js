'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    
      
      return queryInterface.changeColumn('CRMOUVerification', 'purpose', {
        type: Sequelize.STRING,
        allowNull: false
      })

    
  },

  down: (queryInterface, Sequelize) => {
      
      return queryInterface.changeColumn('CRMOUVerification', 'purpose', {
        type: Sequelize.TEXT,
        allowNull: false
      })

  }
};