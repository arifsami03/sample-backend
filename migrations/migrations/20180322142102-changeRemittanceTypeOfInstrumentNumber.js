'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('CRRemittance', 'instrumentNumber', {
      type: Sequelize.STRING
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('CRRemittance', 'instrumentNumber', {
      type: Sequelize.INTEGER
    });
  }
};
