'use strict';

module.exports = {
  /**
   * FSI stands for Fee Structure Installement
   */
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CAProgramDetail', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      programDetailId: { type: Sequelize.INTEGER, allowNull: false },
      campusAdmissionId: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('CAProgramDetail', ['programDetailId'], {
        type: 'foreign key', name: 'FK_CAProgramDetail_ProgramDetail',
        references: { table: 'ProgramDetail', field: 'id' },
        onDelete: 'cascade', onUpdate: 'cascade'
      })
    }).then(() => {
      return queryInterface.addConstraint('CAProgramDetail', ['campusAdmissionId'], {
        type: 'foreign key', name: 'FK_CAProgramDetail_CampusAdmission',
        references: { table: 'CampusAdmission', field: 'id' },
        onDelete: 'cascade', onUpdate: 'cascade'
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CAProgramDetail');
  }

}