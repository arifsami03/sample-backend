'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('CRApplication', 'instituteTypeId', {
      type: Sequelize.INTEGER, allowNull: false, defaultValue: false
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CRApplication', 'instituteTypeId');
  }
};
