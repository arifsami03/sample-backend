'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Configuration', 'parentKey', {
      type: Sequelize.STRING
    }).then(() => {
      return queryInterface.addColumn('Configuration', 'parentKeyId', {
        type: Sequelize.INTEGER
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Configuration', 'parentKey').then(() => {
      return queryInterface.removeColumn('Configuration', 'parentKeyId');
    });
  }
};
