'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    //Important: incase if you are not using MSSQL you might need to change [User] to User
    return queryInterface.sequelize.query(`UPDATE [User] SET portal = 'institute' WHERE portal = 'university';`)
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`UPDATE [User] SET portal = 'university' WHERE portal = 'institute';`)
  }
};
