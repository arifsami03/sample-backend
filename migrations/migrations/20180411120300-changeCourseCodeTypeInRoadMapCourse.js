'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('RoadMapCourse', 'courseCode', {
      type: Sequelize.STRING,
      allowNull: false
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('RoadMapCourse', 'courseCode', {
      type: Sequelize.INTEGER,
      allowNull: false
    });
  }
};
