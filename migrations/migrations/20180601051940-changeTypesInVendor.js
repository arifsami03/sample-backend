'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('Vendor', 'faxNumber', {
      type: Sequelize.STRING
    }).then(() => {
      return queryInterface.changeColumn('Vendor', 'ntnNumber', {
        type: Sequelize.STRING
      }).then(() => {
        return queryInterface.changeColumn('Vendor', 'salesTaxNumber', {
          type: Sequelize.STRING
        })
      })
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('Vendor', 'faxNumber', {
      type: Sequelize.INTEGER
    }).then(() => {
      return queryInterface.changeColumn('Vendor', 'ntnNumber', {
        type: Sequelize.INTEGER
      }).then(() => {
        return queryInterface.changeColumn('Vendor', 'salesTaxNumber', {
          type: Sequelize.INTEGER
        })
      })
    });
  }
};
