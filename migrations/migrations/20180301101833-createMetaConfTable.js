'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'MetaConf',
      {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        metaKey: { type: Sequelize.STRING(50), allowNull: false, defaultValue: false },
        metaValue: { type: Sequelize.TEXT, allowNull: false, defaultValue: false },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER },
        updatedBy: { type: Sequelize.INTEGER },
      },
      {
        // engine: 'InnoDB',                     // default: 'InnoDB'
        // charset: null,                        // default: null
      }
    ).then(() => {
      return queryInterface.bulkInsert('MetaConf', [
        {
          metaKey: 'appName',
          metaValue: 'Idrak',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ], {});
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('MetaConf');

  }
};
