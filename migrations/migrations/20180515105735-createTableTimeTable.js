'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('TimeTable', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        programDetailId: {type: Sequelize.INTEGER,allowNull: false},
        classId: { type: Sequelize.INTEGER, allowNull: false },
        title: { type: Sequelize.STRING,allowNull: false},
        
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER },
        updatedBy: { type: Sequelize.INTEGER }
      })
      .then(() => {
        return queryInterface
          .addConstraint('TimeTable', ['programDetailId'], {
            type: 'foreign key',
            name: 'FK_TimeTable_ProgramDetail',
            references: {
              table: 'ProgramDetail',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          })
          .then(() => {
            return queryInterface.addConstraint('TimeTable', ['classId'], {
              type: 'foreign key',
              name: 'FK_TimeTable_Classes',
              references: {
                table: 'Classes',
                field: 'id'
              },
              onDelete: 'cascade',
              onUpdate: 'cascade'
            });
          });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('TimeTable');
  }
};
