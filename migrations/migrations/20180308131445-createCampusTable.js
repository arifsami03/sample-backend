'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('CRApplication', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        CRApplicantId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: false
        },
        campusName: {
          type: Sequelize.STRING(55),
          allowNull: false
        },
        levelOfEducation: {
          type: Sequelize.STRING,
          allowNull: true
        },
        tentativeSessionStart: {
          type: Sequelize.STRING,
          allowNull: true
        },
        campusLocation: {
          type: Sequelize.STRING,
          allowNull: true
        },
        buildingAvailable: {
          type: Sequelize.BOOLEAN, // BOOLEAN VALUE
          allowNull: true
        },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER, allowNull: true },
        updatedBy: { type: Sequelize.INTEGER, allowNull: true }
      })
      .then(() => {
        return queryInterface.addConstraint('CRApplication', ['CRApplicantId'], {
          type: 'foreign key',
          name: 'FK_Application_CRApplicant',
          references: {
            table: 'CRApplicant',
            field: 'id'
          }
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('CRApplication', 'FK_Application_CRApplicant')
      .then(() => {
        return queryInterface.dropTable('CRApplication');
      });
  }
};
