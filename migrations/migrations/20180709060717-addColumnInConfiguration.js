'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface
      .addColumn('Configuration', 'isSingle', {
        type: Sequelize.BOOLEAN
      })
      .then(() => {
        return queryInterface.addColumn('Configuration', 'label', {
          type: Sequelize.STRING
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Configuration', 'isSingle').then(() => {
      return queryInterface.removeColumn('Configuration', 'label');
    });
  }
};
