'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('Slot', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        fromTime: { type: Sequelize.TIME,  allowNull: false },
        toTime: { type: Sequelize.TIME, allowNull: false },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER },
        updatedBy: { type: Sequelize.INTEGER }
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Slot');
  }
};
