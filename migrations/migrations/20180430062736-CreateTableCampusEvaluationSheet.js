'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CREvaluationSheet', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      CRApplicationId: { type: Sequelize.INTEGER, allowNull: false},
      ESCategoryId: { type: Sequelize.INTEGER, allowNull: false },      
      ESId: { type: Sequelize.INTEGER, allowNull: false  },
      ESTitle: { type: Sequelize.STRING, allowNull: false },
      status: { type: Sequelize.STRING, allowNull: false },
      isRecommendedForApproval: { type: Sequelize.BOOLEAN, allowNull: true },
      remarks: { type: Sequelize.TEXT, allowNull: true },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER, allowNull: true },
      updatedBy: { type: Sequelize.INTEGER, allowNull: true }
    }).then(() => {
      return queryInterface.addConstraint('CREvaluationSheet', ['CRApplicationId'], {
        type: 'foreign key', name: 'FK_CREvaluationSheet_CRApplication',
        references: { table: 'CRApplication', field: 'id' }
      })
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('CREvaluationSheet', 'FK_CREvaluationSheet_CRApplication').then(() => {
    return queryInterface.dropTable('CREvaluationSheet');
    });
  }
};