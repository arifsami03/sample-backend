'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // for adding coulumn with allowNull: false, first truncate all data

    return queryInterface.bulkDelete('ESQuestionOption', null, {}).then(() => {
      return queryInterface.bulkDelete('ESQuestion', null, {}).then(() => {
        return queryInterface.bulkDelete('ESSection', null, {}).then(() => {
          return queryInterface.bulkDelete('EvaluationSheet', null, {}).then(() => {
            return queryInterface.addColumn('EvaluationSheet', 'ESCategoryId', {
              type: Sequelize.INTEGER, allowNull: false
            }).then(() => {
              // ESFor column was added previously but it not need any more in our updated scenario so delete it
              return queryInterface.removeColumn('EvaluationSheet', 'ESFor').then(() => {
                return queryInterface.addConstraint('EvaluationSheet', ['ESCategoryId'], {
                  type: 'foreign key', name: 'FK_EvaluationSheet_ESCategory',
                  references: { table: 'ESCategory', field: 'id' }
                });
              });
            });
          });
        });
      });
    });

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('EvaluationSheet', 'FK_EvaluationSheet_ESCategory').then(() => {
      return queryInterface.addColumn('EvaluationSheet', 'ESFor', {
        type: Sequelize.STRING
      }).then(() => {
        return queryInterface.removeColumn('EvaluationSheet', 'ESCategoryId');
      });
    });
  }
};
