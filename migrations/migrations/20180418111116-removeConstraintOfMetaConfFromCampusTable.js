'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('CRApplication', 'FK_Application_MetaConf');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`UPDATE CRApplication SET CRApplication.cityId = NULL;`).then(() => {
      return queryInterface.addConstraint('CRApplication', ['cityId'], {
        type: 'foreign key', name: 'FK_Application_MetaConf',
        references: { table: 'MetaConf', field: 'id' }
      });
    });
  }
};
