'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`UPDATE Configuration SET isSingle = ?, label = ? WHERE "key" = ?`, {
      replacements: [true, 'App Name', 'appName']
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`UPDATE Configuration SET isSingle = ?, label = ? WHERE "key" = ?`, {
      replacements: [false, '', 'appName']
    })
  }
};
