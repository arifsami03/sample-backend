'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('MetaConfOption', 'FK_MetaConfOption_MetaConf').then(() => {
      return queryInterface.dropTable('MetaConfOption');
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.createTable('MetaConfOption', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      metaConfId: { type: Sequelize.INTEGER, allowNull: false },
      key: { type: Sequelize.STRING, allowNull: false },
      value: { type: Sequelize.STRING, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER, allowNull: true },
      updatedBy: { type: Sequelize.INTEGER, allowNull: true }
    }).then(() => {
    }).then(() => {
      return queryInterface.addConstraint('MetaConfOption', ['metaConfId'], {
        type: 'foreign key', name: 'FK_MetaConfOption_MetaConf',
        references: { table: 'MetaConf', field: 'id' }
      })
    });
  }
};