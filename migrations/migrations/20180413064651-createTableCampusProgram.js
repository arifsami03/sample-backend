'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CRProgram', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      CRApplicationId: { type: Sequelize.INTEGER, allowNull: false },
      programId: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER, allowNull: true },
      updatedBy: { type: Sequelize.INTEGER, allowNull: true }
    }).then(() => {
      return queryInterface.addConstraint('CRProgram', ['CRApplicationId'], {
        type: 'foreign key', name: 'FK_CRProgram_CRApplication',
        references: { table: 'CRApplication', field: 'id' }
      }).then(() => {
        return queryInterface.addConstraint('CRProgram', ['programId'], {
          type: 'foreign key', name: 'FK_CRProgram_Program',
          references: { table: 'Program', field: 'id' }
        })
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('CRProgram', 'FK_CRProgram_Program').then(() => {
      return queryInterface.removeConstraint('CRProgram', 'FK_CRProgram_CRApplication').then(() => {
        return queryInterface.dropTable('CRProgram');
      });
    });

  }
};