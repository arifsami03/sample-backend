'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('Board', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        name: { type: Sequelize.STRING,  allowNull: false },
        abbreviation: {type: Sequelize.STRING, allowNull: false },
        website: { type: Sequelize.STRING, allowNull: false },
        mobileNumber: {type: Sequelize.STRING,allowNull: false },
        address: {type: Sequelize.STRING, allowNull: false },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER },
        updatedBy: { type: Sequelize.INTEGER }
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Board');
  }
};
