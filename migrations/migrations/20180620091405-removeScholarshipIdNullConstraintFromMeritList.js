'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('MeritList', 'FK_MeritList_Scholarship',{allowNull : true})


  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addConstraint('MeritList', ['scholarshipId'], {
      type: 'foreign key',
      name: 'FK_MeritList_Scholarship',
      references: {
        table: 'Scholarship',
        field: 'id'
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    });
  }
};
