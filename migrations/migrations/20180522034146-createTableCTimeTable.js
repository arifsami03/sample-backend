'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CampusTimeTable', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        programDetailId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        classId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        timeTableId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },

        title: {
          type: Sequelize.STRING,
          allowNull: false
        },
        campusId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        validFrom: {
          type: Sequelize.DATE,
          allowNull: false
        },
        validTo: {
          type: Sequelize.DATE,
          allowNull: false
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER
        },
        updatedBy: {
          type: Sequelize.INTEGER
        }
      })
      // .then(() => {
      //   return queryInterface.addConstraint('CampusTimeTable', ['timeTableId'], {
      //     type: 'foreign key',
      //     name: 'FK_CampusTimeTable_TimeTable',
      //     references: {
      //       table: 'TimeTable',
      //       field: 'id'
      //     },
      //     onDelete: 'cascade',
      //     onUpdate: 'cascade'
      //   })
      // })
      .then(() => {
        return queryInterface.addConstraint('CampusTimeTable', ['programDetailId'], {
          type: 'foreign key',
          name: 'FK_CampusTimeTable_ProgramDetail',
          references: {
            table: 'ProgramDetail',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        })
      })
      .then(() => {
        return queryInterface.addConstraint('CampusTimeTable', ['classId'], {
          type: 'foreign key',
          name: 'FK_CampusTimeTable_Classes',
          references: {
            table: 'Classes',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        })
      })
      .then(() => {
        return queryInterface.addConstraint('CampusTimeTable', ['campusId'], {
          type: 'foreign key',
          name: 'FK_CampusTimeTable_Campus',
          references: {
            table: 'Campus',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CampusTimeTable');
  }
};