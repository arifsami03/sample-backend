'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('EducationLevel', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      education: { type: Sequelize.STRING, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('EducationLevel');
  }
};
