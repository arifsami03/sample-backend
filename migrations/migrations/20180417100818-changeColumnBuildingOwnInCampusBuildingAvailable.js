'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CRAvailableBuilding', 'buildingOwn').then(() => {
      return queryInterface.addColumn('CRAvailableBuilding', 'buildingOwn', {
        type: Sequelize.BOOLEAN
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CRAvailableBuilding', 'buildingOwn').then(() => {
      return queryInterface.addColumn('CRAvailableBuilding', 'buildingOwn', {
        type: Sequelize.BOOLEAN, allowNull: false, defaultValue: false
      });
    });
  }
};