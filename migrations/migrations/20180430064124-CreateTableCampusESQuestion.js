'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CRESQuestion', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      CRESSectionId: { type: Sequelize.INTEGER, allowNull: false },
      ESQuestionId: { type: Sequelize.INTEGER, allowNull: false },

      question: { type: Sequelize.STRING, allowNull: false },    
      countField: { type: Sequelize.BOOLEAN, allowNull: true },
      countValue: { type: Sequelize.INTEGER, allowNull: true },
      
      remarksField: { type: Sequelize.BOOLEAN, allowNull: true },
      remarks: { type: Sequelize.TEXT, allowNull: true },

      optionType: { type: Sequelize.STRING, allowNull: false },
      optionsCSV: { type: Sequelize.TEXT, allowNull: true },
      answer: { type: Sequelize.TEXT, allowNull: true },

      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('CRESQuestion', ['CRESSectionId'], {
        type: 'foreign key', name: 'FK_CRESQuestion_CRESSection',
        references: { table: 'CRESSection', field: 'id' }
      })
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('CRESQuestion', 'FK_CRESQuestion_CRESSection').then(() => {
      return queryInterface.dropTable('CRESQuestion');
    });
  }
};