'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('CTTDDay', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        CTTDetailId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        dayId: {
          type: Sequelize.STRING,
          allowNull: false
        },
        

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER
        },
        updatedBy: {
          type: Sequelize.INTEGER
        }
      })
      .then(() => {
        return queryInterface
          .addConstraint('CTTDDay', ['CTTDetailId'], {
            type: 'foreign key',
            name: 'FK_CTTDDay_CTTDetail',
            references: {
              table: 'CTTDetail',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          })

      })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CTTDDay');
  }
};