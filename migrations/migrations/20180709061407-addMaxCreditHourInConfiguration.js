'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Configuration', [
      {
        key: 'maxCreditHour',
        value: '',
        isSingle: true,
        label: 'Max Credit Hour',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Configuration', { key: 'maxCreditHour' });
  }
};
