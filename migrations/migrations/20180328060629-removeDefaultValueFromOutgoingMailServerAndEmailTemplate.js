'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .removeColumn('OutgoingMailServer', 'name')
      .then(() => {
        return queryInterface.addColumn('OutgoingMailServer', 'name', {
          type: Sequelize.STRING(100),
          allowNull: false
        });
      })
      .then(() => {
        return queryInterface.removeColumn('EmailTemplate', 'name').then(() => {
          return queryInterface.addColumn('EmailTemplate', 'name', {
            type: Sequelize.STRING(100),
            allowNull: false
          });
        });
      })
      .then(() => {
        return queryInterface
          .removeColumn('EmailTemplate', 'subject')
          .then(() => {
            return queryInterface.addColumn('EmailTemplate', 'subject', {
              type: Sequelize.STRING,
              allowNull: false
            });
          });
      });
  },

  down: (queryInterface, Sequelize) => {
    return true;
  }
};
