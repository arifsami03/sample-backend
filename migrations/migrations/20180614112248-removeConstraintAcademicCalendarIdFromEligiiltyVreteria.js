'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ECPreReqProgram', null, {}).then(() => {
      return queryInterface.bulkDelete('ECSubject', null, {}).then(() => {
        return queryInterface.bulkDelete('ECPassingYear', null, {}).then(() => {
          return queryInterface.bulkDelete('EligibilityCriteria', null, {}).then(() => {
            return queryInterface.removeConstraint('EligibilityCriteria', 'FK_EligibilityCriteria_AcademicCalendar').then(() => {
              return queryInterface.removeColumn('EligibilityCriteria', 'academicCalendarId')
            });
          })
        })
      })
    })

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('EligibilityCriteria', 'academicCalendarId', {
      type: Sequelize.INTEGER,
    }).then(() => {
      return queryInterface.addConstraint('EligibilityCriteria', ['academicCalendarId'], {
        type: 'foreign key',
        name: 'FK_EligibilityCriteria_AcademicCalendar',
        references: {
          table: 'AcademicCalendar',
          field: 'id'
        }
      })
    });
  }
};
