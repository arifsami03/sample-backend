'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('CampusDefaultUser', 'deleted', { type: Sequelize.BOOLEAN, allowNull: false, defaultValue: false })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CampusDefaultUser', 'deleted');
  }
};
