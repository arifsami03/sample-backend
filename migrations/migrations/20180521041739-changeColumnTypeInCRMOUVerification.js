'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('CRMOUVerification', 'instrumentNumber', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('CRMOUVerification', 'instrumentNumber', {
      type: Sequelize.INTEGER
    });
  }
};
