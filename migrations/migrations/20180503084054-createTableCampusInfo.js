'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Campus', {
     
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      campusId: {
        type: Sequelize.STRING,
        allowNull: false
      },
      CRApplicationId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      codeNumber: {
        type: Sequelize.STRING,
        allowNull: false
      },
      campusName: {
        type: Sequelize.STRING,
        allowNull: false
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false
      },
      mobileNumber: {
        type: Sequelize.STRING,
        allowNull: false
      },
      address: {
        type: Sequelize.STRING,
        allowNull: false
      },
      faxNumber: {
        type: Sequelize.STRING,
        allowNull: false
      },
      academicYearId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      createdBy: {
        type: Sequelize.INTEGER
      },
      updatedBy: {
        type: Sequelize.INTEGER
      }

    }).then(() => {
      return queryInterface.addConstraint('Campus', ['CRApplicationId'], {
        type: 'foreign key',
        name: 'FK_Campus_CRApplication',
        references: {
          table: 'CRApplication',
          field: 'id'
        }
      })
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('Campus', 'FK_Campus_CRApplication').then(() => {
          return queryInterface.dropTable('Campus');
    });
  }
};