'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('WorkFlowLog', 'fromStateId', {
      type: Sequelize.INTEGER,
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('WorkFlowLog', 'fromStateId', {
      type: Sequelize.INTEGER,
      allowNull: false
    });
  }
};