'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('FeeStructureFeeHeads', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      feeStructureId: { type: Sequelize.INTEGER, allowNull: false },
      feeHeadsId: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('FeeStructureFeeHeads', ['feeStructureId'], {
        type: 'foreign key', name: 'FK_FeeStructureFeeHeads_FeeStructure',
        references: { table: 'FeeStructure', field: 'id' }
      }).then(() => {
        return queryInterface.addConstraint('FeeStructureFeeHeads', ['feeHeadsId'], {
          type: 'foreign key', name: 'FK_FeeStructureFeeHeads_FeeHeads',
          references: { table: 'FeeHeads', field: 'id' }
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('FeeStructureFeeHeads', 'FK_FeeStructureFeeHeads_FeeHeads').then(() => {
      return queryInterface.removeConstraint('FeeStructureFeeHeads', 'FK_FeeStructureFeeHeads_FeeStructure').then(() => {
        return queryInterface.dropTable('FeeStructureFeeHeads');
      });
    });
  }
};