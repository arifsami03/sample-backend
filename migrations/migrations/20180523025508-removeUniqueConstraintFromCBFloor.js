'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('CBFloor', 'Unique_CBFloor_code');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addConstraint('CBFloor', ['code'], {
      type: 'unique',
      name: 'Unique_CBFloor_code'
    });
  }
};
