'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
    .addColumn('CRAddress', 'buildingAvailableType', {
      type: Sequelize.BOOLEAN
    })
    
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CRAddress', 'buildingAvailableType')
    
   
  }
};
