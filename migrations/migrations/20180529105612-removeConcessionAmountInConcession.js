'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Concession', 'concessionAmount').then(() => {
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('Concession', 'concessionAmount', {
        type: Sequelize.FLOAT
      })


  }
};
