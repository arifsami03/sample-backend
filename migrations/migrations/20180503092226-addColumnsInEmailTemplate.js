'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('EmailTemplate', 'code', {
        type: Sequelize.STRING
      })
      .then(() => {
        return queryInterface.addColumn('EmailTemplate', 'deletable', {
          type: Sequelize.BOOLEAN,
          defaultValue: true
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('EmailTemplate', 'code').then(() => {
      return queryInterface.removeColumn('EmailTemplate', 'deletable');
    });
  }
};
