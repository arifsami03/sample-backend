'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('Classes', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        name: {
          type: Sequelize.STRING
        },
        programDetailId: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        updatedBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        }
      })
      .then(() => {
        return queryInterface.addConstraint('Classes', ['programDetailId'], {
          type: 'foreign key',
          name: 'FK_Classes_ProgramDetail',
          references: {
            table: 'ProgramDetail',
            field: 'id'
          }
        })
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('Classes', 'FK_Classes_ProgramDetail').then(() => {
        return queryInterface.dropTable('Classes');
      });

  }
};