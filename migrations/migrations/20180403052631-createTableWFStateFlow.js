'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('WFStateFlow', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        fromId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        toId: {
          type: Sequelize.INTEGER
        },
        description: {
          type: Sequelize.TEXT
        },
        WFId: {
          type: Sequelize.STRING,
          allowNull: false,
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        updatedBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        }
      })
      .then(() => {
        return queryInterface.addConstraint('WFStateFlow', ['WFId'], {
          type: 'foreign key',
          name: 'FK_WFStateFlow_WorkFlow',
          references: {
            table: 'WorkFlow',
            field: 'WFId'
          }
        })
      }).then(() => {
        return queryInterface.addConstraint('WFStateFlow', ['fromId'], {
          type: 'foreign key',
          name: 'FK_WFStateFlow_WFState_From',
          references: {
            table: 'WFState',
            field: 'id'
          }
        })
      }).then(() => {
        return queryInterface.addConstraint('WFStateFlow', ['toId'], {
          type: 'foreign key',
          name: 'FK_WFStateFlow_WFState_To',
          references: {
            table: 'WFState',
            field: 'id'
          }
        })
      });

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('WFStateFlow', 'FK_WFStateFlow_WorkFlow').then(() => {
      return queryInterface.removeConstraint('WFStateFlow', 'FK_WFStateFlow_WFState_From').then(() => {
        return queryInterface.removeConstraint('WFStateFlow', 'FK_WFStateFlow_WFState_To').then(() => {
          return queryInterface.dropTable('WFStateFlow');
        })
      })
    });
  }
};