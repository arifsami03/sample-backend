'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

      return queryInterface.addColumn('CREvaluationSheet', 'comments', {
          type: Sequelize.TEXT,
        })
        
  },

  down: (queryInterface, Sequelize) => {

      return queryInterface.removeColumn('CREvaluationSheet', 'comments');
  }
};
