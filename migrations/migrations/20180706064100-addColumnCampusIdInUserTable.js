'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

          return queryInterface.addColumn('User', 'campusId', { type: Sequelize.INTEGER,allowNull:true }).then(() => {

            return queryInterface.addConstraint('User', ['campusId'], {
              type: 'foreign key', name: 'FK_User_Campus',
              references: { table: 'Campus', field: 'id' }
            });
          });
  },

  down: (queryInterface, Sequelize) => {

    return queryInterface.removeConstraint('User', 'FK_User_Campus').then(() => {

      return queryInterface.removeColumn('User', 'campusId')
    });
  }
};
