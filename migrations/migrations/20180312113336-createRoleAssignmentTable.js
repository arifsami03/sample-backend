'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('RoleAssignment', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        roleId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: false
        },
        parent: {
          type: Sequelize.STRING,
          allowNull: false,
          defaultValue: false
        },
        parentId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: false
        },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER, allowNull: true },
        updatedBy: { type: Sequelize.INTEGER, allowNull: true }
      })
      .then(() => {
        return queryInterface.addConstraint('RoleAssignment', ['roleId'], {
          type: 'foreign key',
          name: 'FK_RoleAssignment_Role',
          references: {
            table: 'Role',
            field: 'id'
          }
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('RoleAssignment', 'FK_RoleAssignment_Role')
      .then(() => {
        return queryInterface.dropTable('RoleAssignment');
      });
  }
};
