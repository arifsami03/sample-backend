'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Campus', 'academicMonth', {
      type: Sequelize.INTEGER,
    }).then(() => {
      return queryInterface.addColumn('Campus', 'academicYear', {
        type: Sequelize.INTEGER,
      })
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Campus', 'academicMonth').then(() => {
      return queryInterface.removeColumn('Campus', 'academicYear')
    })
  }
};
