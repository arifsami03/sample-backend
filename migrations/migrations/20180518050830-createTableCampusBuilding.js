'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CampusBuilding', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      campusId: { type: Sequelize.INTEGER, allowNull: false },
      name: { type: Sequelize.STRING, allowNull: false },
      abbreviation: { type: Sequelize.STRING, allowNull: false },
      code: { type: Sequelize.STRING, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('CampusBuilding', ['campusId'], {
        type: 'foreign key', name: 'FK_CampusBuilding_Campus',
        references: { table: 'Campus', field: 'id' }
      }).then(() => {
        return queryInterface.addConstraint('CampusBuilding', ['code'], {
          type: 'unique',
          name: 'Unique_CampusBuilding_code'
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CampusBuilding');
  }

}