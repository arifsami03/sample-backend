'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Institute', 'affiliation').then(() => {
      return queryInterface.removeColumn('Institute', 'contactNo');
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('Institute', 'affiliation', {
        type: Sequelize.STRING
      })
      .then(() => {
        return queryInterface.addColumn('Institute', 'contactNo', {
          type: Sequelize.STRING
        });
      });
  }
};
