'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('FeaturePermission', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      featureId: { type: Sequelize.STRING, allowNull: false },
      parent: {
        type: Sequelize.STRING(255),
        allowNull: false,
        defaultValue: false
      },
      parentId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: false
      },
      status: {
        type: Sequelize.STRING(255),
        allowNull: false,
        defaultValue: false
      },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER, allowNull: true },
      updatedBy: { type: Sequelize.INTEGER, allowNull: true }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('FeaturePermission');
  }
};
