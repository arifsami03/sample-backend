'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CRAvailableBuilding', 'address').then(() => {
      return queryInterface.removeColumn('CRAvailableBuilding', 'latitude').then(() => {
        return queryInterface.removeColumn('CRAvailableBuilding', 'longitude');
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('CRAvailableBuilding', 'address', {
        type: Sequelize.STRING
      })
      .then(() => {
        return queryInterface.addColumn('CRAvailableBuilding', 'latitude', {
          type: Sequelize.STRING
        });
      }).then(() => {
        return queryInterface.addColumn('CRAvailableBuilding', 'longitude', {
          type: Sequelize.STRING
        });
      });
  }
};