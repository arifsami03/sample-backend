'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CRESSection', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      CRESId: { type: Sequelize.INTEGER, allowNull: false },
      ESSectionId:{ type: Sequelize.INTEGER, allowNull: false },
      title: { type: Sequelize.STRING, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER, allowNull: true },
      updatedBy: { type: Sequelize.INTEGER, allowNull: true }
    }).then(() => {
      return queryInterface.addConstraint('CRESSection', ['CRESId'], {
        type: 'foreign key', name: 'FK_CRESSection_CREvaluationSheet',
        references: { table: 'CREvaluationSheet', field: 'id' }
      })
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('CRESSection', 'FK_CRESSection_CREvaluationSheet').then(() => {
      return queryInterface.dropTable('CRESSection');
    });
  }
};