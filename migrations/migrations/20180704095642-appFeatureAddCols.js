'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('AppFeature', 'institute', {
      type: Sequelize.BOOLEAN,
      allowNull: true
    }).then(() => {
      return queryInterface.addColumn('AppFeature', 'campus', {
        type: Sequelize.BOOLEAN,
        allowNull: true
      })
    }).then(() => {
      return queryInterface.addColumn('AppFeature', 'teacher', {
        type: Sequelize.BOOLEAN,
        allowNull: true
      })
    }).then(() => {
      return queryInterface.addColumn('AppFeature', 'student', {
        type: Sequelize.BOOLEAN,
        allowNull: true
      })
    }).then(() => {
      return queryInterface.sequelize.query(`UPDATE AppFeature SET institute = 1;`)
    })
  },

  down: (queryInterface, Sequelize) => {
    
    return queryInterface.removeColumn('AppFeature', 'institute').then(() => {

      return queryInterface.removeColumn('AppFeature', 'campus');

    }).then(() => {

      return queryInterface.removeColumn('AppFeature', 'teacher');

    }).then(() => {

      return queryInterface.removeColumn('AppFeature', 'student');

    })
  }
};
