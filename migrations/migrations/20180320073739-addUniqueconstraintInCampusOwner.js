'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addConstraint('CRApplicant', ['email'], {
      type: 'unique',
      name: 'Unique_CRApplicant_Email'
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint(
      'CRApplicant',
      'Unique_CRApplicant_Email'
    );
  }
};
