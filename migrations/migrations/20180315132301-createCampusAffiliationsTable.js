'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'CRAffiliation',
      {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        CRApplicationId: { type: Sequelize.STRING(50), allowNull: false },
        affiliation: { type: Sequelize.STRING },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER },
        updatedBy: { type: Sequelize.INTEGER }
      },
      {
        // engine: 'InnoDB',                     // default: 'InnoDB'
        // charset: null,                        // default: null
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CRAffiliation');
  }
};
