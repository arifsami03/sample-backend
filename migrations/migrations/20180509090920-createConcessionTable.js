'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.createTable('Concession', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      title: { type: Sequelize.STRING, allowNull: false },
      MCConcessionTypeId: { type: Sequelize.INTEGER, allowNull: false },
      MCConcessionCategoryId: { type: Sequelize.INTEGER, allowNull: false },
      concessionFormat: { type: Sequelize.STRING, allowNull: false },
      concessionAmount: { type: Sequelize.FLOAT, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {

      return queryInterface.addConstraint('Concession', ['MCConcessionTypeId'], {
        type: 'foreign key', name: 'FK_Concession_Configuration_ConcessionType',
        references: { table: 'Configuration', field: 'id' }
      })

    }).then(() => {

      return queryInterface.addConstraint('Concession', ['MCConcessionCategoryId'], {
        type: 'foreign key', name: 'FK_Concession_Configuration_ConcessionCategory',
        references: { table: 'Configuration', field: 'id' }
      })

    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Concession');
  }
};