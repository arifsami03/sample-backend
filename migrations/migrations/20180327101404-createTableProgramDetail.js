'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('ProgramDetail', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        name: {
          type: Sequelize.STRING
        },
        programId: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        updatedBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        }
      })
      .then(() => {
        return queryInterface.addConstraint('ProgramDetail', ['programId'], {
          type: 'foreign key',
          name: 'FK_ProgramDetail_Program',
          references: {
            table: 'Program',
            field: 'id'
          }
        })
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('ProgramDetail', 'FK_ProgramDetail_Program').then(() => {
        return queryInterface.dropTable('ProgramDetail');
      });

  }
};