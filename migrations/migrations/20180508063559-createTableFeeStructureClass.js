'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('FeeStructureClasses', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      feeStructureId: { type: Sequelize.INTEGER, allowNull: false },
      classId: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('FeeStructureClasses', ['feeStructureId'], {
        type: 'foreign key', name: 'FK_FeeStructureClasses_FeeStructure',
        references: { table: 'FeeStructure', field: 'id' }
      }).then(() => {
        return queryInterface.addConstraint('FeeStructureClasses', ['classId'], {
          type: 'foreign key', name: 'FK_FeeStructureClasses_Classes',
          references: { table: 'Classes', field: 'id' }
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('FeeStructureClasses', 'FK_FeeStructureClasses_Classes').then(() => {
      return queryInterface.removeConstraint('FeeStructureClasses', 'FK_FeeStructureClasses_FeeStructure').then(() => {
        return queryInterface.dropTable('FeeStructureClasses');
      });
    });
  }
};