'use strict';

module.exports = {
  /**
   * FS stands for Fee Structure Table
   */
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('FSCampus', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      feeStructureId: { type: Sequelize.INTEGER, allowNull: false },
      campusId: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('FSCampus', ['feeStructureId'], {
        type: 'foreign key', name: 'FK_FSCampus_FeeStructure',
        references: { table: 'FeeStructure', field: 'id' }
      }).then(() => {
        return queryInterface.addConstraint('FSCampus', ['campusId'], {
          type: 'foreign key', name: 'FK_FSCampus_Campus',
          references: { table: 'Campus', field: 'id' }
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('FSCampus');
  }

}