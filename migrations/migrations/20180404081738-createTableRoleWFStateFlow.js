'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('RoleWFStateFlow', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        roleId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        WFStateFlowId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        updatedBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        }
      })
      .then(() => {
        return queryInterface.addConstraint('RoleWFStateFlow', ['roleId'], {
          type: 'foreign key',
          name: 'FK_RoleWFStateFlow_Role',
          references: {
            table: 'Role',
            field: 'id'
          }
        })
      }).then(() => {
        return queryInterface.addConstraint('RoleWFStateFlow', ['WFStateFlowId'], {
          type: 'foreign key',
          name: 'FK_RoleWFStateFlow_WFStateFlow',
          references: {
            table: 'WFStateFlow',
            field: 'id'
          }
        })
      })

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('RoleWFStateFlow', 'FK_RoleWFStateFlow_Role').then(() => {
      return queryInterface.removeConstraint('RoleWFStateFlow', 'FK_RoleWFStateFlow_WFStateFlow').then(() => {
          return queryInterface.dropTable('RoleWFStateFlow');
      
      })
    });
  }
};