'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('User', [
      {
        username: 'admin@site.com',
        password: 'pass2word',
        isActive: true,
        isSuperUser: true,
        portal: 'university',//TODO: Need to define portals
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('User', null, {});
  }
};
