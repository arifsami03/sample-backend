'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'Institute',
      {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        name: { type: Sequelize.STRING(50), allowNull: false },
        logo: { type: Sequelize.STRING },
        abbreviation: { type: Sequelize.STRING },
        website: { type: Sequelize.STRING(50) },
        officeEmail: { type: Sequelize.STRING(50) },
        headOfficeLocation: { type: Sequelize.STRING },
        latitude: { type: Sequelize.FLOAT },
        longitude: { type: Sequelize.FLOAT },
        establishedScince: { type: Sequelize.INTEGER },
        contactNo: { type: Sequelize.STRING },
        affiliation: { type: Sequelize.STRING },
        focalPerson: { type: Sequelize.STRING },
        Description: { type: Sequelize.STRING },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER },
        updatedBy: { type: Sequelize.INTEGER }
      },
      {
        // engine: 'InnoDB',                     // default: 'InnoDB'
        // charset: null,                        // default: null
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Institute');
  }
};
