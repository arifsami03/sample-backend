'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('FeeStructureFeeHeads', 'typeOfFeeHeadId', { type: Sequelize.INTEGER }).then(() => {
      return queryInterface.addColumn('FeeStructureFeeHeads', 'amount', { type: Sequelize.INTEGER }).then(() => {
        return queryInterface.addColumn('FeeStructureFeeHeads', 'managementFeeFormat', { type: Sequelize.STRING }).then(() => {
          return queryInterface.addColumn('FeeStructureFeeHeads', 'managementFeeAmount', { type: Sequelize.FLOAT });
        });
      });
    }).then(() => {
      return queryInterface.addConstraint('FeeStructureFeeHeads', ['typeOfFeeHeadId'], {
        type: 'foreign key', name: 'FK_FeeStructureFeeHeads_Configuration',
        references: { table: 'Configuration', field: 'id' }
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('FeeStructureFeeHeads', 'typeOfFeeHeadId').then(() => {
      return queryInterface.removeColumn('FeeStructureFeeHeads', 'amount').then(() => {
        return queryInterface.removeColumn('FeeStructureFeeHeads', 'managementFeeFormat').then(() => {
          return queryInterface.removeColumn('FeeStructureFeeHeads', 'managementFeeAmount');
        });
      });
    });
  }
};