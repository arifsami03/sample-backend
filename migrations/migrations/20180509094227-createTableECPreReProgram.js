'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ECPreReqProgram', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      programId: { type: Sequelize.INTEGER, allowNull: false },
      eligibilityCriteriaId: { type: Sequelize.INTEGER, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('ECPreReqProgram', ['programId'], {
        type: 'foreign key', name: 'FK_ECPreReqProgram_Program',
        references: { table: 'Program', field: 'id' }
      }).then(() => {
        return queryInterface.addConstraint('ECPreReqProgram', ['eligibilityCriteriaId'], {
          type: 'foreign key', name: 'FK_ECPreReqProgram_EligibilityCriteria',
          references: { table: 'EligibilityCriteria', field: 'id' }
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('ECPreReqProgram', 'FK_ECPreReqProgram_EligibilityCriteria').then(() => {
      return queryInterface.removeConstraint('ECPreReqProgram', 'FK_ECPreReqProgram_Program').then(() => {
        return queryInterface.dropTable('ECPreReqProgram');
      });
    });
  }
};