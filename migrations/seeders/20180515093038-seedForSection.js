'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'sections.*', parentId: 'timeTableMod.*', title: 'Sections', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      { id: 'sections.create', parentId: 'sections.*', title: 'Create section', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'sections.index', parentId: 'sections.create', title: 'View section', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'sections.update', parentId: 'sections.*', title: 'Update section', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'sections.index', parentId: 'sections.update', title: 'View section', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'sections.delete', parentId: 'sections.*', title: 'Delete section', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'sections.index', parentId: 'sections.delete', title: 'View section', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'sections.find', parentId: 'sections.index', title: 'View section', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: [
        'sections.*',
        'sections.create',
        'sections.index',
        'sections.update',
        'sections.delete',
        'sections.find']
    }, {});
  }
};