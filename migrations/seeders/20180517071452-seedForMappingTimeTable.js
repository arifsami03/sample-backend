'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        /**
         * Seed related to meriltLists
         */
        { id: 'mappingTimeTables.*', parentId: 'timeTableMod.*', title: 'Mapping Time Table', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'mappingTimeTables.create', parentId: 'mappingTimeTables.*', title: 'Create Mapping Time Table', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'mappingTimeTables.index', parentId: 'mappingTimeTables.create', title: 'View Mapping Time Table', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'mappingTimeTables.update', parentId: 'mappingTimeTables.*', title: 'Update Mapping Time Table', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'mappingTimeTables.index', parentId: 'mappingTimeTables.update', title: 'View Mapping Time Table', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'mappingTimeTables.delete', parentId: 'mappingTimeTables.*', title: 'Delete Mapping Time Tables', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'mappingTimeTables.index', parentId: 'mappingTimeTables.delete', title: 'View Mapping Time Table', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'mappingTimeTables.find', parentId: 'mappingTimeTables.index', title: 'View Mapping Time Table', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        
        { id: 'campus.findAttributesList', parentId: 'mappingTimeTable.find', title: 'Get Campuses List', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        
        { id: 'timeTables.findByPercentage', parentId: 'mappingTimeTables.find', title: 'Get Time Table List', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: ['mappingTimeTable.*', 'mappingTimeTable.create', 'mappingTimeTable.update', 'mappingTimeTable.delete', 'mappingTimeTable.index', 'mappingTimeTable.find', 'campus.findAttributesList', 'timeTable.findByPercentage']
      },
      {}
    );
  }
};
