'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
      
        { id: 'documents.*', parentId: 'admissionMod.*', title: 'Document', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'documents.create', parentId: 'documents.*', title: 'Create Document', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'documents.index', parentId: 'documents.create', title: 'View Document', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'documents.update', parentId: 'documents.*', title: 'Update Document', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'documents.index', parentId: 'documents.update', title: 'View Document', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'documents.delete', parentId: 'documents.*', title: 'Delete Document', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'documents.index', parentId: 'documents.delete', title: 'View Document', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'documents.find', parentId: 'documents.index', title: 'View Document', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: ['documents.*', 'documents.create', 'documents.update', 'documents.delete', 'documents.index', 'documents.find']
      },
      {}
    );
  }
};
