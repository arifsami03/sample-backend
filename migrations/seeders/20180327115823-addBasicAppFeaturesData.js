'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // return queryInterface.bulkDelete('AppFeature', null, {});
    return queryInterface.bulkDelete('AppFeature', null, {}).then(() => {

      return queryInterface.bulkInsert('AppFeature',
        [
          { id: 'securityMod.*', parentId: null, title: 'Security', isVisible: true, weight: 0, createdAt: new Date(), updatedAt: new Date() },

          //Users Management Seeds
          { id: 'users.*', parentId: 'securityMod.*', title: 'Users', isVisible: true, weight: 1, createdAt: new Date(), updatedAt: new Date() },
          { id: 'users.create', parentId: 'users.*', title: 'Create Users', isVisible: false, weight: 2, createdAt: new Date(), updatedAt: new Date() },
          { id: 'users.index', parentId: 'users.create', title: 'View Users', isVisible: false, weight: 3, createdAt: new Date(), updatedAt: new Date() },
          { id: 'users.update', parentId: 'users.*', title: 'Update User', isVisible: false, weight: 4, createdAt: new Date(), updatedAt: new Date() },
          { id: 'users.index', parentId: 'users.update', title: 'View Users', isVisible: false, weight: 5, createdAt: new Date(), updatedAt: new Date() },
          { id: 'users.delete', parentId: 'users.*', title: 'Delete User', isVisible: false, weight: 6, createdAt: new Date(), updatedAt: new Date() },
          { id: 'users.index', parentId: 'users.delete', title: 'View Users', isVisible: false, weight: 7, createdAt: new Date(), updatedAt: new Date() },
          { id: 'roleAssignments.assignToUser', parentId: 'users.*', title: 'Roles Assignments', isVisible: false, weight: 8, createdAt: new Date(), updatedAt: new Date() },
          { id: 'users.findAllRoles', parentId: 'roleAssignments.assignToUser', title: 'Find Roles', isVisible: false, weight: 9, createdAt: new Date(), updatedAt: new Date() },
          { id: 'roleAssignments.delete', parentId: 'roleAssignments.assignToUser', title: 'Revoke Role', isVisible: false, weight: 10, createdAt: new Date(), updatedAt: new Date() },
          { id: 'users.find', parentId: 'users.index', title: 'View User', isVisible: false, weight: 11, createdAt: new Date(), updatedAt: new Date() },

          // Role Management Seeds
          { id: 'roles.*', parentId: 'securityMod.*', title: 'Roles', isVisible: true, weight: 12, createdAt: new Date(), updatedAt: new Date() },
          { id: 'roles.create', parentId: 'roles.*', title: 'Create Roles', isVisible: false, weight: 13, createdAt: new Date(), updatedAt: new Date() },
          { id: 'roles.index', parentId: 'roles.create', title: 'View Roles', isVisible: false, weight: 14, createdAt: new Date(), updatedAt: new Date() },
          { id: 'roles.update', parentId: 'roles.*', title: 'Update Roles', isVisible: false, weight: 15, createdAt: new Date(), updatedAt: new Date() },
          { id: 'roles.index', parentId: 'roles.update', title: 'View Roles', isVisible: false, weight: 16, createdAt: new Date(), updatedAt: new Date() },
          { id: 'roles.delete', parentId: 'roles.*', title: 'Delete Roles', isVisible: false, weight: 17, createdAt: new Date(), updatedAt: new Date() },
          { id: 'roles.index', parentId: 'roles.delete', title: 'View Roles', isVisible: false, weight: 18, createdAt: new Date(), updatedAt: new Date() },
          { id: 'roles.assignUsers.*', parentId: 'roles.*', title: 'User Assignments', isVisible: false, weight: 19, createdAt: new Date(), updatedAt: new Date() },
          { id: 'roleAssignments.assignToUser', parentId: 'roles.assignUsers.*', title: 'Assign role to user', isVisible: false, weight: 20, createdAt: new Date(), updatedAt: new Date() },
          { id: 'roles.findAllUsers', parentId: 'roleAssignments.assignToUser', title: 'View Users to assign', isVisible: false, weight: 21, createdAt: new Date(), updatedAt: new Date() },
          { id: 'roleAssignments.delete', parentId: 'roles.assignUsers.*', title: 'Remove role from user', isVisible: false, weight: 22, createdAt: new Date(), updatedAt: new Date() },
          { id: 'roles.find', parentId: 'roles.index', title: 'View Roles', isVisible: false, weight: 23, createdAt: new Date(), updatedAt: new Date() },
          { id: 'roles.findAllFeatures', parentId: 'roles.find', title: 'View Permissions', isVisible: false, weight: 24, createdAt: new Date(), updatedAt: new Date() },

        ],
        {}
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', null, {});
  }
};
