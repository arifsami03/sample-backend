'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [

        { id: 'campus.index', parentId: 'campusMod.*', title: 'List Campus', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'campus.findCampus', parentId: 'campus.index', title: 'View Approved Campus', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        // { id: 'campus.update', parentId: 'campus.create', title: 'Update Campus Info', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        // { id: 'campus.find', parentId: 'campus.update', title: 'View Campus Info', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        // { id: 'academicCalendars.findAttributesList', parentId: 'campus.find', title: 'View Academic Calendars', isVisible: false, createdAt: new Date(), updatedAt: new Date() },        
        // { id: 'campusMOU.find', parentId: 'campus.find', title: 'View Banks', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        // { id: 'programs.getAllPrograms', parentId: 'campus.find', title: 'View Programs', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        // { id: 'programs.getProgramsWithProgramDetails', parentId: 'campus.find', title: 'View Instrument Type', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: [
          'campus.index',
          // 'campus.update',
          'campus.findCampus',
          // 'campusMOU.find',
          // 'programs.getAllPrograms',
          // 'programs.getProgramsWithProgramDetails'

        ]
      },
      {}
    );
  }
};
