'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.rawSelect('ESQuestion', { where: { question: 'Does the building has guard facility?' } }, ['id']).then(_ESQuestionId => {
      return queryInterface.bulkInsert('ESQuestionOption',
        [
          { ESQuestionId: _ESQuestionId, optionString: 'Yes', createdAt: new Date(), updatedAt: new Date() },
          { ESQuestionId: _ESQuestionId, optionString: 'No', createdAt: new Date(), updatedAt: new Date() }
        ], {});
    }).then(() => {
      return queryInterface.rawSelect('ESQuestion', { where: { question: 'Does the building has security cameras?' } }, ['id']).then(_ESQuestionId => {
        return queryInterface.bulkInsert('ESQuestionOption',
          [
            { ESQuestionId: _ESQuestionId, optionString: 'Yes', createdAt: new Date(), updatedAt: new Date() },
            { ESQuestionId: _ESQuestionId, optionString: 'No', createdAt: new Date(), updatedAt: new Date() }
          ], {});
      });
    }).then(() => {
      return queryInterface.rawSelect('ESQuestion', { where: { question: 'Building provides high security?' } }, ['id']).then(_ESQuestionId => {
        return queryInterface.bulkInsert('ESQuestionOption',
          [
            { ESQuestionId: _ESQuestionId, optionString: 'Strongly Agree', createdAt: new Date(), updatedAt: new Date() },
            { ESQuestionId: _ESQuestionId, optionString: 'Agree', createdAt: new Date(), updatedAt: new Date() },
            { ESQuestionId: _ESQuestionId, optionString: 'Neither Agree nor Disagree', createdAt: new Date(), updatedAt: new Date() },
            { ESQuestionId: _ESQuestionId, optionString: 'Disagree', createdAt: new Date(), updatedAt: new Date() },
            { ESQuestionId: _ESQuestionId, optionString: 'Strongly Disagree', createdAt: new Date(), updatedAt: new Date() }
          ], {});
      });
    }).then(() => {
      return queryInterface.rawSelect('ESQuestion', { where: { question: 'Safety Provided by the building?' } }, ['id']).then(_ESQuestionId => {
        return queryInterface.bulkInsert('ESQuestionOption',
          [
            { ESQuestionId: _ESQuestionId, optionString: 'Excellent', createdAt: new Date(), updatedAt: new Date() },
            { ESQuestionId: _ESQuestionId, optionString: 'Very Good', createdAt: new Date(), updatedAt: new Date() },
            { ESQuestionId: _ESQuestionId, optionString: 'Good', createdAt: new Date(), updatedAt: new Date() },
            { ESQuestionId: _ESQuestionId, optionString: 'Fair', createdAt: new Date(), updatedAt: new Date() },
            { ESQuestionId: _ESQuestionId, optionString: 'Poor', createdAt: new Date(), updatedAt: new Date() }
          ], {});
      });
    }).then(() => {
      return queryInterface.rawSelect('ESQuestion', { where: { question: 'Does the building has internet facility?' } }, ['id']).then(_ESQuestionId => {
        return queryInterface.bulkInsert('ESQuestionOption',
          [
            { ESQuestionId: _ESQuestionId, optionString: 'Yes', createdAt: new Date(), updatedAt: new Date() },
            { ESQuestionId: _ESQuestionId, optionString: 'No', createdAt: new Date(), updatedAt: new Date() }
          ], {});
      });
    }).then(() => {
      return queryInterface.rawSelect('ESQuestion', { where: { question: 'Seating capacity of the room?' } }, ['id']).then(_ESQuestionId => {
        return queryInterface.bulkInsert('ESQuestionOption',
          [
            { ESQuestionId: _ESQuestionId, optionString: 'Excellent', createdAt: new Date(), updatedAt: new Date() },
            { ESQuestionId: _ESQuestionId, optionString: 'Very Good', createdAt: new Date(), updatedAt: new Date() },
            { ESQuestionId: _ESQuestionId, optionString: 'Good', createdAt: new Date(), updatedAt: new Date() },
            { ESQuestionId: _ESQuestionId, optionString: 'Fair', createdAt: new Date(), updatedAt: new Date() },
            { ESQuestionId: _ESQuestionId, optionString: 'Poor', createdAt: new Date(), updatedAt: new Date() }
          ], {});
      });
    }).then(() => {
      return queryInterface.rawSelect('ESQuestion', { where: { question: 'Rooms are air-conditioned?' } }, ['id']).then(_ESQuestionId => {
        return queryInterface.bulkInsert('ESQuestionOption',
          [
            { ESQuestionId: _ESQuestionId, optionString: 'Yes', createdAt: new Date(), updatedAt: new Date() },
            { ESQuestionId: _ESQuestionId, optionString: 'No', createdAt: new Date(), updatedAt: new Date() }
          ], {});
      });
    }).then(() => {
      return queryInterface.rawSelect('ESQuestion', { where: { question: 'Facility of the air-conditioners in classrooms?' } }, ['id']).then(_ESQuestionId => {
        return queryInterface.bulkInsert('ESQuestionOption',
          [
            { ESQuestionId: _ESQuestionId, optionString: 'Yes', createdAt: new Date(), updatedAt: new Date() },
            { ESQuestionId: _ESQuestionId, optionString: 'No', createdAt: new Date(), updatedAt: new Date() }
          ], {});
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ESQuestionOption', null, {});
  }
};
