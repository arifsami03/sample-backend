'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.rawSelect('Configuration', { where: { key: 'country', value: 'Pakistan' } }, ['id']).then(_configurationId => {
      return queryInterface.bulkInsert(
        'ConfigurationOption',
        [
          { configurationId: _configurationId, key: 'abbreviation', value: 'PAK', createdAt: new Date(), updatedAt: new Date() },
          { configurationId: _configurationId, key: 'countryCode', value: '92', createdAt: new Date(), updatedAt: new Date() }
        ],
        {}
      )
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ConfigurationOption', {
      key: ['countryCode', 'abbreviation']
    }, {});
  }
};
