'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // Delete all appFeatures which have configuration.* as parent then add them with settingsMod.* as parent
    return queryInterface.bulkDelete('AppFeature', {
      id: ['evaluationSheets.*', 'workFlows.*', 'emailTemplates.*', 'outGoingMailServers.*', 'banks.*', 'configuration.*']
    }, {}).then(() => {
      return queryInterface.bulkInsert('AppFeature', [
        { id: 'evaluationSheets.*', parentId: 'settingsMod.*', title: 'Evaluation Sheets', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'workFlows.*', parentId: 'settingsMod.*', title: 'Work Flows', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'emailTemplates.*', parentId: 'settingsMod.*', title: 'Email Templates', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'outGoingMailServers.*', parentId: 'settingsMod.*', title: 'Out Going Mail Server', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'banks.*', parentId: 'settingsMod.*', title: 'Banks', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      ], {});
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: ['evaluationSheets.*', 'workFlows.*', 'emailTemplates.*', 'outGoingMailServers.*', 'banks.*', 'configuration.*']
    }, {}).then(() => {
      return queryInterface.bulkInsert('AppFeature', [
        { id: 'configuration.*', parentId: null, title: 'Configuration', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'evaluationSheets.*', parentId: 'configuration.*', title: 'Evaluation Sheets', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'workFlows.*', parentId: 'configuration.*', title: 'Work Flows', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'emailTemplates.*', parentId: 'configuration.*', title: 'Email Templates', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'outGoingMailServers.*', parentId: 'configuration.*', title: 'Out Going Mail Server', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'banks.*', parentId: 'configuration.*', title: 'Banks', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      ], {});
    });
  }
};
