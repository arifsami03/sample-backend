'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Role',
      [
        {
          name: 'Applicant',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    //TODO:high should delete user otherwise undo will not work
    return queryInterface.bulkDelete(
      'Role',
      {
        name: 'Applicant'
      },
      {}
    );
  }
};
