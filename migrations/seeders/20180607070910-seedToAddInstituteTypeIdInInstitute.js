'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.rawSelect('InstituteType', {
      where: {
        name: 'University'
      }
    }, ['id']).then(instituteTypeId => {
      // return queryInterface.sequelize.query('UPDATE Institute set instituteTypeId ='+instituteTypeId+' ');
      return queryInterface.sequelize.query(`UPDATE Institute set instituteTypeId = ${instituteTypeId}`);
    })


  },

  down: (queryInterface, Sequelize) => {
    // return true;

    return queryInterface.sequelize.query(`UPDATE Institute set instituteTypeId = null`);
  }
};