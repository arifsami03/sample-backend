'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        { id: 'concessions.*', parentId: 'feeManagementMod.*', title: 'Concessions', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'concessions.createWithFeeHeads', parentId: 'concessions.*', title: 'Create Concession', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'concessions.index', parentId: 'concessions.createWithFeeHeads', title: 'View Concessions', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'concessions.updateWithFeeHeads', parentId: 'concessions.*', title: 'Update Concession', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'concessions.index', parentId: 'concessions.updateWithFeeHeads', title: 'View Concessions', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'concessions.deleteWithFeeHeads', parentId: 'concessions.*', title: 'Delete Concessions', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'concessions.deleteConcessionFeeHead', parentId: 'concessions.deleteWithFeeHeads', title: 'Delete Concession Fee Head', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'concessions.index', parentId: 'concessions.deleteWithFeeHeads', title: 'View Concessions', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'concessions.findWithFeeHeads', parentId: 'concessions.index', title: 'View Concession', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'feeHeads.findAttributesList', parentId: 'concessions.findWithFeeHeads', title: 'Get Fee Heads List', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: [
          'concessions.*',
          'concessions.createWithFeeHeads',
          'concessions.updateWithFeeHeads',
          'concessions.deleteWithFeeHeads',
          'concessions.deleteConcessionFeeHead',
          'concessions.index',
          'concessions.findWithFeeHeads',
          'feeHeads.findAttributesList'
        ]
      },
      {}
    );
  }
};
