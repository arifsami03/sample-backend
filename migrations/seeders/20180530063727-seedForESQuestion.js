'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.rawSelect('ESSection', { where: { title: 'Building' } }, ['id']).then(_ESSectionId => {
      return queryInterface.bulkInsert('ESQuestion',
        [
          { ESSectionId: _ESSectionId, question: 'Does the building has guard facility?', countField: 0, remarksField: 0, optionType: 'selection', createdAt: new Date(), updatedAt: new Date() },
          { ESSectionId: _ESSectionId, question: 'Does the building has security cameras?', countField: 0, remarksField: 0, optionType: 'selection', createdAt: new Date(), updatedAt: new Date() },
          { ESSectionId: _ESSectionId, question: 'Building provides high security?', countField: 0, remarksField: 0, optionType: 'ratingTypeOne', createdAt: new Date(), updatedAt: new Date() },
          { ESSectionId: _ESSectionId, question: 'Safety Provided by the building?', countField: 0, remarksField: 0, optionType: 'ratingTypeTwo', createdAt: new Date(), updatedAt: new Date() },
          { ESSectionId: _ESSectionId, question: 'Does the building has internet facility?', countField: 0, remarksField: 0, optionType: 'selection', createdAt: new Date(), updatedAt: new Date() },
        ], {})
    }).then(() => {
      return queryInterface.rawSelect('ESSection', { where: { title: 'Classrooms' } }, ['id']).then(_ESSectionId => {
        return queryInterface.bulkInsert('ESQuestion',
          [
            { ESSectionId: _ESSectionId, question: 'Seating capacity of the room?', countField: 0, remarksField: 0, optionType: 'ratingTypeTwo', createdAt: new Date(), updatedAt: new Date() },
            { ESSectionId: _ESSectionId, question: 'Rooms are air-conditioned?', countField: 0, remarksField: 0, optionType: 'selection', createdAt: new Date(), updatedAt: new Date() },
            { ESSectionId: _ESSectionId, question: 'Facility of the air-conditioners in classrooms?', countField: 1, remarksField: 0, optionType: 'selectionWithCount', createdAt: new Date(), updatedAt: new Date() },
          ], {})
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ESQuestion', null, {})

  }
};
