'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('ESCategory',
      [
        { name: 'Preliminary', createdAt: new Date(), updatedAt: new Date() },
        { name: 'Detailed', createdAt: new Date(), updatedAt: new Date() },
        { name: 'Extra Ordinary', createdAt: new Date(), updatedAt: new Date() }
      ],
      {}
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ESCategory', null, {});
  }
};
