'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        { id: 'campusPostRegistraitonMod.*', parentId: null, title: 'Campus Post Registration', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        
        { id: 'postRegistrations.*', parentId: 'campusPostRegistraitonMod.*', title: 'Post Registration', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'postRegistrations.status', parentId: 'postRegistrations.*', title: 'Change Status of Post Registration', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'postRegistrations.index', parentId: 'postRegistrations.status', title: 'View Post Registration', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'postRegistrations.findUserId', parentId: 'postRegistrations.status', title: 'Find User', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'postRegistrations.findStatus', parentId: 'postRegistrations.status', title: 'Find Status', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        
        { id: 'postRegistrations.save', parentId: 'postRegistrations.*', title: 'Save Post Registration', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        //TODO:high not revoking permission I dont know what is the issue or I am implementing incorrect
        { id: 'postRegistrations.findStatus', parentId: 'postRegistrations.save', title: 'Find Status', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'programDetails.getProgramProgramDetails', parentId: 'postRegistrations.*', title: 'View Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: ['campusPostRegistraitonMod.*', 'postRegistrations.*', 'postRegistrations.status', 'postRegistrations.index', 'postRegistrations.findUserId', 'postRegistrations.findStatus', 'postRegistrations.save']
      },
      {}
    );
  }
};
