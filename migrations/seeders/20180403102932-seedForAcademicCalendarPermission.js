'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        { id: 'academicCalendars.*', parentId: 'academicsMod.*', title: 'Academic Calendars', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'academicCalendars.create', parentId: 'academicCalendars.*', title: 'Create Academic Calendar', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'academicCalendars.index', parentId: 'academicCalendars.create', title: 'View Academic Calendars', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'academicCalendars.update', parentId: 'academicCalendars.*', title: 'Update Academic Calendar', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'academicCalendars.index', parentId: 'academicCalendars.update', title: 'View Academic Calendars', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'academicCalendars.delete', parentId: 'academicCalendars.*', title: 'Delete Academic Calendars', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'academicCalendars.index', parentId: 'academicCalendars.delete', title: 'View Academic Calendars', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'academicCalendars.find', parentId: 'academicCalendars.index', title: 'View Academic Calendar', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'programDetails.findAttributesList', parentId: 'academicCalendars.find', title: 'Get Programs List', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: [
          'academicCalendars.*',
          'academicCalendars.create',
          'academicCalendars.update',
          'academicCalendars.delete',
          'academicCalendars.index',
          'academicCalendars.find',
          'programDetails.findAttributesList'
        ]
      },
      {}
    );
  }
};
