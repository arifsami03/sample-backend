'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [

        { id: 'campusMOUVerifications.create', parentId: 'campusMod.*', title: 'Campus MOU Verification', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'campusMOUVerifications.find', parentId: 'campusMOUVerifications.create', title: 'View Campus MOU Verification', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'campusMOUVerifications.update', parentId: 'campusMOUVerifications.create', title: 'Update Campus MOU Verification', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'campusMOUVerifications.find', parentId: 'campusMOUVerifications.update', title: 'View Campus MOU Verification', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'campusMOU.find', parentId: 'campusMOUVerifications.find', title: 'View Banks', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'banks.findAttributesList', parentId: 'campusMOUVerifications.find', title: 'View Banks', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'configurations.index', parentId: 'campusMOUVerifications.find', title: 'View Instrument Type', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: [
          'campusMOUVerifications.create',         
          'campusMOUVerifications.update',
          'campusMOUVerifications.find',
          'banks.findAttributesList',
          'configurations.index'

        ]
      },
      {}
    );
  }
};
