'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.sequelize.query(`UPDATE FeaturePermission SET featureId = ? WHERE featureId = ? `, {
      replacements: ['managementFees.*', 'licenseFees.*']

    }).then(() => {

      return queryInterface.sequelize.query(`UPDATE FeaturePermission SET featureId = ? WHERE featureId = ? `, {
        replacements: ['managementFees.create', 'licenseFees.create']

      }).then(() => {

        return queryInterface.sequelize.query(`UPDATE FeaturePermission SET featureId = ? WHERE featureId = ? `, {
          replacements: ['managementFees.update', 'licenseFees.update']

        }).then(() => {

          return queryInterface.sequelize.query(`UPDATE FeaturePermission SET featureId = ? WHERE featureId = ? `, {
            replacements: ['managementFees.delete', 'licenseFees.delete']

          }).then(() => {

            return queryInterface.sequelize.query(`UPDATE FeaturePermission SET featureId = ? WHERE featureId = ? `, {
              replacements: ['managementFees.index', 'licenseFees.index']

            }).then(() => {

              return queryInterface.sequelize.query(`UPDATE FeaturePermission SET featureId = ? WHERE featureId = ? `, {
                replacements: ['managementFees.find', 'licenseFees.find']

              }).then(() => {

                return queryInterface.sequelize.query(`UPDATE FeaturePermission SET featureId = ? WHERE featureId = ? `, {
                  replacements: ['managementFees.findAttributesList', 'licenseFees.findAttributesList']

                })
              })
            })
          })
        })
      })
    })
  },

  down: (queryInterface, Sequelize) => {
    return true;
  }
}