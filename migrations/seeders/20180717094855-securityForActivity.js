'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
      
        { id: 'activities.*', parentId: 'academicsMod.*', title: 'Activity', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'activities.create', parentId: 'activities.*', title: 'Create Activity', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'activities.index', parentId: 'activities.create', title: 'View Activity', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'activities.update', parentId: 'activities.*', title: 'Update Activity', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'activities.index', parentId: 'activities.update', title: 'View Activity', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'activities.delete', parentId: 'activities.*', title: 'Delete Activity', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'activities.index', parentId: 'activities.delete', title: 'View Activity', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'activities.find', parentId: 'activities.index', title: 'View Activity', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: ['activities.*', 'activities.create', 'activities.update', 'activities.delete', 'activities.index', 'activities.find']
      },
      {}
    );
  }
};
