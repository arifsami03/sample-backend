'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        { id: 'workFlows.*', parentId: 'configuration.*', title: 'Work Flows', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'workFlows.create', parentId: 'workFlows.*', title: 'Create Work Flow', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'workFlows.index', parentId: 'workFlows.create', title: 'View Work Flows', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'workFlows.update', parentId: 'workFlows.*', title: 'Update Work Flow', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'workFlows.index', parentId: 'workFlows.update', title: 'View Work Flows', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'workFlows.delete', parentId: 'workFlows.*', title: 'Delete Work Flow', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'workFlows.index', parentId: 'workFlows.delete', title: 'View Work Flows', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'workFlows.find', parentId: 'workFlows.index', title: 'View Work Flow', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        
        { id: 'WFStates.*', parentId: 'workFlows.*', title: 'Work Flow States', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'WFStates.create', parentId: 'WFStates.*', title: 'Create WF State', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'WFStates.list', parentId: 'WFStates.create', title: 'Get WF States', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'WFStates.update', parentId: 'WFStates.*', title: 'Update WF State', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'WFStates.findById', parentId: 'WFStates.update', title: 'Update WF State', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'WFStates.list', parentId: 'WFStates.update', title: 'Get WF States', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'WFStates.delete', parentId: 'WFStates.*', title: 'Delete WF State', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'WFStates.list', parentId: 'WFStates.delete', title: 'Get WF States', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'WFStates.list', parentId: 'workFlows.find', title: 'Get WF States', isVisible: false, createdAt: new Date(), updatedAt: new Date() },


        { id: 'WFStateFlows.*', parentId: 'workFlows.*', title: 'WF State Flows', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'WFStateFlows.create', parentId: 'WFStateFlows.*', title: 'Create WF State Flows', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'WFStateFlows.list', parentId: 'WFStateFlows.create', title: 'Get WF States Flow', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'WFStateFlows.update', parentId: 'WFStateFlows.*', title: 'Update WF State Flow', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'WFStateFlows.findById', parentId: 'WFStateFlows.update', title: 'Update WF State Flow', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'WFStateFlows.list', parentId: 'WFStateFlows.update', title: 'Get WF State Flows', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'WFStateFlows.delete', parentId: 'WFStateFlows.*', title: 'Delete WF State Flow', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'WFStateFlows.list', parentId: 'WFStateFlows.delete', title: 'Get WF State Flows', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'WFStateFlows.list', parentId: 'workFlows.find', title: 'Get WF State Flows', isVisible: false, createdAt: new Date(), updatedAt: new Date() }

      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: [
          'workFlows.*',
          'workFlows.create',
          'workFlows.update',
          'workFlows.delete',
          'workFlows.index',
          'workFlows.find',
          'WFStates.*',
          'WFStates.create',
          'WFStates.update',
          'WFStates.delete',
          'WFStates.list',
          'WFStates.findById',
          'WFStateFlows.*',
          'WFStateFlows.create',
          'WFStateFlows.update',
          'WFStateFlows.delete',
          'WFStateFlows.list',
          'WFStateFlows.findById',

        ]
      },
      {}
    );
  }
};
