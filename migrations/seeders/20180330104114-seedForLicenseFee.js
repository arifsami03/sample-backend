'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature', [
        { id: 'licenseFees.*', parentId: 'feeManagementMod.*', title: 'License Fee', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'licenseFees.create', parentId: 'licenseFees.*', title: 'Create License Fee', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'licenseFees.index', parentId: 'licenseFees.create', title: 'View License Fees', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'licenseFees.update', parentId: 'licenseFees.*', title: 'Update License Fee', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'licenseFees.index', parentId: 'licenseFees.update', title: 'View License Fees', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'licenseFees.delete', parentId: 'licenseFees.*', title: 'Delete License Fees', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'licenseFees.index', parentId: 'licenseFees.delete', title: 'View License Fees', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'licenseFees.find', parentId: 'licenseFees.index', title: 'View License Fee', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'educationLevels.findAttributesList', parentId: 'licenseFees.find', title: 'Get Education Level', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
      ], {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature', {
        id: [
          'licenseFees.*',
          'licenseFees.create',
          'licenseFees.update',
          'licenseFees.delete',
          'licenseFees.index',
          'licenseFees.find',
          'educationLevels.findAttributesList'
        ]
      }, {}
    );
  }
};