'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('WorkFlow', [{ WFId: 'post-registration', title: 'post-Registration', description: '', createdAt: new Date(), updatedAt: new Date() }], {}).then(workFlow => {
      return queryInterface.bulkInsert('WFState', [{ WFId: 'post-registration', state: 'submit', title: 'Submit', description: '', createdAt: new Date(), updatedAt: new Date() },

      { WFId: 'post-registration', state: 'approve', title: 'Approve', description: '', createdAt: new Date(), updatedAt: new Date() },
      { WFId: 'post-registration', state: 'reject', title: 'Reject', description: '', createdAt: new Date(), updatedAt: new Date() },
      { WFId: 'post-registration', state: 'analysis', title: 'Analysis', description: '', createdAt: new Date(), updatedAt: new Date() },
      ], {})
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('WFState', { WFId: ['post-registration'] }, {}).then(() => {
      return queryInterface.bulkDelete('WorkFlow', { WFId: ['post-registration'] }, {});
    });
  }
};