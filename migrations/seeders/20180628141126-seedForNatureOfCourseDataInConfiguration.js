'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Configuration',
      [
        { key: 'natureOfCourse', value: 'Compulsory', createdAt: new Date(), updatedAt: new Date() },
        { key: 'natureOfCourse', value: 'Elective', createdAt: new Date(), updatedAt: new Date() }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Configuration', { key: ['natureOfCourse'] }, {});
  }
};
