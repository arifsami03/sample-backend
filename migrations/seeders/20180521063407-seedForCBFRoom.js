'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'cbfRooms.*', parentId: 'campusMod.*', title: 'Rooms', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      { id: 'cbfRooms.create', parentId: 'cbfRooms.*', title: 'Create Room', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'cbfRooms.index', parentId: 'cbfRooms.create', title: 'View Rooms', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'cbfRooms.update', parentId: 'cbfRooms.*', title: 'Update Room', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'cbfRooms.index', parentId: 'cbfRooms.update', title: 'View Rooms', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'cbfRooms.delete', parentId: 'cbfRooms.*', title: 'Delete Room', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'cbfRooms.index', parentId: 'cbfRooms.delete', title: 'View Rooms', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'cbfRooms.find', parentId: 'cbfRooms.index', title: 'View Room', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'cbFloors.findAttributesListWithCampusAndBuilding', parentId: 'cbfRooms.find', title: 'View Floor List', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: [
        'cbfRooms.*',
        'cbfRooms.create',
        'cbfRooms.index',
        'cbfRooms.update',
        'cbfRooms.delete',
        'cbfRooms.find',
        'cbFloors.findAttributesListWithCampusAndBuilding']
    }, {});
  }
};