'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // As metaConf table does not exist anymore in our system so return true on up and down scenario
    // TODO:normal: This file must be delete before going to production because MeatConf Table is changed to Configuration Table

    // return true;

    return queryInterface.bulkInsert(
      'Configuration', [{
          key: 'country',
          value: 'Pakistan',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        
      ], {}
    );
  },

  down: (queryInterface, Sequelize) => {
    // return true;

    return queryInterface.bulkDelete('Configuration', {
      key: ['country']
    }, {});
  }
};