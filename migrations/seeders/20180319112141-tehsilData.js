'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // As Configuration table does not exist anymore in our system so return true on up and down scenario
    // TODO:normal: This file must be delete before going to production because MeatConf Table is changed to Configuration Table

    // return true;

    return queryInterface.rawSelect('Configuration', { where: { key: 'city', value: 'Lahore' } }, ['id']).then(cityId => {
      return queryInterface.bulkInsert(
        'Configuration',
        [
          { key: 'tehsil', value: 'Lahore Cantt', parentKey: 'city', parentKeyId: cityId, createdAt: new Date(), updatedAt: new Date() },
          { key: 'tehsil', value: 'Model Town', parentKey: 'city', parentKeyId: cityId, createdAt: new Date(), updatedAt: new Date() },
          { key: 'tehsil', value: 'Raiwind', parentKey: 'city', parentKeyId: cityId, createdAt: new Date(), updatedAt: new Date() },
          { key: 'tehsil', value: 'Johar Town', parentKey: 'city', parentKeyId: cityId, createdAt: new Date(), updatedAt: new Date() },
          { key: 'tehsil', value: 'Lahore City', parentKey: 'city', parentKeyId: cityId, createdAt: new Date(), updatedAt: new Date() }
        ],
        {}
      );
    })
  },

  down: (queryInterface, Sequelize) => {
    // return true;

    return queryInterface.bulkDelete('Configuration', { key: ['tehsil'] }, {});
  }
};
