'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('WorkFlowLog', null, {}).then(() => {

      return queryInterface.bulkDelete('RoleWFStateFlow', null, {})

    }).then(() => {

      return queryInterface.bulkDelete('WFStateFlow', null, {})

    }).then(() => {

      return queryInterface.bulkDelete('WFState', null, {})

    }).then(() => {

      return queryInterface.bulkDelete('WorkFlow', null, {})

    }).then(() => {

      // Add Pre-Registration workflow
      return queryInterface.bulkInsert('WorkFlow', [{ WFId: 'pre-registration', title: 'Campus Pre-Registration', description: 'Campus Pre-Registration Workflow', createdAt: new Date(), updatedAt: new Date() }], {})

    }).then(() => {

      // Add Pre-Registration workflow states
      return queryInterface.bulkInsert('WFState', [
        { WFId: 'pre-registration', state: 'new', title: 'New', description: '', createdAt: new Date(), updatedAt: new Date() },
        { WFId: 'pre-registration', state: 'submit', title: 'Submit', description: 'Submit application', createdAt: new Date(), updatedAt: new Date() },
        { WFId: 'pre-registration', state: 'approve', title: 'Approve', description: 'Approve application', createdAt: new Date(), updatedAt: new Date() },
        { WFId: 'pre-registration', state: 'reject', title: 'Reject', description: 'Reject Applicaiton', createdAt: new Date(), updatedAt: new Date() },
      ], {})

    }).then(() => {

      // Add Pre-Registration WFStateFlow

      return queryInterface.rawSelect('WFState', { where: { state: 'new', WFId: 'pre-registration' } }, ['id']).then(fromRes => {
        return queryInterface.rawSelect('WFState', { where: { state: 'submit', WFId: 'pre-registration' } }, ['id']).then(toRes => {
          return queryInterface.bulkInsert('WFStateFlow', [{ WFId: 'pre-registration', fromId: fromRes, toId: toRes, createdAt: new Date(), updatedAt: new Date() },], {})
        });
      }).then(() => {

        return queryInterface.rawSelect('WFState', { where: { state: 'submit', WFId: 'pre-registration' } }, ['id']).then(fromRes => {
          return queryInterface.rawSelect('WFState', { where: { state: 'approve', WFId: 'pre-registration' } }, ['id']).then(toRes => {
            return queryInterface.bulkInsert('WFStateFlow', [{ WFId: 'pre-registration', fromId: fromRes, toId: toRes, createdAt: new Date(), updatedAt: new Date() },], {})
          });
        });

      }).then(() => {

        return queryInterface.rawSelect('WFState', { where: { state: 'submit', WFId: 'pre-registration' } }, ['id']).then(fromRes => {
          return queryInterface.rawSelect('WFState', { where: { state: 'reject', WFId: 'pre-registration' } }, ['id']).then(toRes => {
            return queryInterface.bulkInsert('WFStateFlow', [{ WFId: 'pre-registration', fromId: fromRes, toId: toRes, createdAt: new Date(), updatedAt: new Date() },], {})
          });
        });

      });


    }).then(() => {
      // Add Post-Registration workflow
      return queryInterface.bulkInsert('WorkFlow', [{ WFId: 'post-registration', title: 'Campus Post-Registration', description: 'Campus Post-Registration Workflow', createdAt: new Date(), updatedAt: new Date() }], {})

    }).then(() => {

      // Add Post-Registration workflow states
      return queryInterface.bulkInsert('WFState', [
        { WFId: 'post-registration', state: 'new', title: 'New', description: '', createdAt: new Date(), updatedAt: new Date() },
        { WFId: 'post-registration', state: 'draft', title: 'Draft', description: 'Application is in draft mode.', createdAt: new Date(), updatedAt: new Date() },
        { WFId: 'post-registration', state: 'submit', title: 'Submit', description: 'Submitted application', createdAt: new Date(), updatedAt: new Date() },
        { WFId: 'post-registration', state: 'forward-for-analysis', title: 'Analysis', description: 'Forward application for analysis', createdAt: new Date(), updatedAt: new Date() },
        { WFId: 'post-registration', state: 'submit-analysis', title: 'Forward to Admin', description: 'Submit application analysis', createdAt: new Date(), updatedAt: new Date() },
        { WFId: 'post-registration', state: 'forward-for-create-mou', title: 'Approve', description: 'Forward application to create MOU', createdAt: new Date(), updatedAt: new Date() },
        { WFId: 'post-registration', state: 'mou-approval', title: 'Forward', description: 'Forward application for MOU approval', createdAt: new Date(), updatedAt: new Date() },
        { WFId: 'post-registration', state: 'create-campus', title: 'Approve', description: 'Create campus', createdAt: new Date(), updatedAt: new Date() },
        { WFId: 'post-registration', state: 'forward-for-account-info', title: 'Forward for Account Info', description: 'Forward application for account informaiton and approval', createdAt: new Date(), updatedAt: new Date() },
        { WFId: 'post-registration', state: 'done', title: 'Approve', description: 'Application is approved', createdAt: new Date(), updatedAt: new Date() },
        { WFId: 'post-registration', state: 'reject', title: 'Reject', description: 'Reject Application', createdAt: new Date(), updatedAt: new Date() },
      ], {})

    }).then(() => {

       // Add Post-Registration WFStateFlow

       // New -> Draft
       return queryInterface.rawSelect('WFState', { where: { state: 'new', WFId: 'post-registration' } }, ['id']).then(fromRes => {
        return queryInterface.rawSelect('WFState', { where: { state: 'draft', WFId: 'post-registration' } }, ['id']).then(toRes => {
          return queryInterface.bulkInsert('WFStateFlow', [{ WFId: 'post-registration', fromId: fromRes, toId: toRes, createdAt: new Date(), updatedAt: new Date() },], {})
        });

      }).then(() => {

        // Draft -> Submit 
        return queryInterface.rawSelect('WFState', { where: { state: 'draft', WFId: 'post-registration' } }, ['id']).then(fromRes => {
          return queryInterface.rawSelect('WFState', { where: { state: 'submit', WFId: 'post-registration' } }, ['id']).then(toRes => {
            return queryInterface.bulkInsert('WFStateFlow', [{ WFId: 'post-registration', fromId: fromRes, toId: toRes, createdAt: new Date(), updatedAt: new Date() },], {})
          });
        });

      }).then(() => {

        // submit -> forward for create mou
        return queryInterface.rawSelect('WFState', { where: { state: 'submit', WFId: 'post-registration' } }, ['id']).then(fromRes => {
          return queryInterface.rawSelect('WFState', { where: { state: 'forward-for-create-mou', WFId: 'post-registration' } }, ['id']).then(toRes => {
            return queryInterface.bulkInsert('WFStateFlow', [{ WFId: 'post-registration', fromId: fromRes, toId: toRes, createdAt: new Date(), updatedAt: new Date() },], {})
          });
        });

      }).then(() => {

        // Submit -> Reject
        return queryInterface.rawSelect('WFState', { where: { state: 'submit', WFId: 'post-registration' } }, ['id']).then(fromRes => {
          return queryInterface.rawSelect('WFState', { where: { state: 'reject', WFId: 'post-registration' } }, ['id']).then(toRes => {
            return queryInterface.bulkInsert('WFStateFlow', [{ WFId: 'post-registration', fromId: fromRes, toId: toRes, createdAt: new Date(), updatedAt: new Date() },], {})
          });
        });

      }).then(() => {

        // Submit -> Forward for analysis
        return queryInterface.rawSelect('WFState', { where: { state: 'submit', WFId: 'post-registration' } }, ['id']).then(fromRes => {
          return queryInterface.rawSelect('WFState', { where: { state: 'forward-for-analysis', WFId: 'post-registration' } }, ['id']).then(toRes => {
            return queryInterface.bulkInsert('WFStateFlow', [{ WFId: 'post-registration', fromId: fromRes, toId: toRes, createdAt: new Date(), updatedAt: new Date() },], {})
          });
        });

      }).then(() => {

        // Analysis -> Submit Analysis (Forward to admin)
        return queryInterface.rawSelect('WFState', { where: { state: 'forward-for-analysis', WFId: 'post-registration' } }, ['id']).then(fromRes => {
          return queryInterface.rawSelect('WFState', { where: { state: 'submit-analysis', WFId: 'post-registration' } }, ['id']).then(toRes => {
            return queryInterface.bulkInsert('WFStateFlow', [{ WFId: 'post-registration', fromId: fromRes, toId: toRes, createdAt: new Date(), updatedAt: new Date() },], {})
          });
        });

      }).then(() => {

        // Submit Analysis (Forward to admin) -> Forward for Analysis
        return queryInterface.rawSelect('WFState', { where: { state: 'submit-analysis', WFId: 'post-registration' } }, ['id']).then(fromRes => {
          return queryInterface.rawSelect('WFState', { where: { state: 'forward-for-analysis', WFId: 'post-registration' } }, ['id']).then(toRes => {
            return queryInterface.bulkInsert('WFStateFlow', [{ WFId: 'post-registration', fromId: fromRes, toId: toRes, createdAt: new Date(), updatedAt: new Date() },], {})
          });
        });

      }).then(() => {

        // Submit Analysis (Forward to admin) -> Reject
        return queryInterface.rawSelect('WFState', { where: { state: 'submit-analysis', WFId: 'post-registration' } }, ['id']).then(fromRes => {
          return queryInterface.rawSelect('WFState', { where: { state: 'reject', WFId: 'post-registration' } }, ['id']).then(toRes => {
            return queryInterface.bulkInsert('WFStateFlow', [{ WFId: 'post-registration', fromId: fromRes, toId: toRes, createdAt: new Date(), updatedAt: new Date() },], {})
          });
        });

      }).then(() => {

        // Submit Analysis (Forward to admin) -> Forward for Create MOU (Approve)
        return queryInterface.rawSelect('WFState', { where: { state: 'submit-analysis', WFId: 'post-registration' } }, ['id']).then(fromRes => {
          return queryInterface.rawSelect('WFState', { where: { state: 'forward-for-create-mou', WFId: 'post-registration' } }, ['id']).then(toRes => {
            return queryInterface.bulkInsert('WFStateFlow', [{ WFId: 'post-registration', fromId: fromRes, toId: toRes, createdAt: new Date(), updatedAt: new Date() },], {})
          });
        });

      }).then(() => {

        // Forward for create MOU (Approve) -> MOU Approval (Forward)
        return queryInterface.rawSelect('WFState', { where: { state: 'forward-for-create-mou', WFId: 'post-registration' } }, ['id']).then(fromRes => {
          return queryInterface.rawSelect('WFState', { where: { state: 'mou-approval', WFId: 'post-registration' } }, ['id']).then(toRes => {
            return queryInterface.bulkInsert('WFStateFlow', [{ WFId: 'post-registration', fromId: fromRes, toId: toRes, createdAt: new Date(), updatedAt: new Date() },], {})
          });
        });

      }).then(() => {

        // MOU Approval (Forward) -> Create Campus
        return queryInterface.rawSelect('WFState', { where: { state: 'mou-approval', WFId: 'post-registration' } }, ['id']).then(fromRes => {
          return queryInterface.rawSelect('WFState', { where: { state: 'create-campus', WFId: 'post-registration' } }, ['id']).then(toRes => {
            return queryInterface.bulkInsert('WFStateFlow', [{ WFId: 'post-registration', fromId: fromRes, toId: toRes, createdAt: new Date(), updatedAt: new Date() },], {})
          });
        });

      }).then(() => {

        // MOU Approval (Forward) -> Reject
        return queryInterface.rawSelect('WFState', { where: { state: 'mou-approval', WFId: 'post-registration' } }, ['id']).then(fromRes => {
          return queryInterface.rawSelect('WFState', { where: { state: 'reject', WFId: 'post-registration' } }, ['id']).then(toRes => {
            return queryInterface.bulkInsert('WFStateFlow', [{ WFId: 'post-registration', fromId: fromRes, toId: toRes, createdAt: new Date(), updatedAt: new Date() },], {})
          });
        });

      }).then(() => {

        // Create Campus -> Forward for account information
        return queryInterface.rawSelect('WFState', { where: { state: 'create-campus', WFId: 'post-registration' } }, ['id']).then(fromRes => {
          return queryInterface.rawSelect('WFState', { where: { state: 'forward-for-account-info', WFId: 'post-registration' } }, ['id']).then(toRes => {
            return queryInterface.bulkInsert('WFStateFlow', [{ WFId: 'post-registration', fromId: fromRes, toId: toRes, createdAt: new Date(), updatedAt: new Date() },], {})
          });
        });

      }).then(() => {

        // Forward for account information -> Done
        return queryInterface.rawSelect('WFState', { where: { state: 'forward-for-account-info', WFId: 'post-registration' } }, ['id']).then(fromRes => {
          return queryInterface.rawSelect('WFState', { where: { state: 'done', WFId: 'post-registration' } }, ['id']).then(toRes => {
            return queryInterface.bulkInsert('WFStateFlow', [{ WFId: 'post-registration', fromId: fromRes, toId: toRes, createdAt: new Date(), updatedAt: new Date() },], {})
          });
        });

      }).then(() => {

        // Forward for account information -> Reject
        return queryInterface.rawSelect('WFState', { where: { state: 'forward-for-account-info', WFId: 'post-registration' } }, ['id']).then(fromRes => {
          return queryInterface.rawSelect('WFState', { where: { state: 'reject', WFId: 'post-registration' } }, ['id']).then(toRes => {
            return queryInterface.bulkInsert('WFStateFlow', [{ WFId: 'post-registration', fromId: fromRes, toId: toRes, createdAt: new Date(), updatedAt: new Date() },], {})
          });
        });

      });


    }).then(() => {
      return queryInterface.rawSelect('WFState', { where: { state: 'submit', WFId: 'pre-registration' } }, ['id']).then(wfStateId => {
        return queryInterface.sequelize.query(`UPDATE CRApplication SET stateId = ${wfStateId}`)
      })
      
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('WorkFlowLog', null, {}).then(() => {

      return queryInterface.bulkDelete('RoleWFStateFlow', null, {})

    }).then(() => {

      return queryInterface.bulkDelete('WFStateFlow', null, {})

    }).then(() => {

      return queryInterface.bulkDelete('WFState', null, {})

    }).then(() => {

      return queryInterface.bulkDelete('WorkFlow', null, {})

    });
  }
};
