'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        { id: 'campusAcademicCalendars.*', parentId: 'campusMod.*', title: 'Academic Calendar Mapping', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'campusAcademicCalendars.createWithCampusAndAcademicCalendarProgramDetails', parentId: 'campusAcademicCalendars.*', title: 'Create Campus Academic Calendar Mapping', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'campusAcademicCalendars.index', parentId: 'campusAcademicCalendars.createWithCampusAndAcademicCalendarProgramDetails', title: 'View Campus Academic Calendar Mapping', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'campusAcademicCalendars.updateWithCampusAndAcademicCalendarProgramDetails', parentId: 'campusAcademicCalendars.*', title: 'Update Campus Academic Calendar Mapping', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'campusAcademicCalendars.index', parentId: 'campusAcademicCalendars.updateWithCampusAndAcademicCalendarProgramDetails', title: 'View Campus Academic Calendar Mapping', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'campusAcademicCalendars.delete', parentId: 'campusAcademicCalendars.*', title: 'Delete Campus Academic Calendar Mapping', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'campusAcademicCalendars.index', parentId: 'campusAcademicCalendars.delete', title: 'View Campus Academic Calendar Mapping', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'campusAcademicCalendars.findWithAcademicCalendars', parentId: 'campusAcademicCalendars.index', title: 'View Campus Academic Calendar Mapping', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'campus.findAttributesList', parentId: 'campusAcademicCalendars.findWithAcademicCalendar', title: 'Get Campus List', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: [
          'campusAcademicCalendars.*',
          'campusAcademicCalendars.createWithCampusAndAcademicCalendarProgramDetails',
          'campusAcademicCalendars.updateWithCampusAndAcademicCalendarProgramDetails',
          'campusAcademicCalendars.delete',
          'campusAcademicCalendars.index',
          'campusAcademicCalendars.findWithAcademicCalendars',
          'campus.findAttributesList'
        ]
      },
      {}
    );
  }
};
