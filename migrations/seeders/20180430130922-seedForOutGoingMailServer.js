'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'OutgoingMailServer',
      [
        {
          isDefault: true,
          smtpServer: 'smtp.gmail.com',
          smtpPort: 465,
          connectionSecurity: 'ssl',
          username: 'developer.muhammadumair@gmail.com',
          password: 'Programming4Fun',
          name: 'Default Server',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('EmailTemplateAssignment', null, {}).then(() => {
      return queryInterface.bulkDelete('EmailTemplate', null, {}).then(() => {
        return queryInterface.bulkDelete('OutgoingMailServer', null, {});
      });
    });
  }
};
