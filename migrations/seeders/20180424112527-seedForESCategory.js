'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        { id: 'settingsMod.*', parentId: null, title: 'Settings', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esCategories.*', parentId: 'settingsMod.*', title: 'Evaluation Sheet Category', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esCategories.create', parentId: 'esCategories.*', title: 'Create ES Category', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esCategories.index', parentId: 'esCategories.create', title: 'View ES Categories', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esCategories.update', parentId: 'esCategories.*', title: 'Update ES Category', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esCategories.index', parentId: 'esCategories.update', title: 'View ES Categories', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esCategories.delete', parentId: 'esCategories.*', title: 'Delete ES Category', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esCategories.index', parentId: 'esCategories.delete', title: 'View ES Categories', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esCategories.find', parentId: 'esCategories.index', title: 'View ES Category', isVisible: false, createdAt: new Date(), updatedAt: new Date() },

      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: [
          'settingsMod.*',
          'esCategories.*',
          'esCategories.create',
          'esCategories.update',
          'esCategories.delete',
          'esCategories.index',
          'esCategories.find',
        ]
      },
      {}
    );
  }
};
