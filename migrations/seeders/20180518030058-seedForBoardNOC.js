'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'boardNOCNotRequired.*', parentId: 'boards.*', title: 'Board NOC', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'boards.addNOCNotRequired', parentId: 'boardNOCNotRequired.*', title: 'Create Board NOC', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'boards.findAllNOCNotRequiredBoards', parentId: 'boardNOCNotRequired.addNOCNotRequired', title: 'View Board NOC', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'boards.findAllBoardsToAddNOC', parentId: 'boardNOCNotRequired.addNOCNotRequired', title: 'View Board NOC for Create', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'boards.deleteNOCNotRequuired', parentId: 'boardNOCNotRequired.*', title: 'Delete Board NOC', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'boards.findAllNOCNotRequiredBoards', parentId: 'boardNOCNotRequired.deleteNOCNotRequuired', title: 'View Board NOC', isVisible: false, createdAt: new Date(), updatedAt: new Date() },

      // Associate Board NOC permissions with Board
      { id: 'boards.addNOCNotRequired', parentId: 'boards.create', title: 'Create Board NOC', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'boards.addNOCNotRequired', parentId: 'boards.update', title: 'Create Board NOC', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'boards.deleteNOCNotRequuired', parentId: 'boards.update', title: 'Delete Board NOC', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'boards.deleteNOCNotRequuired', parentId: 'boards.delete', title: 'Delete Board NOC', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'boards.findAllNOCNotRequiredBoards', parentId: 'boards.find', title: 'View Board NOC', isVisible: false, createdAt: new Date(), updatedAt: new Date() },

    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: [
        'boardNOCNotRequired.*',
        'boards.addNOCNotRequired',
        'boards.deleteNOCNotRequuired',
        'boards.findAllNOCNotRequiredBoards',
        'boards.findAllBoardsToAddNOC']
    }, {});
  }
};