'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'vendors.*', parentId: 'settingsMod.*', title: 'Vendor', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      { id: 'vendors.create', parentId: 'vendors.*', title: 'Create Vendor', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'vendors.index', parentId: 'vendors.create', title: 'View Vendor', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'vendors.update', parentId: 'vendors.*', title: 'Update Vendor', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'vendors.index', parentId: 'vendors.update', title: 'View Vendor', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'vendors.delete', parentId: 'vendors.*', title: 'Delete Vendor', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'vendors.index', parentId: 'vendors.delete', title: 'View Vendor', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'vendors.find', parentId: 'vendors.index', title: 'View Vendor', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: [
        'vendors.*',
        'vendors.create',
        'vendors.index',
        'vendors.update',
        'vendors.delete',
        'vendors.find']
    }, {});
  }
};