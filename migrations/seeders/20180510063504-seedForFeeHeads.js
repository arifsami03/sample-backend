'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'feeHeads.*', parentId: 'feeManagementMod.*', title: 'Fee Heads', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      { id: 'feeHeads.create', parentId: 'feeHeads.*', title: 'Create Fee Heads', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'feeHeads.index', parentId: 'feeHeads.create', title: 'View Fee Heads', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'feeHeads.update', parentId: 'feeHeads.*', title: 'Update Fee Heads', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'feeHeads.index', parentId: 'feeHeads.update', title: 'View Fee Heads', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'feeHeads.delete', parentId: 'feeHeads.*', title: 'Delete Fee Heads', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'feeHeads.index', parentId: 'feeHeads.delete', title: 'View Fee Heads', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'feeHeads.find', parentId: 'feeHeads.index', title: 'View Fee Head', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'configurations.findAttributesList', parentId: 'feeHeads.find', title: 'View Nature of Fee Heads', isVisible: false, createdAt: new Date(), updatedAt: new Date() },

    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: [
        'feeHeads.*',
        'feeHeads.create',
        'feeHeads.index',
        'feeHeads.update',
        'feeHeads.delete',
        'feeHeads.find',
        'configurations.findAttributesList'
      ]
    }, {});
  }
};