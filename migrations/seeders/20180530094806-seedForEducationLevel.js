'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.rawSelect('InstituteType', { where: { name: 'School' } }, ['id']).then(_instituteTypeId => {
      return queryInterface.bulkInsert('EducationLevel',
        [
          { instituteTypeId: _instituteTypeId, education: 'Pre School', createdAt: new Date(), updatedAt: new Date() },
          { instituteTypeId: _instituteTypeId, education: 'Primary School', createdAt: new Date(), updatedAt: new Date() },
          { instituteTypeId: _instituteTypeId, education: 'Elementary School', createdAt: new Date(), updatedAt: new Date() },
          { instituteTypeId: _instituteTypeId, education: 'Secondary School', createdAt: new Date(), updatedAt: new Date() }
        ], {})
    }).then(() => {
      return queryInterface.rawSelect('InstituteType', { where: { name: 'College' } }, ['id']).then(_instituteTypeId => {
        return queryInterface.bulkInsert('EducationLevel',
          [
            { instituteTypeId: _instituteTypeId, education: 'College', createdAt: new Date(), updatedAt: new Date() },
          ], {})
      });
    }).then(() => {
      return queryInterface.rawSelect('InstituteType', { where: { name: 'University' } }, ['id']).then(_instituteTypeId => {
        return queryInterface.bulkInsert('EducationLevel',
          [
            { instituteTypeId: _instituteTypeId, education: 'University', createdAt: new Date(), updatedAt: new Date() },
          ], {})
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('EducationLevel', null, {})

  }
};
