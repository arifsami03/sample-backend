'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.rawSelect('Program', { where: { name: 'Pre School' } }, ['id']).then(_programId => {
      return queryInterface.bulkInsert('ProgramDetail',
        [
          { programId: _programId, name: 'Pre School', createdAt: new Date(), updatedAt: new Date() }
        ], {})
    }).then(() => {
      return queryInterface.rawSelect('Program', { where: { name: 'Primary School' } }, ['id']).then(_programId => {
        return queryInterface.bulkInsert('ProgramDetail',
          [
            { programId: _programId, name: 'Primary School', createdAt: new Date(), updatedAt: new Date() }
          ], {})
      });
    }).then(() => {
      return queryInterface.rawSelect('Program', { where: { name: 'Elementary School' } }, ['id']).then(_programId => {
        return queryInterface.bulkInsert('ProgramDetail',
          [
            { programId: _programId, name: 'Elementary School', createdAt: new Date(), updatedAt: new Date() }
          ], {})
      });
    }).then(() => {
      return queryInterface.rawSelect('Program', { where: { name: 'Secondary School' } }, ['id']).then(_programId => {
        return queryInterface.bulkInsert('ProgramDetail',
          [
            { programId: _programId, name: 'Secondary School', createdAt: new Date(), updatedAt: new Date() }
          ], {})
      });
    }).then(() => {
      return queryInterface.rawSelect('Program', { where: { name: 'F.Sc' } }, ['id']).then(_programId => {
        return queryInterface.bulkInsert('ProgramDetail',
          [
            { programId: _programId, name: 'Pre Medical', createdAt: new Date(), updatedAt: new Date() },
            { programId: _programId, name: 'Pre Engineering', createdAt: new Date(), updatedAt: new Date() }
          ], {})
      });
    }).then(() => {
      return queryInterface.rawSelect('Program', { where: { name: 'I.CS' } }, ['id']).then(_programId => {
        return queryInterface.bulkInsert('ProgramDetail',
          [
            { programId: _programId, name: 'I.CS', createdAt: new Date(), updatedAt: new Date() }
          ], {})
      });
    }).then(() => {
      return queryInterface.rawSelect('Program', { where: { name: 'I.Com' } }, ['id']).then(_programId => {
        return queryInterface.bulkInsert('ProgramDetail',
          [
            { programId: _programId, name: 'I.Com', createdAt: new Date(), updatedAt: new Date() }
          ], {})
      });
    }).then(() => {
      return queryInterface.rawSelect('Program', { where: { name: 'F.A' } }, ['id']).then(_programId => {
        return queryInterface.bulkInsert('ProgramDetail',
          [
            { programId: _programId, name: 'Economics', createdAt: new Date(), updatedAt: new Date() },
            { programId: _programId, name: 'English', createdAt: new Date(), updatedAt: new Date() },
            { programId: _programId, name: 'Sociology', createdAt: new Date(), updatedAt: new Date() }
          ], {})
      });
    }).then(() => {
      return queryInterface.rawSelect('Program', { where: { name: 'B.Com' } }, ['id']).then(_programId => {
        return queryInterface.bulkInsert('ProgramDetail',
          [
            { programId: _programId, name: 'B.Com', createdAt: new Date(), updatedAt: new Date() }
          ], {})
      });
    }).then(() => {
      return queryInterface.rawSelect('Program', { where: { name: 'B.A' } }, ['id']).then(_programId => {
        return queryInterface.bulkInsert('ProgramDetail',
          [
            { programId: _programId, name: 'Economics', createdAt: new Date(), updatedAt: new Date() },
            { programId: _programId, name: 'English', createdAt: new Date(), updatedAt: new Date() },
            { programId: _programId, name: 'Sociology', createdAt: new Date(), updatedAt: new Date() }
          ], {})
      });
    }).then(() => {
      return queryInterface.rawSelect('Program', { where: { name: 'BS' } }, ['id']).then(_programId => {
        return queryInterface.bulkInsert('ProgramDetail',
          [
            { programId: _programId, name: 'CS', createdAt: new Date(), updatedAt: new Date() },
            { programId: _programId, name: 'IT', createdAt: new Date(), updatedAt: new Date() },
            { programId: _programId, name: 'SE', createdAt: new Date(), updatedAt: new Date() }
          ], {})
      });
    }).then(() => {
      return queryInterface.rawSelect('Program', { where: { name: 'MS' } }, ['id']).then(_programId => {
        return queryInterface.bulkInsert('ProgramDetail',
          [
            { programId: _programId, name: 'CS', createdAt: new Date(), updatedAt: new Date() },
            { programId: _programId, name: 'IT', createdAt: new Date(), updatedAt: new Date() },
            { programId: _programId, name: 'SE', createdAt: new Date(), updatedAt: new Date() }
          ], {})
      });
    }).then(() => {
      return queryInterface.rawSelect('Program', { where: { name: 'M.Phil.' } }, ['id']).then(_programId => {
        return queryInterface.bulkInsert('ProgramDetail',
          [
            { programId: _programId, name: 'CS', createdAt: new Date(), updatedAt: new Date() }
          ], {})
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ProgramDetail', null, {})

  }
};
