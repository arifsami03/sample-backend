'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.rawSelect('ESCategory', { where: { name: 'Preliminary' } }, ['id']).then(_ESCategoryId => {
      return queryInterface.bulkInsert('EvaluationSheet',
        [{ ESCategoryId: _ESCategoryId, title: 'General Evaluation Sheet', ESForm: 'application-form', active: true, createdAt: new Date(), updatedAt: new Date() }],
        {}
      )
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('EvaluationSheet', null, {});
  }
};
