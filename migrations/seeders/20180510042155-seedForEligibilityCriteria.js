'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'eligibilityCriterias.*', parentId: 'admissionMod.*', title: 'Eligibility Criteria', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      { id: 'eligibilityCriterias.create', parentId: 'eligibilityCriterias.*', title: 'Create Eligibility Criteria', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'eligibilityCriterias.index', parentId: 'eligibilityCriterias.create', title: 'View Eligibility Criteria', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'eligibilityCriterias.update', parentId: 'eligibilityCriterias.*', title: 'Update Eligibility Criteria', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'eligibilityCriterias.index', parentId: 'eligibilityCriterias.update', title: 'View Eligibility Criteria', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'eligibilityCriterias.delete', parentId: 'eligibilityCriterias.*', title: 'Delete Eligibility Criteria', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'eligibilityCriterias.index', parentId: 'eligibilityCriterias.delete', title: 'View Eligibility Criteria', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'eligibilityCriterias.find', parentId: 'eligibilityCriterias.index', title: 'View Eligibility Criteria', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'academicCalendars.findAttributesList', parentId: 'eligibilityCriterias.find', title: 'View Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'programs.getProgramsList', parentId: 'eligibilityCriterias.find', title: 'View Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'passingYears.getPassingYearNotSelected', parentId: 'eligibilityCriterias.find', title: 'View Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'courses.getSubjectsNotSelected', parentId: 'eligibilityCriterias.find', title: 'View Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'programs.getPreReqNotSelected', parentId: 'eligibilityCriterias.find', title: 'View Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'courses.findAttributesList', parentId: 'eligibilityCriterias.find', title: 'View Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'passingYears.findAttributesList', parentId: 'eligibilityCriterias.find', title: 'View Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      
      
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: [
        'eligibilityCriterias.*',
        'eligibilityCriterias.create',
        'eligibilityCriterias.index',
        'eligibilityCriterias.update',
        'eligibilityCriterias.delete',
        'eligibilityCriterias.find',
        'academicCalendars.findAttributesList',
        'programs.getProgramsList',
        'passingYears.getPassingYearNotSelected',
        'courses.getSubjectsNotSelected',
        'programs.getPreReqNotSelected',
        'courses.findAttributesList',
        'passingYears.findAttributesList'
      ]
    }, {});
  }
};