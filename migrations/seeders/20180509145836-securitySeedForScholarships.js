'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        { id: 'scholarships.*', parentId: 'feeManagementMod.*', title: 'Scholarships', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'scholarships.create', parentId: 'scholarships.*', title: 'Create Scholarship', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'scholarships.index', parentId: 'scholarships.create', title: 'View Scholarships', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'scholarships.update', parentId: 'scholarships.*', title: 'Update Scholarship', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'scholarships.index', parentId: 'scholarships.update', title: 'View Scholarships', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'scholarships.delete', parentId: 'scholarships.*', title: 'Delete Scholarships', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'scholarships.deleteScholarshipFeeHead', parentId: 'scholarships.delete', title: 'Delete Scholarship Fee Head', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'scholarships.index', parentId: 'scholarships.delete', title: 'View Scholarships', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'scholarships.find', parentId: 'scholarships.index', title: 'View Scholarship', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'feeHeads.findAttributesList', parentId: 'scholarships.find', title: 'Get Fee Heads List', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: [
          'scholarships.*',
          'scholarships.create',
          'scholarships.update',
          'scholarships.delete',
          'scholarships.deleteScholarshipFeeHead',
          'scholarships.index',
          'scholarships.find',
          'feeHeads.findAttributesList'
        ]
      },
      {}
    );
  }
};
