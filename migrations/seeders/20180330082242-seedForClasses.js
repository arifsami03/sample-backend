'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'classes.*', parentId: 'academicsMod.*', title: 'Classes', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      { id: 'classes.create', parentId: 'classes.*', title: 'Create Classes', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'classes.index', parentId: 'classes.create', title: 'View Classes', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'classes.update', parentId: 'classes.*', title: 'Update Class', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'classes.index', parentId: 'classes.update', title: 'View Classes', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'classes.delete', parentId: 'classes.*', title: 'Delete Class', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'classes.index', parentId: 'classes.delete', title: 'View Classes', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'classes.find', parentId: 'classes.index', title: 'View Class', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'classes.findByProgramDetail', parentId: 'classes.index', title: 'View Class of Program Detail', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'programDetails.getProgramDetails', parentId: 'classes.find', title: 'Get Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() }

    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: ['classes.*', 'classes.create', 'classes.index', 'classes.update', 'classes.delete', 'classes.find','classes.findByProgramDetail', 'programDetails.getProgramDetails']
    }, {});
  }
};