'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'configuration.*', parentId: null, title: 'Configuration', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      { id: 'banks.*', parentId: 'configuration.*', title: 'Banks', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      { id: 'banks.create', parentId: 'banks.*', title: 'Create Bank', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'banks.index', parentId: 'banks.create', title: 'View Banks', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'banks.update', parentId: 'banks.*', title: 'Update Bank', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'banks.index', parentId: 'banks.update', title: 'View Banks', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'banks.delete', parentId: 'banks.*', title: 'Delete Bank', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'banks.index', parentId: 'banks.delete', title: 'View Banks', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'banks.find', parentId: 'banks.index', title: 'View Bank', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: [
        'configuration.*',
        'banks.*',
        'banks.create',
        'banks.index',
        'banks.update',
        'banks.delete',
        'banks.find']
    }, {});
  }
};