'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'programDetails.*', parentId: 'academicsMod.*', title: 'Program Details', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      { id: 'programDetails.create', parentId: 'programDetails.*', title: 'Create Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'programDetails.index', parentId: 'programDetails.create', title: 'View Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'programDetails.update', parentId: 'programDetails.*', title: 'Update Program Detail', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'programDetails.index', parentId: 'programDetails.update', title: 'View Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'programDetails.delete', parentId: 'programDetails.*', title: 'Delete Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'programDetails.index', parentId: 'programDetails.delete', title: 'View Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'programDetails.find', parentId: 'programDetails.index', title: 'View Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'programs.getProgramsList', parentId: 'programDetails.find', title: 'Get Programs List', isVisible: false, createdAt: new Date(), updatedAt: new Date() }

    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: ['programDetails.*', 'programDetails.create', 'programDetails.index', 'programDetails.update', 'programDetails.delete', 'programDetails.find', 'programs.getProgramsList']
    }, {});
  }
};