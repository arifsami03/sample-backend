'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Configuration',
      [
        {
          key: 'instrumentType',
          value: 'Bank Draft',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          key: 'instrumentType',
          value: 'Cheque',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          key: 'instrumentType',
          value: 'Mail Transfer',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          key: 'instrumentType',
          value: 'Cash letter of credit',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          key: 'instrumentType',
          value: 'Telegraphic Transfer',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'Configuration',
      { key: ['instrumentType'] },
      {}
    );
  }
};
