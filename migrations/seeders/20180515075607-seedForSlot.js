'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'slots.*', parentId: 'timeTableMod.*', title: 'Slots', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      { id: 'slots.create', parentId: 'slots.*', title: 'Create slot', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'slots.index', parentId: 'slots.create', title: 'View slot', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'slots.update', parentId: 'slots.*', title: 'Update slot', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'slots.index', parentId: 'slots.update', title: 'View slot', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'slots.delete', parentId: 'slots.*', title: 'Delete slot', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'slots.index', parentId: 'slots.delete', title: 'View slot', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'slots.find', parentId: 'slots.index', title: 'View slot', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: [
        'slots.*',
        'slots.create',
        'slots.index',
        'slots.update',
        'slots.delete',
        'slots.find']
    }, {});
  }
};