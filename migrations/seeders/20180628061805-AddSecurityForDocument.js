'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
   
        
        { id: 'documents.findByDocFor', parentId: 'documents.index', title: 'View Documents By Form / Process', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: ['documents.findByDocFor']
      },
      {}
    );
  }
};
