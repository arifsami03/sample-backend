'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        { id: 'cacTerms.*', parentId: 'academicsMod.*', title: 'Academic Calendar Terms', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'cacTerms.create', parentId: 'cacTerms.*', title: 'Create Campus Academic Calendar Terms', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'cacTerms.index', parentId: 'cacTerms.create', title: 'View Campus Academic Calendar Terms', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'cacTerms.update', parentId: 'cacTerms.*', title: 'Update Campus Academic Calendar Terms', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'cacTerms.index', parentId: 'cacTerms.update', title: 'View Campus Academic Calendar Terms', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'cacTerms.delete', parentId: 'cacTerms.*', title: 'Delete Campus Academic Calendar Terms', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'cacTerms.index', parentId: 'cacTerms.delete', title: 'View Campus Academic Calendar Terms', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'cacTerms.findByACPD', parentId: 'cacTerms.index', title: 'View Campus Academic Calendar Terms', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'cacTerms.find', parentId: 'cacTerms.findByACPD', title: 'View Campus Academic Calendar Term', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'campusAcademicCalendars.findCampusByACPD', parentId: 'cacTerms.findByACPD', title: 'Get Campuses By Academic Calendar Program Detail', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'academicCalendarProgramDetails.findAttributes', parentId: 'cacTerms.findByACPD', title: 'Get Academic Calendar Program Detail', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: [
          'cacTerms.*',
          'cacTerms.create',
          'cacTerms.update',
          'cacTerms.delete',
          'cacTerms.index',
          'cacTerms.findByACPD',
          'cacTerms.find',
          'campusAcademicCalendars.findCampusByACPD',
          'academicCalendarProgramDetails.findAttributes'

        ]
      },
      {}
    );
  }
};
