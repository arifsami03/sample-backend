'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        { id: 'evaluationSheets.*', parentId: 'configuration.*', title: 'Evaluation Sheets', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'evaluationSheets.create', parentId: 'evaluationSheets.*', title: 'Create Evaluation Sheet', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'evaluationSheets.index', parentId: 'evaluationSheets.create', title: 'View Evaluation Sheets', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'evaluationSheets.update', parentId: 'evaluationSheets.*', title: 'Update Evaluation Sheet', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'evaluationSheets.index', parentId: 'evaluationSheets.update', title: 'View Evaluation Sheets', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'evaluationSheets.delete', parentId: 'evaluationSheets.*', title: 'Delete Evaluation Sheet', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'evaluationSheets.index', parentId: 'evaluationSheets.delete', title: 'View Evaluation Sheets', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'evaluationSheets.find', parentId: 'evaluationSheets.index', title: 'View Evaluation Sheet', isVisible: false, createdAt: new Date(), updatedAt: new Date() },

        { id: 'esSections.*', parentId: 'evaluationSheets.*', title: 'ES Sections', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esSections.create', parentId: 'esSections.*', title: 'Create ES Section', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esSections.findAllESSections', parentId: 'esSections.create', title: 'View ES Sections', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esSections.update', parentId: 'esSections.*', title: 'Update ES Section', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esSections.findAllESSections', parentId: 'esSections.update', title: 'View ES Sections', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esSections.delete', parentId: 'esSections.*', title: 'Delete ES Section', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esSections.findAllESSections', parentId: 'esSections.delete', title: 'View ES Sections', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        // if user has permission for list sections then he must have permission for list evaluation sheets
        { id: 'evaluationSheets.index', parentId: 'esSections.findAllESSections', title: 'View Evaluation Sheets', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esSections.find', parentId: 'esSections.findAllESSections', title: 'View ES Section', isVisible: false, createdAt: new Date(), updatedAt: new Date() },

        { id: 'esQuestions.*', parentId: 'evaluationSheets.*', title: 'ES Questions', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esQuestions.create', parentId: 'esQuestions.*', title: 'Create ES Question', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esQuestions.findAllESQuestions', parentId: 'esQuestions.create', title: 'View ES Questions', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esQuestions.update', parentId: 'esQuestions.*', title: 'Update ES Question', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esQuestions.findAllESQuestions', parentId: 'esQuestions.update', title: 'View ES Questions', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esQuestions.delete', parentId: 'esQuestions.*', title: 'Delete ES Question', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esQuestions.findAllESQuestions', parentId: 'esQuestions.delete', title: 'View ES Questions', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        // if user has permission for list questions then he must have permission for list es-sections
        { id: 'esSections.findAllESSections', parentId: 'esQuestions.findAllESQuestions', title: 'View Evaluation Sheets', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'esQuestions.find', parentId: 'esQuestions.findAllESQuestions', title: 'View ES Question', isVisible: false, createdAt: new Date(), updatedAt: new Date() },

      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: [
          'evaluationSheets.*',
          'evaluationSheets.create',
          'evaluationSheets.update',
          'evaluationSheets.delete',
          'evaluationSheets.index',
          'evaluationSheets.find',
          'esSections.*',
          'esSections.create',
          'esSections.update',
          'esSections.delete',
          'esSections.findAllESSections',
          'esSections.find',
          'esQuestions.*',
          'esQuestions.create',
          'esQuestions.update',
          'esQuestions.delete',
          'esQuestions.findAllESQuestions',
          'esQuestions.find'
        ]
      },
      {}
    );
  }
};
