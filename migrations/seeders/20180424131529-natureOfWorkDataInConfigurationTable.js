'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Configuration',
      [
        {
          key: 'natureOfWork',
          value: 'Businessman',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          key: 'natureOfWork',
          value: 'Private Job',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          key: 'natureOfWork',
          value: 'Government Job',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'Configuration',
      { key: ['natureOfWork'] },
      {}
    );
  }
};
