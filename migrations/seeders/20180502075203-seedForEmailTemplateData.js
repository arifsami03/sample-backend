'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // Entering OutgoingMailServer Id
    return queryInterface.rawSelect('OutgoingMailServer', { where: { name: 'Default Server' } }, ['id']).then(_outGoingResult => {
      return queryInterface.bulkInsert(
        'EmailTemplate',
        [
          {
            emailBody:
              'Dear {{campus-owner-name}} \n\n Your campus pre-registration application has been submitted succesfully. Kindly wait for confirmation of your application. \n\n Regards : \n Mekdiz',
            name: 'Pre-registration submit',
            subject: 'Pre-registration application submitted.',
            to: '{{campus-owner-email}}',
            outgoingMailServerId: _outGoingResult,
            createdAt: new Date(),
            updatedAt: new Date()
          },
          {
            emailBody: 'Dear {{campus-owner-name}} \n\n Your campus pre-registration application has been rejected. \n\n Regards : \n Mekdiz',
            name: 'Pre-registration reject',
            subject: 'Pre-registration application rejected.',
            to: '{{campus-owner-email}}',
            outgoingMailServerId: _outGoingResult,
            createdAt: new Date(),
            updatedAt: new Date()
          },
          {
            emailBody:
              'Dear {{campus-owner-name}} \n\n Your campus pre-registration application has been approved. Now you can login and fill the post-registration form. \n Use this link {{home-url}} to login. \n\n Regards : \n Mekdiz',
            name: 'Pre-registration approve',
            subject: 'Pre-registration application approved.',
            to: '{{campus-owner-email}}',
            outgoingMailServerId: _outGoingResult,
            createdAt: new Date(),
            updatedAt: new Date()
          },
          {
            emailBody: 'Dear {{campus-owner-name}} \n\n Your campus post-registration application has been submitted succesfully. \n Kindly wait for confirmation of your application. \n\n Regards : \n Mekdiz',
            name: 'Post-registration submit',
            subject: 'Post-registration application submitted.',
            to: '{{campus-owner-email}}',
            outgoingMailServerId: _outGoingResult,
            createdAt: new Date(),
            updatedAt: new Date()
          }
        ],
        {}
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('EmailTemplateAssignment', null, {}).then(() => {
      return queryInterface.bulkDelete('EmailTemplate', { name: ['Pre-registration submit', 'Pre-registration reject', 'Pre-registration approve', 'Post-registration submit'] }, {});
    });
  }
};
