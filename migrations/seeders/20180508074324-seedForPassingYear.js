'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'passingYears.*', parentId: 'settingsMod.*', title: 'Passing Year', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      { id: 'passingYears.create', parentId: 'passingYears.*', title: 'Create Passing Year', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'passingYears.index', parentId: 'passingYears.create', title: 'View Passing Year', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'passingYears.update', parentId: 'passingYears.*', title: 'Update Passing Year', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'passingYears.index', parentId: 'passingYears.update', title: 'View Passing Year', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'passingYears.delete', parentId: 'passingYears.*', title: 'Delete Passing Year', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'passingYears.index', parentId: 'passingYears.delete', title: 'View Passing Year', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'passingYears.find', parentId: 'passingYears.index', title: 'View Passing Year', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: [
        'passingYears.*',
        'passingYears.create',
        'passingYears.index',
        'passingYears.update',
        'passingYears.delete',
        'passingYears.find']
    }, {});
  }
};