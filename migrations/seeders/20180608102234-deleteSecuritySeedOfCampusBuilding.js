'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkDelete('AppFeature', {
      id: [
        'campusBuildings.*',
        'campusBuildings.create',
        'campusBuildings.index',
        'campusBuildings.update',
        'campusBuildings.delete',
        'campusBuildings.find']
    }, {});

  },

  down: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'campusBuildings.*', parentId: 'campusMod.*', title: 'Campus Building', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      { id: 'campusBuildings.create', parentId: 'campusBuildings.*', title: 'Create Campus Building', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'campusBuildings.index', parentId: 'campusBuildings.create', title: 'View Campus Buildings', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'campusBuildings.update', parentId: 'campusBuildings.*', title: 'Update Campus Building', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'campusBuildings.index', parentId: 'campusBuildings.update', title: 'View Campus Buildings', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'campusBuildings.delete', parentId: 'campusBuildings.*', title: 'Delete Campus Building', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'campusBuildings.index', parentId: 'campusBuildings.delete', title: 'View Campus Buildings', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'campusBuildings.find', parentId: 'campusBuildings.index', title: 'View Campus Building', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'campus.findAttributesList', parentId: 'campusBuildings.find', title: 'View Campus List', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
    ], {});

  }
};