'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // As metaConf table does not exist anymore in our system so return true on up and down scenario
    // TODO:normal: This file must be delete before going to production because MeatConf Table is changed to Configuration Table

    return true;

    // return queryInterface.bulkInsert(
    //   'MetaConf',
    //   [
    //     {
    //       metaKey: 'natureOfWork',
    //       metaValue: 'Businessman',
    //       createdAt: new Date(),
    //       updatedAt: new Date()
    //     },
    //     {
    //       metaKey: 'natureOfWork',
    //       metaValue: 'Private Job',
    //       createdAt: new Date(),
    //       updatedAt: new Date()
    //     },
    //     {
    //       metaKey: 'natureOfWork',
    //       metaValue: 'Government Job',
    //       createdAt: new Date(),
    //       updatedAt: new Date()
    //     }
    //   ],
    //   {}
    // );
  },

  down: (queryInterface, Sequelize) => {
    return true;

    // return queryInterface.bulkDelete(
    //   'MetaConf',
    //   { metaKey: ['natureOfWork'] },
    //   {}
    // );
  }
};
