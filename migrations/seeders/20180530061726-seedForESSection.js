'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.rawSelect('EvaluationSheet', { where: { title: 'General Evaluation Sheet' } }, ['id']).then(_ESId => {
      return queryInterface.bulkInsert('ESSection',
        [
          { ESId: _ESId, title: 'Building', createdAt: new Date(), updatedAt: new Date() },
          { ESId: _ESId, title: 'Classrooms', createdAt: new Date(), updatedAt: new Date() }
        ], {})
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ESSection', null, {});
  }
};
