'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature',
      [
        { id: 'groups.*', parentId: 'securityMod.*', title: 'Groups', isVisible: true, weight: 12, createdAt: new Date(), updatedAt: new Date() },
        { id: 'groups.create', parentId: 'groups.*', title: 'Create Groups', isVisible: false, weight: 13, createdAt: new Date(), updatedAt: new Date() },
        { id: 'groups.index', parentId: 'groups.create', title: 'View Groups', isVisible: false, weight: 14, createdAt: new Date(), updatedAt: new Date() },
        { id: 'groups.update', parentId: 'groups.*', title: 'Update Groups', isVisible: false, weight: 15, createdAt: new Date(), updatedAt: new Date() },
        { id: 'groups.index', parentId: 'groups.update', title: 'View Groups', isVisible: false, weight: 16, createdAt: new Date(), updatedAt: new Date() },
        { id: 'groups.delete', parentId: 'groups.*', title: 'Delete Groups', isVisible: false, weight: 17, createdAt: new Date(), updatedAt: new Date() },
        { id: 'groups.index', parentId: 'groups.delete', title: 'View Groups', isVisible: false, weight: 18, createdAt: new Date(), updatedAt: new Date() },
        { id: 'groups.find', parentId: 'groups.index', title: 'View Group', isVisible: false, weight: 23, createdAt: new Date(), updatedAt: new Date() },

        // Group Assignments
        { id: 'groups.assignUsers.*', parentId: 'groups.*', title: 'User Assignments', isVisible: false, weight: 19, createdAt: new Date(), updatedAt: new Date() },
        { id: 'userGroups.assign', parentId: 'groups.assignUsers.*', title: 'Assign group to user', isVisible: false, weight: 20, createdAt: new Date(), updatedAt: new Date() },
        { id: 'groups.findAllUsers', parentId: 'userGroups.assign', title: 'View Groups to assign', isVisible: false, weight: 21, createdAt: new Date(), updatedAt: new Date() },
        { id: 'userGroups.revoke', parentId: 'groups.assignUsers.*', title: 'Remove group from user', isVisible: false, weight: 22, createdAt: new Date(), updatedAt: new Date() },

        // Role Assignments
        { id: 'groups.assignRoles.*', parentId: 'groups.*', title: 'Role Assignments', isVisible: false, weight: 19, createdAt: new Date(), updatedAt: new Date() },
        { id: 'roleAssignments.assignToGroup', parentId: 'groups.assignRoles.*', title: 'Assign role to user', isVisible: false, weight: 20, createdAt: new Date(), updatedAt: new Date() },
        { id: 'groups.findAllRoles', parentId: 'roleAssignments.assignToGroup', title: 'View Roles to assign', isVisible: false, weight: 21, createdAt: new Date(), updatedAt: new Date() },
        { id: 'roleAssignments.delete', parentId: 'groups.assignRoles.*', title: 'Remove role from user', isVisible: false, weight: 22, createdAt: new Date(), updatedAt: new Date() },

      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', null, {});
  }
};
