'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // As metaConf table does not exist anymore in our system so return true on up and down scenario
    // TODO:normal: This file must be delete before going to production because MeatConf Table is changed to Configuration Table

    // return true;

    return queryInterface.rawSelect('Configuration', { where: { key: 'country', value: 'Pakistan' } }, ['id']).then(countryId => {
      return queryInterface.bulkInsert(
        'Configuration',
        [
          { key: 'province', value: 'Punjab', parentKey: 'country', parentKeyId: countryId, createdAt: new Date(), updatedAt: new Date() },
          { key: 'province', value: 'Sindh', parentKey: 'country', parentKeyId: countryId, createdAt: new Date(), updatedAt: new Date() },
          { key: 'province', value: 'KPK', parentKey: 'country', parentKeyId: countryId, createdAt: new Date(), updatedAt: new Date() },
          { key: 'province', value: 'Balochistan', parentKey: 'country', parentKeyId: countryId, createdAt: new Date(), updatedAt: new Date() },
          { key: 'province', value: 'Gilgit', parentKey: 'country', parentKeyId: countryId, createdAt: new Date(), updatedAt: new Date() }
        ],
        {}
      );
    })


  },

  down: (queryInterface, Sequelize) => {
    // return true;

    return queryInterface.bulkDelete('Configuration', { key: ['province'] }, {});
  }
};
