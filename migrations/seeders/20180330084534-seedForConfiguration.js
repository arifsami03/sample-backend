'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [{
        id: 'metaconf.*',
        parentId: 'configuration.*',
        title: 'System Configuration',
        isVisible: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 'metaconf.create',
        parentId: 'metaconf.*',
        title: 'Create System Conf',
        isVisible: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 'metaconf.index',
        parentId: 'metaconf.create',
        title: 'View System Conf',
        isVisible: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 'metaconf.update',
        parentId: 'metaconf.*',
        title: 'Update System Conf',
        isVisible: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 'metaconf.index',
        parentId: 'metaconf.update',
        title: 'View System Conf',
        isVisible: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 'metaconf.delete',
        parentId: 'metaconf.*',
        title: 'Delete System Conf',
        isVisible: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 'metaconf.index',
        parentId: 'metaconf.delete',
        title: 'View System Conf',
        isVisible: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 'metaconf.find',
        parentId: 'metaconf.index',
        title: 'View System Conf',
        isVisible: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
     

    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: ['metaconf.*', 'metaconf.create', 'metaconf.index', 'metaconf.update', 'metaconf.delete', 'metaconf.find']
    }, {});
  }
};