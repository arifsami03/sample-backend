'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('InstituteType',
      [
        { name: 'School', createdAt: new Date(), updatedAt: new Date() },
        { name: 'College', createdAt: new Date(), updatedAt: new Date() },
        { name: 'University', createdAt: new Date(), updatedAt: new Date() },
      ],
      {}
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('InstituteType', null, {});
  }
};


