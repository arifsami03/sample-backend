'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [

        { id: 'cpdAccount.create', parentId: 'campusMod.*', title: 'Campus Account Info', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'cpdAccount.find', parentId: 'cpdAccount.create', title: 'View Campus Account Info', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'cpdAccount.update', parentId: 'cpdAccount.create', title: 'Update Campus Account Info', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'cpdAccount.find', parentId: 'cpdAccount.update', title: 'View Campus Account Info', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'cpdAccount.getProgramDetails', parentId: 'cpdAccount.find', title: 'View Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() },        
        { id: 'cpdAccount.getProgramDetailsAccountInfo', parentId: 'cpdAccount.find', title: 'View Program With Account', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'banks.findAttributesList', parentId: 'cpdAccount.find', title: 'View bans', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'programDetails.findAttributesList', parentId: 'cpdAccount.find', title: 'View Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: [
          'cpdAccount.create',         
          'cpdAccount.update',
          'cpdAccount.find',
          'cpdAccount.getProgramDetails',
          'cpdAccount.getProgramDetailsAccountInfo',
          'banks.findAttributesList',
          'programDetails.findAttributesList'

        ]
      },
      {}
    );
  }
};
