'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`UPDATE AppFeature SET id = ?, title = ? WHERE id = ? AND parentId = ? `, {

      replacements: ['managementFees.*', 'Management Fee', 'licenseFees.*', 'feeManagementMod.*']

    }).then(() => {

      return queryInterface.sequelize.query(`UPDATE AppFeature SET id = ?, parentId = ?, title = ? WHERE id = ? AND parentId = ? `, {
        replacements: ['managementFees.create', 'managementFees.*', 'Create Management Fee', 'licenseFees.create', 'licenseFees.*']

      }).then(() => {

        return queryInterface.sequelize.query(`UPDATE AppFeature SET id = ?, parentId = ?, title = ? WHERE id = ? AND parentId = ? `, {
          replacements: ['managementFees.index', 'managementFees.create', 'View Management Fees', 'licenseFees.index', 'licenseFees.create']

        }).then(() => {

          return queryInterface.sequelize.query(`UPDATE AppFeature SET id = ?, parentId = ?, title = ? WHERE id = ? AND parentId = ? `, {
            replacements: ['managementFees.update', 'managementFees.*', 'Update Management Fee', 'licenseFees.update', 'licenseFees.*']

          }).then(() => {

            return queryInterface.sequelize.query(`UPDATE AppFeature SET id = ?, parentId = ?, title = ? WHERE id = ? AND parentId = ? `, {
              replacements: ['managementFees.index', 'managementFees.update', 'View Management Fees', 'licenseFees.index', 'licenseFees.update']

            }).then(() => {

              return queryInterface.sequelize.query(`UPDATE AppFeature SET id = ?, parentId = ?, title = ? WHERE id = ? AND parentId = ? `, {
                replacements: ['managementFees.delete', 'managementFees.*', 'Delete Management Fee', 'licenseFees.delete', 'licenseFees.*']

              }).then(() => {

                return queryInterface.sequelize.query(`UPDATE AppFeature SET id = ?, parentId = ?, title = ? WHERE id = ? AND parentId = ? `, {
                  replacements: ['managementFees.index', 'managementFees.delete', 'View Management Fees', 'licenseFees.index', 'licenseFees.delete']

                }).then(() => {

                  return queryInterface.sequelize.query(`UPDATE AppFeature SET id = ?, parentId = ?, title = ? WHERE id = ? AND parentId = ? `, {
                    replacements: ['managementFees.find', 'managementFees.index', 'View Management Fee', 'licenseFees.find', 'licenseFees.index']

                  }).then(() => {

                    return queryInterface.sequelize.query(`UPDATE AppFeature SET id = ?, title = ? WHERE id = ? AND parentId = ? `, {
                      replacements: ['managementFees.findAttributesList', 'View Management Fee List', 'licenseFees.findAttributesList', 'campusMOU.find']

                    })
                  })
                })
              })
            })
          })
        })
      })
    })
  },

  down: (queryInterface, Sequelize) => {
    return true;
  }
}