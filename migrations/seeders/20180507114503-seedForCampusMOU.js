'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [

        { id: 'campusMOU.create', parentId: 'campusMod.*', title: 'Campus MOU', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'campusMOU.find', parentId: 'campusMOU.create', title: 'View Campus MOU', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'campusMOU.update', parentId: 'campusMOU.create', title: 'Update Campus MOU', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'campusMOU.find', parentId: 'campusMOU.update', title: 'View Campus MOU', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'academicCalendars.findAttributesList', parentId: 'campusMOU.find', title: 'View Academic Calendars', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'educationLevels.findAttributesList', parentId: 'campusMOU.find', title: 'View Level of Education', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'licenseFees.findAttributesList', parentId: 'campusMOU.find', title: 'View License Fee', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: [
          'campusMOU.create',         
          'campusMOU.update',
          'campusMOU.find',
          'academicCalendars.findAttributesList',
          'educationLevels.findAttributesList',
          'licenseFees.findAttributesList'

        ]
      },
      {}
    );
  }
};
