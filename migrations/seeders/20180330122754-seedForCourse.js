'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature', [
        { id: 'courses.*', parentId: 'academicsMod.*', title: 'Courses', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'courses.create', parentId: 'courses.*', title: 'Create Course', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'courses.index', parentId: 'courses.create', title: 'View Courses', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'courses.update', parentId: 'courses.*', title: 'Update Course', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'courses.index', parentId: 'courses.update', title: 'View Courses', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'courses.delete', parentId: 'courses.*', title: 'Delete Course', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'courses.index', parentId: 'courses.delete', title: 'View Courses', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'courses.find', parentId: 'courses.index', title: 'View Course', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
      ], {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature', {
        id: [
          'courses.*',
          'courses.create',
          'courses.update',
          'courses.delete',
          'courses.index',
          'courses.find'
        ]
      }, {}
    );
  }
};