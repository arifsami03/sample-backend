'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('WorkFlow', [{ WFId: 'pre-registration', title: 'Pre-Registration', description: '', createdAt: new Date(), updatedAt: new Date() }], {}).then(workFlow => {
      return queryInterface.bulkInsert('WFState', [{ WFId: 'pre-registration', state: 'new', title: 'New', description: '', createdAt: new Date(), updatedAt: new Date() },
      { WFId: 'pre-registration', state: 'submit', title: 'Submit', description: '', createdAt: new Date(), updatedAt: new Date() },
      { WFId: 'pre-registration', state: 'approve', title: 'Approve', description: '', createdAt: new Date(), updatedAt: new Date() },
      { WFId: 'pre-registration', state: 'reject', title: 'Reject', description: '', createdAt: new Date(), updatedAt: new Date() }], {})
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('WFState', { WFId: ['pre-registration'] }, {}).then(() => {
      return queryInterface.bulkDelete('WorkFlow', { WFId: ['pre-registration'] }, {});
    });
  }
};