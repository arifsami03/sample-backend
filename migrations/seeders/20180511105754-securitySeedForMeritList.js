'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        /**
         * Seed related to meriltLists
         */
        { id: 'meritLists.*', parentId: 'admissionMod.*', title: 'Merit List', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'meritLists.create', parentId: 'meritLists.*', title: 'Create Merit List', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'meritLists.index', parentId: 'meritLists.create', title: 'View Merit List', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'meritLists.update', parentId: 'meritLists.*', title: 'Update Merit List', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'meritLists.index', parentId: 'meritLists.update', title: 'View Merit List', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'meritLists.delete', parentId: 'meritLists.*', title: 'Delete Merit List', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'meritLists.index', parentId: 'meritLists.delete', title: 'View Merit List', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'meritLists.find', parentId: 'meritLists.index', title: 'View Merit List', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        /**
         * Seed related to program details
         */
        { id: 'programDetails.findAttributesList', parentId: 'meritLists.find', title: 'Get Program Details List', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        /**
         * Seed related to scholarships
         */
        { id: 'scholarships.findByPercentage', parentId: 'meritLists.find', title: 'Get Scholarships List', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: ['meritLists.*', 'meritLists.create', 'meritLists.update', 'meritLists.delete', 'meritLists.index', 'meritLists.find', 'programDetails.findAttributesList', 'scholarships.findByPercentage']
      },
      {}
    );
  }
};
