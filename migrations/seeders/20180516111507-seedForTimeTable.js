'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        { id: 'timeTables.*', parentId: 'timeTableMod.*', title: 'Time Table', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'timeTables.create', parentId: 'timeTables.*', title: 'Create Time Table', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'timeTables.index', parentId: 'timeTables.create', title: 'View Time Table', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'timeTables.update', parentId: 'timeTables.*', title: 'Update Time Table', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'timeTables.index', parentId: 'timeTables.update', title: 'View Time Table', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'timeTables.delete', parentId: 'timeTables.*', title: 'Delete Time Table', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'timeTables.index', parentId: 'timeTables.delete', title: 'View Time Table', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'timeTables.find', parentId: 'timeTables.index', title: 'View Time Table', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'slots.findAttributesList', parentId: 'timeTables.find', title: 'View Time Table', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'programDetails.findAttributesList', parentId: 'timeTables.find', title: 'View Time Table', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'courses.findAttributesList', parentId: 'timeTables.find', title: 'View Time Table', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'sections.findAttributesList', parentId: 'timeTables.find', title: 'View Time Table', isVisible: false, createdAt: new Date(), updatedAt: new Date() },

        { id: 'ttDetails.*', parentId: 'timeTables.*', title: 'Time Table Detail', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'ttDetails.create', parentId: 'ttDetails.*', title: 'Create Time Table Detail', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'ttDetails.findAllTTDetail', parentId: 'ttDetails.create', title: 'View Time Table Detail', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'ttDetails.update', parentId: 'ttDetails.*', title: 'Update Time Table Detail', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'ttDetails.findAllTTDetail', parentId: 'ttDetails.update', title: 'View Time Table Detail', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'ttDetails.delete', parentId: 'ttDetails.*', title: 'Delete Time Table Detail', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'ttDetails.findAllTTDetail', parentId: 'ttDetails.delete', title: 'View Time Table Detail', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        // if user has permission for list sections then he must have permission for list Time Table
        { id: 'timeTables.index', parentId: 'ttDetails.findAllTTDetail', title: 'View Time Table', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'ttDetails.find', parentId: 'ttDetails.findAllTTDetail', title: 'View Time Table Detail', isVisible: false, createdAt: new Date(), updatedAt: new Date() },


      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: [
          'timeTables.*',
          'timeTables.create',
          'timeTables.update',
          'timeTables.delete',
          'timeTables.index',
          'timeTables.find',
          'ttDetails.*',
          'ttDetails.create',
          'ttDetails.update',
          'ttDetails.delete',
          'ttDetails.findAllTTDetail',
          'ttDetails.find',
          'slots.findAttributesList',
          'courses.findAttributesList',
          'sections.findAttributesList',
          'programDetails.findAttributesList'

        ]
      },
      {}
    );
  }
};
