'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        { id: 'instituteMod.*', parentId: null, title: 'Institute', isVisible: true, weight: 0, createdAt: new Date(), updatedAt: new Date() },

        //Inserting parent/module level app features here because for now its bit defficult to find which is already inserted or not.
        { id: 'academicsMod.*', parentId: null, title: 'Academics', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'campusMod.*', parentId: null, title: 'Campus', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'feeManagementMod.*', parentId: null, title: 'Fee Management', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        
        { id: 'timeTableMod.*', parentId: null, title: 'Time Tables', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'admissionMod.*', parentId: null, title: 'Admission', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        
        //InstituteType Management Seeds
        { id: 'instituteTypes.*', parentId: 'instituteMod.*', title: 'Institute types', isVisible: true, weight: 1, createdAt: new Date(), updatedAt: new Date() },
        { id: 'instituteTypes.create', parentId: 'instituteTypes.*', title: 'Create Institute type', isVisible: false, weight: 2, createdAt: new Date(), updatedAt: new Date() },
        { id: 'instituteTypes.index', parentId: 'instituteTypes.create', title: 'View Institute types', isVisible: false, weight: 3, createdAt: new Date(), updatedAt: new Date() },
        { id: 'instituteTypes.update', parentId: 'instituteTypes.*', title: 'Update Institute type', isVisible: false, weight: 4, createdAt: new Date(), updatedAt: new Date() },
        { id: 'instituteTypes.index', parentId: 'instituteTypes.update', title: 'View Institute types', isVisible: false, weight: 5, createdAt: new Date(), updatedAt: new Date() },
        { id: 'instituteTypes.delete', parentId: 'instituteTypes.*', title: 'Delete Institute type', isVisible: false, weight: 6, createdAt: new Date(), updatedAt: new Date() },
        { id: 'instituteTypes.index', parentId: 'instituteTypes.delete', title: 'View Institute types', isVisible: false, weight: 7, createdAt: new Date(), updatedAt: new Date() },
        { id: 'instituteTypes.find', parentId: 'instituteTypes.index', title: 'View Institute type', isVisible: false, weight: 8, createdAt: new Date(), updatedAt: new Date() }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: ['instituteMod.*', 'campusMod.*', 'feeManagementMod.*', 'timeTableMod.*', 'admissionMod.*', 'instituteTypes.*', 'instituteTypes.create', 'instituteTypes.update', 'instituteTypes.delete', 'instituteTypes.index', 'instituteTypes.find']
      },
      {}
    );
  }
};
