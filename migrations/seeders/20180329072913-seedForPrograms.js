'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [

      { id: 'programs.*', parentId: 'academicsMod.*', title: 'Programs', isVisible: true, weight: 9, createdAt: new Date(), updatedAt: new Date() },
      { id: 'programs.create', parentId: 'programs.*', title: 'Create Program', isVisible: false, weight: 10, createdAt: new Date(), updatedAt: new Date() },
      { id: 'programs.index', parentId: 'programs.create', title: 'View Programs', isVisible: false, weight: 11, createdAt: new Date(), updatedAt: new Date() },
      { id: 'programs.update', parentId: 'programs.*', title: 'Update Program', isVisible: false, weight: 12, createdAt: new Date(), updatedAt: new Date() },
      { id: 'programs.index', parentId: 'programs.update', title: 'View Programs', isVisible: false, weight: 13, createdAt: new Date(), updatedAt: new Date() },
      { id: 'programs.delete', parentId: 'programs.*', title: 'Delete Programs', isVisible: false, weight: 14, createdAt: new Date(), updatedAt: new Date() },
      { id: 'programs.index', parentId: 'programs.delete', title: 'View Programs', isVisible: false, weight: 15, createdAt: new Date(), updatedAt: new Date() },
      { id: 'programs.find', parentId: 'programs.index', title: 'View Program', isVisible: false, weight: 16, createdAt: new Date(), updatedAt: new Date() }], {});
  },


  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: ['programs.*', 'programs.create', 'programs.index', 'programs.update', 'programs.delete', 'programs.find']
    },
      {});
  }
};