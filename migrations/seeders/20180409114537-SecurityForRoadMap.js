'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        { id: 'roadMaps.*', parentId: 'academicsMod.*', title: 'Roadmaps', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'roadMaps.create', parentId: 'roadMaps.*', title: 'Create Roadmap', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'roadMaps.index', parentId: 'roadMaps.create', title: 'View Roadmaps', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'roadMaps.update', parentId: 'roadMaps.*', title: 'Update Roadmap', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'roadMaps.index', parentId: 'roadMaps.update', title: 'View Roadmaps', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'roadMaps.delete', parentId: 'roadMaps.*', title: 'Delete Roadmaps', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'roadMaps.index', parentId: 'roadMaps.delete', title: 'View Roadmaps', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'roadMaps.find', parentId: 'roadMaps.index', title: 'View Roadmap', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'programDetails.findAttributesList', parentId: 'roadMaps.find', title: 'Get Programs List', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'academicCalendars.findAttributesList', parentId: 'roadMaps.find', title: 'Get Academic Calendars List', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        // seed for roadmap courses
        { id: 'roadMapCourses.*', parentId: 'roadMaps.*', title: 'Roadmap Courses', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'roadMapCourses.create', parentId: 'roadMapCourses.*', title: 'Create  Roadmap Course', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'roadMapCourses.index', parentId: 'roadMapCourses.create', title: 'View  Roadmap Courses', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'roadMapCourses.update', parentId: 'roadMapCourses.*', title: 'Update  Roadmap Course', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'roadMapCourses.index', parentId: 'roadMapCourses.update', title: 'View  Roadmap Course', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'roadMapCourses.delete', parentId: 'roadMapCourses.*', title: 'Delete  Roadmap Courses', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'roadMapCourses.index', parentId: 'roadMapCourses.delete', title: 'View  Roadmap Courses', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'roadMapCourses.find', parentId: 'roadMapCourses.index', title: 'View  Roadmap Course', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'roadMapCourses.findByRoadMap', parentId: 'roadMapCourses.index', title: 'View  Roadmap Course By RoadMap', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'courses.findAttributesList', parentId: 'roadMapCourses.find', title: 'Get Courses List', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'classes.findAttributesList', parentId: 'roadMapCourses.find', title: 'Get Classes List', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: [
          'roadMaps.*',
          'roadMaps.create',
          'roadMaps.update',
          'roadMaps.delete',
          'roadMaps.index',
          'roadMaps.find',
          'programDetails.findAttributesList',
          'academicCalendars.findAttributesList',
          'roadMapCourses.*',
          'roadMapCourses.create',
          'roadMapCourses.update',
          'roadMapCourses.delete',
          'roadMapCourses.index',
          'roadMapCourses.find',
          'roadMapCourses.findByRoadMap',
          'courses.findAttributesList',
          'classes.findAttributesList'
        ]
      },
      {}
    );
  }
};
