'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    
      return queryInterface.bulkInsert('WFState', [{WFId: 'post-registration',state:'forwardToAdmin', title: 'Forward to Admin',description:'', createdAt: new Date(), updatedAt: new Date() },
      {WFId: 'post-registration',state:'forwardToMOU', title: 'Forward to MOU',description:'', createdAt: new Date(), updatedAt: new Date() },
      {WFId: 'post-registration',state:'forwardToMOUApproval', title: 'Forward to MOU Approval',description:'', createdAt: new Date(), updatedAt: new Date() },
      {WFId: 'post-registration',state:'createCampus', title: 'Create Campus',description:'', createdAt: new Date(), updatedAt: new Date() },
      {WFId: 'post-registration',state:'campusAccountInfo', title: 'Campus Account Info',description:'', createdAt: new Date(), updatedAt: new Date() },
    ], {})
    
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('WFState', {WFId: ['post-registration']}, {});
  }
};
