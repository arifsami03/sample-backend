'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        { id: 'outGoingMailServers.*', parentId: 'configuration.*', title: 'Out Going Mail Server', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'outGoingMailServers.create', parentId: 'outGoingMailServers.*', title: 'Create Out Going Mail Server', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'outGoingMailServers.index', parentId: 'outGoingMailServers.create', title: 'View Out Going Mail Servers', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'outGoingMailServers.update', parentId: 'outGoingMailServers.*', title: 'Update Out Going Mail Server', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'outGoingMailServers.index', parentId: 'outGoingMailServers.update', title: 'View Out Going Mail Servers', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'outGoingMailServers.delete', parentId: 'outGoingMailServers.*', title: 'Delete Out Going Mail Servers', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'outGoingMailServers.index', parentId: 'outGoingMailServers.delete', title: 'View Out Going Mail Servers', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'outGoingMailServers.find', parentId: 'outGoingMailServers.index', title: 'View Out Going Mail Server', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'users.findAttributesList', parentId: 'outGoingMailServers.find', title: 'Get Users List', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: [
          'outGoingMailServers.*',
          'outGoingMailServers.create',
          'outGoingMailServers.update',
          'outGoingMailServers.delete',
          'outGoingMailServers.index',
          'outGoingMailServers.find',
          'users.findAttributesList'
        ]
      },
      {}
    );
  }
};
