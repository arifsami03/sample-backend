'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {

        // First delete metaconf from appFeature and featurePermission, then add these appFeature with configurations instead of metaconf
        return queryInterface.bulkDelete('AppFeature', {
            id: ['metaconf.*', 'metaconf.create', 'metaconf.index', 'metaconf.update', 'metaconf.delete', 'metaconf.find']
        }, {}).then(() => {
            return queryInterface.bulkDelete('FeaturePermission', {
                featureId: ['metaconf.*', 'metaconf.create', 'metaconf.index', 'metaconf.update', 'metaconf.delete', 'metaconf.find']
            }, {}).then(() => {
                return queryInterface.bulkInsert('AppFeature', [
                    { id: 'configurations.*', parentId: 'settingsMod.*', title: 'System Configuration', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
                    { id: 'configurations.create', parentId: 'configurations.*', title: 'Create System Conf', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
                    { id: 'configurations.index', parentId: 'configurations.create', title: 'View System Conf', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
                    { id: 'configurations.update', parentId: 'configurations.*', title: 'Update System Conf', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
                    { id: 'configurations.index', parentId: 'configurations.update', title: 'View System Conf', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
                    { id: 'configurations.delete', parentId: 'configurations.*', title: 'Delete System Conf', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
                    { id: 'configurations.index', parentId: 'configurations.delete', title: 'View System Conf', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
                    { id: 'configurations.find', parentId: 'configurations.index', title: 'View System Conf', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
                ], {});
            })
        });

    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('AppFeature', {
            id: ['configurations.*', 'configurations.create', 'configurations.index', 'configurations.update', 'configurations.delete', 'configurations.find']
        }, {});
    }
};