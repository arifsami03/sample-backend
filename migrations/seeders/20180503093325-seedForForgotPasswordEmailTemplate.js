'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // Entering OutgoingMailServer Id
    return queryInterface.rawSelect('OutgoingMailServer', { where: { name: 'Default Server' } }, ['id']).then(_outGoingResult => {
      return queryInterface.bulkInsert(
        'EmailTemplate',
        [
          {
            emailBody: 'We received an account recovery request on Mekdiz University for {{user-email}}. \n If you want to initiate this request, reset your password here {{change-password-url}}',
            name: 'Forgot password',
            subject: 'Reset Password',
            to: '{{user-email}}',
            outgoingMailServerId: _outGoingResult,
            code: 'forgot-password',
            deletable: false,
            createdAt: new Date(),
            updatedAt: new Date()
          }
        ],
        {}
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('EmailTemplate', { name: ['Forgot password'] }, {});
  }
};
