'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'educationLevels.*', parentId: 'instituteMod.*', title: 'Level of Education', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      { id: 'educationLevels.create', parentId: 'educationLevels.*', title: 'Create Level of Education', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'educationLevels.index', parentId: 'educationLevels.create', title: 'View Level of Education', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'educationLevels.update', parentId: 'educationLevels.*', title: 'Update Level of Education', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'educationLevels.index', parentId: 'educationLevels.update', title: 'View Level of Education', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'educationLevels.delete', parentId: 'educationLevels.*', title: 'Delete Level of Education', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'educationLevels.index', parentId: 'educationLevels.delete', title: 'View Level of Education', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'educationLevels.find', parentId: 'educationLevels.index', title: 'View Level of Education', isVisible: false, createdAt: new Date(), updatedAt: new Date() }],
      {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: [
        'educationLevels.*',
        'educationLevels.create',
        'educationLevels.index',
        'educationLevels.update',
        'educationLevels.delete',
        'educationLevels.find']
    }, {});
  }
};