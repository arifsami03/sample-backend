'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        { id: 'preRegistrations.*', parentId: 'campusMod.*', title: 'Pre-Registrations', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'preRegistrations.status', parentId: 'preRegistrations.*', title: 'Change Status of Pre Registration', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'preRegistrations.index', parentId: 'preRegistrations.status', title: 'View Pre Registrations', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'preRegistrations.find', parentId: 'preRegistrations.index', title: 'View Pre Registration', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'preRegistrations.findUserId', parentId: 'preRegistrations.find', title: 'Find User', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'preRegistrations.activeUser', parentId: 'preRegistrations.find', title: 'Set User Active', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: ['preRegistrations.*', 'preRegistrations.status', 'preRegistrations.index', 'preRegistrations.find', 'preRegistrations.findUserId', 'preRegistrations.activeUser']
      },
      {}
    );
  }
};