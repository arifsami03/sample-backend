'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'fsCampus.*', parentId: 'campusMod.*', title: 'Mapping Fee Structure', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      { id: 'fsCampus.create', parentId: 'fsCampus.*', title: 'Create Mapping Fee Structure', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'fsCampus.index', parentId: 'fsCampus.create', title: 'View Mapping Fee Structure', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'fsCampus.update', parentId: 'fsCampus.*', title: 'Update Mapping Fee Structure', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'fsCampus.index', parentId: 'fsCampus.update', title: 'View Mapping Fee Structure', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'fsCampus.delete', parentId: 'fsCampus.*', title: 'Delete Mapping Fee Structure', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'fsCampus.index', parentId: 'fsCampus.delete', title: 'View Mapping Fee Structure', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'fsCampus.find', parentId: 'fsCampus.index', title: 'View Mapping Fee Structure', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'feeStructures.findAttributesList', parentId: 'fsCampus.find', title: 'View List of Fee Structure', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'campus.findAttributesList', parentId: 'fsCampus.find', title: 'View List of Campus', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'configurations.findAttributesList', parentId: 'fsCampus.find', title: 'View List of Configureation(typeOfFeeHeads)', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: [
        'fsCampus.*',
        'fsCampus.create',
        'fsCampus.index',
        'fsCampus.update',
        'fsCampus.delete',
        'fsCampus.find',
        'feeStructures.findAttributesList',
        'campus.findAttributesList',
        'configurations.findAttributesList']
    }, {});
  }
};