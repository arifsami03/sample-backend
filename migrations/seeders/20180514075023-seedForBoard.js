'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'boards.*', parentId: 'settingsMod.*', title: 'Boards', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      { id: 'boards.create', parentId: 'boards.*', title: 'Create Board', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'boards.index', parentId: 'boards.create', title: 'View Board', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'boards.update', parentId: 'boards.*', title: 'Update Board', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'boards.index', parentId: 'boards.update', title: 'View Board', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'boards.delete', parentId: 'boards.*', title: 'Delete Board', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'boards.index', parentId: 'boards.delete', title: 'View Board', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'boards.find', parentId: 'boards.index', title: 'View Board', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: [
        'boards.*',
        'boards.create',
        'boards.index',
        'boards.update',
        'boards.delete',
        'boards.find']
    }, {});
  }
};