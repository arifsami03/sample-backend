'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // For Pre Registartion Reject Email Template
    return queryInterface.rawSelect('WFState', { where: { state: 'submit', WFId: 'pre-registration' } }, ['id']).then(_fromState => {
      return queryInterface.rawSelect('WFState', { where: { state: 'reject', WFId: 'pre-registration' } }, ['id']).then(_toState => {
        return queryInterface.rawSelect('WFStateFlow', { where: { fromId: _fromState, toId: _toState } }, ['id']).then(_stateFlowId => {
          return queryInterface.rawSelect('EmailTemplate', { where: { name: 'Pre-registration reject' } }, ['id']).then(_emailTemplateId => {
            return queryInterface.bulkInsert(
              'EmailTemplateAssignment',
              [{ parentId: _stateFlowId, parent: 'stateflow', emailTemplateId: _emailTemplateId, createdAt: new Date(), updatedAt: new Date() }],
              {}
            );
          });
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('EmailTemplateAssignment', null, {});
  }
};
