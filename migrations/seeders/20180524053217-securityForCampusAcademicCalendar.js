'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        { id: 'campusAcademicCalendars.deleteAll', parentId: 'campusAcademicCalendars.*', title: 'Delete Campus Academic Calendar Mappings', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: [
          'campusAcademicCalendars.deleteAll',
        ]
      },
      {}
    );
  }
};
