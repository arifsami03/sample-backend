'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'cbFloors.*', parentId: 'campusMod.*', title: 'Building Floors', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      { id: 'cbFloors.create', parentId: 'cbFloors.*', title: 'Create Building Floor', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'cbFloors.index', parentId: 'cbFloors.create', title: 'View Building Floors', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'cbFloors.update', parentId: 'cbFloors.*', title: 'Update Building Floor', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'cbFloors.index', parentId: 'cbFloors.update', title: 'View Building Floors', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'cbFloors.delete', parentId: 'cbFloors.*', title: 'Delete Building Floor', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'cbFloors.index', parentId: 'cbFloors.delete', title: 'View Building Floors', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'cbFloors.find', parentId: 'cbFloors.index', title: 'View Building Floor', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'campusBuildings.findAttributesListWithCampusName', parentId: 'cbFloors.find', title: 'View Building List', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: [
        'cbFloors.*',
        'cbFloors.create',
        'cbFloors.index',
        'cbFloors.update',
        'cbFloors.delete',
        'cbFloors.find',
        'campusBuildings.findAttributesListWithCampusName']
    }, {});
  }
};