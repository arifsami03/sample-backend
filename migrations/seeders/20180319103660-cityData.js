'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // As Configuration table does not exist anymore in our system so return true on up and down scenario
    // TODO:normal: This file must be delete before going to production because MeatConf Table is changed to Configuration Table

    return queryInterface.rawSelect('Configuration', { where: { key: 'province', value: 'Punjab' } }, ['id']).then(provinceId => {
      return queryInterface.bulkInsert(
        'Configuration',
        [
          { key: 'city', value: 'Lahore', parentKey: 'province', parentKeyId: provinceId, createdAt: new Date(), updatedAt: new Date() },
          { key: 'city', value: 'Islamabad', parentKey: 'province', parentKeyId: provinceId, createdAt: new Date(), updatedAt: new Date() },
          { key: 'city', value: 'Karachi', parentKey: 'province', parentKeyId: provinceId, createdAt: new Date(), updatedAt: new Date() },
          { key: 'city', value: 'Faisalabad', parentKey: 'province', parentKeyId: provinceId, createdAt: new Date(), updatedAt: new Date() },
          { key: 'city', value: 'Peshawar', parentKey: 'province', parentKeyId: provinceId, createdAt: new Date(), updatedAt: new Date() }
        ],
        {}
      );
    })
  },

  down: (queryInterface, Sequelize) => {

    return queryInterface.bulkDelete('Configuration', { key: ['city'] }, {});
  }
};
