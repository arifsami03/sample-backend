'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        { id: 'emailTemplates.*', parentId: 'configuration.*', title: 'Email Templates', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'emailTemplates.create', parentId: 'emailTemplates.*', title: 'Create Email Template', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'emailTemplates.index', parentId: 'emailTemplates.create', title: 'View Email Templates', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'emailTemplates.update', parentId: 'emailTemplates.*', title: 'Update Email Template', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'emailTemplates.index', parentId: 'emailTemplates.update', title: 'View Email Templates', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'emailTemplates.delete', parentId: 'emailTemplates.*', title: 'Delete Email Templates', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'emailTemplates.index', parentId: 'emailTemplates.delete', title: 'View Email Templates', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'emailTemplates.find', parentId: 'emailTemplates.index', title: 'View Email Template', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
        { id: 'outGoingMailServers.findAttributesList', parentId: 'emailTemplates.find', title: 'Get Mail Servers List', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: ['emailTemplates.*', 'emailTemplates.create', 'emailTemplates.update', 'emailTemplates.delete', 'emailTemplates.index', 'emailTemplates.find', 'outGoingMailServers.findAttributesList']
      },
      {}
    );
  }
};
