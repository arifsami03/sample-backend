'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.rawSelect('EducationLevel', { where: { education: 'Pre School' } }, ['id']).then(_educationLevelId => {
      return queryInterface.bulkInsert('Program',
        [
          { educationLevelId: _educationLevelId, name: 'Pre School', createdAt: new Date(), updatedAt: new Date() }
        ], {})
    }).then(() => {
      return queryInterface.rawSelect('EducationLevel', { where: { education: 'Primary School' } }, ['id']).then(_educationLevelId => {
        return queryInterface.bulkInsert('Program',
          [
            { educationLevelId: _educationLevelId, name: 'Primary School', createdAt: new Date(), updatedAt: new Date() }
          ], {})
      });
    }).then(() => {
      return queryInterface.rawSelect('EducationLevel', { where: { education: 'Elementary School' } }, ['id']).then(_educationLevelId => {
        return queryInterface.bulkInsert('Program',
          [
            { educationLevelId: _educationLevelId, name: 'Elementary School', createdAt: new Date(), updatedAt: new Date() }
          ], {})
      });
    }).then(() => {
      return queryInterface.rawSelect('EducationLevel', { where: { education: 'Secondary School' } }, ['id']).then(_educationLevelId => {
        return queryInterface.bulkInsert('Program',
          [
            { educationLevelId: _educationLevelId, name: 'Secondary School', createdAt: new Date(), updatedAt: new Date() }
          ], {})
      });
    }).then(() => {
      return queryInterface.rawSelect('EducationLevel', { where: { education: 'College' } }, ['id']).then(_educationLevelId => {
        return queryInterface.bulkInsert('Program',
          [
            { educationLevelId: _educationLevelId, name: 'F.Sc', createdAt: new Date(), updatedAt: new Date() },
            { educationLevelId: _educationLevelId, name: 'I.CS', createdAt: new Date(), updatedAt: new Date() },
            { educationLevelId: _educationLevelId, name: 'I.Com', createdAt: new Date(), updatedAt: new Date() },
            { educationLevelId: _educationLevelId, name: 'F.A', createdAt: new Date(), updatedAt: new Date() },
            { educationLevelId: _educationLevelId, name: 'B.Com', createdAt: new Date(), updatedAt: new Date() },
            { educationLevelId: _educationLevelId, name: 'B.A', createdAt: new Date(), updatedAt: new Date() }
          ], {})
      });
    }).then(() => {
      return queryInterface.rawSelect('EducationLevel', { where: { education: 'University' } }, ['id']).then(_educationLevelId => {
        return queryInterface.bulkInsert('Program',
          [
            { educationLevelId: _educationLevelId, name: 'BS', createdAt: new Date(), updatedAt: new Date() },
            { educationLevelId: _educationLevelId, name: 'MS', createdAt: new Date(), updatedAt: new Date() },
            { educationLevelId: _educationLevelId, name: 'M.Phil.', createdAt: new Date(), updatedAt: new Date() }
          ], {})
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Program', null, {})

  }
};
