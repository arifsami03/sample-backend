'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        { id: 'postRegistrations.updateEvaluationSheet', parentId: 'campusMod.*', title: 'Evaluate Campus', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'postRegistrations.findEvaluationSheet', parentId: 'postRegistrations.updateEvaluationSheet', title: 'View Campus Evaluation', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
        { id: 'postRegistrations.findAllEvaluationSheets', parentId: 'postRegistrations.updateEvaluationSheet', title: 'View Campus Evaluation Sheets', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: [
          'postRegistrations.updateEvaluationSheet',         
          'postRegistrations.findEvaluationSheet',
          'postRegistrations.findAllEvaluationSheets',
        ]
      },
      {}
    );
  }
};
