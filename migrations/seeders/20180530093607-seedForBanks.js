'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Bank',
      [
        { name: 'Allied Bank Limited', abbreviation: 'ABL', createdAt: new Date(), updatedAt: new Date() },
        { name: 'Askari Bank Limited', abbreviation: 'AKBL', createdAt: new Date(), updatedAt: new Date() },
        { name: 'Bank Alfalah Limited', abbreviation: 'BAL', createdAt: new Date(), updatedAt: new Date() },
        { name: 'Bank Al-Habib Limited', abbreviation: 'BAHL', createdAt: new Date(), updatedAt: new Date() },
        { name: 'Dubai Islamaic Bank Pakistan Limited', abbreviation: 'DIBPL', createdAt: new Date(), updatedAt: new Date() },
        { name: 'Faysal Bank Limited', abbreviation: 'FBL', createdAt: new Date(), updatedAt: new Date() },
        { name: 'Habib Bank Limited', abbreviation: 'HBL', createdAt: new Date(), updatedAt: new Date() },
        { name: 'MCB Bank Limited', abbreviation: 'MCB', createdAt: new Date(), updatedAt: new Date() },
        { name: 'National Bank of Pakistan', abbreviation: 'NBP', createdAt: new Date(), updatedAt: new Date() },
        { name: 'United Bank Limited', abbreviation: 'UBL', createdAt: new Date(), updatedAt: new Date() }
      ],
      {}
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Bank', null, {});
  }
};


